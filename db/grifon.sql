--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: auth; Type: SCHEMA; Schema: -; Owner: supabase_admin
--

CREATE SCHEMA auth;


ALTER SCHEMA auth OWNER TO supabase_admin;

--
-- Name: extensions; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA extensions;


ALTER SCHEMA extensions OWNER TO postgres;

--
-- Name: pgbouncer; Type: SCHEMA; Schema: -; Owner: pgbouncer
--

CREATE SCHEMA pgbouncer;


ALTER SCHEMA pgbouncer OWNER TO pgbouncer;

--
-- Name: realtime; Type: SCHEMA; Schema: -; Owner: supabase_admin
--

CREATE SCHEMA realtime;


ALTER SCHEMA realtime OWNER TO supabase_admin;

--
-- Name: storage; Type: SCHEMA; Schema: -; Owner: supabase_admin
--

CREATE SCHEMA storage;


ALTER SCHEMA storage OWNER TO supabase_admin;

--
-- Name: pg_stat_statements; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_stat_statements WITH SCHEMA extensions;


--
-- Name: EXTENSION pg_stat_statements; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_stat_statements IS 'track planning and execution statistics of all SQL statements executed';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA extensions;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: pgjwt; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgjwt WITH SCHEMA extensions;


--
-- Name: EXTENSION pgjwt; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgjwt IS 'JSON Web Token API for Postgresql';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA extensions;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: vne_chatmessages_channel_enum; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.vne_chatmessages_channel_enum AS ENUM (
    'whatsapp'
);


ALTER TYPE public.vne_chatmessages_channel_enum OWNER TO postgres;

--
-- Name: vne_chatmessages_type_enum; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.vne_chatmessages_type_enum AS ENUM (
    'text',
    'audio',
    'file',
    'image'
);


ALTER TYPE public.vne_chatmessages_type_enum OWNER TO postgres;

--
-- Name: vne_employeeactions_direction_enum; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.vne_employeeactions_direction_enum AS ENUM (
    'in',
    'out'
);


ALTER TYPE public.vne_employeeactions_direction_enum OWNER TO postgres;

--
-- Name: action; Type: TYPE; Schema: realtime; Owner: supabase_admin
--

CREATE TYPE realtime.action AS ENUM (
    'INSERT',
    'UPDATE',
    'DELETE',
    'TRUNCATE',
    'ERROR'
);


ALTER TYPE realtime.action OWNER TO supabase_admin;

--
-- Name: equality_op; Type: TYPE; Schema: realtime; Owner: supabase_admin
--

CREATE TYPE realtime.equality_op AS ENUM (
    'eq',
    'neq',
    'lt',
    'lte',
    'gt',
    'gte'
);


ALTER TYPE realtime.equality_op OWNER TO supabase_admin;

--
-- Name: user_defined_filter; Type: TYPE; Schema: realtime; Owner: supabase_admin
--

CREATE TYPE realtime.user_defined_filter AS (
	column_name text,
	op realtime.equality_op,
	value text
);


ALTER TYPE realtime.user_defined_filter OWNER TO supabase_admin;

--
-- Name: wal_column; Type: TYPE; Schema: realtime; Owner: supabase_admin
--

CREATE TYPE realtime.wal_column AS (
	name text,
	type text,
	value jsonb,
	is_pkey boolean,
	is_selectable boolean
);


ALTER TYPE realtime.wal_column OWNER TO supabase_admin;

--
-- Name: wal_rls; Type: TYPE; Schema: realtime; Owner: supabase_admin
--

CREATE TYPE realtime.wal_rls AS (
	wal jsonb,
	is_rls_enabled boolean,
	subscription_ids uuid[],
	errors text[]
);


ALTER TYPE realtime.wal_rls OWNER TO supabase_admin;

--
-- Name: email(); Type: FUNCTION; Schema: auth; Owner: supabase_auth_admin
--

CREATE FUNCTION auth.email() RETURNS text
    LANGUAGE sql STABLE
    AS $$
  select 
  	coalesce(
		current_setting('request.jwt.claim.email', true),
		(current_setting('request.jwt.claims', true)::jsonb ->> 'email')
	)::text
$$;


ALTER FUNCTION auth.email() OWNER TO supabase_auth_admin;

--
-- Name: role(); Type: FUNCTION; Schema: auth; Owner: supabase_auth_admin
--

CREATE FUNCTION auth.role() RETURNS text
    LANGUAGE sql STABLE
    AS $$
  select 
  	coalesce(
		current_setting('request.jwt.claim.role', true),
		(current_setting('request.jwt.claims', true)::jsonb ->> 'role')
	)::text
$$;


ALTER FUNCTION auth.role() OWNER TO supabase_auth_admin;

--
-- Name: uid(); Type: FUNCTION; Schema: auth; Owner: supabase_auth_admin
--

CREATE FUNCTION auth.uid() RETURNS uuid
    LANGUAGE sql STABLE
    AS $$
  select
  nullif(
    coalesce(
      current_setting('request.jwt.claim.sub', true),
      (current_setting('request.jwt.claims', true)::jsonb ->> 'sub')
    ),
    ''
  )::uuid
$$;


ALTER FUNCTION auth.uid() OWNER TO supabase_auth_admin;

--
-- Name: grant_pg_cron_access(); Type: FUNCTION; Schema: extensions; Owner: postgres
--

CREATE FUNCTION extensions.grant_pg_cron_access() RETURNS event_trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  schema_is_cron bool;
BEGIN
  schema_is_cron = (
    SELECT n.nspname = 'cron'
    FROM pg_event_trigger_ddl_commands() AS ev
    LEFT JOIN pg_catalog.pg_namespace AS n
      ON ev.objid = n.oid
  );

  IF schema_is_cron
  THEN
    grant usage on schema cron to postgres with grant option;

    alter default privileges in schema cron grant all on tables to postgres with grant option;
    alter default privileges in schema cron grant all on functions to postgres with grant option;
    alter default privileges in schema cron grant all on sequences to postgres with grant option;

    alter default privileges for user supabase_admin in schema cron grant all
        on sequences to postgres with grant option;
    alter default privileges for user supabase_admin in schema cron grant all
        on tables to postgres with grant option;
    alter default privileges for user supabase_admin in schema cron grant all
        on functions to postgres with grant option;

    grant all privileges on all tables in schema cron to postgres with grant option; 

  END IF;

END;
$$;


ALTER FUNCTION extensions.grant_pg_cron_access() OWNER TO postgres;

--
-- Name: FUNCTION grant_pg_cron_access(); Type: COMMENT; Schema: extensions; Owner: postgres
--

COMMENT ON FUNCTION extensions.grant_pg_cron_access() IS 'Grants access to pg_cron';


--
-- Name: grant_pg_net_access(); Type: FUNCTION; Schema: extensions; Owner: postgres
--

CREATE FUNCTION extensions.grant_pg_net_access() RETURNS event_trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF EXISTS (
    SELECT 1
    FROM pg_event_trigger_ddl_commands() AS ev
    JOIN pg_extension AS ext
    ON ev.objid = ext.oid
    WHERE ext.extname = 'pg_net'
  )
  THEN
    IF NOT EXISTS (
      SELECT 1
      FROM pg_roles
      WHERE rolname = 'supabase_functions_admin'
    )
    THEN
      CREATE USER supabase_functions_admin NOINHERIT CREATEROLE LOGIN NOREPLICATION;
    END IF;

    GRANT USAGE ON SCHEMA net TO supabase_functions_admin, postgres, anon, authenticated, service_role;

    ALTER function net.http_get(url text, params jsonb, headers jsonb, timeout_milliseconds integer) SECURITY DEFINER;
    ALTER function net.http_post(url text, body jsonb, params jsonb, headers jsonb, timeout_milliseconds integer) SECURITY DEFINER;
    ALTER function net.http_collect_response(request_id bigint, async boolean) SECURITY DEFINER;

    ALTER function net.http_get(url text, params jsonb, headers jsonb, timeout_milliseconds integer) SET search_path = net;
    ALTER function net.http_post(url text, body jsonb, params jsonb, headers jsonb, timeout_milliseconds integer) SET search_path = net;
    ALTER function net.http_collect_response(request_id bigint, async boolean) SET search_path = net;

    REVOKE ALL ON FUNCTION net.http_get(url text, params jsonb, headers jsonb, timeout_milliseconds integer) FROM PUBLIC;
    REVOKE ALL ON FUNCTION net.http_post(url text, body jsonb, params jsonb, headers jsonb, timeout_milliseconds integer) FROM PUBLIC;
    REVOKE ALL ON FUNCTION net.http_collect_response(request_id bigint, async boolean) FROM PUBLIC;

    GRANT EXECUTE ON FUNCTION net.http_get(url text, params jsonb, headers jsonb, timeout_milliseconds integer) TO supabase_functions_admin, postgres, anon, authenticated, service_role;
    GRANT EXECUTE ON FUNCTION net.http_post(url text, body jsonb, params jsonb, headers jsonb, timeout_milliseconds integer) TO supabase_functions_admin, postgres, anon, authenticated, service_role;
    GRANT EXECUTE ON FUNCTION net.http_collect_response(request_id bigint, async boolean) TO supabase_functions_admin, postgres, anon, authenticated, service_role;
  END IF;
END;
$$;


ALTER FUNCTION extensions.grant_pg_net_access() OWNER TO postgres;

--
-- Name: FUNCTION grant_pg_net_access(); Type: COMMENT; Schema: extensions; Owner: postgres
--

COMMENT ON FUNCTION extensions.grant_pg_net_access() IS 'Grants access to pg_net';


--
-- Name: pgrst_ddl_watch(); Type: FUNCTION; Schema: extensions; Owner: supabase_admin
--

CREATE FUNCTION extensions.pgrst_ddl_watch() RETURNS event_trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  cmd record;
BEGIN
  FOR cmd IN SELECT * FROM pg_event_trigger_ddl_commands()
  LOOP
    IF cmd.command_tag IN (
      'CREATE SCHEMA', 'ALTER SCHEMA'
    , 'CREATE TABLE', 'CREATE TABLE AS', 'SELECT INTO', 'ALTER TABLE'
    , 'CREATE FOREIGN TABLE', 'ALTER FOREIGN TABLE'
    , 'CREATE VIEW', 'ALTER VIEW'
    , 'CREATE MATERIALIZED VIEW', 'ALTER MATERIALIZED VIEW'
    , 'CREATE FUNCTION', 'ALTER FUNCTION'
    , 'CREATE TRIGGER'
    , 'CREATE TYPE'
    , 'CREATE RULE'
    , 'COMMENT'
    )
    -- don't notify in case of CREATE TEMP table or other objects created on pg_temp
    AND cmd.schema_name is distinct from 'pg_temp'
    THEN
      NOTIFY pgrst, 'reload schema';
    END IF;
  END LOOP;
END; $$;


ALTER FUNCTION extensions.pgrst_ddl_watch() OWNER TO supabase_admin;

--
-- Name: pgrst_drop_watch(); Type: FUNCTION; Schema: extensions; Owner: supabase_admin
--

CREATE FUNCTION extensions.pgrst_drop_watch() RETURNS event_trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  obj record;
BEGIN
  FOR obj IN SELECT * FROM pg_event_trigger_dropped_objects()
  LOOP
    IF obj.object_type IN (
      'schema'
    , 'table'
    , 'foreign table'
    , 'view'
    , 'materialized view'
    , 'function'
    , 'trigger'
    , 'type'
    , 'rule'
    )
    AND obj.is_temporary IS false -- no pg_temp objects
    THEN
      NOTIFY pgrst, 'reload schema';
    END IF;
  END LOOP;
END; $$;


ALTER FUNCTION extensions.pgrst_drop_watch() OWNER TO supabase_admin;

--
-- Name: get_auth(text); Type: FUNCTION; Schema: pgbouncer; Owner: postgres
--

CREATE FUNCTION pgbouncer.get_auth(p_usename text) RETURNS TABLE(username text, password text)
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
    RAISE WARNING 'PgBouncer auth request: %', p_usename;

    RETURN QUERY
    SELECT usename::TEXT, passwd::TEXT FROM pg_catalog.pg_shadow
    WHERE usename = p_usename;
END;
$$;


ALTER FUNCTION pgbouncer.get_auth(p_usename text) OWNER TO postgres;

--
-- Name: apply_rls(jsonb, integer); Type: FUNCTION; Schema: realtime; Owner: supabase_admin
--

CREATE FUNCTION realtime.apply_rls(wal jsonb, max_record_bytes integer DEFAULT (1024 * 1024)) RETURNS SETOF realtime.wal_rls
    LANGUAGE plpgsql
    AS $$
      declare
        -- Regclass of the table e.g. public.notes
        entity_ regclass = (quote_ident(wal ->> 'schema') || '.' || quote_ident(wal ->> 'table'))::regclass;

        -- I, U, D, T: insert, update ...
        action realtime.action = (
          case wal ->> 'action'
            when 'I' then 'INSERT'
            when 'U' then 'UPDATE'
            when 'D' then 'DELETE'
            else 'ERROR'
          end
        );

        -- Is row level security enabled for the table
        is_rls_enabled bool = relrowsecurity from pg_class where oid = entity_;

        subscriptions realtime.subscription[] = array_agg(subs)
          from
            realtime.subscription subs
          where
            subs.entity = entity_;

        -- Subscription vars
        roles regrole[] = array_agg(distinct us.claims_role)
          from
            unnest(subscriptions) us;

        working_role regrole;
        claimed_role regrole;
        claims jsonb;

        subscription_id uuid;
        subscription_has_access bool;
        visible_to_subscription_ids uuid[] = '{}';

        -- structured info for wal's columns
        columns realtime.wal_column[];
        -- previous identity values for update/delete
        old_columns realtime.wal_column[];

        error_record_exceeds_max_size boolean = octet_length(wal::text) > max_record_bytes;

        -- Primary jsonb output for record
        output jsonb;

      begin
        perform set_config('role', null, true);

        columns =
          array_agg(
            (
              x->>'name',
              x->>'type',
              realtime.cast((x->'value') #>> '{}', (x->>'type')::regtype),
              (pks ->> 'name') is not null,
              true
            )::realtime.wal_column
          )
          from
            jsonb_array_elements(wal -> 'columns') x
            left join jsonb_array_elements(wal -> 'pk') pks
              on (x ->> 'name') = (pks ->> 'name');

        old_columns =
          array_agg(
            (
              x->>'name',
              x->>'type',
              realtime.cast((x->'value') #>> '{}', (x->>'type')::regtype),
              (pks ->> 'name') is not null,
              true
            )::realtime.wal_column
          )
          from
            jsonb_array_elements(wal -> 'identity') x
            left join jsonb_array_elements(wal -> 'pk') pks
              on (x ->> 'name') = (pks ->> 'name');

        for working_role in select * from unnest(roles) loop

          -- Update `is_selectable` for columns and old_columns
          columns =
            array_agg(
              (
                c.name,
                c.type,
                c.value,
                c.is_pkey,
                pg_catalog.has_column_privilege(working_role, entity_, c.name, 'SELECT')
              )::realtime.wal_column
            )
            from
              unnest(columns) c;

          old_columns =
            array_agg(
              (
                c.name,
                c.type,
                c.value,
                c.is_pkey,
                pg_catalog.has_column_privilege(working_role, entity_, c.name, 'SELECT')
              )::realtime.wal_column
            )
            from
              unnest(old_columns) c;

          if action <> 'DELETE' and count(1) = 0 from unnest(columns) c where c.is_pkey then
            return next (
              null,
              is_rls_enabled,
              -- subscriptions is already filtered by entity
              (select array_agg(s.subscription_id) from unnest(subscriptions) as s where claims_role = working_role),
              array['Error 400: Bad Request, no primary key']
            )::realtime.wal_rls;

          -- The claims role does not have SELECT permission to the primary key of entity
          elsif action <> 'DELETE' and sum(c.is_selectable::int) <> count(1) from unnest(columns) c where c.is_pkey then
            return next (
              null,
              is_rls_enabled,
              (select array_agg(s.subscription_id) from unnest(subscriptions) as s where claims_role = working_role),
              array['Error 401: Unauthorized']
            )::realtime.wal_rls;

          else
            output = jsonb_build_object(
              'schema', wal ->> 'schema',
              'table', wal ->> 'table',
              'type', action,
              'commit_timestamp', to_char(
                (wal ->> 'timestamp')::timestamptz,
                'YYYY-MM-DD"T"HH24:MI:SS"Z"'
              ),
              'columns', (
                select
                  jsonb_agg(
                    jsonb_build_object(
                      'name', pa.attname,
                      'type', pt.typname
                    )
                    order by pa.attnum asc
                  )
                    from
                      pg_attribute pa
                      join pg_type pt
                        on pa.atttypid = pt.oid
                    where
                      attrelid = entity_
                      and attnum > 0
                      and pg_catalog.has_column_privilege(working_role, entity_, pa.attname, 'SELECT')
              )
            )
            -- Add "record" key for insert and update
            || case
                when error_record_exceeds_max_size then jsonb_build_object('record', '{}'::jsonb)
                when action in ('INSERT', 'UPDATE') then
                  jsonb_build_object(
                    'record',
                    (select jsonb_object_agg((c).name, (c).value) from unnest(columns) c where (c).is_selectable)
                  )
                else '{}'::jsonb
            end
            -- Add "old_record" key for update and delete
            || case
                when error_record_exceeds_max_size then jsonb_build_object('old_record', '{}'::jsonb)
                when action in ('UPDATE', 'DELETE') then
                  jsonb_build_object(
                    'old_record',
                    (select jsonb_object_agg((c).name, (c).value) from unnest(old_columns) c where (c).is_selectable)
                  )
                else '{}'::jsonb
            end;

            -- Create the prepared statement
            if is_rls_enabled and action <> 'DELETE' then
              if (select 1 from pg_prepared_statements where name = 'walrus_rls_stmt' limit 1) > 0 then
                deallocate walrus_rls_stmt;
              end if;
              execute realtime.build_prepared_statement_sql('walrus_rls_stmt', entity_, columns);
            end if;

            visible_to_subscription_ids = '{}';

            for subscription_id, claims in (
                select
                  subs.subscription_id,
                  subs.claims
                from
                  unnest(subscriptions) subs
                where
                  subs.entity = entity_
                  and subs.claims_role = working_role
                  and realtime.is_visible_through_filters(columns, subs.filters)
              ) loop

              if not is_rls_enabled or action = 'DELETE' then
                visible_to_subscription_ids = visible_to_subscription_ids || subscription_id;
              else
                -- Check if RLS allows the role to see the record
                perform
                  set_config('role', working_role::text, true),
                  set_config('request.jwt.claims', claims::text, true);

                execute 'execute walrus_rls_stmt' into subscription_has_access;

                if subscription_has_access then
                  visible_to_subscription_ids = visible_to_subscription_ids || subscription_id;
                end if;
              end if;
            end loop;

            perform set_config('role', null, true);

            return next (
              output,
              is_rls_enabled,
              visible_to_subscription_ids,
              case
                when error_record_exceeds_max_size then array['Error 413: Payload Too Large']
                else '{}'
              end
            )::realtime.wal_rls;

          end if;
        end loop;

        perform set_config('role', null, true);
      end;
      $$;


ALTER FUNCTION realtime.apply_rls(wal jsonb, max_record_bytes integer) OWNER TO supabase_admin;

--
-- Name: build_prepared_statement_sql(text, regclass, realtime.wal_column[]); Type: FUNCTION; Schema: realtime; Owner: supabase_admin
--

CREATE FUNCTION realtime.build_prepared_statement_sql(prepared_statement_name text, entity regclass, columns realtime.wal_column[]) RETURNS text
    LANGUAGE sql
    AS $$
    /*
    Builds a sql string that, if executed, creates a prepared statement to
    tests retrive a row from *entity* by its primary key columns.

    Example
      select realtime.build_prepared_statment_sql('public.notes', '{"id"}'::text[], '{"bigint"}'::text[])
    */
      select
    'prepare ' || prepared_statement_name || ' as
      select
        exists(
          select
            1
          from
            ' || entity || '
          where
            ' || string_agg(quote_ident(pkc.name) || '=' || quote_nullable(pkc.value #>> '{}') , ' and ') || '
        )'
      from
        unnest(columns) pkc
      where
        pkc.is_pkey
      group by
        entity
    $$;


ALTER FUNCTION realtime.build_prepared_statement_sql(prepared_statement_name text, entity regclass, columns realtime.wal_column[]) OWNER TO supabase_admin;

--
-- Name: cast(text, regtype); Type: FUNCTION; Schema: realtime; Owner: supabase_admin
--

CREATE FUNCTION realtime."cast"(val text, type_ regtype) RETURNS jsonb
    LANGUAGE plpgsql IMMUTABLE
    AS $$
    declare
      res jsonb;
    begin
      execute format('select to_jsonb(%L::'|| type_::text || ')', val)  into res;
      return res;
    end
    $$;


ALTER FUNCTION realtime."cast"(val text, type_ regtype) OWNER TO supabase_admin;

--
-- Name: check_equality_op(realtime.equality_op, regtype, text, text); Type: FUNCTION; Schema: realtime; Owner: supabase_admin
--

CREATE FUNCTION realtime.check_equality_op(op realtime.equality_op, type_ regtype, val_1 text, val_2 text) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE
    AS $$
    /*
    Casts *val_1* and *val_2* as type *type_* and check the *op* condition for truthiness
    */
    declare
      op_symbol text = (
        case
          when op = 'eq' then '='
          when op = 'neq' then '!='
          when op = 'lt' then '<'
          when op = 'lte' then '<='
          when op = 'gt' then '>'
          when op = 'gte' then '>='
          else 'UNKNOWN OP'
        end
      );
      res boolean;
    begin
      execute format('select %L::'|| type_::text || ' ' || op_symbol || ' %L::'|| type_::text, val_1, val_2) into res;
      return res;
    end;
    $$;


ALTER FUNCTION realtime.check_equality_op(op realtime.equality_op, type_ regtype, val_1 text, val_2 text) OWNER TO supabase_admin;

--
-- Name: is_visible_through_filters(realtime.wal_column[], realtime.user_defined_filter[]); Type: FUNCTION; Schema: realtime; Owner: supabase_admin
--

CREATE FUNCTION realtime.is_visible_through_filters(columns realtime.wal_column[], filters realtime.user_defined_filter[]) RETURNS boolean
    LANGUAGE sql IMMUTABLE
    AS $$
    /*
    Should the record be visible (true) or filtered out (false) after *filters* are applied
    */
    select
      -- Default to allowed when no filters present
      coalesce(
        sum(
          realtime.check_equality_op(
            op:=f.op,
            type_:=col.type::regtype,
            -- cast jsonb to text
            val_1:=col.value #>> '{}',
            val_2:=f.value
          )::int
        ) = count(1),
        true
      )
    from
      unnest(filters) f
      join unnest(columns) col
          on f.column_name = col.name;
    $$;


ALTER FUNCTION realtime.is_visible_through_filters(columns realtime.wal_column[], filters realtime.user_defined_filter[]) OWNER TO supabase_admin;

--
-- Name: quote_wal2json(regclass); Type: FUNCTION; Schema: realtime; Owner: supabase_admin
--

CREATE FUNCTION realtime.quote_wal2json(entity regclass) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $$
      select
        (
          select string_agg('' || ch,'')
          from unnest(string_to_array(nsp.nspname::text, null)) with ordinality x(ch, idx)
          where
            not (x.idx = 1 and x.ch = '"')
            and not (
              x.idx = array_length(string_to_array(nsp.nspname::text, null), 1)
              and x.ch = '"'
            )
        )
        || '.'
        || (
          select string_agg('' || ch,'')
          from unnest(string_to_array(pc.relname::text, null)) with ordinality x(ch, idx)
          where
            not (x.idx = 1 and x.ch = '"')
            and not (
              x.idx = array_length(string_to_array(nsp.nspname::text, null), 1)
              and x.ch = '"'
            )
          )
      from
        pg_class pc
        join pg_namespace nsp
          on pc.relnamespace = nsp.oid
      where
        pc.oid = entity
    $$;


ALTER FUNCTION realtime.quote_wal2json(entity regclass) OWNER TO supabase_admin;

--
-- Name: subscription_check_filters(); Type: FUNCTION; Schema: realtime; Owner: supabase_admin
--

CREATE FUNCTION realtime.subscription_check_filters() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        /*
          Validates that the user defined filters for a subscription:
            - refer to valid columns that the claimed role may access
            - values are coercable to the correct column type
        */
      declare
        col_names text[] = coalesce(
            array_agg(c.column_name order by c.ordinal_position),
            '{}'::text[]
          )
          from
            information_schema.columns c
          where
            format('%I.%I', c.table_schema, c.table_name)::regclass = new.entity
            and pg_catalog.has_column_privilege((new.claims ->> 'role'), new.entity, c.column_name, 'SELECT');
        filter realtime.user_defined_filter;
        col_type regtype;
      begin
        for filter in select * from unnest(new.filters) loop
          -- Filtered column is valid
          if not filter.column_name = any(col_names) then
            raise exception 'invalid column for filter %', filter.column_name;
          end if;

          -- Type is sanitized and safe for string interpolation
          col_type = (
            select atttypid::regtype
            from pg_catalog.pg_attribute
            where attrelid = new.entity
              and attname = filter.column_name
          );
          if col_type is null then
            raise exception 'failed to lookup type for column %', filter.column_name;
          end if;
          -- raises an exception if value is not coercable to type
          perform realtime.cast(filter.value, col_type);
        end loop;

        -- Apply consistent order to filters so the unique constraint on
        -- (subscription_id, entity, filters) can't be tricked by a different filter order
        new.filters = coalesce(
          array_agg(f order by f.column_name, f.op, f.value),
          '{}'
        ) from unnest(new.filters) f;

        return new;
      end;
    $$;


ALTER FUNCTION realtime.subscription_check_filters() OWNER TO supabase_admin;

--
-- Name: to_regrole(text); Type: FUNCTION; Schema: realtime; Owner: supabase_admin
--

CREATE FUNCTION realtime.to_regrole(role_name text) RETURNS regrole
    LANGUAGE sql IMMUTABLE
    AS $$ select role_name::regrole $$;


ALTER FUNCTION realtime.to_regrole(role_name text) OWNER TO supabase_admin;

--
-- Name: extension(text); Type: FUNCTION; Schema: storage; Owner: supabase_storage_admin
--

CREATE FUNCTION storage.extension(name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
_parts text[];
_filename text;
BEGIN
	select string_to_array(name, '/') into _parts;
	select _parts[array_length(_parts,1)] into _filename;
	-- @todo return the last part instead of 2
	return split_part(_filename, '.', 2);
END
$$;


ALTER FUNCTION storage.extension(name text) OWNER TO supabase_storage_admin;

--
-- Name: filename(text); Type: FUNCTION; Schema: storage; Owner: supabase_storage_admin
--

CREATE FUNCTION storage.filename(name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
_parts text[];
BEGIN
	select string_to_array(name, '/') into _parts;
	return _parts[array_length(_parts,1)];
END
$$;


ALTER FUNCTION storage.filename(name text) OWNER TO supabase_storage_admin;

--
-- Name: foldername(text); Type: FUNCTION; Schema: storage; Owner: supabase_storage_admin
--

CREATE FUNCTION storage.foldername(name text) RETURNS text[]
    LANGUAGE plpgsql
    AS $$
DECLARE
_parts text[];
BEGIN
	select string_to_array(name, '/') into _parts;
	return _parts[1:array_length(_parts,1)-1];
END
$$;


ALTER FUNCTION storage.foldername(name text) OWNER TO supabase_storage_admin;

--
-- Name: get_size_by_bucket(); Type: FUNCTION; Schema: storage; Owner: supabase_storage_admin
--

CREATE FUNCTION storage.get_size_by_bucket() RETURNS TABLE(size bigint, bucket_id text)
    LANGUAGE plpgsql
    AS $$
BEGIN
    return query
        select sum((metadata->>'size')::int) as size, obj.bucket_id
        from "storage".objects as obj
        group by obj.bucket_id;
END
$$;


ALTER FUNCTION storage.get_size_by_bucket() OWNER TO supabase_storage_admin;

--
-- Name: search(text, text, integer, integer, integer); Type: FUNCTION; Schema: storage; Owner: supabase_storage_admin
--

CREATE FUNCTION storage.search(prefix text, bucketname text, limits integer DEFAULT 100, levels integer DEFAULT 1, offsets integer DEFAULT 0) RETURNS TABLE(name text, id uuid, updated_at timestamp with time zone, created_at timestamp with time zone, last_accessed_at timestamp with time zone, metadata jsonb)
    LANGUAGE plpgsql
    AS $$
BEGIN
	return query 
		with files_folders as (
			select path_tokens[levels] as folder
			from storage.objects
			where objects.name ilike prefix || '%'
			and bucket_id = bucketname
			GROUP by folder
			limit limits
			offset offsets
		) 
		select files_folders.folder as name, objects.id, objects.updated_at, objects.created_at, objects.last_accessed_at, objects.metadata from files_folders 
		left join storage.objects
		on prefix || files_folders.folder = objects.name and objects.bucket_id=bucketname;
END
$$;


ALTER FUNCTION storage.search(prefix text, bucketname text, limits integer, levels integer, offsets integer) OWNER TO supabase_storage_admin;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: audit_log_entries; Type: TABLE; Schema: auth; Owner: supabase_auth_admin
--

CREATE TABLE auth.audit_log_entries (
    instance_id uuid,
    id uuid NOT NULL,
    payload json,
    created_at timestamp with time zone
);


ALTER TABLE auth.audit_log_entries OWNER TO supabase_auth_admin;

--
-- Name: TABLE audit_log_entries; Type: COMMENT; Schema: auth; Owner: supabase_auth_admin
--

COMMENT ON TABLE auth.audit_log_entries IS 'Auth: Audit trail for user actions.';


--
-- Name: identities; Type: TABLE; Schema: auth; Owner: supabase_auth_admin
--

CREATE TABLE auth.identities (
    id text NOT NULL,
    user_id uuid NOT NULL,
    identity_data jsonb NOT NULL,
    provider text NOT NULL,
    last_sign_in_at timestamp with time zone,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE auth.identities OWNER TO supabase_auth_admin;

--
-- Name: TABLE identities; Type: COMMENT; Schema: auth; Owner: supabase_auth_admin
--

COMMENT ON TABLE auth.identities IS 'Auth: Stores identities associated to a user.';


--
-- Name: instances; Type: TABLE; Schema: auth; Owner: supabase_auth_admin
--

CREATE TABLE auth.instances (
    id uuid NOT NULL,
    uuid uuid,
    raw_base_config text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE auth.instances OWNER TO supabase_auth_admin;

--
-- Name: TABLE instances; Type: COMMENT; Schema: auth; Owner: supabase_auth_admin
--

COMMENT ON TABLE auth.instances IS 'Auth: Manages users across multiple sites.';


--
-- Name: refresh_tokens; Type: TABLE; Schema: auth; Owner: supabase_auth_admin
--

CREATE TABLE auth.refresh_tokens (
    instance_id uuid,
    id bigint NOT NULL,
    token character varying(255),
    user_id character varying(255),
    revoked boolean,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    parent character varying(255)
);


ALTER TABLE auth.refresh_tokens OWNER TO supabase_auth_admin;

--
-- Name: TABLE refresh_tokens; Type: COMMENT; Schema: auth; Owner: supabase_auth_admin
--

COMMENT ON TABLE auth.refresh_tokens IS 'Auth: Store of tokens used to refresh JWT tokens once they expire.';


--
-- Name: refresh_tokens_id_seq; Type: SEQUENCE; Schema: auth; Owner: supabase_auth_admin
--

CREATE SEQUENCE auth.refresh_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth.refresh_tokens_id_seq OWNER TO supabase_auth_admin;

--
-- Name: refresh_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: auth; Owner: supabase_auth_admin
--

ALTER SEQUENCE auth.refresh_tokens_id_seq OWNED BY auth.refresh_tokens.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: auth; Owner: supabase_auth_admin
--

CREATE TABLE auth.schema_migrations (
    version character varying(255) NOT NULL
);


ALTER TABLE auth.schema_migrations OWNER TO supabase_auth_admin;

--
-- Name: TABLE schema_migrations; Type: COMMENT; Schema: auth; Owner: supabase_auth_admin
--

COMMENT ON TABLE auth.schema_migrations IS 'Auth: Manages updates to the auth system.';


--
-- Name: users; Type: TABLE; Schema: auth; Owner: supabase_auth_admin
--

CREATE TABLE auth.users (
    instance_id uuid,
    id uuid NOT NULL,
    aud character varying(255),
    role character varying(255),
    email character varying(255),
    encrypted_password character varying(255),
    email_confirmed_at timestamp with time zone,
    invited_at timestamp with time zone,
    confirmation_token character varying(255),
    confirmation_sent_at timestamp with time zone,
    recovery_token character varying(255),
    recovery_sent_at timestamp with time zone,
    email_change_token_new character varying(255),
    email_change character varying(255),
    email_change_sent_at timestamp with time zone,
    last_sign_in_at timestamp with time zone,
    raw_app_meta_data jsonb,
    raw_user_meta_data jsonb,
    is_super_admin boolean,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    phone character varying(15) DEFAULT NULL::character varying,
    phone_confirmed_at timestamp with time zone,
    phone_change character varying(15) DEFAULT ''::character varying,
    phone_change_token character varying(255) DEFAULT ''::character varying,
    phone_change_sent_at timestamp with time zone,
    confirmed_at timestamp with time zone GENERATED ALWAYS AS (LEAST(email_confirmed_at, phone_confirmed_at)) STORED,
    email_change_token_current character varying(255) DEFAULT ''::character varying,
    email_change_confirm_status smallint DEFAULT 0,
    banned_until timestamp with time zone,
    CONSTRAINT users_email_change_confirm_status_check CHECK (((email_change_confirm_status >= 0) AND (email_change_confirm_status <= 2)))
);


ALTER TABLE auth.users OWNER TO supabase_auth_admin;

--
-- Name: TABLE users; Type: COMMENT; Schema: auth; Owner: supabase_auth_admin
--

COMMENT ON TABLE auth.users IS 'Auth: Stores user login data within a secure schema.';


--
-- Name: typeorm_metadata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.typeorm_metadata (
    type character varying NOT NULL,
    database character varying,
    schema character varying,
    "table" character varying,
    name character varying,
    value text
);


ALTER TABLE public.typeorm_metadata OWNER TO postgres;

--
-- Name: vne_actionfiles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_actionfiles (
    id integer NOT NULL,
    action_id integer NOT NULL,
    url character varying,
    description character varying,
    name character varying
);


ALTER TABLE public.vne_actionfiles OWNER TO postgres;

--
-- Name: vne_actionfiles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_actionfiles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_actionfiles_id_seq OWNER TO postgres;

--
-- Name: vne_actionfiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_actionfiles_id_seq OWNED BY public.vne_actionfiles.id;


--
-- Name: vne_actionreminders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_actionreminders (
    id integer NOT NULL,
    action_id integer,
    date date,
    description character varying
);


ALTER TABLE public.vne_actionreminders OWNER TO postgres;

--
-- Name: vne_actionreminders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_actionreminders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_actionreminders_id_seq OWNER TO postgres;

--
-- Name: vne_actionreminders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_actionreminders_id_seq OWNED BY public.vne_actionreminders.id;


--
-- Name: vne_actions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_actions (
    id integer NOT NULL,
    company_id integer,
    employee_id integer,
    employeeaction_id integer,
    chatmessage_id integer,
    deal_id integer,
    comment text,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    actionstatus_id integer
);


ALTER TABLE public.vne_actions OWNER TO postgres;

--
-- Name: vne_actions_actiontags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_actions_actiontags (
    action_id integer NOT NULL,
    actiontag_id integer NOT NULL
);


ALTER TABLE public.vne_actions_actiontags OWNER TO postgres;

--
-- Name: vne_actions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_actions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_actions_id_seq OWNER TO postgres;

--
-- Name: vne_actions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_actions_id_seq OWNED BY public.vne_actions.id;


--
-- Name: vne_actionstatuses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_actionstatuses (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE public.vne_actionstatuses OWNER TO postgres;

--
-- Name: vne_actionstatuses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_actionstatuses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_actionstatuses_id_seq OWNER TO postgres;

--
-- Name: vne_actionstatuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_actionstatuses_id_seq OWNED BY public.vne_actionstatuses.id;


--
-- Name: vne_actiontags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_actiontags (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE public.vne_actiontags OWNER TO postgres;

--
-- Name: vne_actiontags_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_actiontags_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_actiontags_id_seq OWNER TO postgres;

--
-- Name: vne_actiontags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_actiontags_id_seq OWNED BY public.vne_actiontags.id;


--
-- Name: vne_admingroups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_admingroups (
    id integer NOT NULL,
    code character varying,
    name character varying,
    defended boolean DEFAULT false NOT NULL
);


ALTER TABLE public.vne_admingroups OWNER TO postgres;

--
-- Name: vne_admingroups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_admingroups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_admingroups_id_seq OWNER TO postgres;

--
-- Name: vne_admingroups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_admingroups_id_seq OWNED BY public.vne_admingroups.id;


--
-- Name: vne_admins; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_admins (
    id integer NOT NULL,
    group_id integer NOT NULL,
    name character varying,
    email character varying NOT NULL,
    password character varying NOT NULL,
    img character varying,
    active boolean DEFAULT true NOT NULL,
    defended boolean DEFAULT false NOT NULL
);


ALTER TABLE public.vne_admins OWNER TO postgres;

--
-- Name: vne_admins_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_admins_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_admins_id_seq OWNER TO postgres;

--
-- Name: vne_admins_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_admins_id_seq OWNED BY public.vne_admins.id;


--
-- Name: vne_chatmessages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_chatmessages (
    id integer NOT NULL,
    outer_id character varying,
    contractor_id integer,
    channel public.vne_chatmessages_channel_enum DEFAULT 'whatsapp'::public.vne_chatmessages_channel_enum NOT NULL,
    type public.vne_chatmessages_type_enum DEFAULT 'text'::public.vne_chatmessages_type_enum NOT NULL,
    text text,
    is_task boolean DEFAULT true NOT NULL,
    is_answer boolean DEFAULT false NOT NULL,
    is_read boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    fileurl character varying,
    filename character varying
);


ALTER TABLE public.vne_chatmessages OWNER TO postgres;

--
-- Name: vne_chatmessages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_chatmessages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_chatmessages_id_seq OWNER TO postgres;

--
-- Name: vne_chatmessages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_chatmessages_id_seq OWNED BY public.vne_chatmessages.id;


--
-- Name: vne_companies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_companies (
    id integer NOT NULL,
    name character varying,
    email character varying,
    n8n_code character varying
);


ALTER TABLE public.vne_companies OWNER TO postgres;

--
-- Name: vne_companies_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_companies_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_companies_id_seq OWNER TO postgres;

--
-- Name: vne_companies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_companies_id_seq OWNED BY public.vne_companies.id;


--
-- Name: vne_contractorgroups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_contractorgroups (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE public.vne_contractorgroups OWNER TO postgres;

--
-- Name: vne_contractorgroups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_contractorgroups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_contractorgroups_id_seq OWNER TO postgres;

--
-- Name: vne_contractorgroups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_contractorgroups_id_seq OWNED BY public.vne_contractorgroups.id;


--
-- Name: vne_contractors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_contractors (
    id integer NOT NULL,
    company_id integer,
    name character varying,
    phone character varying,
    img character varying,
    chat_active boolean DEFAULT false NOT NULL,
    chat_at timestamp without time zone,
    banned boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.vne_contractors OWNER TO postgres;

--
-- Name: vne_contractors_contractorgroups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_contractors_contractorgroups (
    contractor_id integer NOT NULL,
    contractorgroup_id integer NOT NULL
);


ALTER TABLE public.vne_contractors_contractorgroups OWNER TO postgres;

--
-- Name: vne_contractors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_contractors_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_contractors_id_seq OWNER TO postgres;

--
-- Name: vne_contractors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_contractors_id_seq OWNED BY public.vne_contractors.id;


--
-- Name: vne_dealdates; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_dealdates (
    id integer NOT NULL,
    deal_id integer,
    date date,
    description character varying
);


ALTER TABLE public.vne_dealdates OWNER TO postgres;

--
-- Name: vne_dealdates_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_dealdates_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_dealdates_id_seq OWNER TO postgres;

--
-- Name: vne_dealdates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_dealdates_id_seq OWNED BY public.vne_dealdates.id;


--
-- Name: vne_deals; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_deals (
    id integer NOT NULL,
    company_id integer,
    contact_contractor_id integer,
    client_contractor_id integer,
    partner_contractor_id integer,
    name character varying,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    description text,
    closed boolean DEFAULT false NOT NULL
);


ALTER TABLE public.vne_deals OWNER TO postgres;

--
-- Name: vne_deals_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_deals_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_deals_id_seq OWNER TO postgres;

--
-- Name: vne_deals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_deals_id_seq OWNED BY public.vne_deals.id;


--
-- Name: vne_employeeactions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_employeeactions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    code character varying,
    name character varying,
    direction public.vne_employeeactions_direction_enum DEFAULT 'in'::public.vne_employeeactions_direction_enum NOT NULL,
    response character varying
);


ALTER TABLE public.vne_employeeactions OWNER TO postgres;

--
-- Name: vne_employeeactions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_employeeactions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_employeeactions_id_seq OWNER TO postgres;

--
-- Name: vne_employeeactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_employeeactions_id_seq OWNED BY public.vne_employeeactions.id;


--
-- Name: vne_employeegroups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_employeegroups (
    id integer NOT NULL,
    code character varying,
    name character varying
);


ALTER TABLE public.vne_employeegroups OWNER TO postgres;

--
-- Name: vne_employeegroups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_employeegroups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_employeegroups_id_seq OWNER TO postgres;

--
-- Name: vne_employeegroups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_employeegroups_id_seq OWNED BY public.vne_employeegroups.id;


--
-- Name: vne_employees; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_employees (
    id integer NOT NULL,
    company_id integer NOT NULL,
    group_id integer NOT NULL,
    email character varying NOT NULL,
    password character varying NOT NULL,
    name character varying,
    active boolean DEFAULT true NOT NULL,
    img character varying
);


ALTER TABLE public.vne_employees OWNER TO postgres;

--
-- Name: vne_employees_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_employees_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_employees_id_seq OWNER TO postgres;

--
-- Name: vne_employees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_employees_id_seq OWNED BY public.vne_employees.id;


--
-- Name: vne_mailtemplates; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_mailtemplates (
    id integer NOT NULL,
    name character varying NOT NULL,
    subject character varying,
    content text,
    defended boolean DEFAULT false NOT NULL
);


ALTER TABLE public.vne_mailtemplates OWNER TO postgres;

--
-- Name: vne_mailtemplates_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_mailtemplates_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_mailtemplates_id_seq OWNER TO postgres;

--
-- Name: vne_mailtemplates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_mailtemplates_id_seq OWNED BY public.vne_mailtemplates.id;


--
-- Name: vne_settings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_settings (
    id integer NOT NULL,
    p character varying,
    v character varying,
    c character varying,
    pos integer DEFAULT 0 NOT NULL,
    in_app boolean DEFAULT false NOT NULL,
    defended boolean DEFAULT false NOT NULL,
    hidden boolean DEFAULT false NOT NULL
);


ALTER TABLE public.vne_settings OWNER TO postgres;

--
-- Name: vne_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_settings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_settings_id_seq OWNER TO postgres;

--
-- Name: vne_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_settings_id_seq OWNED BY public.vne_settings.id;


--
-- Name: vne_verifications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vne_verifications (
    id integer NOT NULL,
    email character varying,
    code character varying,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.vne_verifications OWNER TO postgres;

--
-- Name: vne_verifications_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vne_verifications_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vne_verifications_id_seq OWNER TO postgres;

--
-- Name: vne_verifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vne_verifications_id_seq OWNED BY public.vne_verifications.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: realtime; Owner: supabase_admin
--

CREATE TABLE realtime.schema_migrations (
    version bigint NOT NULL,
    inserted_at timestamp(0) without time zone
);


ALTER TABLE realtime.schema_migrations OWNER TO supabase_admin;

--
-- Name: subscription; Type: TABLE; Schema: realtime; Owner: supabase_admin
--

CREATE TABLE realtime.subscription (
    id bigint NOT NULL,
    subscription_id uuid NOT NULL,
    entity regclass NOT NULL,
    filters realtime.user_defined_filter[] DEFAULT '{}'::realtime.user_defined_filter[] NOT NULL,
    claims jsonb NOT NULL,
    claims_role regrole GENERATED ALWAYS AS (realtime.to_regrole((claims ->> 'role'::text))) STORED NOT NULL,
    created_at timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL
);


ALTER TABLE realtime.subscription OWNER TO supabase_admin;

--
-- Name: subscription_id_seq; Type: SEQUENCE; Schema: realtime; Owner: supabase_admin
--

ALTER TABLE realtime.subscription ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME realtime.subscription_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: buckets; Type: TABLE; Schema: storage; Owner: supabase_storage_admin
--

CREATE TABLE storage.buckets (
    id text NOT NULL,
    name text NOT NULL,
    owner uuid,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    public boolean DEFAULT false
);


ALTER TABLE storage.buckets OWNER TO supabase_storage_admin;

--
-- Name: migrations; Type: TABLE; Schema: storage; Owner: supabase_storage_admin
--

CREATE TABLE storage.migrations (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    hash character varying(40) NOT NULL,
    executed_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE storage.migrations OWNER TO supabase_storage_admin;

--
-- Name: objects; Type: TABLE; Schema: storage; Owner: supabase_storage_admin
--

CREATE TABLE storage.objects (
    id uuid DEFAULT extensions.uuid_generate_v4() NOT NULL,
    bucket_id text,
    name text,
    owner uuid,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    last_accessed_at timestamp with time zone DEFAULT now(),
    metadata jsonb,
    path_tokens text[] GENERATED ALWAYS AS (string_to_array(name, '/'::text)) STORED
);


ALTER TABLE storage.objects OWNER TO supabase_storage_admin;

--
-- Name: refresh_tokens id; Type: DEFAULT; Schema: auth; Owner: supabase_auth_admin
--

ALTER TABLE ONLY auth.refresh_tokens ALTER COLUMN id SET DEFAULT nextval('auth.refresh_tokens_id_seq'::regclass);


--
-- Name: vne_actionfiles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actionfiles ALTER COLUMN id SET DEFAULT nextval('public.vne_actionfiles_id_seq'::regclass);


--
-- Name: vne_actionreminders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actionreminders ALTER COLUMN id SET DEFAULT nextval('public.vne_actionreminders_id_seq'::regclass);


--
-- Name: vne_actions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actions ALTER COLUMN id SET DEFAULT nextval('public.vne_actions_id_seq'::regclass);


--
-- Name: vne_actionstatuses id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actionstatuses ALTER COLUMN id SET DEFAULT nextval('public.vne_actionstatuses_id_seq'::regclass);


--
-- Name: vne_actiontags id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actiontags ALTER COLUMN id SET DEFAULT nextval('public.vne_actiontags_id_seq'::regclass);


--
-- Name: vne_admingroups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_admingroups ALTER COLUMN id SET DEFAULT nextval('public.vne_admingroups_id_seq'::regclass);


--
-- Name: vne_admins id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_admins ALTER COLUMN id SET DEFAULT nextval('public.vne_admins_id_seq'::regclass);


--
-- Name: vne_chatmessages id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_chatmessages ALTER COLUMN id SET DEFAULT nextval('public.vne_chatmessages_id_seq'::regclass);


--
-- Name: vne_companies id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_companies ALTER COLUMN id SET DEFAULT nextval('public.vne_companies_id_seq'::regclass);


--
-- Name: vne_contractorgroups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_contractorgroups ALTER COLUMN id SET DEFAULT nextval('public.vne_contractorgroups_id_seq'::regclass);


--
-- Name: vne_contractors id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_contractors ALTER COLUMN id SET DEFAULT nextval('public.vne_contractors_id_seq'::regclass);


--
-- Name: vne_dealdates id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_dealdates ALTER COLUMN id SET DEFAULT nextval('public.vne_dealdates_id_seq'::regclass);


--
-- Name: vne_deals id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_deals ALTER COLUMN id SET DEFAULT nextval('public.vne_deals_id_seq'::regclass);


--
-- Name: vne_employeeactions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_employeeactions ALTER COLUMN id SET DEFAULT nextval('public.vne_employeeactions_id_seq'::regclass);


--
-- Name: vne_employeegroups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_employeegroups ALTER COLUMN id SET DEFAULT nextval('public.vne_employeegroups_id_seq'::regclass);


--
-- Name: vne_employees id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_employees ALTER COLUMN id SET DEFAULT nextval('public.vne_employees_id_seq'::regclass);


--
-- Name: vne_mailtemplates id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_mailtemplates ALTER COLUMN id SET DEFAULT nextval('public.vne_mailtemplates_id_seq'::regclass);


--
-- Name: vne_settings id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_settings ALTER COLUMN id SET DEFAULT nextval('public.vne_settings_id_seq'::regclass);


--
-- Name: vne_verifications id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_verifications ALTER COLUMN id SET DEFAULT nextval('public.vne_verifications_id_seq'::regclass);


--
-- Data for Name: audit_log_entries; Type: TABLE DATA; Schema: auth; Owner: supabase_auth_admin
--

COPY auth.audit_log_entries (instance_id, id, payload, created_at) FROM stdin;
\.


--
-- Data for Name: identities; Type: TABLE DATA; Schema: auth; Owner: supabase_auth_admin
--

COPY auth.identities (id, user_id, identity_data, provider, last_sign_in_at, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: instances; Type: TABLE DATA; Schema: auth; Owner: supabase_auth_admin
--

COPY auth.instances (id, uuid, raw_base_config, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: refresh_tokens; Type: TABLE DATA; Schema: auth; Owner: supabase_auth_admin
--

COPY auth.refresh_tokens (instance_id, id, token, user_id, revoked, created_at, updated_at, parent) FROM stdin;
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: auth; Owner: supabase_auth_admin
--

COPY auth.schema_migrations (version) FROM stdin;
20171026211738
20171026211808
20171026211834
20180103212743
20180108183307
20180119214651
20180125194653
20210710035447
20210722035447
20210730183235
20210909172000
20210927181326
20211122151130
20211124214934
20211202183645
20220114185221
20220114185340
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: auth; Owner: supabase_auth_admin
--

COPY auth.users (instance_id, id, aud, role, email, encrypted_password, email_confirmed_at, invited_at, confirmation_token, confirmation_sent_at, recovery_token, recovery_sent_at, email_change_token_new, email_change, email_change_sent_at, last_sign_in_at, raw_app_meta_data, raw_user_meta_data, is_super_admin, created_at, updated_at, phone, phone_confirmed_at, phone_change, phone_change_token, phone_change_sent_at, email_change_token_current, email_change_confirm_status, banned_until) FROM stdin;
\.


--
-- Data for Name: typeorm_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.typeorm_metadata (type, database, schema, "table", name, value) FROM stdin;
\.


--
-- Data for Name: vne_actionfiles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_actionfiles (id, action_id, url, description, name) FROM stdin;
201	147	web-app/grifon/out/docs/1644422493846.jpg	\N	winter1.jpg
202	147	web-app/grifon/out/docs/1644422495199.jpg	\N	winter2.jpg
203	147	web-app/grifon/out/docs/1644422496123.jpg	\N	winter3.jpg
204	148	web-app/grifon/in/docs/1644422246996.jpeg		1644422246996.jpeg
205	148	web-app/grifon/in/docs/1644422711524.jpg	\N	winter1.jpg
206	148	web-app/grifon/in/docs/1644422712817.jpg	\N	winter2.jpg
207	148	web-app/grifon/in/docs/1644422713966.jpg	\N	winter3.jpg
208	149	web-app/grifon/in/docs/1644422239941.sql		velo.sql
209	149	web-app/grifon/in/docs/1644422827189.jpg	\N	spring4.jpg
210	149	web-app/grifon/in/docs/1644422828243.jpg	\N	summer.jpg
211	149	web-app/grifon/in/docs/1644422829515.jpg	\N	texture1.jpg
212	150	web-app/grifon/in/docs/1644423240580.json		Gupshup.json
213	150	web-app/grifon/in/docs/1644423271141.jpg	\N	texture1.jpg
214	150	web-app/grifon/in/docs/1644423271846.jpg	\N	texture2.jpg
215	151	web-app/grifon/in/audio/1644425221741.mp3	файл 1	1644425221741.mp3
216	151	web-app/grifon/in/docs/1644425343110.jpg	\N	winter1.jpg
217	151	web-app/grifon/in/docs/1644425344975.jpg	\N	winter2.jpg
218	151	web-app/grifon/in/docs/1644425346384.jpg	\N	winter3.jpg
219	152	web-app/grifon/out/docs/1644425381783.jpg	ййй	winter1.jpg
220	152	web-app/grifon/out/docs/1644425383188.jpg	ццц	winter2.jpg
221	153	web-app/grifon/in/docs/1644425246303.sql		ut.sql
222	153	web-app/grifon/in/docs/1644425416116.jpg	\N	winter1.jpg
223	153	web-app/grifon/in/docs/1644425417789.jpg	\N	winter2.jpg
224	154	web-app/grifon/out/docs/1644425451262.jpg	ффф	texture1.jpg
225	154	web-app/grifon/out/docs/1644425452116.jpg	ыыы	texture2.jpg
226	155	web-app/grifon/in/docs/1644425237744.jpeg		1644425237744.jpeg
227	155	web-app/grifon/in/docs/1644425562596.jpg	ккк	winter1.jpg
228	156	web-app/grifon/in/docs/1644425613732.docx		Правила заповнення форми декларації особи.docx
229	156	web-app/grifon/in/docs/1644425690231.jpg	\N	winter1.jpg
230	156	web-app/grifon/in/docs/1644425691568.jpg	\N	winter2.jpg
231	157	web-app/grifon/out/docs/1644425729029.jpg	\N	summer.jpg
232	157	web-app/grifon/out/docs/1644425730413.jpg	\N	texture1.jpg
233	157	web-app/grifon/out/docs/1644425730991.jpg	\N	texture2.jpg
234	158	web-app/grifon/out/docs/1644427208762.jpg	asdf22	texture1.jpg
235	158	web-app/grifon/out/docs/1644427209443.jpg	zxcv33	texture2.jpg
236	160	web-app/grifon/in/docs/1644425613732.docx	111	Правила заповнення форми декларації особи.docx
237	160	web-app/grifon/in/docs/1644481164205.jpg	222	texture1.jpg
238	160	web-app/grifon/in/docs/1644481164870.jpg	333	texture2.jpg
239	161	web-app/grifon/out/docs/1644481223487.jpg	qqq	winter1.jpg
240	161	web-app/grifon/out/docs/1644481224709.jpg	www	winter2.jpg
241	162	web-app/grifon/out/docs/1644482065692.jpg	какой-то файл	texture1.jpg
242	163	web-app/grifon/in/docs/1644425613732.docx	zdxfg	Правила заповнення форми декларації особи.docx
\.


--
-- Data for Name: vne_actionreminders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_actionreminders (id, action_id, date, description) FROM stdin;
1	162	2022-02-11	позвонить клиенту
2	162	2022-03-16	написать клиенту
3	162	2022-03-24	сообщить начальству
4	162	2022-02-10	еще что-нибудь
5	163	2022-02-10	sdfsa
6	164	2022-02-10	qqq
7	164	2022-02-11	www
\.


--
-- Data for Name: vne_actions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_actions (id, company_id, employee_id, employeeaction_id, chatmessage_id, deal_id, comment, created_at, actionstatus_id) FROM stdin;
147	1	1	4	183	264	камент	2022-02-09 16:01:37.302695	\N
148	1	1	3	185	264	еуые	2022-02-09 16:05:15.202896	\N
149	1	1	3	184	263	тест	2022-02-09 16:07:10.433483	\N
150	1	1	3	189	260		2022-02-09 16:14:32.902018	\N
151	1	1	3	192	256	камент	2022-02-09 16:49:07.83225	\N
152	1	1	4	193	253	камент 2	2022-02-09 16:49:44.446633	\N
153	1	1	3	194	255		2022-02-09 16:50:19.333583	\N
154	1	1	4	194	264	камент 3	2022-02-09 16:50:53.14549	\N
155	1	1	3	193	264		2022-02-09 16:52:44.189878	\N
156	1	1	3	203	252	камент	2022-02-09 16:54:52.920326	\N
157	1	1	4	203	265	камент 4	2022-02-09 16:55:31.634568	\N
158	1	1	4	206	265	comment11	2022-02-09 17:20:10.394745	\N
160	1	1	3	203	259		2022-02-10 08:19:25.75506	\N
161	1	1	4	206	264	test	2022-02-10 08:20:26.006334	\N
162	1	1	4	206	256	тест коммент.	2022-02-10 08:34:26.692688	\N
163	1	1	3	203	264		2022-02-10 10:53:35.895052	\N
164	1	1	3	206	253		2022-02-10 14:58:59.703849	1
159	1	1	2	203	265	test	2022-02-10 06:08:28.567664	2
165	1	1	3	206	264		2022-02-11 09:22:12.393246	\N
\.


--
-- Data for Name: vne_actions_actiontags; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_actions_actiontags (action_id, actiontag_id) FROM stdin;
165	2
165	1
\.


--
-- Data for Name: vne_actionstatuses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_actionstatuses (id, name) FROM stdin;
1	выполнено
2	принято
\.


--
-- Data for Name: vne_actiontags; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_actiontags (id, name) FROM stdin;
1	срочно
2	ошибка
\.


--
-- Data for Name: vne_admingroups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_admingroups (id, code, name, defended) FROM stdin;
\.


--
-- Data for Name: vne_admins; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_admins (id, group_id, name, email, password, img, active, defended) FROM stdin;
\.


--
-- Data for Name: vne_chatmessages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_chatmessages (id, outer_id, contractor_id, channel, type, text, is_task, is_answer, is_read, created_at, fileurl, filename) FROM stdin;
182	\N	115	whatsapp	text	текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение	t	f	f	2022-02-09 15:57:07.542855	\N	\N
183	\N	115	whatsapp	audio	\N	t	f	f	2022-02-09 15:57:16.265626	web-app/grifon/in/audio/1644422233008.mp3	1644422233008.mp3
184	\N	115	whatsapp	file	\N	t	f	f	2022-02-09 15:57:27.537527	web-app/grifon/in/docs/1644422239941.sql	velo.sql
185	\N	115	whatsapp	image	\N	t	f	f	2022-02-09 15:57:30.572462	web-app/grifon/in/docs/1644422246996.jpeg	1644422246996.jpeg
186	22559b1b-3a31-4b20-ae1c-1461b87714d7	115	whatsapp	text	<em>[Запрос документов]</em><br>камент	t	t	t	2022-02-09 16:01:37.591465	\N	\N
187	816aa2b1-ae64-4758-9172-40a71b0fae71	115	whatsapp	text	<em>[Получение документов]</em><br>еуые	t	t	t	2022-02-09 16:05:15.50644	\N	\N
188	c0210afc-4d89-42b6-9d1f-b46a1df679c5	115	whatsapp	text	<em>[Получение документов]</em><br>тест	t	t	t	2022-02-09 16:07:10.711342	\N	\N
189	\N	115	whatsapp	file	\N	t	f	f	2022-02-09 16:14:06.269126	web-app/grifon/in/docs/1644423240580.json	Gupshup.json
191	\N	115	whatsapp	text	текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение	t	f	f	2022-02-09 16:46:59.612066	\N	\N
190	cb63b8c7-0fe4-4017-a7c6-bf062f1041b1	115	whatsapp	text	<em>[Получение документов]</em>	t	t	t	2022-02-09 16:14:33.145639	\N	\N
192	\N	115	whatsapp	audio	\N	t	f	f	2022-02-09 16:47:04.685854	web-app/grifon/in/audio/1644425221741.mp3	1644425221741.mp3
193	\N	115	whatsapp	image	шьп ьып	t	f	f	2022-02-09 16:47:22.550876	web-app/grifon/in/docs/1644425237744.jpeg	1644425237744.jpeg
194	\N	115	whatsapp	file	\N	t	f	f	2022-02-09 16:47:30.231309	web-app/grifon/in/docs/1644425246303.sql	ut.sql
198	931df0d2-ffc0-4229-8bbf-d4169f9aec71	115	whatsapp	text	<em>[Получение документов]</em><br>камент	t	t	t	2022-02-09 16:49:08.416195	\N	\N
199	cb6fb214-8c81-4235-b6cc-2c7d8ecd5ee4	115	whatsapp	text	<em>[Запрос документов]</em><br>камент 2	t	t	t	2022-02-09 16:49:44.889011	\N	\N
195	e46b79a4-211f-4456-83d8-3ccd28151143	115	whatsapp	text	<blockquote>Алексей Иванов, 18:47: Аудиосообщение</blockquote>еуые	t	t	t	2022-02-09 16:47:50.558379	\N	\N
197	1fe13475-e38d-40d5-9888-a312b7825c6d	115	whatsapp	text	<blockquote>Алексей Иванов, 18:47: Файл</blockquote>тест	t	t	t	2022-02-09 16:48:11.420278	\N	\N
196	2422b823-9dad-4f7a-8623-fa67ea94dc92	115	whatsapp	text	<blockquote>Алексей Иванов, 18:47: шьп ьып</blockquote>кууке	t	t	t	2022-02-09 16:48:03.2272	\N	\N
200	3dd37ccf-b892-43c8-b105-10f15704cfed	115	whatsapp	text	<em>[Получение документов]</em>	t	t	t	2022-02-09 16:50:19.759722	\N	\N
202	67a52c18-ab5e-4380-8eba-a515e7cfd49d	115	whatsapp	text	<em>[Получение документов]</em>	t	t	t	2022-02-09 16:52:44.463747	\N	\N
201	675a97c6-fe02-4277-bc93-c6f6d7d169a4	115	whatsapp	text	<em>[Запрос документов]</em><br>камент 3	t	t	t	2022-02-09 16:50:53.6282	\N	\N
203	\N	115	whatsapp	file	\N	t	f	f	2022-02-09 16:53:36.595556	web-app/grifon/in/docs/1644425613732.docx	Правила заповнення форми декларації особи.docx
204	466c0843-7982-4f39-af98-7b837970a4fb	115	whatsapp	text	<em>[Получение документов]</em><br>камент	t	t	t	2022-02-09 16:54:53.204876	\N	\N
205	5371f8c2-48c7-43c4-83e8-a8f796704ca5	115	whatsapp	text	<em>[Запрос документов]</em><br>камент 4	t	t	t	2022-02-09 16:55:31.912428	\N	\N
206	\N	115	whatsapp	text	текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение текстовое сообщение	t	f	f	2022-02-09 17:19:32.610034	\N	\N
207	11bd8abf-269d-4899-beef-96bd2ec80ad0	115	whatsapp	text	<em>[Запрос документов]</em><br>comment	t	t	t	2022-02-09 17:20:10.718377	\N	\N
210	4f4bbbde-f8db-4334-9458-d126f19ea37c	115	whatsapp	text	<em>[Запрос документов]</em><br>test	t	t	t	2022-02-10 08:20:26.286104	\N	\N
208	9a0ce062-d24f-4934-aaab-44a2d64c9f81	115	whatsapp	text	<em>[Расчет страховки]</em><br>test	t	t	t	2022-02-10 06:08:28.807839	\N	\N
209	dcf85b26-69d9-423d-b920-2d4d354e699a	115	whatsapp	text	<em>[Получение документов]</em>	t	t	t	2022-02-10 08:19:26.067862	\N	\N
211	5875874e-5845-4548-b6b5-166c8b72c7e7	115	whatsapp	text	<em>[Запрос документов]</em><br>тест коммент.	t	t	t	2022-02-10 08:34:27.026152	\N	\N
212	\N	122	whatsapp	text	Добрый день<br>Интересует страховка по ипотеке ВТБ, 3 года платили страховку именно втб<br>Хотел узнать  можно ли делать страховку другой страховой компании если да то через вас	t	f	f	2022-02-10 10:43:11.916812	\N	\N
215	16c749fb-9bc9-41d2-a179-833670c0b473	115	whatsapp	text	<em>[Получение документов]</em>	t	t	f	2022-02-11 09:22:12.758456	\N	\N
214	a6b5b4f4-f5da-4092-8068-672a54d46966	115	whatsapp	text	<em>[Получение документов]</em>	t	t	t	2022-02-10 14:59:00.053371	\N	\N
213	3b78728e-b08c-472b-a1f0-60e4115b276d	115	whatsapp	text	<em>[Получение документов]</em>	t	t	t	2022-02-10 10:53:36.261606	\N	\N
216	\N	115	whatsapp	audio	\N	t	f	f	2022-02-11 11:34:59.001743	web-app/grifon/in/audio/1644579296661.mp3	1644579296661.mp3
217	\N	115	whatsapp	audio	\N	t	f	f	2022-02-11 11:49:29.759379	web-app/grifon/in/audio/1644580166763.mp3	1644580166763.mp3
218	\N	115	whatsapp	text	test test test test test test test test test test test test test vtest	t	f	f	2022-02-11 15:59:29.932616	\N	\N
219	\N	115	whatsapp	text	test test test test test test test test test test test test test vtest	t	f	f	2022-02-11 15:59:30.550058	\N	\N
220	\N	115	whatsapp	file	\N	t	f	f	2022-02-11 16:00:03.194506	web-app/grifon/in/docs/1644595198641.vcf	Алексей Здоров.vcf
221	\N	115	whatsapp	file	\N	t	f	f	2022-02-11 16:00:05.496534	web-app/grifon/in/docs/1644595203527.vcf	Алексей Здоров.vcf
222	\N	115	whatsapp	image	test	t	f	f	2022-02-11 16:00:20.338749	web-app/grifon/in/docs/1644595215527.jpeg	1644595215527.jpeg
223	\N	115	whatsapp	image	test	t	f	f	2022-02-11 16:00:24.1294	web-app/grifon/in/docs/1644595220681.jpeg	1644595220681.jpeg
224	\N	115	whatsapp	audio	\N	t	f	f	2022-02-11 16:00:40.049859	web-app/grifon/in/audio/1644595237758.mp3	1644595237758.mp3
225	\N	115	whatsapp	audio	\N	t	f	f	2022-02-11 16:00:41.976888	web-app/grifon/in/audio/1644595240384.mp3	1644595240384.mp3
226	\N	115	whatsapp	audio	\N	t	f	f	2022-02-11 16:06:07.826477	web-app/grifon/in/audio/1644595566089.mp3	1644595566089.mp3
\.


--
-- Data for Name: vne_companies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_companies (id, name, email, n8n_code) FROM stdin;
1	Microsoft	viovalya@gmail.com	grifon
\.


--
-- Data for Name: vne_contractorgroups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_contractorgroups (id, name) FROM stdin;
1	Старая база
2	Должники
3	Конкуренты
\.


--
-- Data for Name: vne_contractors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_contractors (id, company_id, name, phone, img, chat_active, chat_at, banned, created_at) FROM stdin;
27	1	Андрей Мышкин	3805700000023	2022-2/1644578761140_200.jpg	f	1970-01-01 00:00:00	f	2022-02-03 00:18:40.986324
19	1	Алексей Кротов	38057000000151	2022-2/1643908349949_200.jpg	f	1970-01-01 00:00:00	f	2022-02-03 00:18:39.837727
44	1	Макар Зайцев	3805700000040	\N	f	1970-01-01 00:00:00	f	2022-02-03 00:18:43.525052
48	1	Армен Федоров	3805700000044	\N	f	1970-01-01 00:00:00	f	2022-02-03 00:18:44.112453
5	1	Леонид Иванов	380570000001	\N	f	1970-01-01 00:00:00	f	2022-02-03 00:18:37.781767
4	1	Руслан Сидоров	380570000000	\N	f	1970-01-01 00:00:00	f	2022-02-03 00:18:37.619404
13	1	Алексей Жуков	380570000009	2022-2/1643874619308_200.jpg	f	1970-01-01 00:00:00	f	2022-02-03 00:18:38.975703
32	1	Алексей Медведев	3805700000028	2022-2/1644240001533_200.jpg	f	1970-01-01 00:00:00	f	2022-02-03 00:18:41.711262
2	1	Андрей Собачкин	380661234567	2022-2/1643757983752_200.jpg	t	2022-02-02 23:38:09.772	f	2022-02-01 23:12:53.951067
29	1	Антон Кошкин	3805700000025	2022-2/1643875367023_200.jpg	f	1970-01-01 00:00:00	f	2022-02-03 00:18:41.269894
3	1	Виктор Мышкин	380950001122	\N	f	1970-01-01 00:00:00	t	2022-02-01 23:13:31.911069
6	1	Дмитрий Коровкин	380570000002	\N	f	\N	f	2022-02-03 00:18:37.931663
7	1	Денис Сидоров	380570000003	\N	f	\N	f	2022-02-03 00:18:38.076873
8	1	Константин Волков	380570000004	\N	f	\N	f	2022-02-03 00:18:38.243374
9	1	Виталий Зайцев	380570000005	\N	f	\N	f	2022-02-03 00:18:38.387259
10	1	Николай Петров	380570000006	\N	f	\N	f	2022-02-03 00:18:38.540831
11	1	Константин Алексеев	380570000007	\N	f	\N	f	2022-02-03 00:18:38.685383
12	1	Николай Сидоров	380570000008	\N	f	\N	f	2022-02-03 00:18:38.836242
14	1	Денис Мышкин	3805700000010	\N	f	\N	f	2022-02-03 00:18:39.120828
15	1	Федор Алексеев	3805700000011	\N	f	\N	f	2022-02-03 00:18:39.267401
16	1	Федор Алексеев	3805700000012	\N	f	\N	f	2022-02-03 00:18:39.41871
17	1	Петр Федоров	3805700000013	\N	f	\N	f	2022-02-03 00:18:39.560899
18	1	Евгений Алексеев	3805700000014	\N	f	\N	f	2022-02-03 00:18:39.700265
20	1	Николай Мышкин	3805700000016	\N	f	\N	f	2022-02-03 00:18:39.980656
21	1	Петр Сидоров	3805700000017	\N	f	\N	f	2022-02-03 00:18:40.12546
22	1	Дмитрий Хрюшкин	3805700000018	\N	f	\N	f	2022-02-03 00:18:40.267733
24	1	Иван Медведев	3805700000020	\N	f	\N	f	2022-02-03 00:18:40.545566
25	1	Сергей Иванов	3805700000021	\N	f	\N	f	2022-02-03 00:18:40.690973
26	1	Петр Сидоров	3805700000022	\N	f	\N	f	2022-02-03 00:18:40.837532
28	1	Константин Волков	3805700000024	\N	f	\N	f	2022-02-03 00:18:41.127028
30	1	Петр Федоров	3805700000026	\N	f	\N	f	2022-02-03 00:18:41.418897
31	1	Алексей Хрюшкин	3805700000027	\N	f	\N	f	2022-02-03 00:18:41.562707
33	1	Иван Коровкин	3805700000029	\N	f	\N	f	2022-02-03 00:18:41.860339
34	1	Денис Алексеев	3805700000030	\N	f	\N	f	2022-02-03 00:18:42.018539
35	1	Сергей Сидоров	3805700000031	\N	f	\N	f	2022-02-03 00:18:42.180307
36	1	Петр Алексеев	3805700000032	\N	f	\N	f	2022-02-03 00:18:42.329294
38	1	Федор Петров	3805700000034	\N	f	\N	f	2022-02-03 00:18:42.64795
40	1	Виктор Коровкин	3805700000036	\N	f	\N	f	2022-02-03 00:18:42.944481
41	1	Виктор Зайцев	3805700000037	\N	f	\N	f	2022-02-03 00:18:43.088394
42	1	Евгений Волков	3805700000038	\N	f	\N	f	2022-02-03 00:18:43.247222
43	1	Виктор Хрюшкин	3805700000039	\N	f	\N	f	2022-02-03 00:18:43.387313
45	1	Андрей Петров	3805700000041	\N	f	\N	f	2022-02-03 00:18:43.674822
46	1	Дмитрий Медведев	3805700000042	\N	f	\N	f	2022-02-03 00:18:43.815248
47	1	Иван Мышкин	3805700000043	\N	f	\N	f	2022-02-03 00:18:43.967927
49	1	Андрей Зайцев	3805700000045	\N	f	\N	f	2022-02-03 00:18:44.289052
50	1	Денис Медведев	3805700000046	\N	f	\N	f	2022-02-03 00:18:44.441415
51	1	Виктор Федоров	3805700000047	\N	f	\N	f	2022-02-03 00:18:44.588633
52	1	Николай Зайцев	3805700000048	\N	f	\N	f	2022-02-03 00:18:44.735035
53	1	Евгений Хрюшкин	3805700000049	\N	f	\N	f	2022-02-03 00:18:44.889577
54	1	Денис Иванов	3805700000050	\N	f	\N	f	2022-02-03 00:18:45.037719
55	1	Федор Федоров	3805700000051	\N	f	\N	f	2022-02-03 00:18:45.184483
56	1	Константин Алексеев	3805700000052	\N	f	\N	f	2022-02-03 00:18:45.3214
57	1	Денис Сидоров	3805700000053	\N	f	\N	f	2022-02-03 00:18:45.504464
58	1	Иван Зайцев	3805700000054	\N	f	\N	f	2022-02-03 00:18:45.649102
59	1	Евгений Петров	3805700000055	\N	f	\N	f	2022-02-03 00:18:45.798696
60	1	Евгений Федоров	3805700000056	\N	f	\N	f	2022-02-03 00:18:45.935818
61	1	Иван Мышкин	3805700000057	\N	f	\N	f	2022-02-03 00:18:46.073428
62	1	Константин Федоров	3805700000058	\N	f	\N	f	2022-02-03 00:18:46.217003
63	1	Виктор Николаев	3805700000059	\N	f	\N	f	2022-02-03 00:18:46.357273
23	1	Алексей Николаев	3805700000019	2022-2/1644304999060_200.jpg	f	1970-01-01 00:00:00	f	2022-02-03 00:18:40.406018
37	1	Федор Зайцев	3805700000033	\N	f	1970-01-01 00:00:00	f	2022-02-03 00:18:42.479782
64	1	Дмитрий Коровкин	3805700000060	\N	f	\N	f	2022-02-03 00:18:46.498896
65	1	Константин Иванов	3805700000061	\N	f	\N	f	2022-02-03 00:18:46.640283
66	1	Виталий Петров	3805700000062	\N	f	\N	f	2022-02-03 00:18:46.789725
67	1	Денис Федоров	3805700000063	\N	f	\N	f	2022-02-03 00:18:46.937432
68	1	Константин Хрюшкин	3805700000064	\N	f	\N	f	2022-02-03 00:18:47.082852
69	1	Евгений Зайцев	3805700000065	\N	f	\N	f	2022-02-03 00:18:47.221855
70	1	Иван Алексеев	3805700000066	\N	f	\N	f	2022-02-03 00:18:47.363767
71	1	Петр Федоров	3805700000067	\N	f	\N	f	2022-02-03 00:18:47.507994
72	1	Иван Алексеев	3805700000068	\N	f	\N	f	2022-02-03 00:18:47.650413
73	1	Константин Федоров	3805700000069	\N	f	\N	f	2022-02-03 00:18:47.809066
74	1	Андрей Сидоров	3805700000070	\N	f	\N	f	2022-02-03 00:18:47.971936
75	1	Виталий Алексеев	3805700000071	\N	f	\N	f	2022-02-03 00:18:48.124535
76	1	Виталий Иванов	3805700000072	\N	f	\N	f	2022-02-03 00:18:48.330175
77	1	Петр Сидоров	3805700000073	\N	f	\N	f	2022-02-03 00:18:48.519791
78	1	Николай Сидоров	3805700000074	\N	f	\N	f	2022-02-03 00:18:48.666371
79	1	Николай Иванов	3805700000075	\N	f	\N	f	2022-02-03 00:18:48.810539
80	1	Виталий Кошкин	3805700000076	\N	f	\N	f	2022-02-03 00:18:48.951173
81	1	Денис Коровкин	3805700000077	\N	f	\N	f	2022-02-03 00:18:49.100886
82	1	Николай Хрюшкин	3805700000078	\N	f	\N	f	2022-02-03 00:18:49.242385
83	1	Иван Федоров	3805700000079	\N	f	\N	f	2022-02-03 00:18:49.395253
84	1	Виктор Коровкин	3805700000080	\N	f	\N	f	2022-02-03 00:18:49.544285
85	1	Виталий Кошкин	3805700000081	\N	f	\N	f	2022-02-03 00:18:49.690025
86	1	Виктор Мышкин	3805700000082	\N	f	\N	f	2022-02-03 00:18:49.827759
87	1	Виталий Медведев	3805700000083	\N	f	\N	f	2022-02-03 00:18:49.967476
88	1	Петр Федоров	3805700000084	\N	f	\N	f	2022-02-03 00:18:50.121829
89	1	Дмитрий Федоров	3805700000085	\N	f	\N	f	2022-02-03 00:18:50.274985
90	1	Николай Федоров	3805700000086	\N	f	\N	f	2022-02-03 00:18:50.425789
91	1	Константин Иванов	3805700000087	\N	f	\N	f	2022-02-03 00:18:50.570965
93	1	Дмитрий Алексеев	3805700000089	\N	f	\N	f	2022-02-03 00:18:50.850755
94	1	Денис Зайцев	3805700000090	\N	f	\N	f	2022-02-03 00:18:50.995008
95	1	Константин Сидоров	3805700000091	\N	f	\N	f	2022-02-03 00:18:51.141117
96	1	Андрей Волков	3805700000092	\N	f	\N	f	2022-02-03 00:18:51.281557
97	1	Виталий Сидоров	3805700000093	\N	f	\N	f	2022-02-03 00:18:51.430447
98	1	Сергей Федоров	3805700000094	\N	f	\N	f	2022-02-03 00:18:51.568613
99	1	Андрей Алексеев	3805700000095	\N	f	\N	f	2022-02-03 00:18:51.71084
100	1	Константин Иванов	3805700000096	\N	f	\N	f	2022-02-03 00:18:51.864305
101	1	Андрей Кошкин	3805700000097	\N	f	\N	f	2022-02-03 00:18:52.0154
102	1	Константин Иванов	3805700000098	\N	f	\N	f	2022-02-03 00:18:52.169929
103	1	Евгений Сидоров	3805700000099	\N	f	\N	f	2022-02-03 00:18:52.313014
92	1	Алексей Мышкин	3805700000088	\N	f	1970-01-01 00:00:00	f	2022-02-03 00:18:50.71397
110	1	gsresrfsdg	79690620292	\N	t	2022-02-04 10:18:00.175	f	2022-02-04 10:18:00.249419
122	1	Григорий	79110923135	\N	t	2022-02-10 10:43:11.629	f	2022-02-10 10:43:11.765395
117	1	Василий Петров	12345312	\N	f	\N	f	2022-02-04 10:48:18.08789
116	1	Михаил от Ивана	79227050024	\N	t	2022-02-04 10:55:55.072	f	2022-02-04 10:45:35.020771
119	1	Иван 	9111111111	\N	f	\N	f	2022-02-04 12:29:06.289124
118	1	Екатерина Крестовская	79119700011	\N	t	2022-02-06 12:42:44.905	f	2022-02-04 12:25:58.377419
120	1	Мария Петрова	03	\N	f	\N	f	2022-02-08 05:04:44.818582
121	1	Мария Иванова	04	\N	f	\N	f	2022-02-08 05:05:51.41414
115	1	Алексей Иванов	380664021350	2022-2/1644383295193_200.jpg	t	2022-02-11 16:06:06.061	f	2022-02-04 10:43:33.577357
\.


--
-- Data for Name: vne_contractors_contractorgroups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_contractors_contractorgroups (contractor_id, contractorgroup_id) FROM stdin;
2	1
2	2
13	1
13	2
\.


--
-- Data for Name: vne_dealdates; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_dealdates (id, deal_id, date, description) FROM stdin;
1	251	2022-02-07	test date
3	\N	2022-03-17	test2
2	\N	2022-02-08	test1
4	252	2022-02-10	отправить доки
5	252	2022-03-30	принять деньги
6	253	2022-02-17	note 1
7	253	2022-04-14	note 2
8	255	2022-02-24	сдать менеджеру
11	260	2022-04-14	note 3
9	260	2022-02-17	note 1
10	260	2022-03-08	note 2
12	256	2022-02-11	
13	265	2022-02-11	просто дата
14	265	2022-02-10	что-то надо сделать
\.


--
-- Data for Name: vne_deals; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_deals (id, company_id, contact_contractor_id, client_contractor_id, partner_contractor_id, name, created_at, description, closed) FROM stdin;
257	1	115	\N	\N		2022-02-09 05:07:59.461582		f
258	1	115	\N	\N		2022-02-09 06:52:33.695799		f
260	1	115	23	13	Tst deal1	2022-02-09 07:29:31.488461	description description description description description description description description description description description description description 2	f
262	1	19	\N	\N		2022-02-09 08:37:20.355663		f
252	1	115	\N	32	Сделка века	2022-02-08 07:48:06.850994	описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века описание сделки века 	f
264	1	115	\N	\N	Страхование автомобиля	2022-02-09 09:53:21.418244	описание описание описание описание описание описание описание описание описание описание описание описание описание описание описание описание описание описание описание 	f
251	1	115	13	121	Новая пробная сделка	2022-02-07 14:11:36.104289	краткое описание новой пробной сделки	f
265	1	115	\N	\N	Пробная	2022-02-09 16:55:31.634568	текст текст текст текст текст текст текст текст текст текст текст 	t
263	1	115	96	\N	Tets	2022-02-09 08:37:42.35019		t
255	1	115	\N	\N	Еще одна сделка	2022-02-08 12:50:01.712729	описание сделки	f
254	1	115	\N	\N		2022-02-08 10:54:06.806678		t
259	1	115	84	\N		2022-02-09 06:54:40.209489		f
256	1	115	\N	\N	Хорошая сделка	2022-02-09 05:06:12.977748	краткое описание краткое описание краткое описание краткое описание краткое описание краткое описание краткое описание краткое описание краткое описание 	f
253	1	115	\N	\N	Super deal	2022-02-08 08:33:29.966727	short desc short desc short desc short desc short desc short desc short desc short desc short desc short desc 	f
\.


--
-- Data for Name: vne_employeeactions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_employeeactions (id, group_id, code, name, direction, response) FROM stdin;
3	1	insur_get_docs	Получение документов	in	Документы приняты.
2	1	insur_calc	Расчет страховки	out	Расчет выполнен.
4	1	insur_request_docs	Запрос документов	out	Предоставьте документы по образцам.
1	2	hypo_get_request	Прием заявки на кредитование	in	Заявка принята и будет рассмотрена в ближайшее время.
\.


--
-- Data for Name: vne_employeegroups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_employeegroups (id, code, name) FROM stdin;
1	insur	Страховой брокер
2	hypo	Ипотечный брокер
\.


--
-- Data for Name: vne_employees; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_employees (id, company_id, group_id, email, password, name, active, img) FROM stdin;
1	1	1	7573497@gmail.com	$2b$10$A2SyAMvpR.48kNph15wobOzohtY9piSn/z0ssK4XTA5XmwN.v.5bO	Alex Koshkin	t	test.jpg
\.


--
-- Data for Name: vne_mailtemplates; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_mailtemplates (id, name, subject, content, defended) FROM stdin;
1	employee-email-verification	Проверочный код системы "Название"	<html>\n<body>\nВаш код: {{code}}\n</body>\n</html>	t
\.


--
-- Data for Name: vne_settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_settings (id, p, v, c, pos, in_app, defended, hidden) FROM stdin;
1	active	1	\N	1	t	t	t
2	smtp-host	smtp.gmail.com	\N	2	f	f	f
3	smtp-port	587	\N	3	f	f	f
4	smtp-login	viodev.robot@gmail.com	\N	4	f	f	f
6	smtp-from	viodev.robot@gmail.com	\N	6	f	f	f
8	supabase-api-url	https://xwpsfuhmrmqkxtirhnue.supabase.co	\N	8	f	f	f
7	supabase-storage-url	https://xwpsfuhmrmqkxtirhnue.supabase.in/storage/v1/object/public	\N	7	t	f	f
9	supabase-api-key	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTY0MTY0NTU0OSwiZXhwIjoxOTU3MjIxNTQ5fQ.ZZ83E_Kyznxbv-W53-so9w_DuKp-SqMlDvfvDIJ8iZc	\N	9	f	f	f
10	n8n-msgtogupshup-url	https://wf.grifons.ru/webhook/20a0cf28-6f69-4563-a141-499b4022fb8b	\N	10	f	f	f
12	n8n-msgtogupshup-url-test	https://wf.grifons.ru/webhook-test/20a0cf28-6f69-4563-a141-499b4022fb8b	\N	11	f	f	f
14	n8n-actiontogupshup-url-test	https://wf.grifons.ru/webhook-test/d7c324e9-6dd8-4eed-9f1e-6a94f6bafac0	\N	13	f	f	f
13	n8n-actiontogupshup-url	https://wf.grifons.ru/webhook/d7c324e9-6dd8-4eed-9f1e-6a94f6bafac0	\N	12	f	f	f
5	smtp-pw	6vl1TfeXq	\N	5	f	f	f
\.


--
-- Data for Name: vne_verifications; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vne_verifications (id, email, code, created_at) FROM stdin;
33	7573497@gmail.com	595234	2022-02-05 08:06:09.158207
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: realtime; Owner: supabase_admin
--

COPY realtime.schema_migrations (version, inserted_at) FROM stdin;
20211116024918	2022-01-08 12:40:15
20211116045059	2022-01-08 12:40:15
20211116050929	2022-01-08 12:40:15
20211116051442	2022-01-08 12:40:15
20211116212300	2022-01-08 12:40:15
20211116213355	2022-01-08 12:40:15
20211116213934	2022-01-08 12:40:15
20211116214523	2022-01-08 12:40:15
20211122062447	2022-01-08 12:40:15
20211124070109	2022-01-08 12:40:15
20211202204204	2022-01-08 12:40:15
20211202204605	2022-01-08 12:40:15
20211210212804	2022-01-26 01:41:47
20220107221237	2022-01-26 01:41:47
\.


--
-- Data for Name: subscription; Type: TABLE DATA; Schema: realtime; Owner: supabase_admin
--

COPY realtime.subscription (id, subscription_id, entity, filters, claims, created_at) FROM stdin;
\.


--
-- Data for Name: buckets; Type: TABLE DATA; Schema: storage; Owner: supabase_storage_admin
--

COPY storage.buckets (id, name, owner, created_at, updated_at, public) FROM stdin;
docs	docs	\N	2022-01-18 14:57:26.739086+00	2022-01-18 14:57:26.739086+00	t
web-app	web-app	\N	2022-01-28 22:16:55.034797+00	2022-01-28 22:16:55.034797+00	t
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: storage; Owner: supabase_storage_admin
--

COPY storage.migrations (id, name, hash, executed_at) FROM stdin;
0	create-migrations-table	e18db593bcde2aca2a408c4d1100f6abba2195df	2022-01-08 12:40:17.634689
1	initialmigration	6ab16121fbaa08bbd11b712d05f358f9b555d777	2022-01-08 12:40:17.643272
2	pathtoken-column	49756be03be4c17bb85fe70d4a861f27de7e49ad	2022-01-08 12:40:17.651457
3	add-migrations-rls	bb5d124c53d68635a883e399426c6a5a25fc893d	2022-01-08 12:40:17.674046
4	add-size-functions	6d79007d04f5acd288c9c250c42d2d5fd286c54d	2022-01-08 12:40:17.67681
5	change-column-name-in-get-size	fd65688505d2ffa9fbdc58a944348dd8604d688c	2022-01-08 12:40:17.680038
6	add-rls-to-buckets	63e2bab75a2040fee8e3fb3f15a0d26f3380e9b6	2022-01-08 12:40:17.683138
7	add-public-to-buckets	82568934f8a4d9e0a85f126f6fb483ad8214c418	2022-01-08 12:40:17.685867
8	fix-search-function	1a43a40eddb525f2e2f26efd709e6c06e58e059c	2022-01-08 12:40:17.689039
\.


--
-- Data for Name: objects; Type: TABLE DATA; Schema: storage; Owner: supabase_storage_admin
--

COPY storage.objects (id, bucket_id, name, owner, created_at, updated_at, last_accessed_at, metadata) FROM stdin;
12124646-dd8a-4ca0-b743-1590e5f93b3b	web-app	grifon/in/docs/1643946381125.jpeg	\N	2022-02-04 03:46:22.621672+00	2022-02-04 03:46:22.621672+00	2022-02-04 03:46:22.621672+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
d26749b1-8a3e-4624-a7ab-e8c07934edaa	web-app	grifon/in/docs/1643946382900.jpeg	\N	2022-02-04 03:46:23.441741+00	2022-02-04 03:46:23.441741+00	2022-02-04 03:46:23.441741+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
2b1c85d8-ceca-4b8d-aba2-1921808caed9	web-app	grifon/in/docs/1643946383655.jpeg	\N	2022-02-04 03:46:24.187973+00	2022-02-04 03:46:24.187973+00	2022-02-04 03:46:24.187973+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
6c8ddd44-0f8b-434a-8392-620ca0c47aa9	web-app	grifon/in/audio/1643606905934.mp3	\N	2022-01-31 05:28:28.081249+00	2022-01-31 05:28:28.081249+00	2022-01-31 05:28:28.081249+00	{"size": 57259, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
da45aa73-2acb-4c1b-959e-fb37e08f9b5e	web-app	grifon/in/docs/1643608104517.xlsx	\N	2022-01-31 05:48:29.568854+00	2022-01-31 05:48:29.568854+00	2022-01-31 05:48:29.568854+00	{"size": 533113, "mimetype": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "cacheControl": "max-age=3600"}
acf30e3d-d6fa-4562-9dc2-0b53e15b7013	web-app	grifon/in/docs/1643609255696.jpeg	\N	2022-01-31 06:07:38.078636+00	2022-01-31 06:07:38.078636+00	2022-01-31 06:07:38.078636+00	{"size": 34546, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
47d62d29-b469-44a8-8e67-830ff209412f	web-app	grifon/in/docs/1643611724850.jpeg	\N	2022-01-31 06:48:48.311779+00	2022-01-31 06:48:48.311779+00	2022-01-31 06:48:48.311779+00	{"size": 134256, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
d0939fc1-b44e-4904-830d-726409097b7b	web-app	grifon/in/audio/1643615248066.mp3	\N	2022-01-31 07:47:30.158981+00	2022-01-31 07:47:30.158981+00	2022-01-31 07:47:30.158981+00	{"size": 66872, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
bb5944f0-1538-4e75-8181-490fb7615f08	web-app	grifon/in/audio/1643615703947.mp3	\N	2022-01-31 07:55:10.636621+00	2022-01-31 07:55:10.636621+00	2022-01-31 07:55:10.636621+00	{"size": 2070144, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
48c03beb-f752-44d9-8092-d375ccfcf7f9	web-app	grifon/in/audio/1643670043095.mp3	\N	2022-01-31 23:00:45.855131+00	2022-01-31 23:00:45.855131+00	2022-01-31 23:00:45.855131+00	{"size": 47232, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
7e0cd07a-9c03-4732-8388-d98c4a91f5fa	web-app	grifon/in/docs/1643947486305.txt	\N	2022-02-04 04:04:46.742129+00	2022-02-04 04:04:46.742129+00	2022-02-04 04:04:46.742129+00	{"size": 3323, "mimetype": "text/plain", "cacheControl": "max-age=3600"}
0a4c743f-e97c-4720-b27e-ba865f5049a9	web-app	grifon/out/.emptyFolderPlaceholder	\N	2022-01-28 23:09:33.201901+00	2022-01-28 23:09:33.201901+00	2022-01-28 23:09:33.201901+00	{"size": 0, "mimetype": "application/octet-stream", "cacheControl": "max-age=3600"}
f127aae3-aacd-4863-8099-c7f9ddb828a9	docs	insurance-important.pdf	\N	2022-01-18 14:58:55.232657+00	2022-01-18 14:58:55.232657+00	2022-01-18 14:58:55.232657+00	{"size": 59661, "mimetype": "application/pdf", "cacheControl": "max-age=3600"}
51001cf6-0924-415e-9f0c-1470bbf17c3e	web-app	grifon/in/docs/1643947486952.cfg	\N	2022-02-04 04:04:47.24762+00	2022-02-04 04:04:47.24762+00	2022-02-04 04:04:47.24762+00	{"size": 314, "mimetype": "text/plain", "cacheControl": "max-age=3600"}
c7733f23-65cd-4d4b-8f5a-5bab4430b9ef	docs	test-calc.pdf	\N	2022-01-18 14:59:49.173139+00	2022-01-18 14:59:49.173139+00	2022-01-18 14:59:49.173139+00	{"size": 50406, "mimetype": "application/pdf", "cacheControl": "max-age=3600"}
bcf2093b-c285-43c2-806a-40dd9aec3218	web-app	grifon/in/.emptyFolderPlaceholder	\N	2022-01-28 23:42:12.355637+00	2022-01-28 23:42:12.355637+00	2022-01-28 23:42:12.355637+00	{"size": 0, "mimetype": "application/octet-stream", "cacheControl": "max-age=3600"}
cbc3a493-7ca5-4269-bf86-31a00d4a0823	web-app	grifon/in/docs/1643947487413.vcf	\N	2022-02-04 04:04:47.882268+00	2022-02-04 04:04:47.882268+00	2022-02-04 04:04:47.882268+00	{"size": 42489, "mimetype": "text/directory", "cacheControl": "max-age=3600"}
6fea0e6e-0912-416e-8fd8-282925e45107	web-app	grifon/in/docs/1643972155244.jpeg	\N	2022-02-04 10:55:58.765983+00	2022-02-04 10:55:58.765983+00	2022-02-04 10:55:58.765983+00	{"size": 76094, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
def782c6-8619-40f2-97ef-c24058d7702f	web-app	grifon/in/docs/1643977949124.pdf	\N	2022-02-04 12:32:29.905233+00	2022-02-04 12:32:29.905233+00	2022-02-04 12:32:29.905233+00	{"size": 50789, "mimetype": "application/pdf", "cacheControl": "max-age=3600"}
0c3f483e-307b-4ed8-b651-7161edaf56ef	web-app	grifon/in/docs/1643977950012.pdf	\N	2022-02-04 12:32:30.540991+00	2022-02-04 12:32:30.540991+00	2022-02-04 12:32:30.540991+00	{"size": 50789, "mimetype": "application/pdf", "cacheControl": "max-age=3600"}
081fec69-1e4e-4b61-8a9e-ffe15c042a09	web-app	grifon/out/docs/1644055342738.jpg	\N	2022-02-05 10:02:24.147115+00	2022-02-05 10:02:24.147115+00	2022-02-05 10:02:24.147115+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
e11bec84-fe52-46b6-8793-ff88313091e8	web-app	grifon/out/docs/1644055344344.jpg	\N	2022-02-05 10:02:24.932025+00	2022-02-05 10:02:24.932025+00	2022-02-05 10:02:24.932025+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
2869ff9c-406b-40cc-837d-c5d21807a9eb	web-app	grifon/out/docs/1644055720599.jpg	\N	2022-02-05 10:08:42.145138+00	2022-02-05 10:08:42.145138+00	2022-02-05 10:08:42.145138+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
105b3c67-38be-4d72-bd00-307189f21e48	web-app	grifon/out/docs/1644055722350.jpg	\N	2022-02-05 10:08:42.906935+00	2022-02-05 10:08:42.906935+00	2022-02-05 10:08:42.906935+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
086d4956-0b54-4862-b780-2c1639a2e512	web-app	grifon/out/docs/1644055915833.jpg	\N	2022-02-05 10:11:57.071075+00	2022-02-05 10:11:57.071075+00	2022-02-05 10:11:57.071075+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
8ae4bacb-6da2-41a0-b32e-65db4ed34afe	web-app	grifon/out/docs/1644055917367.jpg	\N	2022-02-05 10:11:57.832498+00	2022-02-05 10:11:57.832498+00	2022-02-05 10:11:57.832498+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
4da55115-b0cd-4584-91da-4e8644df08c4	web-app	grifon/out/docs/1644056080523.jpg	\N	2022-02-05 10:14:41.934034+00	2022-02-05 10:14:41.934034+00	2022-02-05 10:14:41.934034+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
ec2ff7c4-70e6-41d9-94a2-bee4ed3e3cb0	web-app	grifon/in/docs/1643952444979.jpg	\N	2022-02-04 05:27:26.533298+00	2022-02-04 05:27:26.533298+00	2022-02-04 05:27:26.533298+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
467c005f-2733-4394-a9c1-c17386a18b6a	web-app	grifon/in/docs/1643952446750.jpg	\N	2022-02-04 05:27:27.191198+00	2022-02-04 05:27:27.191198+00	2022-02-04 05:27:27.191198+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
2413c096-c52c-46a8-98ba-45aef8442481	web-app	grifon/in/docs/1643952447342.vcf	\N	2022-02-04 05:27:27.767757+00	2022-02-04 05:27:27.767757+00	2022-02-04 05:27:27.767757+00	{"size": 42489, "mimetype": "text/directory", "cacheControl": "max-age=3600"}
dbef0cd4-dd65-4347-a744-9e808ddaa7da	web-app	grifon/in/docs/1643607997520.vcf	\N	2022-01-31 05:46:40.273132+00	2022-01-31 05:46:40.273132+00	2022-01-31 05:46:40.273132+00	{"size": 42489, "mimetype": "text/directory", "cacheControl": "max-age=3600"}
a19bc302-4a61-45fe-b569-7045711a21b0	web-app	grifon/in/docs/1643608949781.jpeg	\N	2022-01-31 06:02:33.762039+00	2022-01-31 06:02:33.762039+00	2022-01-31 06:02:33.762039+00	{"size": 265631, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
55672abd-a75d-46d6-ae0f-a7f00c2875f6	web-app	grifon/in/docs/1643610301485.jpeg	\N	2022-01-31 06:25:03.784218+00	2022-01-31 06:25:03.784218+00	2022-01-31 06:25:03.784218+00	{"size": 21593, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
48d22b9b-e29e-49e4-8c6f-2bbe621a5548	web-app	grifon/in/docs/1643614957580.jpeg	\N	2022-01-31 07:42:39.854672+00	2022-01-31 07:42:39.854672+00	2022-01-31 07:42:39.854672+00	{"size": 29745, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
e129f7ca-bcfd-4de9-8515-14a8b547ca1f	web-app	grifon/in/audio/1643615314803.mp3	\N	2022-01-31 07:48:37.806847+00	2022-01-31 07:48:37.806847+00	2022-01-31 07:48:37.806847+00	{"size": 551287, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
528327d8-45ad-4782-9c02-7daf59648f6b	web-app	grifon/in/docs/1643615788848.jpeg	\N	2022-01-31 07:56:31.220773+00	2022-01-31 07:56:31.220773+00	2022-01-31 07:56:31.220773+00	{"size": 29745, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
74fa43bb-d9c6-44be-848d-c9bd4c13b162	web-app	grifon/in/audio/1643757638022.mp3	\N	2022-02-01 23:20:41.182952+00	2022-02-01 23:20:41.182952+00	2022-02-01 23:20:41.182952+00	{"size": 78720, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
695dd990-9a41-415a-80e8-2b03504794ee	web-app	grifon/out/docs/1643946766351.false	\N	2022-02-04 03:52:46.975867+00	2022-02-04 03:52:46.975867+00	2022-02-04 03:52:46.975867+00	{"size": 42489, "mimetype": "text/directory", "cacheControl": "max-age=3600"}
5db20957-fd49-47ed-be26-1e2875376630	web-app	grifon/out/docs/1643946767072.txt	\N	2022-02-04 03:52:47.510093+00	2022-02-04 03:52:47.510093+00	2022-02-04 03:52:47.510093+00	{"size": 3323, "mimetype": "text/plain", "cacheControl": "max-age=3600"}
2ca23279-d3ae-44f0-a9dd-11070b9ac140	web-app	grifon/in/docs/1643952443761.jpg	\N	2022-02-04 05:27:24.713751+00	2022-02-04 05:27:24.713751+00	2022-02-04 05:27:24.713751+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
3bfedc13-ff49-434a-a69c-df8a3c054435	web-app	grifon/in/docs/1643972229766.png	\N	2022-02-04 10:57:10.575621+00	2022-02-04 10:57:10.575621+00	2022-02-04 10:57:10.575621+00	{"size": 229645, "mimetype": "image/png", "cacheControl": "max-age=3600"}
0b1c9105-8062-4eaa-8245-601da55983b8	web-app	grifon/out/docs/1644055259721.jpg	\N	2022-02-05 10:01:00.946212+00	2022-02-05 10:01:00.946212+00	2022-02-05 10:01:00.946212+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
f4bd7961-222a-444c-b9fd-cb0cc235ff73	web-app	grifon/out/docs/1644055261248.jpg	\N	2022-02-05 10:01:01.786084+00	2022-02-05 10:01:01.786084+00	2022-02-05 10:01:01.786084+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
f577beff-125d-4c1d-907d-e9c716c6ddbc	web-app	grifon/out/docs/1644055393872.jpg	\N	2022-02-05 10:03:15.081424+00	2022-02-05 10:03:15.081424+00	2022-02-05 10:03:15.081424+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
161081e0-079a-4460-a914-1bd3f27f4b68	web-app	grifon/out/docs/1644055395310.jpg	\N	2022-02-05 10:03:15.91471+00	2022-02-05 10:03:15.91471+00	2022-02-05 10:03:15.91471+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
b5be1c3e-f065-4da1-baf1-9e4f4476f360	web-app	grifon/out/docs/1644055856321.jpg	\N	2022-02-05 10:10:57.714712+00	2022-02-05 10:10:57.714712+00	2022-02-05 10:10:57.714712+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
2f5ea35a-6abf-4e83-b3d7-95619e3a0a95	web-app	grifon/out/docs/1644055857967.jpg	\N	2022-02-05 10:10:58.495556+00	2022-02-05 10:10:58.495556+00	2022-02-05 10:10:58.495556+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
2486356d-0e9b-4e6b-b597-b621abf41e1d	web-app	grifon/out/docs/1644055864069.jpg	\N	2022-02-05 10:11:05.147742+00	2022-02-05 10:11:05.147742+00	2022-02-05 10:11:05.147742+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
6f58f68a-2cf0-4d68-a579-fe239ccc1482	web-app	grifon/out/docs/1644055865429.jpg	\N	2022-02-05 10:11:05.978316+00	2022-02-05 10:11:05.978316+00	2022-02-05 10:11:05.978316+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
1a467e1f-dd29-41cb-871f-bf7895e4c333	web-app	grifon/out/docs/1644055963058.jpg	\N	2022-02-05 10:12:44.753332+00	2022-02-05 10:12:44.753332+00	2022-02-05 10:12:44.753332+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
9543f224-3c19-4df2-8286-cd7fc7bd0913	web-app	grifon/out/docs/1644055964999.jpg	\N	2022-02-05 10:12:45.515619+00	2022-02-05 10:12:45.515619+00	2022-02-05 10:12:45.515619+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
2b1ce7c1-b9af-44ef-b2d6-744b7a36a60d	web-app	grifon/out/docs/1644056082221.jpg	\N	2022-02-05 10:14:42.718707+00	2022-02-05 10:14:42.718707+00	2022-02-05 10:14:42.718707+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
1fd4ba60-0b25-464c-8b33-d5459db142f8	web-app	grifon/out/docs/1644056145366.jpg	\N	2022-02-05 10:15:47.017363+00	2022-02-05 10:15:47.017363+00	2022-02-05 10:15:47.017363+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
238f17a9-203f-436e-922f-2948f1fa655e	web-app	grifon/out/docs/1644056147318.jpg	\N	2022-02-05 10:15:47.844511+00	2022-02-05 10:15:47.844511+00	2022-02-05 10:15:47.844511+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
914e5035-f43b-4c4d-b896-910bb7f737cf	web-app	grifon/in/docs/1643947118677.false	\N	2022-02-04 03:58:39.09379+00	2022-02-04 03:58:39.09379+00	2022-02-04 03:58:39.09379+00	{"size": 42489, "mimetype": "text/directory", "cacheControl": "max-age=3600"}
d4180b8f-52a1-4ce2-a4e4-162b9108fdc8	web-app	grifon/out/docs/1643971702573.pdf	\N	2022-02-04 10:48:23.250207+00	2022-02-04 10:48:23.250207+00	2022-02-04 10:48:23.250207+00	{"size": 51064, "mimetype": "application/pdf", "cacheControl": "max-age=3600"}
72ab4e33-9fd9-4e46-9ae6-415faf71ff3c	web-app	grifon/in/audio/1643606555506.mp3	\N	2022-01-31 05:22:40.237918+00	2022-01-31 05:22:40.237918+00	2022-01-31 05:22:40.237918+00	{"size": 1121801, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
3110f2c0-e0bb-4c78-89dc-b95b0a2fe0bd	web-app	grifon/in/docs/1643608080049.pdf	\N	2022-01-31 05:48:03.196023+00	2022-01-31 05:48:03.196023+00	2022-01-31 05:48:03.196023+00	{"size": 51064, "mimetype": "application/pdf", "cacheControl": "max-age=3600"}
76264783-57cc-42ce-9f91-bd4cbe9b8fbc	web-app	grifon/out/docs/1643977760958.pdf	\N	2022-02-04 12:29:21.63951+00	2022-02-04 12:29:21.63951+00	2022-02-04 12:29:21.63951+00	{"size": 48314, "mimetype": "application/pdf", "cacheControl": "max-age=3600"}
2e9db8b1-79f8-443b-9f4f-bf9ad38e06c0	web-app	grifon/in/docs/1643609044214.jpeg	\N	2022-01-31 06:04:06.364616+00	2022-01-31 06:04:06.364616+00	2022-01-31 06:04:06.364616+00	{"size": 34546, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
ff9bd981-e97e-4d51-9e0b-6a46f3d85eb9	web-app	grifon/in/docs/1643610352458.jpeg	\N	2022-01-31 06:25:54.147509+00	2022-01-31 06:25:54.147509+00	2022-01-31 06:25:54.147509+00	{"size": 14877, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
1694eb6c-8dc3-4e25-8b79-68fcf02283c7	web-app	grifon/in/audio/1643615023870.mp3	\N	2022-01-31 07:43:45.776345+00	2022-01-31 07:43:45.776345+00	2022-01-31 07:43:45.776345+00	{"size": 43466, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
6f863d59-c6ef-43fd-b098-b1a667688b9a	web-app	grifon/in/audio/1643615525010.mp3	\N	2022-01-31 07:52:09.892895+00	2022-01-31 07:52:09.892895+00	2022-01-31 07:52:09.892895+00	{"size": 1100928, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
bad74586-dddd-4f4c-b92b-cab43eabf294	web-app	grifon/in/audio/1643615892820.mp3	\N	2022-01-31 07:58:14.925497+00	2022-01-31 07:58:14.925497+00	2022-01-31 07:58:14.925497+00	{"size": 49920, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
a6d8bf5b-4994-44a0-9b11-b536a6d58db7	web-app	grifon/in/docs/1643757697590.jpeg	\N	2022-02-01 23:21:39.538176+00	2022-02-01 23:21:39.538176+00	2022-02-01 23:21:39.538176+00	{"size": 29745, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
32b5f487-0b20-4b12-b88c-3194bcdf9e16	web-app	grifon/in/docs/1643947117574.txt	\N	2022-02-04 03:58:38.033904+00	2022-02-04 03:58:38.033904+00	2022-02-04 03:58:38.033904+00	{"size": 3323, "mimetype": "text/plain", "cacheControl": "max-age=3600"}
0a926e64-b113-4f89-ac9e-46c51ac39a66	web-app	grifon/in/docs/1643947118247.false	\N	2022-02-04 03:58:38.513771+00	2022-02-04 03:58:38.513771+00	2022-02-04 03:58:38.513771+00	{"size": 314, "mimetype": "text/plain;charset=UTF-8", "cacheControl": "max-age=3600"}
13dce112-ec88-4ae7-a3ca-b1b04f2b6d70	web-app	grifon/out/docs/1644055299855.jpg	\N	2022-02-05 10:01:41.231185+00	2022-02-05 10:01:41.231185+00	2022-02-05 10:01:41.231185+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
22b69ed4-279d-4999-9583-a75a4455bd50	web-app	grifon/out/docs/1644055301471.jpg	\N	2022-02-05 10:01:42.034444+00	2022-02-05 10:01:42.034444+00	2022-02-05 10:01:42.034444+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
83b02ec6-82eb-4543-b5fa-914995382348	web-app	grifon/out/docs/1644055432444.jpg	\N	2022-02-05 10:03:53.797422+00	2022-02-05 10:03:53.797422+00	2022-02-05 10:03:53.797422+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
d0710d20-aa66-4753-b805-7d2ae84a132a	web-app	grifon/out/docs/1644055434072.jpg	\N	2022-02-05 10:03:54.666413+00	2022-02-05 10:03:54.666413+00	2022-02-05 10:03:54.666413+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
4473377c-241f-4fac-8148-146a72ee4a48	web-app	grifon/out/docs/1644055877731.jpg	\N	2022-02-05 10:11:18.952418+00	2022-02-05 10:11:18.952418+00	2022-02-05 10:11:18.952418+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
f2c73cc0-ceb5-4138-b574-7e935630e07a	web-app	grifon/out/docs/1644055879253.jpg	\N	2022-02-05 10:11:19.752548+00	2022-02-05 10:11:19.752548+00	2022-02-05 10:11:19.752548+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
950550fd-5aef-481c-92ef-8aa12a16b7b7	web-app	grifon/out/docs/1644055981998.jpg	\N	2022-02-05 10:13:03.16713+00	2022-02-05 10:13:03.16713+00	2022-02-05 10:13:03.16713+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
c0938b3a-37b3-431a-b1cf-23110c51a976	web-app	grifon/out/docs/1644055983624.jpg	\N	2022-02-05 10:13:04.1106+00	2022-02-05 10:13:04.1106+00	2022-02-05 10:13:04.1106+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
1cdaa3eb-aa08-4bf2-83c3-8f5c3c2426b0	web-app	grifon/out/docs/1644056093322.jpg	\N	2022-02-05 10:14:54.382069+00	2022-02-05 10:14:54.382069+00	2022-02-05 10:14:54.382069+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
c6e0b246-c6f2-45ee-a302-586bbaa14950	web-app	grifon/out/docs/1644056094634.jpg	\N	2022-02-05 10:14:55.116063+00	2022-02-05 10:14:55.116063+00	2022-02-05 10:14:55.116063+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
99ebecfb-5d13-41ca-b1ad-0fc002bbf52a	web-app	grifon/out/docs/1644056264581.jpg	\N	2022-02-05 10:17:45.864702+00	2022-02-05 10:17:45.864702+00	2022-02-05 10:17:45.864702+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
ec9b4aa1-2b21-4653-8e3c-06163645bcee	web-app	grifon/out/docs/1644056266163.jpg	\N	2022-02-05 10:17:46.675881+00	2022-02-05 10:17:46.675881+00	2022-02-05 10:17:46.675881+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
edfc0dac-5909-46e7-a954-3e9b37fe13e9	web-app	grifon/out/docs/1644056347067.jpg	\N	2022-02-05 10:19:08.4581+00	2022-02-05 10:19:08.4581+00	2022-02-05 10:19:08.4581+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
5fbc4aab-4200-4f55-bf4d-b8e2b2c3f0a3	web-app	grifon/out/docs/1644056348741.jpg	\N	2022-02-05 10:19:09.330933+00	2022-02-05 10:19:09.330933+00	2022-02-05 10:19:09.330933+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
ca61a967-076e-4e8a-af5a-0611bf8b1331	web-app	grifon/out/docs/1644056440436.jpg	\N	2022-02-05 10:20:41.651458+00	2022-02-05 10:20:41.651458+00	2022-02-05 10:20:41.651458+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
91f3b6f2-00a6-43a8-bc54-d3091f4b6d6d	web-app	grifon/out/docs/1644056441896.jpg	\N	2022-02-05 10:20:42.39409+00	2022-02-05 10:20:42.39409+00	2022-02-05 10:20:42.39409+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
7b8429be-f94f-40a0-ac1e-7ad7436b8898	web-app	grifon/out/docs/1644056528196.jpg	\N	2022-02-05 10:22:08.981282+00	2022-02-05 10:22:08.981282+00	2022-02-05 10:22:08.981282+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
f021f433-3863-4908-b59b-21e853c487fe	web-app	grifon/out/docs/1644056529144.jpg	\N	2022-02-05 10:22:09.659607+00	2022-02-05 10:22:09.659607+00	2022-02-05 10:22:09.659607+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
dc2ba4f2-286e-4da7-84be-c13779a6d698	web-app	grifon/out/docs/1644056634753.jpg	\N	2022-02-05 10:23:55.756459+00	2022-02-05 10:23:55.756459+00	2022-02-05 10:23:55.756459+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
33b35b1a-83a0-4ae3-b1b5-827f0f50ff42	web-app	grifon/out/docs/1644056636053.jpg	\N	2022-02-05 10:23:57.306844+00	2022-02-05 10:23:57.306844+00	2022-02-05 10:23:57.306844+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
19f3101d-2363-4ad0-824b-7cb4bab01603	web-app	grifon/out/docs/1644056637489.jpg	\N	2022-02-05 10:23:57.982727+00	2022-02-05 10:23:57.982727+00	2022-02-05 10:23:57.982727+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
ee30911c-ade6-4c02-90ff-1b4bb4793590	web-app	grifon/out/docs/1644056898723.jpg	\N	2022-02-05 10:28:19.79575+00	2022-02-05 10:28:19.79575+00	2022-02-05 10:28:19.79575+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
a7ce0daa-9753-4656-a337-dd911340b6ce	web-app	grifon/out/docs/1644056900013.jpg	\N	2022-02-05 10:28:21.304284+00	2022-02-05 10:28:21.304284+00	2022-02-05 10:28:21.304284+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
5b2c2a03-f87b-4eef-9ff3-6e793dc04bad	web-app	grifon/out/docs/1644056901516.jpg	\N	2022-02-05 10:28:22.096798+00	2022-02-05 10:28:22.096798+00	2022-02-05 10:28:22.096798+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
41060752-5395-4024-9e70-d121d073a363	web-app	grifon/in/docs/1644057519147.jpg	\N	2022-02-05 10:38:40.250804+00	2022-02-05 10:38:40.250804+00	2022-02-05 10:38:40.250804+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
b85d3903-a407-48c1-a756-05c28c70b105	web-app	grifon/in/docs/1644057520414.jpg	\N	2022-02-05 10:38:41.69997+00	2022-02-05 10:38:41.69997+00	2022-02-05 10:38:41.69997+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
c7b5db96-bcf6-43b6-9ee0-2a2e0e38ff9c	web-app	grifon/in/docs/1644057521806.jpg	\N	2022-02-05 10:38:42.45746+00	2022-02-05 10:38:42.45746+00	2022-02-05 10:38:42.45746+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
16453a70-d75e-4f95-8f1f-270cfffc4b8d	web-app	grifon/out/docs/1644057589001.jpg	\N	2022-02-05 10:39:49.608239+00	2022-02-05 10:39:49.608239+00	2022-02-05 10:39:49.608239+00	{"size": 107645, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
9035df77-dbeb-4afc-9cfe-098b2899a18b	web-app	grifon/out/docs/1644057589872.jpg	\N	2022-02-05 10:39:50.368559+00	2022-02-05 10:39:50.368559+00	2022-02-05 10:39:50.368559+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
870c9a52-9179-48d5-8760-e3079bad99cf	web-app	grifon/out/docs/1644058330690.jpg	\N	2022-02-05 10:52:12.040572+00	2022-02-05 10:52:12.040572+00	2022-02-05 10:52:12.040572+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
d7743e26-6c27-4ed9-b79b-957047b6693c	web-app	grifon/out/docs/1644058332286.jpg	\N	2022-02-05 10:52:12.786542+00	2022-02-05 10:52:12.786542+00	2022-02-05 10:52:12.786542+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
432d5cd2-033f-40f5-9e0c-39a5bf44fe43	web-app	grifon/out/docs/1644058436770.jpg	\N	2022-02-05 10:53:58.102763+00	2022-02-05 10:53:58.102763+00	2022-02-05 10:53:58.102763+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
6cf1afd2-8b07-4216-82ab-cd341d5b52dd	web-app	grifon/out/docs/1644058438287.jpg	\N	2022-02-05 10:53:58.798448+00	2022-02-05 10:53:58.798448+00	2022-02-05 10:53:58.798448+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
8b722c6d-bd75-4bb5-9c4f-41f92b57c73c	web-app	grifon/out/docs/1644058818577.jpg	\N	2022-02-05 11:00:20.02123+00	2022-02-05 11:00:20.02123+00	2022-02-05 11:00:20.02123+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
77e21708-4d0c-49da-a974-564950feb244	web-app	grifon/out/docs/1644058820265.jpg	\N	2022-02-05 11:00:20.823871+00	2022-02-05 11:00:20.823871+00	2022-02-05 11:00:20.823871+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
a0f0ab06-ba69-4f1b-b2c1-9abe9e45de67	web-app	grifon/in/docs/1644058868274.jpg	\N	2022-02-05 11:01:08.884747+00	2022-02-05 11:01:08.884747+00	2022-02-05 11:01:08.884747+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
5071f7ef-12f4-4881-ac44-e88c28be8852	web-app	grifon/in/docs/1644058869041.jpg	\N	2022-02-05 11:01:09.622149+00	2022-02-05 11:01:09.622149+00	2022-02-05 11:01:09.622149+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
be8813b3-a1f5-4908-a016-007411740380	web-app	grifon/out/docs/1644058914467.jpg	\N	2022-02-05 11:01:55.404028+00	2022-02-05 11:01:55.404028+00	2022-02-05 11:01:55.404028+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
0ee52560-67bf-44bf-bcdd-4b9f11bb2668	web-app	grifon/out/docs/1644058915563.jpg	\N	2022-02-05 11:01:56.84403+00	2022-02-05 11:01:56.84403+00	2022-02-05 11:01:56.84403+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
160614f4-45b8-45a6-a8f3-4fdbd78e27f1	web-app	grifon/out/docs/1644058916979.jpg	\N	2022-02-05 11:01:57.555889+00	2022-02-05 11:01:57.555889+00	2022-02-05 11:01:57.555889+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
dd5cc228-eacb-455f-ad4d-a4c6e00608f9	web-app	grifon/out/docs/1644059031507.jpg	\N	2022-02-05 11:03:52.496818+00	2022-02-05 11:03:52.496818+00	2022-02-05 11:03:52.496818+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
6d466075-40d3-40a5-a55e-59346f6fb405	web-app	grifon/out/docs/1644059032598.jpg	\N	2022-02-05 11:03:54.257284+00	2022-02-05 11:03:54.257284+00	2022-02-05 11:03:54.257284+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
28169cd1-6ed9-46e2-9775-fa3bb71c82bb	web-app	grifon/out/docs/1644059324920.jpg	\N	2022-02-05 11:08:45.749792+00	2022-02-05 11:08:45.749792+00	2022-02-05 11:08:45.749792+00	{"size": 265631, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
42e15c2e-68ba-4f4a-a629-5f0e154f38cf	web-app	grifon/out/docs/1644059326019.jpg	\N	2022-02-05 11:08:46.831354+00	2022-02-05 11:08:46.831354+00	2022-02-05 11:08:46.831354+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
d284f1ec-7c47-4025-846b-b3c94ca466f5	web-app	grifon/out/docs/1644059418685.jpg	\N	2022-02-05 11:10:19.789586+00	2022-02-05 11:10:19.789586+00	2022-02-05 11:10:19.789586+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
cfde719b-fea8-4089-9eb3-02090d78545d	web-app	grifon/out/docs/1644059420058.jpg	\N	2022-02-05 11:10:21.092833+00	2022-02-05 11:10:21.092833+00	2022-02-05 11:10:21.092833+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
27c48ee4-6bdc-487c-97f3-7d8b7745016b	web-app	grifon/out/docs/1644059457848.jpg	\N	2022-02-05 11:10:58.786729+00	2022-02-05 11:10:58.786729+00	2022-02-05 11:10:58.786729+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
cb7005a8-a249-4fa5-a233-ead08d66f8a0	web-app	grifon/out/docs/1644059458984.jpg	\N	2022-02-05 11:11:00.177155+00	2022-02-05 11:11:00.177155+00	2022-02-05 11:11:00.177155+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
a242dc8f-3cce-491b-9384-a79037c78d10	web-app	grifon/out/docs/1644059738883.jpg	\N	2022-02-05 11:15:39.604743+00	2022-02-05 11:15:39.604743+00	2022-02-05 11:15:39.604743+00	{"size": 107645, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
cb6ba77c-d419-45ab-9664-6d6f2513007a	web-app	grifon/out/docs/1644059739788.jpg	\N	2022-02-05 11:15:40.531156+00	2022-02-05 11:15:40.531156+00	2022-02-05 11:15:40.531156+00	{"size": 265631, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
30dbd772-03ea-4386-a42e-45e0e867fd16	web-app	grifon/out/docs/1644059781023.jpg	\N	2022-02-05 11:16:21.656605+00	2022-02-05 11:16:21.656605+00	2022-02-05 11:16:21.656605+00	{"size": 107645, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
dc70e38a-3d59-4136-9db0-f2ce1a71b99b	web-app	grifon/out/docs/1644059781848.jpg	\N	2022-02-05 11:16:22.628518+00	2022-02-05 11:16:22.628518+00	2022-02-05 11:16:22.628518+00	{"size": 265631, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
d2cd86d2-fc27-4eaf-947e-3aec02aa5a2b	web-app	grifon/out/docs/1644059882242.jpg	\N	2022-02-05 11:18:02.977018+00	2022-02-05 11:18:02.977018+00	2022-02-05 11:18:02.977018+00	{"size": 107645, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
6930d8aa-e0e2-49a4-b6fe-75c3930659c3	web-app	grifon/out/docs/1644059883157.jpg	\N	2022-02-05 11:18:03.823094+00	2022-02-05 11:18:03.823094+00	2022-02-05 11:18:03.823094+00	{"size": 265631, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
aa8dbe9f-71e1-41d1-9721-1ad1cee1c1b0	web-app	grifon/out/docs/1644059942044.jpg	\N	2022-02-05 11:19:02.65012+00	2022-02-05 11:19:02.65012+00	2022-02-05 11:19:02.65012+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
4a717207-9027-40cc-b2e9-2364cc15464b	web-app	grifon/out/docs/1644059942860.jpg	\N	2022-02-05 11:19:03.667109+00	2022-02-05 11:19:03.667109+00	2022-02-05 11:19:03.667109+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
9d3b8da8-f781-49d5-a04d-3e9ba7a23f14	web-app	grifon/out/docs/1644060085393.jpg	\N	2022-02-05 11:21:25.950097+00	2022-02-05 11:21:25.950097+00	2022-02-05 11:21:25.950097+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
61d117c0-753f-42e8-a108-c5faf9dabef6	web-app	grifon/out/docs/1644060086162.jpg	\N	2022-02-05 11:21:27.679337+00	2022-02-05 11:21:27.679337+00	2022-02-05 11:21:27.679337+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
69f1e81b-f55d-479d-966c-8b560a6ae5c2	web-app	grifon/out/docs/1644060131182.jpg	\N	2022-02-05 11:22:11.855157+00	2022-02-05 11:22:11.855157+00	2022-02-05 11:22:11.855157+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
89580711-515d-4bba-b571-9bfd6faae46f	web-app	grifon/out/docs/1644060132049.jpg	\N	2022-02-05 11:22:17.713168+00	2022-02-05 11:22:17.713168+00	2022-02-05 11:22:17.713168+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
94de2665-8806-4822-affa-3a0914458723	web-app	grifon/out/docs/1644060390870.jpg	\N	2022-02-05 11:26:32.197279+00	2022-02-05 11:26:32.197279+00	2022-02-05 11:26:32.197279+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
4a57322c-2285-4ee9-a8b7-22e5f0a5e367	web-app	grifon/out/docs/1644060392431.jpg	\N	2022-02-05 11:26:33.04892+00	2022-02-05 11:26:33.04892+00	2022-02-05 11:26:33.04892+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
e1a33e5d-b79b-400e-8670-94928ef08268	web-app	grifon/out/docs/1644060489966.jpg	\N	2022-02-05 11:28:11.347409+00	2022-02-05 11:28:11.347409+00	2022-02-05 11:28:11.347409+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
8b9cd04f-2160-4b75-acc8-595d6b07d25a	web-app	grifon/out/docs/1644060491474.jpg	\N	2022-02-05 11:28:12.076145+00	2022-02-05 11:28:12.076145+00	2022-02-05 11:28:12.076145+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
4876a6dc-e25e-4711-b9d6-a3546a8028fa	web-app	grifon/out/docs/1644060569406.jpg	\N	2022-02-05 11:29:30.703933+00	2022-02-05 11:29:30.703933+00	2022-02-05 11:29:30.703933+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
ad6d0c22-5ea6-49d8-8760-dcb23b4b33d6	web-app	grifon/out/docs/1644060570906.jpg	\N	2022-02-05 11:29:31.49673+00	2022-02-05 11:29:31.49673+00	2022-02-05 11:29:31.49673+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
a21d2058-b71e-497e-9839-d5b099700d68	web-app	grifon/out/docs/1644060714895.jpg	\N	2022-02-05 11:31:56.063214+00	2022-02-05 11:31:56.063214+00	2022-02-05 11:31:56.063214+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
2ad50293-2762-484d-b432-713e2a4123c4	web-app	grifon/out/docs/1644060716259.jpg	\N	2022-02-05 11:31:57.073243+00	2022-02-05 11:31:57.073243+00	2022-02-05 11:31:57.073243+00	{"size": 265631, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
c8619581-0985-4a8e-a322-3d3f6815b1ab	web-app	grifon/out/docs/1644066529330.jpg	\N	2022-02-05 13:08:50.047266+00	2022-02-05 13:08:50.047266+00	2022-02-05 13:08:50.047266+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
68d673da-a80f-4d68-9536-f33bcdff34e0	web-app	grifon/out/docs/1644066530282.jpg	\N	2022-02-05 13:08:51.464139+00	2022-02-05 13:08:51.464139+00	2022-02-05 13:08:51.464139+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
1def34be-c0a8-461f-938a-117636fe27da	web-app	grifon/out/docs/1644066606463.jpg	\N	2022-02-05 13:10:07.091558+00	2022-02-05 13:10:07.091558+00	2022-02-05 13:10:07.091558+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
cfcfc83a-2534-461c-8d11-bf1689e49de7	web-app	grifon/out/docs/1644066607304.jpg	\N	2022-02-05 13:10:08.534492+00	2022-02-05 13:10:08.534492+00	2022-02-05 13:10:08.534492+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
e57b1153-ef60-4fb9-ae90-32158ad0fb0a	web-app	grifon/out/docs/1644066688719.jpg	\N	2022-02-05 13:11:29.938585+00	2022-02-05 13:11:29.938585+00	2022-02-05 13:11:29.938585+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
0fb629fa-4386-4bb4-9faf-f16926b360dd	web-app	grifon/out/docs/1644066690208.jpg	\N	2022-02-05 13:11:30.742234+00	2022-02-05 13:11:30.742234+00	2022-02-05 13:11:30.742234+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
89688fc6-e2a8-4dbe-b926-4c05b4aefa95	web-app	grifon/out/docs/1644066940982.jpg	\N	2022-02-05 13:15:42.481777+00	2022-02-05 13:15:42.481777+00	2022-02-05 13:15:42.481777+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
9f7f1eed-2e90-46dc-ac23-09c4786b67f2	web-app	grifon/out/docs/1644067110310.jpg	\N	2022-02-05 13:18:31.128721+00	2022-02-05 13:18:31.128721+00	2022-02-05 13:18:31.128721+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
f6ae2ea4-49b5-447c-8fe1-95050cbc2df9	web-app	grifon/out/docs/1644067258792.jpg	\N	2022-02-05 13:20:59.509238+00	2022-02-05 13:20:59.509238+00	2022-02-05 13:20:59.509238+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
a09786a5-e55e-492c-8855-5e3792a58d5f	web-app	grifon/out/docs/1644067344149.jpg	\N	2022-02-05 13:22:25.276176+00	2022-02-05 13:22:25.276176+00	2022-02-05 13:22:25.276176+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
f568c718-7d69-4258-a134-2901644cd210	web-app	grifon/out/docs/1644067996448.jpg	\N	2022-02-05 13:33:17.307678+00	2022-02-05 13:33:17.307678+00	2022-02-05 13:33:17.307678+00	{"size": 265631, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
f4fbd158-332c-4b98-90cc-00145a8c1929	web-app	grifon/out/docs/1644068068923.jpg	\N	2022-02-05 13:34:29.683746+00	2022-02-05 13:34:29.683746+00	2022-02-05 13:34:29.683746+00	{"size": 265631, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
f6e52112-19a8-4ea3-b518-a5dad8ff45bc	web-app	grifon/out/docs/1644068149519.jpg	\N	2022-02-05 13:35:50.181812+00	2022-02-05 13:35:50.181812+00	2022-02-05 13:35:50.181812+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
ce7ea39e-594c-4c39-b979-7aa5734d8940	web-app	grifon/in/docs/1644068216470.jpg	\N	2022-02-05 13:36:57.380607+00	2022-02-05 13:36:57.380607+00	2022-02-05 13:36:57.380607+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
a198d5cf-91b8-43cf-9edd-6e574326a12b	web-app	grifon/out/docs/1644068281820.jpg	\N	2022-02-05 13:38:03.06841+00	2022-02-05 13:38:03.06841+00	2022-02-05 13:38:03.06841+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
d3bfb737-4b6d-4541-a018-ff03b88e85c4	web-app	grifon/out/docs/1644068324607.jpg	\N	2022-02-05 13:38:45.490283+00	2022-02-05 13:38:45.490283+00	2022-02-05 13:38:45.490283+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
84398ccf-bb92-4e74-ac82-ead4c33494c6	web-app	grifon/out/docs/1644070634570.jpg	\N	2022-02-05 14:17:15.487062+00	2022-02-05 14:17:15.487062+00	2022-02-05 14:17:15.487062+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
97913cb3-3519-4da5-8496-60dae0ffbf57	web-app	grifon/out/docs/1644070690303.jpg	\N	2022-02-05 14:18:10.925302+00	2022-02-05 14:18:10.925302+00	2022-02-05 14:18:10.925302+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
01d1c25e-02d9-4272-930e-92fbd1434e47	web-app	grifon/out/docs/1644070867571.jpg	\N	2022-02-05 14:21:08.224069+00	2022-02-05 14:21:08.224069+00	2022-02-05 14:21:08.224069+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
9be75e6e-0c5f-41ba-b0f8-6aecdd2f48c5	web-app	grifon/out/docs/1644070947884.jpg	\N	2022-02-05 14:22:29.176839+00	2022-02-05 14:22:29.176839+00	2022-02-05 14:22:29.176839+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
8fd12116-ab85-4459-ab38-4be5ee7eac2b	web-app	grifon/in/docs/1644071425638.jpg	\N	2022-02-05 14:30:26.702548+00	2022-02-05 14:30:26.702548+00	2022-02-05 14:30:26.702548+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
bf263bf8-f861-4da9-8edf-72f8554133cf	web-app	grifon/out/docs/1644071571152.jpg	\N	2022-02-05 14:32:52.311674+00	2022-02-05 14:32:52.311674+00	2022-02-05 14:32:52.311674+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
b7a20190-e98f-4bca-a853-4bdc2200f7b3	web-app	grifon/out/docs/1644071572555.jpg	\N	2022-02-05 14:32:53.017992+00	2022-02-05 14:32:53.017992+00	2022-02-05 14:32:53.017992+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
f7f54e9c-ab67-419b-9037-675886a4ae5a	web-app	grifon/out/docs/1644071639107.jpg	\N	2022-02-05 14:33:59.540296+00	2022-02-05 14:33:59.540296+00	2022-02-05 14:33:59.540296+00	{"size": 28366, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
1941fce2-3335-478c-b26e-11fd1480188e	web-app	grifon/out/docs/1644071639763.jpg	\N	2022-02-05 14:34:00.359564+00	2022-02-05 14:34:00.359564+00	2022-02-05 14:34:00.359564+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
a3e6fac8-33dd-44c2-85ce-65dc22f69153	web-app	grifon/out/docs/1644071830168.jpg	\N	2022-02-05 14:37:10.960399+00	2022-02-05 14:37:10.960399+00	2022-02-05 14:37:10.960399+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
a60ffb86-d619-417b-8e34-e0abd8a46b94	web-app	grifon/out/docs/1644071831168.jpg	\N	2022-02-05 14:37:12.354822+00	2022-02-05 14:37:12.354822+00	2022-02-05 14:37:12.354822+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
80c4b9f9-82c5-4a29-93b5-1cfa3004be3f	web-app	grifon/out/docs/1644072038127.jpg	\N	2022-02-05 14:40:38.809282+00	2022-02-05 14:40:38.809282+00	2022-02-05 14:40:38.809282+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
ac9a44e2-1605-4a8a-b4ba-bc7be5890311	web-app	grifon/out/docs/1644072038972.jpg	\N	2022-02-05 14:40:39.539335+00	2022-02-05 14:40:39.539335+00	2022-02-05 14:40:39.539335+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
003b2bd2-e5f3-4779-85f4-094831ad200c	web-app	grifon/out/docs/1644072128690.jpg	\N	2022-02-05 14:42:09.559161+00	2022-02-05 14:42:09.559161+00	2022-02-05 14:42:09.559161+00	{"size": 265631, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
1a50f696-df52-4d0a-be7c-2197e15275d7	web-app	grifon/out/docs/1644072129753.jpg	\N	2022-02-05 14:42:10.646203+00	2022-02-05 14:42:10.646203+00	2022-02-05 14:42:10.646203+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
fd095af8-3d79-4608-be3b-c3cf0f263e00	web-app	grifon/out/docs/1644072411139.jpg	\N	2022-02-05 14:46:51.844058+00	2022-02-05 14:46:51.844058+00	2022-02-05 14:46:51.844058+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
96d902c7-6d88-4d61-908b-21a7da2d2700	web-app	grifon/out/docs/1644072411979.jpg	\N	2022-02-05 14:46:52.561953+00	2022-02-05 14:46:52.561953+00	2022-02-05 14:46:52.561953+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
fd4c861b-796b-45fc-8d61-c60744cd14bf	web-app	grifon/out/docs/1644072476021.jpg	\N	2022-02-05 14:47:56.635783+00	2022-02-05 14:47:56.635783+00	2022-02-05 14:47:56.635783+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
f63569fe-2009-4736-ad30-cc42d5d3c0f4	web-app	grifon/out/docs/1644072476781.jpg	\N	2022-02-05 14:47:57.380263+00	2022-02-05 14:47:57.380263+00	2022-02-05 14:47:57.380263+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
e3dbb757-309a-43d7-955d-333e88a34c2b	web-app	grifon/out/docs/1644072534847.jpg	\N	2022-02-05 14:48:55.576026+00	2022-02-05 14:48:55.576026+00	2022-02-05 14:48:55.576026+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
e36dd703-c7da-4151-afb6-ca26d1259fa0	web-app	grifon/out/docs/1644072535685.jpg	\N	2022-02-05 14:48:56.287917+00	2022-02-05 14:48:56.287917+00	2022-02-05 14:48:56.287917+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
26e7ebb0-9e00-4168-80ad-fe2ba172cb41	web-app	grifon/out/docs/1644072804015.jpg	\N	2022-02-05 14:53:25.571875+00	2022-02-05 14:53:25.571875+00	2022-02-05 14:53:25.571875+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
948f7433-de49-4ff3-9a55-3bc75aed9347	web-app	grifon/out/docs/1644072805714.jpg	\N	2022-02-05 14:53:26.319508+00	2022-02-05 14:53:26.319508+00	2022-02-05 14:53:26.319508+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
1072ca3c-b743-4e97-8784-38f9f12faf79	web-app	grifon/out/docs/1644072889875.jpg	\N	2022-02-05 14:54:51.335174+00	2022-02-05 14:54:51.335174+00	2022-02-05 14:54:51.335174+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
e7769697-3dfe-4346-95da-c96242399c81	web-app	grifon/out/docs/1644072891483.jpg	\N	2022-02-05 14:54:52.06964+00	2022-02-05 14:54:52.06964+00	2022-02-05 14:54:52.06964+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
42db1b78-0353-4e98-85f2-1c560e3601bd	web-app	grifon/out/docs/1644072892127.jpg	\N	2022-02-05 14:54:52.764949+00	2022-02-05 14:54:52.764949+00	2022-02-05 14:54:52.764949+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
9a45740f-9f74-4be1-8731-8e76db1bbd9e	web-app	grifon/out/docs/1644072910957.jpg	\N	2022-02-05 14:55:12.2454+00	2022-02-05 14:55:12.2454+00	2022-02-05 14:55:12.2454+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
63ce65e7-c5c0-40d0-ac49-60ff354186ea	web-app	grifon/out/docs/1644072912387.jpg	\N	2022-02-05 14:55:13.005077+00	2022-02-05 14:55:13.005077+00	2022-02-05 14:55:13.005077+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
b45a7b8b-b9af-47fb-a39c-6be0ddc9812f	web-app	grifon/out/docs/1644072913049.jpg	\N	2022-02-05 14:55:13.703094+00	2022-02-05 14:55:13.703094+00	2022-02-05 14:55:13.703094+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
165791b3-cce3-4670-95bd-1828a5b4dd3d	web-app	grifon/out/docs/1644141625082.jpg	\N	2022-02-06 10:00:26.321098+00	2022-02-06 10:00:26.321098+00	2022-02-06 10:00:26.321098+00	{"size": 599374, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
0cca2f35-7b3d-4563-94c9-a28329cae7e0	web-app	grifon/out/docs/1644141626873.jpg	\N	2022-02-06 10:00:27.623241+00	2022-02-06 10:00:27.623241+00	2022-02-06 10:00:27.623241+00	{"size": 426911, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
5f8472a3-fb61-4121-96cc-1643f8034a36	web-app	grifon/out/docs/1644141627964.jpg	\N	2022-02-06 10:00:28.630575+00	2022-02-06 10:00:28.630575+00	2022-02-06 10:00:28.630575+00	{"size": 309978, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
b877ce44-4390-437a-83a6-6da986a7b8c9	web-app	grifon/out/docs/1644141733926.jpg	\N	2022-02-06 10:02:14.969844+00	2022-02-06 10:02:14.969844+00	2022-02-06 10:02:14.969844+00	{"size": 599374, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
c6b2dbe7-8440-4dab-8e2a-648f3071c002	web-app	grifon/out/docs/1644141735331.jpg	\N	2022-02-06 10:02:16.085445+00	2022-02-06 10:02:16.085445+00	2022-02-06 10:02:16.085445+00	{"size": 426911, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
f297703a-d928-4599-b9b9-a74a03dfef0c	web-app	grifon/in/docs/1644151337371.pdf	\N	2022-02-06 12:42:22.000558+00	2022-02-06 12:42:22.000558+00	2022-02-06 12:42:22.000558+00	{"size": 249171, "mimetype": "application/pdf", "cacheControl": "max-age=3600"}
58016b29-197c-461c-a864-fb7379129f49	web-app	grifon/in/docs/1644151355182.jpeg	\N	2022-02-06 12:42:38.056839+00	2022-02-06 12:42:38.056839+00	2022-02-06 12:42:38.056839+00	{"size": 120612, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
4f0142ca-1a6b-40d5-9168-4719afdaa0c6	web-app	grifon/in/docs/1644151365181.jpeg	\N	2022-02-06 12:42:51.099021+00	2022-02-06 12:42:51.099021+00	2022-02-06 12:42:51.099021+00	{"size": 101208, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
ed3a5b7d-ce31-40bd-a698-de63f57614f0	web-app	grifon/out/docs/1644240445923.jpg	\N	2022-02-07 13:27:26.70512+00	2022-02-07 13:27:26.70512+00	2022-02-07 13:27:26.70512+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
eae55606-ac1f-4b32-b880-0a3f0f464785	web-app	grifon/in/docs/1644241639297.json	\N	2022-02-07 13:47:22.488062+00	2022-02-07 13:47:22.488062+00	2022-02-07 13:47:22.488062+00	{"size": 28411, "mimetype": "application/json", "cacheControl": "max-age=3600"}
8525ba09-002f-4faa-a2c8-f60a6a88cd8c	web-app	grifon/out/docs/1644241787975.jpg	\N	2022-02-07 13:49:48.695589+00	2022-02-07 13:49:48.695589+00	2022-02-07 13:49:48.695589+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
5568792e-e5bb-4ea9-bd60-5b80d19445f4	web-app	grifon/out/docs/1644241788816.jpg	\N	2022-02-07 13:49:49.446291+00	2022-02-07 13:49:49.446291+00	2022-02-07 13:49:49.446291+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
d4827d49-69bb-41ad-92d4-ee3904c15187	web-app	grifon/in/docs/1644303842045.ts	\N	2022-02-08 07:04:04.801691+00	2022-02-08 07:04:04.801691+00	2022-02-08 07:04:04.801691+00	{"size": 3037, "mimetype": "application/octet-stream", "cacheControl": "max-age=3600"}
f820a10b-4a6d-4550-8766-fe4d347f90d4	web-app	grifon/out/docs/1644411293094.jpg	\N	2022-02-09 12:54:53.619987+00	2022-02-09 12:54:53.619987+00	2022-02-09 12:54:53.619987+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
73bd264e-fdaf-4eeb-8176-0a3925a9f04d	web-app	grifon/out/docs/1644411293941.jpg	\N	2022-02-09 12:54:54.322461+00	2022-02-09 12:54:54.322461+00	2022-02-09 12:54:54.322461+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
6a342e0f-3725-4867-b463-3ebae6851a45	web-app	grifon/out/docs/1644411417171.jpg	\N	2022-02-09 12:56:57.658086+00	2022-02-09 12:56:57.658086+00	2022-02-09 12:56:57.658086+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
9b072dfb-5911-4ee6-bfa9-54e821d5f479	web-app	grifon/out/docs/1644411417844.jpg	\N	2022-02-09 12:56:58.322216+00	2022-02-09 12:56:58.322216+00	2022-02-09 12:56:58.322216+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
2263e04c-6d55-4733-9a83-ee8ae88fe2e8	web-app	grifon/in/docs/1644411515771.sql	\N	2022-02-09 12:58:39.698719+00	2022-02-09 12:58:39.698719+00	2022-02-09 12:58:39.698719+00	{"size": 78899, "mimetype": "application/x-sql", "cacheControl": "max-age=3600"}
c6e191cc-d1e5-4a69-9302-8a79fdc02a44	web-app	grifon/out/docs/1644411711091.jpg	\N	2022-02-09 13:01:51.631246+00	2022-02-09 13:01:51.631246+00	2022-02-09 13:01:51.631246+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
35ecea7a-6976-4b87-98c2-3b46a09b8d3c	web-app	grifon/out/docs/1644411711809.jpg	\N	2022-02-09 13:01:52.281434+00	2022-02-09 13:01:52.281434+00	2022-02-09 13:01:52.281434+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
d74bc3f4-531e-440f-be83-67501518c999	web-app	grifon/in/docs/1644413324831.sql	\N	2022-02-09 13:28:49.596504+00	2022-02-09 13:28:49.596504+00	2022-02-09 13:28:49.596504+00	{"size": 78899, "mimetype": "application/x-sql", "cacheControl": "max-age=3600"}
06caf0b2-d0b3-431b-804f-b4eadce380b2	web-app	grifon/in/docs/1644413370434.sql	\N	2022-02-09 13:29:33.828056+00	2022-02-09 13:29:33.828056+00	2022-02-09 13:29:33.828056+00	{"size": 78899, "mimetype": "application/x-sql", "cacheControl": "max-age=3600"}
ae923c66-33aa-4cd9-917f-7a194e28a7f7	web-app	grifon/in/audio/1644413561274.mp3	\N	2022-02-09 13:32:43.4503+00	2022-02-09 13:32:43.4503+00	2022-02-09 13:32:43.4503+00	{"size": 29568, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
6a1f1b2f-c790-4f06-9a5f-25f679cc33bd	web-app	grifon/in/docs/1644413605260.jpeg	\N	2022-02-09 13:33:29.709714+00	2022-02-09 13:33:29.709714+00	2022-02-09 13:33:29.709714+00	{"size": 334594, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
8c33a897-0321-47be-926b-499f8bf668aa	web-app	grifon/in/docs/1644414738947.conf	\N	2022-02-09 13:52:21.481771+00	2022-02-09 13:52:21.481771+00	2022-02-09 13:52:21.481771+00	{"size": 2474, "mimetype": "text/plain", "cacheControl": "max-age=3600"}
ba28685a-85f1-4098-a24c-3c8fb5722acd	web-app	grifon/in/docs/1644414756519.jpeg	\N	2022-02-09 13:52:41.169406+00	2022-02-09 13:52:41.169406+00	2022-02-09 13:52:41.169406+00	{"size": 316302, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
b80be3c1-3531-4620-b489-c606ddafa148	web-app	grifon/in/audio/1644414759891.mp3	\N	2022-02-09 13:52:41.996549+00	2022-02-09 13:52:41.996549+00	2022-02-09 13:52:41.996549+00	{"size": 30720, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
1124dd00-12b8-44c7-9b8a-7a3f5b6f8b24	web-app	grifon/in/docs/1644415232174.ttf	\N	2022-02-09 14:00:35.379077+00	2022-02-09 14:00:35.379077+00	2022-02-09 14:00:35.379077+00	{"size": 51864, "mimetype": "application/font-sfnt", "cacheControl": "max-age=3600"}
2fc3d347-9adf-45a4-b992-369e75d0bd80	web-app	grifon/in/docs/1644415308262.ttf	\N	2022-02-09 14:01:51.357518+00	2022-02-09 14:01:51.357518+00	2022-02-09 14:01:51.357518+00	{"size": 51864, "mimetype": "application/font-sfnt", "cacheControl": "max-age=3600"}
86eebc03-53e5-4261-a00b-223ca35a4930	web-app	grifon/in/docs/1644422246996.jpeg	\N	2022-02-09 15:57:30.203961+00	2022-02-09 15:57:30.203961+00	2022-02-09 15:57:30.203961+00	{"size": 265631, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
671bee9f-7635-4173-a927-ddfed2a9e00f	web-app	grifon/in/audio/1644422233008.mp3	\N	2022-02-09 15:57:15.981551+00	2022-02-09 15:57:15.981551+00	2022-02-09 15:57:15.981551+00	{"size": 36096, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
d1ccc3a0-0dbd-4f4f-9706-737ec03ac5e5	web-app	grifon/in/docs/1644422239941.sql	\N	2022-02-09 15:57:26.718642+00	2022-02-09 15:57:26.718642+00	2022-02-09 15:57:26.718642+00	{"size": 1201242, "mimetype": "application/x-sql", "cacheControl": "max-age=3600"}
c79f5926-87e3-4059-9802-77e9b8315c81	web-app	grifon/out/docs/1644422493846.jpg	\N	2022-02-09 16:01:34.9621+00	2022-02-09 16:01:34.9621+00	2022-02-09 16:01:34.9621+00	{"size": 599374, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
3387d09f-aaa8-4233-84d5-3a4eefb61da0	web-app	grifon/out/docs/1644422495199.jpg	\N	2022-02-09 16:01:36.001056+00	2022-02-09 16:01:36.001056+00	2022-02-09 16:01:36.001056+00	{"size": 426911, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
a018a569-5781-4d79-b9ea-7c41e1d3419b	web-app	grifon/out/docs/1644422496123.jpg	\N	2022-02-09 16:01:36.858653+00	2022-02-09 16:01:36.858653+00	2022-02-09 16:01:36.858653+00	{"size": 309978, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
22091e57-f886-44b0-a662-027243d9a490	web-app	grifon/in/docs/1644422711524.jpg	\N	2022-02-09 16:05:12.667717+00	2022-02-09 16:05:12.667717+00	2022-02-09 16:05:12.667717+00	{"size": 599374, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
4c23cee1-9015-4f98-92c2-dc8e595f1294	web-app	grifon/in/docs/1644422712817.jpg	\N	2022-02-09 16:05:13.722711+00	2022-02-09 16:05:13.722711+00	2022-02-09 16:05:13.722711+00	{"size": 426911, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
e0a9c04b-ce25-4ecb-840f-ec5161e68287	web-app	grifon/in/docs/1644422713966.jpg	\N	2022-02-09 16:05:14.743371+00	2022-02-09 16:05:14.743371+00	2022-02-09 16:05:14.743371+00	{"size": 309978, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
a470822b-973d-412f-9a30-651f2576329c	web-app	grifon/in/docs/1644422827189.jpg	\N	2022-02-09 16:07:08.060811+00	2022-02-09 16:07:08.060811+00	2022-02-09 16:07:08.060811+00	{"size": 387148, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
c9c5f71c-f86e-42f9-bb35-865ba3fc5072	web-app	grifon/in/docs/1644422828243.jpg	\N	2022-02-09 16:07:09.407805+00	2022-02-09 16:07:09.407805+00	2022-02-09 16:07:09.407805+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
20854e07-c17d-4490-b8c9-c02e1d1c5cf7	web-app	grifon/in/docs/1644422829515.jpg	\N	2022-02-09 16:07:09.99268+00	2022-02-09 16:07:09.99268+00	2022-02-09 16:07:09.99268+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
32cbae37-368b-417c-b1a9-ba89565898a1	web-app	grifon/in/docs/1644423240580.json	\N	2022-02-09 16:14:05.953057+00	2022-02-09 16:14:05.953057+00	2022-02-09 16:14:05.953057+00	{"size": 28411, "mimetype": "application/json", "cacheControl": "max-age=3600"}
ea36f8ae-5e47-41c6-a983-7d93a065974b	web-app	grifon/in/docs/1644423271141.jpg	\N	2022-02-09 16:14:31.745708+00	2022-02-09 16:14:31.745708+00	2022-02-09 16:14:31.745708+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
69ff5b27-df92-4247-9a9e-9f55becef077	web-app	grifon/in/docs/1644423271846.jpg	\N	2022-02-09 16:14:32.328604+00	2022-02-09 16:14:32.328604+00	2022-02-09 16:14:32.328604+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
39be6648-0fd6-4327-bb5a-5af0fc406a3f	web-app	grifon/in/audio/1644425221741.mp3	\N	2022-02-09 16:47:04.457644+00	2022-02-09 16:47:04.457644+00	2022-02-09 16:47:04.457644+00	{"size": 23808, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
91a7f3c9-f556-40fc-b696-3d3d9bbf67fe	web-app	grifon/in/docs/1644425237744.jpeg	\N	2022-02-09 16:47:22.324881+00	2022-02-09 16:47:22.324881+00	2022-02-09 16:47:22.324881+00	{"size": 134256, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
dfe7a7f4-9706-43d7-aea5-a9dabd11b9a8	web-app	grifon/in/docs/1644425246303.sql	\N	2022-02-09 16:47:29.910881+00	2022-02-09 16:47:29.910881+00	2022-02-09 16:47:29.910881+00	{"size": 78899, "mimetype": "application/x-sql", "cacheControl": "max-age=3600"}
744f45d7-0cab-4a4c-8afb-33992ed7963b	web-app	grifon/in/docs/1644425343110.jpg	\N	2022-02-09 16:49:04.608318+00	2022-02-09 16:49:04.608318+00	2022-02-09 16:49:04.608318+00	{"size": 599374, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
0c3386de-7d25-44cc-a806-e884174e97a7	web-app	grifon/in/docs/1644425344975.jpg	\N	2022-02-09 16:49:06.067141+00	2022-02-09 16:49:06.067141+00	2022-02-09 16:49:06.067141+00	{"size": 426911, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
09957df8-3d8b-4c91-86ab-9b63a713793c	web-app	grifon/in/docs/1644425346384.jpg	\N	2022-02-09 16:49:07.230026+00	2022-02-09 16:49:07.230026+00	2022-02-09 16:49:07.230026+00	{"size": 309978, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
589f31d6-ce2b-47d0-9931-2f7fc23401d9	web-app	grifon/out/docs/1644425381783.jpg	\N	2022-02-09 16:49:42.97977+00	2022-02-09 16:49:42.97977+00	2022-02-09 16:49:42.97977+00	{"size": 599374, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
a721db5d-9b5b-43e2-b759-0555c929fc5b	web-app	grifon/out/docs/1644425383188.jpg	\N	2022-02-09 16:49:43.969155+00	2022-02-09 16:49:43.969155+00	2022-02-09 16:49:43.969155+00	{"size": 426911, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
c04f7691-ec60-4c6a-a1c9-9394be7f3ca9	web-app	grifon/in/docs/1644425416116.jpg	\N	2022-02-09 16:50:17.566752+00	2022-02-09 16:50:17.566752+00	2022-02-09 16:50:17.566752+00	{"size": 599374, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
4bd12bd2-7299-4e69-9c3d-34b40369b8ca	web-app	grifon/in/docs/1644425417789.jpg	\N	2022-02-09 16:50:18.821202+00	2022-02-09 16:50:18.821202+00	2022-02-09 16:50:18.821202+00	{"size": 426911, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
cf0d6105-00f2-425b-aa03-58fd6d7c384a	web-app	grifon/out/docs/1644425451262.jpg	\N	2022-02-09 16:50:51.873453+00	2022-02-09 16:50:51.873453+00	2022-02-09 16:50:51.873453+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
ae0f18fc-c035-4aea-b792-c656b36ffe30	web-app	grifon/out/docs/1644425452116.jpg	\N	2022-02-09 16:50:52.64085+00	2022-02-09 16:50:52.64085+00	2022-02-09 16:50:52.64085+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
352487b5-860c-4ef6-bb35-80a271387104	web-app	grifon/in/docs/1644425562596.jpg	\N	2022-02-09 16:52:43.652049+00	2022-02-09 16:52:43.652049+00	2022-02-09 16:52:43.652049+00	{"size": 599374, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
df4958b1-8e8e-45c4-9d68-945d45e24913	web-app	grifon/in/docs/1644425613732.docx	\N	2022-02-09 16:53:36.327991+00	2022-02-09 16:53:36.327991+00	2022-02-09 16:53:36.327991+00	{"size": 21353, "mimetype": "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "cacheControl": "max-age=3600"}
017bbdcc-f040-4ff0-bb65-873bc9b491d4	web-app	grifon/in/docs/1644425690231.jpg	\N	2022-02-09 16:54:51.245377+00	2022-02-09 16:54:51.245377+00	2022-02-09 16:54:51.245377+00	{"size": 599374, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
1b401fe4-89a5-41b3-b38d-8315579d8386	web-app	grifon/in/docs/1644425691568.jpg	\N	2022-02-09 16:54:52.409941+00	2022-02-09 16:54:52.409941+00	2022-02-09 16:54:52.409941+00	{"size": 426911, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
8d0c1b11-3eef-4a8c-9fce-6fe0ba117f92	web-app	grifon/out/docs/1644425729029.jpg	\N	2022-02-09 16:55:30.135105+00	2022-02-09 16:55:30.135105+00	2022-02-09 16:55:30.135105+00	{"size": 711567, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
338902e5-d08b-4395-93b1-f6eca3768a47	web-app	grifon/out/docs/1644425730413.jpg	\N	2022-02-09 16:55:30.802823+00	2022-02-09 16:55:30.802823+00	2022-02-09 16:55:30.802823+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
89a2db65-1d96-4df7-8933-f4f63384865e	web-app	grifon/out/docs/1644425730991.jpg	\N	2022-02-09 16:55:31.40458+00	2022-02-09 16:55:31.40458+00	2022-02-09 16:55:31.40458+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
9c3e9cd5-35aa-45ba-a68a-89ff693130b4	web-app	grifon/out/docs/1644427208762.jpg	\N	2022-02-09 17:20:09.266482+00	2022-02-09 17:20:09.266482+00	2022-02-09 17:20:09.266482+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
865e4518-142a-4f89-a80c-4b2ed48eea6a	web-app	grifon/out/docs/1644427209443.jpg	\N	2022-02-09 17:20:09.982935+00	2022-02-09 17:20:09.982935+00	2022-02-09 17:20:09.982935+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
12c5c5d0-1e53-4bb5-8b9f-6a672fb79116	web-app	grifon/in/docs/1644481164205.jpg	\N	2022-02-10 08:19:24.747409+00	2022-02-10 08:19:24.747409+00	2022-02-10 08:19:24.747409+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
c02101e6-8e2d-46d8-b516-9a7d7e8271d2	web-app	grifon/in/docs/1644481164870.jpg	\N	2022-02-10 08:19:25.380449+00	2022-02-10 08:19:25.380449+00	2022-02-10 08:19:25.380449+00	{"size": 92035, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
4730862e-4d54-4464-9ea2-ccc6aef53337	web-app	grifon/out/docs/1644481223487.jpg	\N	2022-02-10 08:20:24.60458+00	2022-02-10 08:20:24.60458+00	2022-02-10 08:20:24.60458+00	{"size": 599374, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
b207a7db-c173-4493-b80a-f196c907f13c	web-app	grifon/out/docs/1644481224709.jpg	\N	2022-02-10 08:20:25.580482+00	2022-02-10 08:20:25.580482+00	2022-02-10 08:20:25.580482+00	{"size": 426911, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
d0e4e80f-8919-4301-8c76-5ba0c8a208cd	web-app	grifon/out/docs/1644482065692.jpg	\N	2022-02-10 08:34:26.250723+00	2022-02-10 08:34:26.250723+00	2022-02-10 08:34:26.250723+00	{"size": 75848, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
15763d54-5c59-4707-bc54-4c22742efc61	web-app	grifon/in/audio/1644579296661.mp3	\N	2022-02-11 11:34:58.693126+00	2022-02-11 11:34:58.693126+00	2022-02-11 11:34:58.693126+00	{"size": 37632, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
93793c1a-152d-4d9e-a62f-a761aecdc2cc	web-app	grifon/in/audio/1644580166763.mp3	\N	2022-02-11 11:49:29.454026+00	2022-02-11 11:49:29.454026+00	2022-02-11 11:49:29.454026+00	{"size": 37632, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
8195533b-ea06-4025-869e-65c57a79e2a2	web-app	grifon/in/docs/1644595198641.vcf	\N	2022-02-11 16:00:02.37603+00	2022-02-11 16:00:02.37603+00	2022-02-11 16:00:02.37603+00	{"size": 42489, "mimetype": "text/directory", "cacheControl": "max-age=3600"}
83d92217-638c-45d5-a5cc-27ae44cc8891	web-app	grifon/in/docs/1644595203527.vcf	\N	2022-02-11 16:00:05.288094+00	2022-02-11 16:00:05.288094+00	2022-02-11 16:00:05.288094+00	{"size": 42489, "mimetype": "text/directory", "cacheControl": "max-age=3600"}
32413f5e-fe9a-47ae-844c-e4b5cfa1787a	web-app	grifon/in/docs/1644595215527.jpeg	\N	2022-02-11 16:00:19.939149+00	2022-02-11 16:00:19.939149+00	2022-02-11 16:00:19.939149+00	{"size": 334594, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
2ba18e2d-68cf-4c39-9bf9-049492668d1c	web-app	grifon/in/docs/1644595220681.jpeg	\N	2022-02-11 16:00:23.903411+00	2022-02-11 16:00:23.903411+00	2022-02-11 16:00:23.903411+00	{"size": 334594, "mimetype": "image/jpeg", "cacheControl": "max-age=3600"}
ce19286b-f4ad-46cb-9a81-7b97a4d0531a	web-app	grifon/in/audio/1644595237758.mp3	\N	2022-02-11 16:00:39.812023+00	2022-02-11 16:00:39.812023+00	2022-02-11 16:00:39.812023+00	{"size": 36096, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
eba5b408-c97e-446d-b989-3e66db5a2180	web-app	grifon/in/audio/1644595240384.mp3	\N	2022-02-11 16:00:41.798013+00	2022-02-11 16:00:41.798013+00	2022-02-11 16:00:41.798013+00	{"size": 36096, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
7389d3ee-b2ac-429e-a5b6-1e335a290f88	web-app	grifon/in/audio/1644595566089.mp3	\N	2022-02-11 16:06:07.558739+00	2022-02-11 16:06:07.558739+00	2022-02-11 16:06:07.558739+00	{"size": 41472, "mimetype": "audio/mpeg", "cacheControl": "max-age=3600"}
\.


--
-- Name: refresh_tokens_id_seq; Type: SEQUENCE SET; Schema: auth; Owner: supabase_auth_admin
--

SELECT pg_catalog.setval('auth.refresh_tokens_id_seq', 1, false);


--
-- Name: vne_actionfiles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_actionfiles_id_seq', 242, true);


--
-- Name: vne_actionreminders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_actionreminders_id_seq', 7, true);


--
-- Name: vne_actions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_actions_id_seq', 165, true);


--
-- Name: vne_actionstatuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_actionstatuses_id_seq', 2, true);


--
-- Name: vne_actiontags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_actiontags_id_seq', 12, true);


--
-- Name: vne_admingroups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_admingroups_id_seq', 1, false);


--
-- Name: vne_admins_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_admins_id_seq', 1, false);


--
-- Name: vne_chatmessages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_chatmessages_id_seq', 226, true);


--
-- Name: vne_companies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_companies_id_seq', 1, true);


--
-- Name: vne_contractorgroups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_contractorgroups_id_seq', 3, true);


--
-- Name: vne_contractors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_contractors_id_seq', 122, true);


--
-- Name: vne_dealdates_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_dealdates_id_seq', 14, true);


--
-- Name: vne_deals_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_deals_id_seq', 265, true);


--
-- Name: vne_employeeactions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_employeeactions_id_seq', 4, true);


--
-- Name: vne_employeegroups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_employeegroups_id_seq', 2, true);


--
-- Name: vne_employees_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_employees_id_seq', 1, true);


--
-- Name: vne_mailtemplates_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_mailtemplates_id_seq', 1, true);


--
-- Name: vne_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_settings_id_seq', 14, true);


--
-- Name: vne_verifications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vne_verifications_id_seq', 33, true);


--
-- Name: subscription_id_seq; Type: SEQUENCE SET; Schema: realtime; Owner: supabase_admin
--

SELECT pg_catalog.setval('realtime.subscription_id_seq', 1, false);


--
-- Name: audit_log_entries audit_log_entries_pkey; Type: CONSTRAINT; Schema: auth; Owner: supabase_auth_admin
--

ALTER TABLE ONLY auth.audit_log_entries
    ADD CONSTRAINT audit_log_entries_pkey PRIMARY KEY (id);


--
-- Name: identities identities_pkey; Type: CONSTRAINT; Schema: auth; Owner: supabase_auth_admin
--

ALTER TABLE ONLY auth.identities
    ADD CONSTRAINT identities_pkey PRIMARY KEY (provider, id);


--
-- Name: instances instances_pkey; Type: CONSTRAINT; Schema: auth; Owner: supabase_auth_admin
--

ALTER TABLE ONLY auth.instances
    ADD CONSTRAINT instances_pkey PRIMARY KEY (id);


--
-- Name: refresh_tokens refresh_tokens_pkey; Type: CONSTRAINT; Schema: auth; Owner: supabase_auth_admin
--

ALTER TABLE ONLY auth.refresh_tokens
    ADD CONSTRAINT refresh_tokens_pkey PRIMARY KEY (id);


--
-- Name: refresh_tokens refresh_tokens_token_unique; Type: CONSTRAINT; Schema: auth; Owner: supabase_auth_admin
--

ALTER TABLE ONLY auth.refresh_tokens
    ADD CONSTRAINT refresh_tokens_token_unique UNIQUE (token);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: auth; Owner: supabase_auth_admin
--

ALTER TABLE ONLY auth.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: auth; Owner: supabase_auth_admin
--

ALTER TABLE ONLY auth.users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_phone_key; Type: CONSTRAINT; Schema: auth; Owner: supabase_auth_admin
--

ALTER TABLE ONLY auth.users
    ADD CONSTRAINT users_phone_key UNIQUE (phone);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: auth; Owner: supabase_auth_admin
--

ALTER TABLE ONLY auth.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: vne_settings PK_00e1517e9faaaba0f525bcb590e; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_settings
    ADD CONSTRAINT "PK_00e1517e9faaaba0f525bcb590e" PRIMARY KEY (id);


--
-- Name: vne_mailtemplates PK_0ad69d17564005feccf82893808; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_mailtemplates
    ADD CONSTRAINT "PK_0ad69d17564005feccf82893808" PRIMARY KEY (id);


--
-- Name: vne_admins PK_0e169cb063501c963352dfbaaac; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_admins
    ADD CONSTRAINT "PK_0e169cb063501c963352dfbaaac" PRIMARY KEY (id);


--
-- Name: vne_chatmessages PK_0ebae6b7236b2baf0ae835e7b13; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_chatmessages
    ADD CONSTRAINT "PK_0ebae6b7236b2baf0ae835e7b13" PRIMARY KEY (id);


--
-- Name: vne_actions PK_1023c9070c8d71995ffe7acaf17; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actions
    ADD CONSTRAINT "PK_1023c9070c8d71995ffe7acaf17" PRIMARY KEY (id);


--
-- Name: vne_actions_actiontags PK_23e5e6b2643d728ecd8c9a1d1f9; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actions_actiontags
    ADD CONSTRAINT "PK_23e5e6b2643d728ecd8c9a1d1f9" PRIMARY KEY (action_id, actiontag_id);


--
-- Name: vne_verifications PK_3fc019d7efa36c70725995fe6f0; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_verifications
    ADD CONSTRAINT "PK_3fc019d7efa36c70725995fe6f0" PRIMARY KEY (id);


--
-- Name: vne_contractorgroups PK_71138e9515ebc4464031a96b325; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_contractorgroups
    ADD CONSTRAINT "PK_71138e9515ebc4464031a96b325" PRIMARY KEY (id);


--
-- Name: vne_actiontags PK_7425a3d198ba3cfe3f0f91cb267; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actiontags
    ADD CONSTRAINT "PK_7425a3d198ba3cfe3f0f91cb267" PRIMARY KEY (id);


--
-- Name: vne_employeeactions PK_76068508146b979c9500f18c71e; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_employeeactions
    ADD CONSTRAINT "PK_76068508146b979c9500f18c71e" PRIMARY KEY (id);


--
-- Name: vne_admingroups PK_885fdcbc51834c0641662d5d550; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_admingroups
    ADD CONSTRAINT "PK_885fdcbc51834c0641662d5d550" PRIMARY KEY (id);


--
-- Name: vne_deals PK_9932219d3b7dc4cb3ba2f3c2fd8; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_deals
    ADD CONSTRAINT "PK_9932219d3b7dc4cb3ba2f3c2fd8" PRIMARY KEY (id);


--
-- Name: vne_dealdates PK_a2658cae01033e741c0419f50e7; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_dealdates
    ADD CONSTRAINT "PK_a2658cae01033e741c0419f50e7" PRIMARY KEY (id);


--
-- Name: vne_employeegroups PK_a9236196314a85555038fa15f8a; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_employeegroups
    ADD CONSTRAINT "PK_a9236196314a85555038fa15f8a" PRIMARY KEY (id);


--
-- Name: vne_employees PK_be6da89c037f846b6b791a35f8b; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_employees
    ADD CONSTRAINT "PK_be6da89c037f846b6b791a35f8b" PRIMARY KEY (id);


--
-- Name: vne_contractors_contractorgroups PK_ccf8da97393697e65c34cdb6cda; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_contractors_contractorgroups
    ADD CONSTRAINT "PK_ccf8da97393697e65c34cdb6cda" PRIMARY KEY (contractor_id, contractorgroup_id);


--
-- Name: vne_actionfiles PK_d58e42699d255cc02622ea866c2; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actionfiles
    ADD CONSTRAINT "PK_d58e42699d255cc02622ea866c2" PRIMARY KEY (id);


--
-- Name: vne_actionreminders PK_f0149e23eb6bcb1e7c7f03347fe; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actionreminders
    ADD CONSTRAINT "PK_f0149e23eb6bcb1e7c7f03347fe" PRIMARY KEY (id);


--
-- Name: vne_companies PK_f4c7e028e70ef90d28e237e3890; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_companies
    ADD CONSTRAINT "PK_f4c7e028e70ef90d28e237e3890" PRIMARY KEY (id);


--
-- Name: vne_actionstatuses PK_fa0369794bdbe2f1f36da48cc7a; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actionstatuses
    ADD CONSTRAINT "PK_fa0369794bdbe2f1f36da48cc7a" PRIMARY KEY (id);


--
-- Name: vne_contractors PK_fdb5e11dede35e74ef3c48664f3; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_contractors
    ADD CONSTRAINT "PK_fdb5e11dede35e74ef3c48664f3" PRIMARY KEY (id);


--
-- Name: vne_admins UQ_0ebf4ce544e97dc18ccf67dadb4; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_admins
    ADD CONSTRAINT "UQ_0ebf4ce544e97dc18ccf67dadb4" UNIQUE (email);


--
-- Name: vne_employees UQ_752fad6244eb8729bba8e6558da; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_employees
    ADD CONSTRAINT "UQ_752fad6244eb8729bba8e6558da" UNIQUE (email);


--
-- Name: vne_mailtemplates UQ_8dcdd7fca3bc0ba922e66e6e632; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_mailtemplates
    ADD CONSTRAINT "UQ_8dcdd7fca3bc0ba922e66e6e632" UNIQUE (name);


--
-- Name: vne_settings UQ_961714c023daa0f447930913e0d; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_settings
    ADD CONSTRAINT "UQ_961714c023daa0f447930913e0d" UNIQUE (p);


--
-- Name: vne_companies UQ_9e46919b10c3d8984a61d543f84; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_companies
    ADD CONSTRAINT "UQ_9e46919b10c3d8984a61d543f84" UNIQUE (email);


--
-- Name: subscription pk_subscription; Type: CONSTRAINT; Schema: realtime; Owner: supabase_admin
--

ALTER TABLE ONLY realtime.subscription
    ADD CONSTRAINT pk_subscription PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: realtime; Owner: supabase_admin
--

ALTER TABLE ONLY realtime.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: buckets buckets_pkey; Type: CONSTRAINT; Schema: storage; Owner: supabase_storage_admin
--

ALTER TABLE ONLY storage.buckets
    ADD CONSTRAINT buckets_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_name_key; Type: CONSTRAINT; Schema: storage; Owner: supabase_storage_admin
--

ALTER TABLE ONLY storage.migrations
    ADD CONSTRAINT migrations_name_key UNIQUE (name);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: storage; Owner: supabase_storage_admin
--

ALTER TABLE ONLY storage.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: objects objects_pkey; Type: CONSTRAINT; Schema: storage; Owner: supabase_storage_admin
--

ALTER TABLE ONLY storage.objects
    ADD CONSTRAINT objects_pkey PRIMARY KEY (id);


--
-- Name: audit_logs_instance_id_idx; Type: INDEX; Schema: auth; Owner: supabase_auth_admin
--

CREATE INDEX audit_logs_instance_id_idx ON auth.audit_log_entries USING btree (instance_id);


--
-- Name: identities_user_id_idx; Type: INDEX; Schema: auth; Owner: supabase_auth_admin
--

CREATE INDEX identities_user_id_idx ON auth.identities USING btree (user_id);


--
-- Name: refresh_tokens_instance_id_idx; Type: INDEX; Schema: auth; Owner: supabase_auth_admin
--

CREATE INDEX refresh_tokens_instance_id_idx ON auth.refresh_tokens USING btree (instance_id);


--
-- Name: refresh_tokens_instance_id_user_id_idx; Type: INDEX; Schema: auth; Owner: supabase_auth_admin
--

CREATE INDEX refresh_tokens_instance_id_user_id_idx ON auth.refresh_tokens USING btree (instance_id, user_id);


--
-- Name: refresh_tokens_parent_idx; Type: INDEX; Schema: auth; Owner: supabase_auth_admin
--

CREATE INDEX refresh_tokens_parent_idx ON auth.refresh_tokens USING btree (parent);


--
-- Name: refresh_tokens_token_idx; Type: INDEX; Schema: auth; Owner: supabase_auth_admin
--

CREATE INDEX refresh_tokens_token_idx ON auth.refresh_tokens USING btree (token);


--
-- Name: users_instance_id_email_idx; Type: INDEX; Schema: auth; Owner: supabase_auth_admin
--

CREATE INDEX users_instance_id_email_idx ON auth.users USING btree (instance_id, lower((email)::text));


--
-- Name: users_instance_id_idx; Type: INDEX; Schema: auth; Owner: supabase_auth_admin
--

CREATE INDEX users_instance_id_idx ON auth.users USING btree (instance_id);


--
-- Name: IDX_0db9d1b8d6b847e0457bc61b21; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_0db9d1b8d6b847e0457bc61b21" ON public.vne_contractors_contractorgroups USING btree (contractorgroup_id);


--
-- Name: IDX_2cfafc96a755b453e7c8cb1bc4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_2cfafc96a755b453e7c8cb1bc4" ON public.vne_companies USING btree (n8n_code);


--
-- Name: IDX_38001f1383b358b9e7b850ca9d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_38001f1383b358b9e7b850ca9d" ON public.vne_deals USING btree (description);


--
-- Name: IDX_536e6686f69b12c6b806449a19; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_536e6686f69b12c6b806449a19" ON public.vne_actions_actiontags USING btree (actiontag_id);


--
-- Name: IDX_596e568df8f5f9d1bb26e73703; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_596e568df8f5f9d1bb26e73703" ON public.vne_deals USING btree (name);


--
-- Name: IDX_5f1d3ff1209da29e709744b8e8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_5f1d3ff1209da29e709744b8e8" ON public.vne_actions_actiontags USING btree (action_id);


--
-- Name: IDX_6e70e520126d2ad23ee6e95019; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_6e70e520126d2ad23ee6e95019" ON public.vne_contractors_contractorgroups USING btree (contractor_id);


--
-- Name: IDX_75eac2bfbebf888d0dc867aaf5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_75eac2bfbebf888d0dc867aaf5" ON public.vne_companies USING btree (name);


--
-- Name: IDX_7fe4dae60a92066ca92ff26e49; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_7fe4dae60a92066ca92ff26e49" ON public.vne_verifications USING btree (code);


--
-- Name: IDX_8f14a1d4a619d5fc71c61c8c7b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_8f14a1d4a619d5fc71c61c8c7b" ON public.vne_contractors USING btree (name);


--
-- Name: IDX_946c7ac422f293b516e7b6b2e3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_946c7ac422f293b516e7b6b2e3" ON public.vne_chatmessages USING btree (outer_id);


--
-- Name: IDX_a74428463d60d109fd0541c179; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_a74428463d60d109fd0541c179" ON public.vne_verifications USING btree (email);


--
-- Name: IDX_b61bb701e09961e9e041eb989b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_b61bb701e09961e9e041eb989b" ON public.vne_deals USING btree (created_at);


--
-- Name: IDX_bf94470722a9df45d78bb67a11; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "IDX_bf94470722a9df45d78bb67a11" ON public.vne_deals USING btree (closed);


--
-- Name: ix_realtime_subscription_entity; Type: INDEX; Schema: realtime; Owner: supabase_admin
--

CREATE INDEX ix_realtime_subscription_entity ON realtime.subscription USING hash (entity);


--
-- Name: subscription_subscription_id_entity_filters_key; Type: INDEX; Schema: realtime; Owner: supabase_admin
--

CREATE UNIQUE INDEX subscription_subscription_id_entity_filters_key ON realtime.subscription USING btree (subscription_id, entity, filters);


--
-- Name: bname; Type: INDEX; Schema: storage; Owner: supabase_storage_admin
--

CREATE UNIQUE INDEX bname ON storage.buckets USING btree (name);


--
-- Name: bucketid_objname; Type: INDEX; Schema: storage; Owner: supabase_storage_admin
--

CREATE UNIQUE INDEX bucketid_objname ON storage.objects USING btree (bucket_id, name);


--
-- Name: name_prefix_search; Type: INDEX; Schema: storage; Owner: supabase_storage_admin
--

CREATE INDEX name_prefix_search ON storage.objects USING btree (name text_pattern_ops);


--
-- Name: subscription tr_check_filters; Type: TRIGGER; Schema: realtime; Owner: supabase_admin
--

CREATE TRIGGER tr_check_filters BEFORE INSERT OR UPDATE ON realtime.subscription FOR EACH ROW EXECUTE FUNCTION realtime.subscription_check_filters();


--
-- Name: identities identities_user_id_fkey; Type: FK CONSTRAINT; Schema: auth; Owner: supabase_auth_admin
--

ALTER TABLE ONLY auth.identities
    ADD CONSTRAINT identities_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth.users(id) ON DELETE CASCADE;


--
-- Name: refresh_tokens refresh_tokens_parent_fkey; Type: FK CONSTRAINT; Schema: auth; Owner: supabase_auth_admin
--

ALTER TABLE ONLY auth.refresh_tokens
    ADD CONSTRAINT refresh_tokens_parent_fkey FOREIGN KEY (parent) REFERENCES auth.refresh_tokens(token);


--
-- Name: vne_employees FK_01a10169bf6f193ff640dc3ed2e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_employees
    ADD CONSTRAINT "FK_01a10169bf6f193ff640dc3ed2e" FOREIGN KEY (company_id) REFERENCES public.vne_companies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_dealdates FK_0729ddd53f456d87743346cc9e8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_dealdates
    ADD CONSTRAINT "FK_0729ddd53f456d87743346cc9e8" FOREIGN KEY (deal_id) REFERENCES public.vne_deals(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_actions FK_09ebcd40c2a0018b3e868d55345; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actions
    ADD CONSTRAINT "FK_09ebcd40c2a0018b3e868d55345" FOREIGN KEY (employee_id) REFERENCES public.vne_employees(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: vne_employeeactions FK_0c61b8c1014eb04f14f47919207; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_employeeactions
    ADD CONSTRAINT "FK_0c61b8c1014eb04f14f47919207" FOREIGN KEY (group_id) REFERENCES public.vne_employeegroups(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: vne_contractors_contractorgroups FK_0db9d1b8d6b847e0457bc61b215; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_contractors_contractorgroups
    ADD CONSTRAINT "FK_0db9d1b8d6b847e0457bc61b215" FOREIGN KEY (contractorgroup_id) REFERENCES public.vne_contractorgroups(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_deals FK_1263992d15631b3160628931be5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_deals
    ADD CONSTRAINT "FK_1263992d15631b3160628931be5" FOREIGN KEY (company_id) REFERENCES public.vne_companies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_actionfiles FK_3254029afa6ee73dc02e92c7b4f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actionfiles
    ADD CONSTRAINT "FK_3254029afa6ee73dc02e92c7b4f" FOREIGN KEY (action_id) REFERENCES public.vne_actions(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_deals FK_3bdc336e273ad705b03bdd30f26; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_deals
    ADD CONSTRAINT "FK_3bdc336e273ad705b03bdd30f26" FOREIGN KEY (client_contractor_id) REFERENCES public.vne_contractors(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: vne_chatmessages FK_4d5bb946ecd82a017ae597c0da9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_chatmessages
    ADD CONSTRAINT "FK_4d5bb946ecd82a017ae597c0da9" FOREIGN KEY (contractor_id) REFERENCES public.vne_contractors(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_actions_actiontags FK_536e6686f69b12c6b806449a199; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actions_actiontags
    ADD CONSTRAINT "FK_536e6686f69b12c6b806449a199" FOREIGN KEY (actiontag_id) REFERENCES public.vne_actiontags(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_deals FK_5810ca45a7616c33cfdccbfac45; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_deals
    ADD CONSTRAINT "FK_5810ca45a7616c33cfdccbfac45" FOREIGN KEY (partner_contractor_id) REFERENCES public.vne_contractors(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: vne_actions_actiontags FK_5f1d3ff1209da29e709744b8e87; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actions_actiontags
    ADD CONSTRAINT "FK_5f1d3ff1209da29e709744b8e87" FOREIGN KEY (action_id) REFERENCES public.vne_actions(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_actions FK_63528f269fb27ed81f4f0384154; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actions
    ADD CONSTRAINT "FK_63528f269fb27ed81f4f0384154" FOREIGN KEY (actionstatus_id) REFERENCES public.vne_actionstatuses(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: vne_actions FK_6472956aac3aab8c5422dc394a0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actions
    ADD CONSTRAINT "FK_6472956aac3aab8c5422dc394a0" FOREIGN KEY (deal_id) REFERENCES public.vne_deals(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_contractors_contractorgroups FK_6e70e520126d2ad23ee6e95019d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_contractors_contractorgroups
    ADD CONSTRAINT "FK_6e70e520126d2ad23ee6e95019d" FOREIGN KEY (contractor_id) REFERENCES public.vne_contractors(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_deals FK_87da7884917b64f1d177015ec8b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_deals
    ADD CONSTRAINT "FK_87da7884917b64f1d177015ec8b" FOREIGN KEY (contact_contractor_id) REFERENCES public.vne_contractors(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: vne_actions FK_b9a78f886f483698808c771fcb5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actions
    ADD CONSTRAINT "FK_b9a78f886f483698808c771fcb5" FOREIGN KEY (company_id) REFERENCES public.vne_companies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_actions FK_c25f7df88a89bb3e6105f85768b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actions
    ADD CONSTRAINT "FK_c25f7df88a89bb3e6105f85768b" FOREIGN KEY (employeeaction_id) REFERENCES public.vne_employeeactions(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: vne_admins FK_dbd643803fd2311e34b4360eb56; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_admins
    ADD CONSTRAINT "FK_dbd643803fd2311e34b4360eb56" FOREIGN KEY (group_id) REFERENCES public.vne_admingroups(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_employees FK_f024eb9361db6f3519d43abb26b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_employees
    ADD CONSTRAINT "FK_f024eb9361db6f3519d43abb26b" FOREIGN KEY (group_id) REFERENCES public.vne_employeegroups(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: vne_actions FK_f60b03a84770d5430440a9f7f70; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actions
    ADD CONSTRAINT "FK_f60b03a84770d5430440a9f7f70" FOREIGN KEY (chatmessage_id) REFERENCES public.vne_chatmessages(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_actionreminders FK_f7b387e564273f57e0c4a119055; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_actionreminders
    ADD CONSTRAINT "FK_f7b387e564273f57e0c4a119055" FOREIGN KEY (action_id) REFERENCES public.vne_actions(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vne_contractors FK_ff9eef6766d82db3898500458c2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vne_contractors
    ADD CONSTRAINT "FK_ff9eef6766d82db3898500458c2" FOREIGN KEY (company_id) REFERENCES public.vne_companies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: buckets buckets_owner_fkey; Type: FK CONSTRAINT; Schema: storage; Owner: supabase_storage_admin
--

ALTER TABLE ONLY storage.buckets
    ADD CONSTRAINT buckets_owner_fkey FOREIGN KEY (owner) REFERENCES auth.users(id);


--
-- Name: objects objects_bucketId_fkey; Type: FK CONSTRAINT; Schema: storage; Owner: supabase_storage_admin
--

ALTER TABLE ONLY storage.objects
    ADD CONSTRAINT "objects_bucketId_fkey" FOREIGN KEY (bucket_id) REFERENCES storage.buckets(id);


--
-- Name: objects objects_owner_fkey; Type: FK CONSTRAINT; Schema: storage; Owner: supabase_storage_admin
--

ALTER TABLE ONLY storage.objects
    ADD CONSTRAINT objects_owner_fkey FOREIGN KEY (owner) REFERENCES auth.users(id);


--
-- Name: buckets; Type: ROW SECURITY; Schema: storage; Owner: supabase_storage_admin
--

ALTER TABLE storage.buckets ENABLE ROW LEVEL SECURITY;

--
-- Name: objects free insert to web-app; Type: POLICY; Schema: storage; Owner: supabase_storage_admin
--

CREATE POLICY "free insert to web-app" ON storage.objects FOR INSERT WITH CHECK ((bucket_id = 'web-app'::text));


--
-- Name: migrations; Type: ROW SECURITY; Schema: storage; Owner: supabase_storage_admin
--

ALTER TABLE storage.migrations ENABLE ROW LEVEL SECURITY;

--
-- Name: objects; Type: ROW SECURITY; Schema: storage; Owner: supabase_storage_admin
--

ALTER TABLE storage.objects ENABLE ROW LEVEL SECURITY;

--
-- Name: supabase_realtime; Type: PUBLICATION; Schema: -; Owner: postgres
--

CREATE PUBLICATION supabase_realtime WITH (publish = 'insert, update, delete, truncate');


ALTER PUBLICATION supabase_realtime OWNER TO postgres;

--
-- Name: SCHEMA auth; Type: ACL; Schema: -; Owner: supabase_admin
--

GRANT USAGE ON SCHEMA auth TO anon;
GRANT USAGE ON SCHEMA auth TO authenticated;
GRANT USAGE ON SCHEMA auth TO service_role;
GRANT ALL ON SCHEMA auth TO supabase_auth_admin;
GRANT ALL ON SCHEMA auth TO dashboard_user;
GRANT ALL ON SCHEMA auth TO postgres;


--
-- Name: SCHEMA extensions; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA extensions TO anon;
GRANT USAGE ON SCHEMA extensions TO authenticated;
GRANT USAGE ON SCHEMA extensions TO service_role;
GRANT ALL ON SCHEMA extensions TO dashboard_user;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA public TO anon;
GRANT USAGE ON SCHEMA public TO authenticated;
GRANT USAGE ON SCHEMA public TO service_role;


--
-- Name: SCHEMA realtime; Type: ACL; Schema: -; Owner: supabase_admin
--

GRANT USAGE ON SCHEMA realtime TO postgres;


--
-- Name: SCHEMA storage; Type: ACL; Schema: -; Owner: supabase_admin
--

GRANT ALL ON SCHEMA storage TO postgres;
GRANT USAGE ON SCHEMA storage TO anon;
GRANT USAGE ON SCHEMA storage TO authenticated;
GRANT USAGE ON SCHEMA storage TO service_role;
GRANT ALL ON SCHEMA storage TO supabase_storage_admin;
GRANT ALL ON SCHEMA storage TO dashboard_user;


--
-- Name: FUNCTION email(); Type: ACL; Schema: auth; Owner: supabase_auth_admin
--

GRANT ALL ON FUNCTION auth.email() TO dashboard_user;


--
-- Name: FUNCTION role(); Type: ACL; Schema: auth; Owner: supabase_auth_admin
--

GRANT ALL ON FUNCTION auth.role() TO dashboard_user;


--
-- Name: FUNCTION uid(); Type: ACL; Schema: auth; Owner: supabase_auth_admin
--

GRANT ALL ON FUNCTION auth.uid() TO dashboard_user;


--
-- Name: FUNCTION algorithm_sign(signables text, secret text, algorithm text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.algorithm_sign(signables text, secret text, algorithm text) TO dashboard_user;


--
-- Name: FUNCTION armor(bytea); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.armor(bytea) TO dashboard_user;


--
-- Name: FUNCTION armor(bytea, text[], text[]); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.armor(bytea, text[], text[]) TO dashboard_user;


--
-- Name: FUNCTION crypt(text, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.crypt(text, text) TO dashboard_user;


--
-- Name: FUNCTION dearmor(text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.dearmor(text) TO dashboard_user;


--
-- Name: FUNCTION decrypt(bytea, bytea, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.decrypt(bytea, bytea, text) TO dashboard_user;


--
-- Name: FUNCTION decrypt_iv(bytea, bytea, bytea, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.decrypt_iv(bytea, bytea, bytea, text) TO dashboard_user;


--
-- Name: FUNCTION digest(bytea, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.digest(bytea, text) TO dashboard_user;


--
-- Name: FUNCTION digest(text, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.digest(text, text) TO dashboard_user;


--
-- Name: FUNCTION encrypt(bytea, bytea, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.encrypt(bytea, bytea, text) TO dashboard_user;


--
-- Name: FUNCTION encrypt_iv(bytea, bytea, bytea, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.encrypt_iv(bytea, bytea, bytea, text) TO dashboard_user;


--
-- Name: FUNCTION gen_random_bytes(integer); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.gen_random_bytes(integer) TO dashboard_user;


--
-- Name: FUNCTION gen_random_uuid(); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.gen_random_uuid() TO dashboard_user;


--
-- Name: FUNCTION gen_salt(text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.gen_salt(text) TO dashboard_user;


--
-- Name: FUNCTION gen_salt(text, integer); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.gen_salt(text, integer) TO dashboard_user;


--
-- Name: FUNCTION grant_pg_cron_access(); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.grant_pg_cron_access() TO dashboard_user;


--
-- Name: FUNCTION grant_pg_net_access(); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.grant_pg_net_access() TO dashboard_user;


--
-- Name: FUNCTION hmac(bytea, bytea, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.hmac(bytea, bytea, text) TO dashboard_user;


--
-- Name: FUNCTION hmac(text, text, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.hmac(text, text, text) TO dashboard_user;


--
-- Name: FUNCTION pg_stat_statements(showtext boolean, OUT userid oid, OUT dbid oid, OUT toplevel boolean, OUT queryid bigint, OUT query text, OUT plans bigint, OUT total_plan_time double precision, OUT min_plan_time double precision, OUT max_plan_time double precision, OUT mean_plan_time double precision, OUT stddev_plan_time double precision, OUT calls bigint, OUT total_exec_time double precision, OUT min_exec_time double precision, OUT max_exec_time double precision, OUT mean_exec_time double precision, OUT stddev_exec_time double precision, OUT rows bigint, OUT shared_blks_hit bigint, OUT shared_blks_read bigint, OUT shared_blks_dirtied bigint, OUT shared_blks_written bigint, OUT local_blks_hit bigint, OUT local_blks_read bigint, OUT local_blks_dirtied bigint, OUT local_blks_written bigint, OUT temp_blks_read bigint, OUT temp_blks_written bigint, OUT blk_read_time double precision, OUT blk_write_time double precision, OUT wal_records bigint, OUT wal_fpi bigint, OUT wal_bytes numeric); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pg_stat_statements(showtext boolean, OUT userid oid, OUT dbid oid, OUT toplevel boolean, OUT queryid bigint, OUT query text, OUT plans bigint, OUT total_plan_time double precision, OUT min_plan_time double precision, OUT max_plan_time double precision, OUT mean_plan_time double precision, OUT stddev_plan_time double precision, OUT calls bigint, OUT total_exec_time double precision, OUT min_exec_time double precision, OUT max_exec_time double precision, OUT mean_exec_time double precision, OUT stddev_exec_time double precision, OUT rows bigint, OUT shared_blks_hit bigint, OUT shared_blks_read bigint, OUT shared_blks_dirtied bigint, OUT shared_blks_written bigint, OUT local_blks_hit bigint, OUT local_blks_read bigint, OUT local_blks_dirtied bigint, OUT local_blks_written bigint, OUT temp_blks_read bigint, OUT temp_blks_written bigint, OUT blk_read_time double precision, OUT blk_write_time double precision, OUT wal_records bigint, OUT wal_fpi bigint, OUT wal_bytes numeric) TO dashboard_user;


--
-- Name: FUNCTION pg_stat_statements_info(OUT dealloc bigint, OUT stats_reset timestamp with time zone); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pg_stat_statements_info(OUT dealloc bigint, OUT stats_reset timestamp with time zone) TO dashboard_user;


--
-- Name: FUNCTION pg_stat_statements_reset(userid oid, dbid oid, queryid bigint); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pg_stat_statements_reset(userid oid, dbid oid, queryid bigint) TO dashboard_user;


--
-- Name: FUNCTION pgp_armor_headers(text, OUT key text, OUT value text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_armor_headers(text, OUT key text, OUT value text) TO dashboard_user;


--
-- Name: FUNCTION pgp_key_id(bytea); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_key_id(bytea) TO dashboard_user;


--
-- Name: FUNCTION pgp_pub_decrypt(bytea, bytea); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_pub_decrypt(bytea, bytea) TO dashboard_user;


--
-- Name: FUNCTION pgp_pub_decrypt(bytea, bytea, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_pub_decrypt(bytea, bytea, text) TO dashboard_user;


--
-- Name: FUNCTION pgp_pub_decrypt(bytea, bytea, text, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_pub_decrypt(bytea, bytea, text, text) TO dashboard_user;


--
-- Name: FUNCTION pgp_pub_decrypt_bytea(bytea, bytea); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_pub_decrypt_bytea(bytea, bytea) TO dashboard_user;


--
-- Name: FUNCTION pgp_pub_decrypt_bytea(bytea, bytea, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_pub_decrypt_bytea(bytea, bytea, text) TO dashboard_user;


--
-- Name: FUNCTION pgp_pub_decrypt_bytea(bytea, bytea, text, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_pub_decrypt_bytea(bytea, bytea, text, text) TO dashboard_user;


--
-- Name: FUNCTION pgp_pub_encrypt(text, bytea); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_pub_encrypt(text, bytea) TO dashboard_user;


--
-- Name: FUNCTION pgp_pub_encrypt(text, bytea, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_pub_encrypt(text, bytea, text) TO dashboard_user;


--
-- Name: FUNCTION pgp_pub_encrypt_bytea(bytea, bytea); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_pub_encrypt_bytea(bytea, bytea) TO dashboard_user;


--
-- Name: FUNCTION pgp_pub_encrypt_bytea(bytea, bytea, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_pub_encrypt_bytea(bytea, bytea, text) TO dashboard_user;


--
-- Name: FUNCTION pgp_sym_decrypt(bytea, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_sym_decrypt(bytea, text) TO dashboard_user;


--
-- Name: FUNCTION pgp_sym_decrypt(bytea, text, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_sym_decrypt(bytea, text, text) TO dashboard_user;


--
-- Name: FUNCTION pgp_sym_decrypt_bytea(bytea, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_sym_decrypt_bytea(bytea, text) TO dashboard_user;


--
-- Name: FUNCTION pgp_sym_decrypt_bytea(bytea, text, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_sym_decrypt_bytea(bytea, text, text) TO dashboard_user;


--
-- Name: FUNCTION pgp_sym_encrypt(text, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_sym_encrypt(text, text) TO dashboard_user;


--
-- Name: FUNCTION pgp_sym_encrypt(text, text, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_sym_encrypt(text, text, text) TO dashboard_user;


--
-- Name: FUNCTION pgp_sym_encrypt_bytea(bytea, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_sym_encrypt_bytea(bytea, text) TO dashboard_user;


--
-- Name: FUNCTION pgp_sym_encrypt_bytea(bytea, text, text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.pgp_sym_encrypt_bytea(bytea, text, text) TO dashboard_user;


--
-- Name: FUNCTION sign(payload json, secret text, algorithm text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.sign(payload json, secret text, algorithm text) TO dashboard_user;


--
-- Name: FUNCTION try_cast_double(inp text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.try_cast_double(inp text) TO dashboard_user;


--
-- Name: FUNCTION url_decode(data text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.url_decode(data text) TO dashboard_user;


--
-- Name: FUNCTION url_encode(data bytea); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.url_encode(data bytea) TO dashboard_user;


--
-- Name: FUNCTION uuid_generate_v1(); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.uuid_generate_v1() TO dashboard_user;


--
-- Name: FUNCTION uuid_generate_v1mc(); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.uuid_generate_v1mc() TO dashboard_user;


--
-- Name: FUNCTION uuid_generate_v3(namespace uuid, name text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.uuid_generate_v3(namespace uuid, name text) TO dashboard_user;


--
-- Name: FUNCTION uuid_generate_v4(); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.uuid_generate_v4() TO dashboard_user;


--
-- Name: FUNCTION uuid_generate_v5(namespace uuid, name text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.uuid_generate_v5(namespace uuid, name text) TO dashboard_user;


--
-- Name: FUNCTION uuid_nil(); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.uuid_nil() TO dashboard_user;


--
-- Name: FUNCTION uuid_ns_dns(); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.uuid_ns_dns() TO dashboard_user;


--
-- Name: FUNCTION uuid_ns_oid(); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.uuid_ns_oid() TO dashboard_user;


--
-- Name: FUNCTION uuid_ns_url(); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.uuid_ns_url() TO dashboard_user;


--
-- Name: FUNCTION uuid_ns_x500(); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.uuid_ns_x500() TO dashboard_user;


--
-- Name: FUNCTION verify(token text, secret text, algorithm text); Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON FUNCTION extensions.verify(token text, secret text, algorithm text) TO dashboard_user;


--
-- Name: FUNCTION get_auth(p_usename text); Type: ACL; Schema: pgbouncer; Owner: postgres
--

REVOKE ALL ON FUNCTION pgbouncer.get_auth(p_usename text) FROM PUBLIC;
GRANT ALL ON FUNCTION pgbouncer.get_auth(p_usename text) TO pgbouncer;


--
-- Name: FUNCTION apply_rls(wal jsonb, max_record_bytes integer); Type: ACL; Schema: realtime; Owner: supabase_admin
--

GRANT ALL ON FUNCTION realtime.apply_rls(wal jsonb, max_record_bytes integer) TO postgres;
GRANT ALL ON FUNCTION realtime.apply_rls(wal jsonb, max_record_bytes integer) TO dashboard_user;


--
-- Name: FUNCTION build_prepared_statement_sql(prepared_statement_name text, entity regclass, columns realtime.wal_column[]); Type: ACL; Schema: realtime; Owner: supabase_admin
--

GRANT ALL ON FUNCTION realtime.build_prepared_statement_sql(prepared_statement_name text, entity regclass, columns realtime.wal_column[]) TO postgres;
GRANT ALL ON FUNCTION realtime.build_prepared_statement_sql(prepared_statement_name text, entity regclass, columns realtime.wal_column[]) TO dashboard_user;


--
-- Name: FUNCTION "cast"(val text, type_ regtype); Type: ACL; Schema: realtime; Owner: supabase_admin
--

GRANT ALL ON FUNCTION realtime."cast"(val text, type_ regtype) TO postgres;
GRANT ALL ON FUNCTION realtime."cast"(val text, type_ regtype) TO dashboard_user;


--
-- Name: FUNCTION check_equality_op(op realtime.equality_op, type_ regtype, val_1 text, val_2 text); Type: ACL; Schema: realtime; Owner: supabase_admin
--

GRANT ALL ON FUNCTION realtime.check_equality_op(op realtime.equality_op, type_ regtype, val_1 text, val_2 text) TO postgres;
GRANT ALL ON FUNCTION realtime.check_equality_op(op realtime.equality_op, type_ regtype, val_1 text, val_2 text) TO dashboard_user;


--
-- Name: FUNCTION is_visible_through_filters(columns realtime.wal_column[], filters realtime.user_defined_filter[]); Type: ACL; Schema: realtime; Owner: supabase_admin
--

GRANT ALL ON FUNCTION realtime.is_visible_through_filters(columns realtime.wal_column[], filters realtime.user_defined_filter[]) TO postgres;
GRANT ALL ON FUNCTION realtime.is_visible_through_filters(columns realtime.wal_column[], filters realtime.user_defined_filter[]) TO dashboard_user;


--
-- Name: FUNCTION quote_wal2json(entity regclass); Type: ACL; Schema: realtime; Owner: supabase_admin
--

GRANT ALL ON FUNCTION realtime.quote_wal2json(entity regclass) TO postgres;
GRANT ALL ON FUNCTION realtime.quote_wal2json(entity regclass) TO dashboard_user;


--
-- Name: FUNCTION subscription_check_filters(); Type: ACL; Schema: realtime; Owner: supabase_admin
--

GRANT ALL ON FUNCTION realtime.subscription_check_filters() TO postgres;
GRANT ALL ON FUNCTION realtime.subscription_check_filters() TO dashboard_user;


--
-- Name: FUNCTION to_regrole(role_name text); Type: ACL; Schema: realtime; Owner: supabase_admin
--

GRANT ALL ON FUNCTION realtime.to_regrole(role_name text) TO postgres;
GRANT ALL ON FUNCTION realtime.to_regrole(role_name text) TO dashboard_user;


--
-- Name: FUNCTION extension(name text); Type: ACL; Schema: storage; Owner: supabase_storage_admin
--

GRANT ALL ON FUNCTION storage.extension(name text) TO anon;
GRANT ALL ON FUNCTION storage.extension(name text) TO authenticated;
GRANT ALL ON FUNCTION storage.extension(name text) TO service_role;
GRANT ALL ON FUNCTION storage.extension(name text) TO dashboard_user;
GRANT ALL ON FUNCTION storage.extension(name text) TO postgres;


--
-- Name: FUNCTION filename(name text); Type: ACL; Schema: storage; Owner: supabase_storage_admin
--

GRANT ALL ON FUNCTION storage.filename(name text) TO anon;
GRANT ALL ON FUNCTION storage.filename(name text) TO authenticated;
GRANT ALL ON FUNCTION storage.filename(name text) TO service_role;
GRANT ALL ON FUNCTION storage.filename(name text) TO dashboard_user;
GRANT ALL ON FUNCTION storage.filename(name text) TO postgres;


--
-- Name: FUNCTION foldername(name text); Type: ACL; Schema: storage; Owner: supabase_storage_admin
--

GRANT ALL ON FUNCTION storage.foldername(name text) TO anon;
GRANT ALL ON FUNCTION storage.foldername(name text) TO authenticated;
GRANT ALL ON FUNCTION storage.foldername(name text) TO service_role;
GRANT ALL ON FUNCTION storage.foldername(name text) TO dashboard_user;
GRANT ALL ON FUNCTION storage.foldername(name text) TO postgres;


--
-- Name: FUNCTION search(prefix text, bucketname text, limits integer, levels integer, offsets integer); Type: ACL; Schema: storage; Owner: supabase_storage_admin
--

GRANT ALL ON FUNCTION storage.search(prefix text, bucketname text, limits integer, levels integer, offsets integer) TO anon;
GRANT ALL ON FUNCTION storage.search(prefix text, bucketname text, limits integer, levels integer, offsets integer) TO authenticated;
GRANT ALL ON FUNCTION storage.search(prefix text, bucketname text, limits integer, levels integer, offsets integer) TO service_role;
GRANT ALL ON FUNCTION storage.search(prefix text, bucketname text, limits integer, levels integer, offsets integer) TO dashboard_user;
GRANT ALL ON FUNCTION storage.search(prefix text, bucketname text, limits integer, levels integer, offsets integer) TO postgres;


--
-- Name: TABLE audit_log_entries; Type: ACL; Schema: auth; Owner: supabase_auth_admin
--

GRANT ALL ON TABLE auth.audit_log_entries TO dashboard_user;
GRANT ALL ON TABLE auth.audit_log_entries TO postgres;


--
-- Name: TABLE identities; Type: ACL; Schema: auth; Owner: supabase_auth_admin
--

GRANT ALL ON TABLE auth.identities TO postgres;
GRANT ALL ON TABLE auth.identities TO dashboard_user;


--
-- Name: TABLE instances; Type: ACL; Schema: auth; Owner: supabase_auth_admin
--

GRANT ALL ON TABLE auth.instances TO dashboard_user;
GRANT ALL ON TABLE auth.instances TO postgres;


--
-- Name: TABLE refresh_tokens; Type: ACL; Schema: auth; Owner: supabase_auth_admin
--

GRANT ALL ON TABLE auth.refresh_tokens TO dashboard_user;
GRANT ALL ON TABLE auth.refresh_tokens TO postgres;


--
-- Name: SEQUENCE refresh_tokens_id_seq; Type: ACL; Schema: auth; Owner: supabase_auth_admin
--

GRANT ALL ON SEQUENCE auth.refresh_tokens_id_seq TO dashboard_user;
GRANT ALL ON SEQUENCE auth.refresh_tokens_id_seq TO postgres;


--
-- Name: TABLE schema_migrations; Type: ACL; Schema: auth; Owner: supabase_auth_admin
--

GRANT ALL ON TABLE auth.schema_migrations TO dashboard_user;
GRANT ALL ON TABLE auth.schema_migrations TO postgres;


--
-- Name: TABLE users; Type: ACL; Schema: auth; Owner: supabase_auth_admin
--

GRANT ALL ON TABLE auth.users TO dashboard_user;
GRANT ALL ON TABLE auth.users TO postgres;


--
-- Name: TABLE pg_stat_statements; Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON TABLE extensions.pg_stat_statements TO dashboard_user;


--
-- Name: TABLE pg_stat_statements_info; Type: ACL; Schema: extensions; Owner: postgres
--

GRANT ALL ON TABLE extensions.pg_stat_statements_info TO dashboard_user;


--
-- Name: TABLE typeorm_metadata; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.typeorm_metadata TO anon;
GRANT ALL ON TABLE public.typeorm_metadata TO authenticated;
GRANT ALL ON TABLE public.typeorm_metadata TO service_role;


--
-- Name: TABLE vne_actionfiles; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_actionfiles TO anon;
GRANT ALL ON TABLE public.vne_actionfiles TO authenticated;
GRANT ALL ON TABLE public.vne_actionfiles TO service_role;


--
-- Name: SEQUENCE vne_actionfiles_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_actionfiles_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_actionfiles_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_actionfiles_id_seq TO service_role;


--
-- Name: TABLE vne_actionreminders; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_actionreminders TO anon;
GRANT ALL ON TABLE public.vne_actionreminders TO authenticated;
GRANT ALL ON TABLE public.vne_actionreminders TO service_role;


--
-- Name: SEQUENCE vne_actionreminders_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_actionreminders_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_actionreminders_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_actionreminders_id_seq TO service_role;


--
-- Name: TABLE vne_actions; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_actions TO anon;
GRANT ALL ON TABLE public.vne_actions TO authenticated;
GRANT ALL ON TABLE public.vne_actions TO service_role;


--
-- Name: TABLE vne_actions_actiontags; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_actions_actiontags TO anon;
GRANT ALL ON TABLE public.vne_actions_actiontags TO authenticated;
GRANT ALL ON TABLE public.vne_actions_actiontags TO service_role;


--
-- Name: SEQUENCE vne_actions_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_actions_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_actions_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_actions_id_seq TO service_role;


--
-- Name: TABLE vne_actionstatuses; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_actionstatuses TO anon;
GRANT ALL ON TABLE public.vne_actionstatuses TO authenticated;
GRANT ALL ON TABLE public.vne_actionstatuses TO service_role;


--
-- Name: SEQUENCE vne_actionstatuses_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_actionstatuses_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_actionstatuses_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_actionstatuses_id_seq TO service_role;


--
-- Name: TABLE vne_actiontags; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_actiontags TO anon;
GRANT ALL ON TABLE public.vne_actiontags TO authenticated;
GRANT ALL ON TABLE public.vne_actiontags TO service_role;


--
-- Name: SEQUENCE vne_actiontags_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_actiontags_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_actiontags_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_actiontags_id_seq TO service_role;


--
-- Name: TABLE vne_admingroups; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_admingroups TO anon;
GRANT ALL ON TABLE public.vne_admingroups TO authenticated;
GRANT ALL ON TABLE public.vne_admingroups TO service_role;


--
-- Name: SEQUENCE vne_admingroups_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_admingroups_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_admingroups_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_admingroups_id_seq TO service_role;


--
-- Name: TABLE vne_admins; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_admins TO anon;
GRANT ALL ON TABLE public.vne_admins TO authenticated;
GRANT ALL ON TABLE public.vne_admins TO service_role;


--
-- Name: SEQUENCE vne_admins_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_admins_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_admins_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_admins_id_seq TO service_role;


--
-- Name: TABLE vne_chatmessages; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_chatmessages TO anon;
GRANT ALL ON TABLE public.vne_chatmessages TO authenticated;
GRANT ALL ON TABLE public.vne_chatmessages TO service_role;


--
-- Name: SEQUENCE vne_chatmessages_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_chatmessages_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_chatmessages_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_chatmessages_id_seq TO service_role;


--
-- Name: TABLE vne_companies; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_companies TO anon;
GRANT ALL ON TABLE public.vne_companies TO authenticated;
GRANT ALL ON TABLE public.vne_companies TO service_role;


--
-- Name: SEQUENCE vne_companies_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_companies_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_companies_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_companies_id_seq TO service_role;


--
-- Name: TABLE vne_contractorgroups; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_contractorgroups TO anon;
GRANT ALL ON TABLE public.vne_contractorgroups TO authenticated;
GRANT ALL ON TABLE public.vne_contractorgroups TO service_role;


--
-- Name: SEQUENCE vne_contractorgroups_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_contractorgroups_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_contractorgroups_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_contractorgroups_id_seq TO service_role;


--
-- Name: TABLE vne_contractors; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_contractors TO anon;
GRANT ALL ON TABLE public.vne_contractors TO authenticated;
GRANT ALL ON TABLE public.vne_contractors TO service_role;


--
-- Name: TABLE vne_contractors_contractorgroups; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_contractors_contractorgroups TO anon;
GRANT ALL ON TABLE public.vne_contractors_contractorgroups TO authenticated;
GRANT ALL ON TABLE public.vne_contractors_contractorgroups TO service_role;


--
-- Name: SEQUENCE vne_contractors_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_contractors_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_contractors_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_contractors_id_seq TO service_role;


--
-- Name: TABLE vne_dealdates; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_dealdates TO anon;
GRANT ALL ON TABLE public.vne_dealdates TO authenticated;
GRANT ALL ON TABLE public.vne_dealdates TO service_role;


--
-- Name: SEQUENCE vne_dealdates_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_dealdates_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_dealdates_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_dealdates_id_seq TO service_role;


--
-- Name: TABLE vne_deals; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_deals TO anon;
GRANT ALL ON TABLE public.vne_deals TO authenticated;
GRANT ALL ON TABLE public.vne_deals TO service_role;


--
-- Name: SEQUENCE vne_deals_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_deals_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_deals_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_deals_id_seq TO service_role;


--
-- Name: TABLE vne_employeeactions; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_employeeactions TO anon;
GRANT ALL ON TABLE public.vne_employeeactions TO authenticated;
GRANT ALL ON TABLE public.vne_employeeactions TO service_role;


--
-- Name: SEQUENCE vne_employeeactions_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_employeeactions_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_employeeactions_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_employeeactions_id_seq TO service_role;


--
-- Name: TABLE vne_employeegroups; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_employeegroups TO anon;
GRANT ALL ON TABLE public.vne_employeegroups TO authenticated;
GRANT ALL ON TABLE public.vne_employeegroups TO service_role;


--
-- Name: SEQUENCE vne_employeegroups_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_employeegroups_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_employeegroups_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_employeegroups_id_seq TO service_role;


--
-- Name: TABLE vne_employees; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_employees TO anon;
GRANT ALL ON TABLE public.vne_employees TO authenticated;
GRANT ALL ON TABLE public.vne_employees TO service_role;


--
-- Name: SEQUENCE vne_employees_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_employees_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_employees_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_employees_id_seq TO service_role;


--
-- Name: TABLE vne_mailtemplates; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_mailtemplates TO anon;
GRANT ALL ON TABLE public.vne_mailtemplates TO authenticated;
GRANT ALL ON TABLE public.vne_mailtemplates TO service_role;


--
-- Name: SEQUENCE vne_mailtemplates_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_mailtemplates_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_mailtemplates_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_mailtemplates_id_seq TO service_role;


--
-- Name: TABLE vne_settings; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_settings TO anon;
GRANT ALL ON TABLE public.vne_settings TO authenticated;
GRANT ALL ON TABLE public.vne_settings TO service_role;


--
-- Name: SEQUENCE vne_settings_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_settings_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_settings_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_settings_id_seq TO service_role;


--
-- Name: TABLE vne_verifications; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.vne_verifications TO anon;
GRANT ALL ON TABLE public.vne_verifications TO authenticated;
GRANT ALL ON TABLE public.vne_verifications TO service_role;


--
-- Name: SEQUENCE vne_verifications_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.vne_verifications_id_seq TO anon;
GRANT ALL ON SEQUENCE public.vne_verifications_id_seq TO authenticated;
GRANT ALL ON SEQUENCE public.vne_verifications_id_seq TO service_role;


--
-- Name: TABLE schema_migrations; Type: ACL; Schema: realtime; Owner: supabase_admin
--

GRANT ALL ON TABLE realtime.schema_migrations TO postgres;
GRANT ALL ON TABLE realtime.schema_migrations TO dashboard_user;


--
-- Name: TABLE subscription; Type: ACL; Schema: realtime; Owner: supabase_admin
--

GRANT ALL ON TABLE realtime.subscription TO postgres;
GRANT ALL ON TABLE realtime.subscription TO dashboard_user;


--
-- Name: SEQUENCE subscription_id_seq; Type: ACL; Schema: realtime; Owner: supabase_admin
--

GRANT ALL ON SEQUENCE realtime.subscription_id_seq TO postgres;
GRANT ALL ON SEQUENCE realtime.subscription_id_seq TO dashboard_user;


--
-- Name: TABLE buckets; Type: ACL; Schema: storage; Owner: supabase_storage_admin
--

GRANT ALL ON TABLE storage.buckets TO anon;
GRANT ALL ON TABLE storage.buckets TO authenticated;
GRANT ALL ON TABLE storage.buckets TO service_role;
GRANT ALL ON TABLE storage.buckets TO postgres;


--
-- Name: TABLE migrations; Type: ACL; Schema: storage; Owner: supabase_storage_admin
--

GRANT ALL ON TABLE storage.migrations TO anon;
GRANT ALL ON TABLE storage.migrations TO authenticated;
GRANT ALL ON TABLE storage.migrations TO service_role;
GRANT ALL ON TABLE storage.migrations TO postgres;


--
-- Name: TABLE objects; Type: ACL; Schema: storage; Owner: supabase_storage_admin
--

GRANT ALL ON TABLE storage.objects TO anon;
GRANT ALL ON TABLE storage.objects TO authenticated;
GRANT ALL ON TABLE storage.objects TO service_role;
GRANT ALL ON TABLE storage.objects TO postgres;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: auth; Owner: supabase_auth_admin
--

ALTER DEFAULT PRIVILEGES FOR ROLE supabase_auth_admin IN SCHEMA auth GRANT ALL ON SEQUENCES  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_auth_admin IN SCHEMA auth GRANT ALL ON SEQUENCES  TO dashboard_user;


--
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: auth; Owner: supabase_auth_admin
--

ALTER DEFAULT PRIVILEGES FOR ROLE supabase_auth_admin IN SCHEMA auth GRANT ALL ON FUNCTIONS  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_auth_admin IN SCHEMA auth GRANT ALL ON FUNCTIONS  TO dashboard_user;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: auth; Owner: supabase_auth_admin
--

ALTER DEFAULT PRIVILEGES FOR ROLE supabase_auth_admin IN SCHEMA auth GRANT ALL ON TABLES  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_auth_admin IN SCHEMA auth GRANT ALL ON TABLES  TO dashboard_user;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: public; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON SEQUENCES  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON SEQUENCES  TO anon;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON SEQUENCES  TO authenticated;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON SEQUENCES  TO service_role;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: public; Owner: supabase_admin
--

ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA public GRANT ALL ON SEQUENCES  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA public GRANT ALL ON SEQUENCES  TO anon;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA public GRANT ALL ON SEQUENCES  TO authenticated;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA public GRANT ALL ON SEQUENCES  TO service_role;


--
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: public; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON FUNCTIONS  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON FUNCTIONS  TO anon;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON FUNCTIONS  TO authenticated;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON FUNCTIONS  TO service_role;


--
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: public; Owner: supabase_admin
--

ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA public GRANT ALL ON FUNCTIONS  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA public GRANT ALL ON FUNCTIONS  TO anon;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA public GRANT ALL ON FUNCTIONS  TO authenticated;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA public GRANT ALL ON FUNCTIONS  TO service_role;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: public; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON TABLES  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON TABLES  TO anon;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON TABLES  TO authenticated;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON TABLES  TO service_role;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: public; Owner: supabase_admin
--

ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA public GRANT ALL ON TABLES  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA public GRANT ALL ON TABLES  TO anon;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA public GRANT ALL ON TABLES  TO authenticated;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA public GRANT ALL ON TABLES  TO service_role;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: realtime; Owner: supabase_admin
--

ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA realtime GRANT ALL ON SEQUENCES  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA realtime GRANT ALL ON SEQUENCES  TO dashboard_user;


--
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: realtime; Owner: supabase_admin
--

ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA realtime GRANT ALL ON FUNCTIONS  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA realtime GRANT ALL ON FUNCTIONS  TO dashboard_user;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: realtime; Owner: supabase_admin
--

ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA realtime GRANT ALL ON TABLES  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE supabase_admin IN SCHEMA realtime GRANT ALL ON TABLES  TO dashboard_user;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: storage; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA storage GRANT ALL ON SEQUENCES  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA storage GRANT ALL ON SEQUENCES  TO anon;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA storage GRANT ALL ON SEQUENCES  TO authenticated;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA storage GRANT ALL ON SEQUENCES  TO service_role;


--
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: storage; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA storage GRANT ALL ON FUNCTIONS  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA storage GRANT ALL ON FUNCTIONS  TO anon;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA storage GRANT ALL ON FUNCTIONS  TO authenticated;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA storage GRANT ALL ON FUNCTIONS  TO service_role;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: storage; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA storage GRANT ALL ON TABLES  TO postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA storage GRANT ALL ON TABLES  TO anon;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA storage GRANT ALL ON TABLES  TO authenticated;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA storage GRANT ALL ON TABLES  TO service_role;


--
-- Name: issue_pg_cron_access; Type: EVENT TRIGGER; Schema: -; Owner: postgres
--

CREATE EVENT TRIGGER issue_pg_cron_access ON ddl_command_end
         WHEN TAG IN ('CREATE SCHEMA')
   EXECUTE FUNCTION extensions.grant_pg_cron_access();


ALTER EVENT TRIGGER issue_pg_cron_access OWNER TO postgres;

--
-- Name: issue_pg_net_access; Type: EVENT TRIGGER; Schema: -; Owner: postgres
--

CREATE EVENT TRIGGER issue_pg_net_access ON ddl_command_end
         WHEN TAG IN ('CREATE EXTENSION')
   EXECUTE FUNCTION extensions.grant_pg_net_access();


ALTER EVENT TRIGGER issue_pg_net_access OWNER TO postgres;

--
-- Name: pgrst_ddl_watch; Type: EVENT TRIGGER; Schema: -; Owner: supabase_admin
--

CREATE EVENT TRIGGER pgrst_ddl_watch ON ddl_command_end
   EXECUTE FUNCTION extensions.pgrst_ddl_watch();


ALTER EVENT TRIGGER pgrst_ddl_watch OWNER TO supabase_admin;

--
-- Name: pgrst_drop_watch; Type: EVENT TRIGGER; Schema: -; Owner: supabase_admin
--

CREATE EVENT TRIGGER pgrst_drop_watch ON sql_drop
   EXECUTE FUNCTION extensions.pgrst_drop_watch();


ALTER EVENT TRIGGER pgrst_drop_watch OWNER TO supabase_admin;

--
-- PostgreSQL database dump complete
--

