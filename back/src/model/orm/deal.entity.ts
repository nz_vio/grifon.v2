import { Column, CreateDateColumn, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Action } from "./action.entity";
import { Company } from "./company.entity";
import { Contractor } from "./contractor.entity";
import { DealDate } from "./deal.date.entity";

@Entity({name: "vne_deals"})
export class Deal {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true, default: null})
    company_id: number;

    @Column({nullable: true, default: null})
    contact_contractor_id: number;

    @Column({nullable: true, default: null})
    client_contractor_id: number;

    @Column({nullable: true, default: null})
    partner_contractor_id: number;

    @Index()
    @Column({nullable: true, default: null})
    name: string;

    @Index()
    @Column({nullable: true, default: null, type: "text"})
    description: string;    

    @Index()
    @Column({nullable: false, default: false})
    closed: boolean;

    @Index()
    @CreateDateColumn({type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    created_at: Date;

    // relations
    @ManyToOne(type => Company, {onDelete: "CASCADE", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "company_id"})
    company: Company;

    @ManyToOne(type => Contractor, {onDelete: "SET NULL", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "contact_contractor_id"})
    contact: Contractor;

    @ManyToOne(type => Contractor, {onDelete: "SET NULL", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "client_contractor_id"})
    client: Contractor;

    @ManyToOne(type => Contractor, {onDelete: "SET NULL", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "partner_contractor_id"})
    partner: Contractor;

    @OneToMany(type => Action, action => action.deal, {cascade: false})
    actions: Action[];

    @OneToMany(type => DealDate, date => date.deal, {cascade: true})
    dates: DealDate[];
}