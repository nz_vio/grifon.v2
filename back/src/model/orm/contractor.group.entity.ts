import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Contractor } from "./contractor.entity";

@Entity({name: "vne_contractorgroups"})
export class ContractorGroup {
    @PrimaryGeneratedColumn()
    id: number;    

    @Column({nullable: true, default: null})
    name: string;

    // relations
    @ManyToMany(() => Contractor, contractor => contractor.groups, {onUpdate: "CASCADE", onDelete: "CASCADE", cascade: false})    
    contractors: Contractor[];
}