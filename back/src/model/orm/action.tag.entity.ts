import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Action } from "./action.entity";

@Entity({name: "vne_actiontags"})
export class ActionTag {
    @PrimaryGeneratedColumn()
    id: number;    

    @Column({nullable: true, default: null})
    name: string;

    // relations
    @ManyToMany(() => Action, action => action.tags, {onUpdate: "CASCADE", onDelete: "CASCADE", cascade: false})    
    actions: Action[];
}