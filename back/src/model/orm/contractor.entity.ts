import { Column, CreateDateColumn, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ChatMessage } from "./chat.message.entity";
import { Company } from "./company.entity";
import { ContractorGroup } from "./contractor.group.entity";
import { Deal } from "./deal.entity";

// здесь я не стал использовать many-to-many, клиенты теоретически могут повторяться у разных компаний, но с точки зрения разных компаний это разные объекты со своей историей и поведением
@Entity({name: "vne_contractors"})
export class Contractor {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column({nullable: true, default: null})
    company_id: number;
    
    @Index()
    @Column({nullable: true, default: null})
    name: string;

    @Column({nullable: true, default: null})
    phone: string;

    @Column({nullable: true, default: null})
    img: string;

    @Column({nullable: false, default: false})
    chat_active: boolean;

    @Column({nullable: true, default: null, type: "timestamp", precision: null})
    chat_at: Date;    

    @Column({nullable: false, default: false})
    banned: boolean;

    @CreateDateColumn({type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    created_at: Date;

    // relations
    @ManyToOne(type => Company, {onDelete: "CASCADE", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "company_id"})
    company: Company;

    @ManyToMany(() => ContractorGroup, group => group.contractors, {onUpdate: "CASCADE", onDelete: "CASCADE", cascade: false})
    @JoinTable({
        name: "vne_contractors_contractorgroups",
        joinColumn: {
            name: "contractor_id",
            referencedColumnName: "id"
        },
        inverseJoinColumn: {
            name: "contractorgroup_id",
            referencedColumnName: "id"
        }
    })
    groups: ContractorGroup[];

    @OneToMany(type => ChatMessage, chat_message => chat_message.contractor, {cascade: false})
    chat_messages: ChatMessage[];    
}