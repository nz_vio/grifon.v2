import { Column, CreateDateColumn, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ActionFile } from "./action.file.entity";
import { ActionReminder } from "./action.reminder.entity";
import { ActionStatus } from "./action.status.entity";
import { ActionTag } from "./action.tag.entity";
import { ChatMessage } from "./chat.message.entity";
import { Company } from "./company.entity";
import { Deal } from "./deal.entity";
import { EmployeeAction } from "./employee.action.entity";
import { Employee } from "./employee.entity";

@Entity({name: "vne_actions"})
export class Action {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true, default: null})
    company_id: number;

    @Column({nullable: true, default: null})
    employee_id: number;

    @Column({nullable: true, default: null})
    employeeaction_id: number;

    @Column({nullable: true, default: null})
    chatmessage_id: number;

    @Column({nullable: true, default: null})
    deal_id: number;  
    
    @Column({nullable: true, default: null})
    actionstatus_id: number;  

    @Column({nullable: true, default: null, type: "text"})
    comment: string;

    @CreateDateColumn({type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    created_at: Date;

    // relations
    @OneToMany(type => ActionFile, file => file.action, {cascade: true})
    files: ActionFile[];

    @OneToMany(type => ActionReminder, reminder => reminder.action, {cascade: true})
    reminders: ActionReminder[];

    @ManyToOne(type => Company, {onDelete: "CASCADE", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "company_id"})
    company: Company;

    @ManyToOne(type => Employee, {onDelete: "RESTRICT", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "employee_id"})
    employee: Employee;

    @ManyToOne(type => EmployeeAction, {onDelete: "RESTRICT", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "employeeaction_id"})
    employeeAction: EmployeeAction;

    @ManyToOne(type => ChatMessage, {onDelete: "CASCADE", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "chatmessage_id"})
    message: ChatMessage;

    @ManyToOne(type => Deal, {onDelete: "CASCADE", onUpdate: "CASCADE", cascade: true})
    @JoinColumn({name: "deal_id"})
    deal: Deal;    

    @ManyToOne(type => ActionStatus, {onDelete: "SET NULL", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "actionstatus_id"})
    status: ActionStatus;

    @ManyToMany(() => ActionTag, tag => tag.actions, {onUpdate: "CASCADE", onDelete: "CASCADE", cascade: false})
    @JoinTable({
        name: "vne_actions_actiontags",
        joinColumn: {
            name: "action_id",
            referencedColumnName: "id"
        },
        inverseJoinColumn: {
            name: "actiontag_id",
            referencedColumnName: "id"
        }
    })
    tags: ActionTag[];
}