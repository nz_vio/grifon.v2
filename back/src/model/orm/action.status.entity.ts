import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "vne_actionstatuses"})
export class ActionStatus {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true, default: null})
    name: string;
}