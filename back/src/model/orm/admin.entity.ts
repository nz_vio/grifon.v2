import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { AdminGroup } from "./admin.group.entity";

@Entity({name: "vne_admins"})
export class Admin {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false, default: null})
    group_id: number;      
    
    @Column({nullable: false, unique: true})
    email: string;

    @Column({nullable: false, select: false})
    password: string;

    @Column({nullable: true, default: null})
    name: string;  

    @Column({nullable: true, default: null})
    img: string;
    
    @Column({nullable: false, default: true})
    active: boolean;

    @Column({nullable: false, default: false})
    defended: boolean;

    // relations
    @ManyToOne(type => AdminGroup, {onDelete: "CASCADE", onUpdate: "CASCADE"})
    @JoinColumn({name: "group_id"})
    group: AdminGroup;
}
