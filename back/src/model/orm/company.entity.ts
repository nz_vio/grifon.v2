import { Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Contractor } from "./contractor.entity";
import { Employee } from "./employee.entity";

@Entity({name: "vne_companies"})
export class Company {
    @PrimaryGeneratedColumn()
    id: number;

    @Index()
    @Column({nullable: true, default: null})
    name: string;

    @Column({nullable: true, default: null, unique: true})
    email: string;

    @Index()
    @Column({nullable: true, default: null})
    n8n_code: string;    

    // relations
    @OneToMany(type => Employee, employee => employee.company, {cascade: true})
    employees: Employee[];

    @OneToMany(type => Contractor, contractor => contractor.company, {cascade: false})
    contractors: Contractor[];
}