import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "vne_mailtemplates"})
export class Mailtemplate {
    @PrimaryGeneratedColumn()
    id: number;    
    
    @Column({nullable: false, unique: true})
    name: string;

    @Column({nullable: true, default: null})
    subject: string;

    @Column({nullable: true, default: null, type: "text"})
    content: string;

    @Column({nullable: false, default: false})
    defended: boolean;    
}