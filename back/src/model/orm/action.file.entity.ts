import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Action } from "./action.entity";

@Entity({name: "vne_actionfiles"})
export class ActionFile {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    action_id: number;

    @Column({nullable: true, default: null})
    url: string;

    @Column({nullable: true, default: null})
    name: string;

    @Column({nullable: true, default: null})
    description: string;

    // relations
    @ManyToOne(type => Action, {onDelete: "CASCADE", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "action_id"})
    action: Action;
}