import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { EmployeeGroup } from "./employee.group.entity";


export enum EmployeeActionDirection {
    In = "in",
    Out = "out",
}

@Entity({name: "vne_employeeactions"})
export class EmployeeAction {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    group_id: number;

    @Column({nullable: true, default: null})
    code: string;

    @Column({type: "enum", enum: EmployeeActionDirection, nullable: false, default: EmployeeActionDirection.In})
    direction: EmployeeActionDirection;    
    
    @Column({nullable: true, default: null})
    name: string;

    @Column({nullable: true, default: null})
    response: string;

    // relations
    @ManyToOne(type => EmployeeGroup, {onDelete: "SET NULL", onUpdate: "CASCADE"})
    @JoinColumn({name: "group_id"})
    group: EmployeeGroup;
}