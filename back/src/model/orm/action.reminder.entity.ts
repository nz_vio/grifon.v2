import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Action } from "./action.entity";

@Entity({name: "vne_actionreminders"})
export class ActionReminder {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true, default: null})
    action_id: number;

    @Column({nullable: true, default: null, type: "date"})
    date: string;

    @Column({nullable: true, default: null})
    description: string;

    @ManyToOne(type => Action, {onDelete: "CASCADE", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "action_id"})
    action: Action;
}