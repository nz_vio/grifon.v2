import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { EmployeeAction } from "./employee.action.entity";

// по сути это просто группы сотрудников - во всех компаниях одинаковые отделы, которые определяют допустимые действия для сотрудников
@Entity({name: "vne_employeegroups"})
export class EmployeeGroup {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column({nullable: true, default: null})
    code: string;

    @Column({nullable: true, default: null})
    name: string;

    // relations
    @OneToMany(type => EmployeeAction, action => action.group, {cascade: true})
    actions: EmployeeAction[];
}