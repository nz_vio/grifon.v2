import { Column, CreateDateColumn, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Action } from "./action.entity";
import { Contractor } from "./contractor.entity";

export enum ChatMessageChannel {
    Whatsapp = "whatsapp",      
}

export enum ChatMessageType {
    Text = "text",  
    Audio = "audio",
    File = "file",
    Image = "image",    
}

@Entity({name: "vne_chatmessages"})
export class ChatMessage {
    @PrimaryGeneratedColumn()
    id: number;

    @Index()
    @Column({nullable: true, default: null})
    outer_id: string;

    // messages bind to contractor, it is not an author of messages, it is like a source of them
    @Column({nullable: true, default: null})
    contractor_id: number;    

    @Column({type: "enum", enum: ChatMessageChannel, nullable: false, default: ChatMessageChannel.Whatsapp})
    channel: ChatMessageChannel;

    @Column({type: "enum", enum: ChatMessageType, nullable: false, default: ChatMessageType.Text})
    type: ChatMessageType;
    
    @Column({nullable: true, default: null, type: "text"})
    text: string;

    @Column({nullable: true, default: null})
    fileurl: string;

    @Column({nullable: true, default: null})
    filename: string;
    
    @Column({nullable: false, default: true})
    is_task: boolean;
    
    @Column({nullable: false, default: false})
    is_answer: boolean;

    @Column({nullable: false, default: false})
    is_read: boolean;
    
    @CreateDateColumn({type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    created_at: Date;

    // relations
    @ManyToOne(type => Contractor, {onDelete: "CASCADE", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "contractor_id"})
    contractor: Contractor;

    @OneToMany(type => Action, action => action.message, {cascade: false})
    actions: Action[];
}