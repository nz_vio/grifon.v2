import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Company } from "./company.entity";
import { EmployeeGroup } from "./employee.group.entity";

@Entity({name: "vne_employees"})
export class Employee {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    company_id: number;

    @Column({nullable: false})
    group_id: number;

    @Column({nullable: false, unique: true})
    email: string;

    @Column({nullable: false, select: false})
    password: string;

    @Column({nullable: true, default: null})
    name: string;     
    
    @Column({nullable: true, default: null})
    img: string;
    
    @Column({nullable: false, default: true})
    active: boolean;    

    // relations
    @ManyToOne(type => Company, {
        onDelete: "CASCADE", 
        onUpdate: "CASCADE", 
        cascade: ["insert"], // можно создавать компанию каскадом при регистрации, т.е. сделать employee.company = ... и сохранить employee
    }) 
    @JoinColumn({name: "company_id"})
    company: Company;
    
    @ManyToOne(type => EmployeeGroup, {onDelete: "RESTRICT", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "group_id"})
    group: EmployeeGroup;
}