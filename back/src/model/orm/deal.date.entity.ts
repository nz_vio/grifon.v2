import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Deal } from "./deal.entity";

@Entity({name: "vne_dealdates"})
export class DealDate {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true, default: null})
    deal_id: number;

    @Column({nullable: true, default: null, type: "date"})
    date: string;

    @Column({nullable: true, default: null})
    description: string;

    @ManyToOne(type => Deal, {onDelete: "CASCADE", onUpdate: "CASCADE", cascade: false})
    @JoinColumn({name: "deal_id"})
    deal: Deal;
}