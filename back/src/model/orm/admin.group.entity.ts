import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "vne_admingroups"})
export class AdminGroup {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column({nullable: true, default: null})
    code: string;

    @Column({nullable: true, default: null})
    name: string;    

    @Column({nullable: false, default: false})
    defended: boolean;    
}
