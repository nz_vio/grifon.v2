export interface IGetList {
    readonly sortBy?: string;
    readonly sortDir?: number;
    readonly from?: number;
    readonly q?: number;
    readonly filter?: any; // JSON | string   
    readonly created_at?: string; // date limit used in infinite scrollable lists    
}
