export interface IGupshupResponse {
    readonly status: GupshupResponseStatus;
    readonly messageId: string;
}

export enum GupshupResponseStatus {
    Submitted = "submitted",
}