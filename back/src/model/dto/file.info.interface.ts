export interface IFileInfo {
    fileurl: string;
    filename: string;
}