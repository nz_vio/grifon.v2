export interface IFile {
    readonly name: string;
    readonly description?: string;
    readonly data: string;
    readonly contentType: string;
}