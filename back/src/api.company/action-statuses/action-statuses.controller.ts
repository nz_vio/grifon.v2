import { Controller, Post, Body, UseGuards } from "@nestjs/common";
import { EmployeesGuard } from "src/common/guards/employees.guard";
import { IAnswer } from 'src/model/dto/answer.interface';
import { IGetList } from "src/model/dto/getlist.interface";
import { ActionStatus } from "src/model/orm/action.status.entity";
import { ActionStatusesService } from "./action-statuses.service";

@Controller('api/company/action-statuses')
export class ActionStatusesController {
    constructor (private actionStatusesService: ActionStatusesService) {}              

    // get all
    @Post("all")
    @UseGuards(EmployeesGuard)
    public all(@Body() dto: IGetList): Promise<IAnswer<ActionStatus[]>> {
        return this.actionStatusesService.all(dto);
    }    
}
