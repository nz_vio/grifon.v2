import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { TypeOrmModule } from "@nestjs/typeorm";
import { jwtEmployeeConstants } from "src/common/auth.constants";
import { ActionStatus } from "src/model/orm/action.status.entity";
import { Employee } from "src/model/orm/employee.entity";
import { ActionStatusesController } from "./action-statuses.controller";
import { ActionStatusesService } from "./action-statuses.service";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            ActionStatus,
            Employee,
        ]),
        JwtModule.register(jwtEmployeeConstants),                  
    ],    
    providers: [ActionStatusesService],
    controllers: [ActionStatusesController],
})
export class ActionStatusesModule {}
