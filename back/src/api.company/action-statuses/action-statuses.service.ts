import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { IAnswer } from "src/model/dto/answer.interface";
import { IGetList } from "src/model/dto/getlist.interface";
import { ActionStatus } from "src/model/orm/action.status.entity";
import { Repository } from "typeorm";

@Injectable()
export class ActionStatusesService {
    constructor(@InjectRepository(ActionStatus) private actionStatusRepository: Repository<ActionStatus>) {}
    
    public async all(dto: IGetList): Promise<IAnswer<ActionStatus[]>> {
        try {            
            const data = await this.actionStatusRepository.find({where: dto.filter, order: {[dto.sortBy]: dto.sortDir}});            
            return {statusCode: 200, data};
        } catch (err) {
            const errTxt: string = `Error in ActionStatusesService.all: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    } 
}
    