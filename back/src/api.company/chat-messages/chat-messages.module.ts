import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { TypeOrmModule } from "@nestjs/typeorm";
import { jwtEmployeeConstants } from "src/common/auth.constants";
import { CommonModule } from "src/common/common.module";
import { ChatMessage } from "src/model/orm/chat.message.entity";
import { Company } from "src/model/orm/company.entity";
import { Contractor } from "src/model/orm/contractor.entity";
import { Employee } from "src/model/orm/employee.entity";
import { ChatMessagesController } from "./chat-messages.controller";
import { ChatMessagesGateway } from "./chat-messages.gateway";
import { ChatMessagesService } from "./chat-messages.service";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Contractor,
            ChatMessage,
            Company,
            Employee,
        ]),
        JwtModule.register(jwtEmployeeConstants),  
        CommonModule,                   
    ],    
    providers: [
        ChatMessagesService,
        ChatMessagesGateway,
    ],
    controllers: [ChatMessagesController],
    exports: [ChatMessagesGateway],
})
export class ChatMessagesModule {}
