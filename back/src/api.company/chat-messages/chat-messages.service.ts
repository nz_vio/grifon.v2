import { forwardRef, Inject, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import * as fs from "fs/promises";
import * as mime from "mime-types";
import { AppService } from "src/common/services/app.service";
import { AudioService } from "src/common/services/audio.service";
import { DownloadService } from "src/common/services/download.service";
import { N8NService } from "src/common/services/n8n.service";
import { SupabaseService } from "src/common/services/supabase.service";
import { IAnswer } from "src/model/dto/answer.interface";
import { IFileInfo } from "src/model/dto/file.info.interface";
import { IGetList } from "src/model/dto/getlist.interface";
import { GupshupResponseStatus, IGupshupResponse } from "src/model/dto/gupshup.response.interface";
import { ChatMessage, ChatMessageType } from "src/model/orm/chat.message.entity";
import { Company } from "src/model/orm/company.entity";
import { Contractor } from "src/model/orm/contractor.entity";
import { LessThanOrEqual, Repository } from "typeorm";
import { ChatMessagesGateway } from "./chat-messages.gateway";
import { IChatMessageCreate } from "./dto/chat.message.create.interface";
import { IInEvent, InEventType } from "./dto/in.event.interface";
import { IInMessage } from "./dto/in.message.interface";
import { IOutMessage } from "./dto/out.message.interface";

@Injectable()
export class ChatMessagesService {
    constructor (
        @InjectRepository(Contractor) private contractorRepository: Repository<Contractor>,
        @InjectRepository(ChatMessage) private chatMessageRepository: Repository<ChatMessage>,
        @InjectRepository(Company) private companyRepository: Repository<Company>,
        @Inject(forwardRef(() => ChatMessagesGateway)) private chatMessagesGateway: ChatMessagesGateway, // resolve circular dependency                
        private appService: AppService,
        private downloadService: DownloadService,        
        private audioService: AudioService,
        private supabaseService: SupabaseService,
        private n8nService: N8NService,
    ) {}

    // webhook for messages from N8N
    public async onMessage(dto: IInMessage): Promise<IAnswer<void>> {
        try {
            // check company
            const company = await this.companyRepository.findOne({where: {n8n_code: dto.company_code}});

            if (!company) {
                return {statusCode: 404, error: "company not found"};
            }
        
            // process contractor
            let contractor = await this.contractorRepository.findOne({where: {company_id: company.id, phone: dto.phone}});

            if (contractor?.banned) {
                return {statusCode: 401, error: "contractor banned"};
            }
        
            if (!contractor) {
                contractor = new Contractor();
                contractor.company_id = company.id;
                contractor.name = String(dto.name).trim().replace(/[^\w\u0430-\u044f\s]/gi, ''); // alphanumeric (with cyrillic) and spaces
                contractor.phone = dto.phone;            
            }

            contractor.chat_active = true;
            contractor.chat_at = new Date();
            await this.contractorRepository.save(contractor);
            
            // process message
            let message = new ChatMessage();
            message.contractor_id = contractor.id;
            message.channel = dto.channel;
            message.type = dto.type;            
            
            if (message.type === ChatMessageType.Text) {
                message.text = String(dto.text).trim().replace(/(?:\r\n|\r|\n)/g, '<br>');
            } else if (message.type === ChatMessageType.Audio) {
                const {fileurl, filename} = await this.processInAudio(dto);                
                message.fileurl = fileurl;                  
                message.filename = filename;
            } else if (message.type === ChatMessageType.File) {
                const {fileurl, filename} = await this.processInFile(dto);                
                message.fileurl = fileurl;                  
                message.filename = filename;            
            } else if (message.type === ChatMessageType.Image) {
                const {fileurl, filename} = await this.processInImage(dto);                
                message.fileurl = fileurl;                  
                message.filename = filename;
                dto.text ? message.text = String(dto.text).trim().replace(/(?:\r\n|\r|\n)/g, '<br>') : null;
            }
            
            await this.chatMessageRepository.save(message);
            
            // translate to chat    
            message = await this.chatMessageRepository.findOne(message.id, {relations: ["contractor", "contractor.groups", "actions"]}); // здесь actions всегда пуст, но на фронте удобнее всегда иметь в этом поле массив
            this.chatMessagesGateway.server.emit(`message-${company.id}`, message);
            
            return {statusCode: 200};
        } catch (err) {
            const errTxt: string = `Error in ChatMessagesService.onMessage: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }   
    
    // webhook for other events from N8N
    public async onEvent(dto: IInEvent): Promise<IAnswer<void>> {
        try {                        
            if (dto.type === InEventType.Read) {
                return this.onReadEvent(dto.outer_id);
            }
            
            return {statusCode: 200};
        } catch (err) {
            const errTxt: string = `Error in ChatMessagesService.onEvent: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }

    // process "read" event
    private async onReadEvent(outer_id: string): Promise<IAnswer<void>> {
        // find message
        const message = await this.chatMessageRepository.findOne({where: {outer_id}, relations: ["contractor", "contractor.company"]});        

        if (!message || !message.contractor || !message.contractor.company) {
            return {statusCode: 404, error: "entity not found"};
        }
        
        // update message
        message.is_read = true;
        this.chatMessageRepository.save(message);       
        // translate to socket    
        this.chatMessagesGateway.server.emit(`message-read-${message.contractor.company.id}`, message.id);                
    }
    
    // messages
    public async chunk(dto: IGetList): Promise<IAnswer<ChatMessage[]>> {
        try {
            const filter = dto.filter;
            dto.created_at ? filter.created_at = LessThanOrEqual(new Date(dto.created_at)) : null;            
            const data = await this.chatMessageRepository.find({where: filter, order: {[dto.sortBy]: dto.sortDir}, take: dto.q, skip: dto.from, relations: ["contractor", "contractor.groups", "actions", "actions.employeeAction"]});            
            const allLength = await this.chatMessageRepository.count(filter);                    
            return {statusCode: 200, data, allLength};
        } catch (err) {
            const errTxt: string = `Error in ChatMessagesService.messagesChunk: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }

    // create new message (answer)
    public async create(dto: IChatMessageCreate): Promise<void> {
        try {
            // check contractor
            const contractor = await this.contractorRepository.findOne(dto.contractor_id, {relations: ["company"]});

            if (!contractor || !contractor.company) {
                return;
            }
            
            // save message
            let message = this.chatMessageRepository.create(dto);
            message.is_answer = true;
            await this.chatMessageRepository.save(message);            

            // translate to chat (before sending to N8N!)
            message = await this.chatMessageRepository.findOne(message.id, {relations: ["contractor", "contractor.groups", "actions"]}); // здесь actions всегда пуст, но на фронте удобнее всегда иметь в этом поле массив
            this.chatMessagesGateway.server.emit(`message-${contractor.company.id}`, message);

            // update contractor
            contractor.chat_at = new Date();
            await this.contractorRepository.save(contractor);

            // send to n8n
            const outMessage: IOutMessage = {
                company_code: contractor.company.n8n_code,
                channel: dto.channel,
                type: dto.type,
                phone: contractor.phone,
                text: this.html2messenger(dto.text),
            };
            const response: IGupshupResponse = await this.n8nService.msgToGupshup(outMessage);
            
            // save outer_id (will use for status updating by socket)
            if (response.status === GupshupResponseStatus.Submitted) {                                
                message.outer_id = response.messageId;
                this.chatMessageRepository.save(message);
            }
        } catch (err) {
            const errTxt: string = `Error in ChatMessagesService.messagesCreate: ${String(err)}`;
            console.log(errTxt);        
        }
    }    
    
    ////////////// utils /////////////

    private async processInAudio(dto: IInMessage): Promise<IFileInfo> {                
        const tempFolder = `../static/temp`;        
        let filename: string = `${Math.round(new Date().getTime()).toString()}`;                   
        await this.downloadService.download(dto.fileurl, tempFolder, filename);
        const outBucket = "web-app";
        const outFolder = `${dto.company_code}/in/audio`;           
        let outContentType = dto.contenttype;  

        if (dto.contenttype === "audio/ogg; codecs=opus") {
            await this.audioService.ogg2mp3(`${tempFolder}/${filename}`, `${tempFolder}/${filename}.mp3`, true);
            filename += ".mp3";    
            outContentType = "audio/mpeg";
        }
        
        const file = await fs.readFile(`${tempFolder}/${filename}`);
        await this.supabaseService.uploadFile(outBucket, `${outFolder}/${filename}`, file, outContentType);                
        fs.unlink(`${tempFolder}/${filename}`);
        
        return {
            fileurl: `${outBucket}/${outFolder}/${filename}`, 
            filename,
        };
    }

    private async processInFile(dto: IInMessage): Promise<IFileInfo> {    
        const tempFolder = `../static/temp`;  
        const extension = this.appService.getFileExtension(dto.filename);      
        const filename: string = `${Math.round(new Date().getTime()).toString()}.${extension}`;                   
        await this.downloadService.download(dto.fileurl.split("?")[0], tempFolder, filename);
        const outBucket = "web-app";
        const outFolder = `${dto.company_code}/in/docs`;
        const file = await fs.readFile(`${tempFolder}/${filename}`);
        await this.supabaseService.uploadFile(outBucket, `${outFolder}/${filename}`, file, dto.contenttype);                
        fs.unlink(`${tempFolder}/${filename}`);        
        
        return {
            fileurl: `${outBucket}/${outFolder}/${filename}`, 
            filename: dto.filename, // !
        };
    }

    private async processInImage(dto: IInMessage): Promise<IFileInfo> {    
        const tempFolder = `../static/temp`;          
        const extension = mime.extension(dto.contenttype);
        const filename: string = `${Math.round(new Date().getTime()).toString()}.${extension}`;                   
        await this.downloadService.download(dto.fileurl.split("?")[0], tempFolder, filename);
        const outBucket = "web-app";
        const outFolder = `${dto.company_code}/in/docs`;
        const file = await fs.readFile(`${tempFolder}/${filename}`);
        await this.supabaseService.uploadFile(outBucket, `${outFolder}/${filename}`, file, dto.contenttype);                
        fs.unlink(`${tempFolder}/${filename}`);        
        
        return {
            fileurl: `${outBucket}/${outFolder}/${filename}`, 
            filename,
        };
    }    

    private html2messenger(content: string): string {
        return content
            .replace("<blockquote>", "_>>")
            .replace("</blockquote>", "_ \n---\n")
            .replace("\"", "")
            .replace("'", "")
            .replace("`", "");
    }
}