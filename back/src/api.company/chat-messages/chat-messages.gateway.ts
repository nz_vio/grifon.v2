import { forwardRef, Inject } from '@nestjs/common';
import { WebSocketGateway, WebSocketServer, OnGatewayConnection, OnGatewayDisconnect, SubscribeMessage, MessageBody } from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { cfg } from 'src/config';
import { ChatMessagesService } from './chat-messages.service';
import { IChatMessageCreate } from './dto/chat.message.create.interface';

@WebSocketGateway(cfg.wsPort, {path: "/socket/chat", cors: true})
export class ChatMessagesGateway implements OnGatewayConnection, OnGatewayDisconnect {
    @WebSocketServer() 
    public server: Server;      

    constructor(
        @Inject(forwardRef(() => ChatMessagesService)) private chatMessagesService: ChatMessagesService, // resolve circular dependency
    ) {}
    
    public handleDisconnect(client: Socket) {
        console.log(new Date(), `socket client disconnected: ${client.id}`);
    }

    public handleConnection(client: Socket, ...args: any[]) {                
        console.log(new Date(), `socket client connected: ${client.id}`);        
    }

    @SubscribeMessage('create')
    public handleCreate(@MessageBody() dto: IChatMessageCreate): void {
        this.chatMessagesService.create(dto);
    }
}
