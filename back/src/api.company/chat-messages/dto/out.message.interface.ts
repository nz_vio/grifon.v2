import { ChatMessageType } from "src/model/orm/chat.message.entity";

export interface IOutMessage {
    company_code: string;
    channel: string;
    type: ChatMessageType;
    phone: string;
    text: string;    
}