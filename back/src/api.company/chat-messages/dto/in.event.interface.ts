import { ChatMessageChannel } from "src/model/orm/chat.message.entity";

export enum InEventType {
    Enqueued = "enqueued",
    Failed = "failed",
    Sent = "sent",
    Delivered = "delivered",
    Read = "read",    
}

export interface IInEvent {
    readonly type: InEventType,
    readonly outer_id: string;
}