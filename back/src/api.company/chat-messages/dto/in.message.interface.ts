import { ChatMessageChannel, ChatMessageType } from "src/model/orm/chat.message.entity";

export interface IInMessage {
    readonly type: ChatMessageType,
    readonly channel: ChatMessageChannel,
    readonly company_code: string; // сравнивается с company.n8n_code
    readonly name: string;
    readonly phone: string;
    readonly text?: string;
    readonly fileurl?: string;
    readonly filename?: string;
    readonly contenttype?: string;        
}