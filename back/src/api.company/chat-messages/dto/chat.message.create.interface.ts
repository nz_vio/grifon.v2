import { ChatMessageChannel, ChatMessageType } from "src/model/orm/chat.message.entity";

export interface IChatMessageCreate {
    readonly contractor_id: number;
    readonly channel: ChatMessageChannel;
    readonly type: ChatMessageType;
    readonly text: string;    
}