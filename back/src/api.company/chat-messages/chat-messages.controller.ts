import { Controller, Post, Body, UseGuards, Param } from "@nestjs/common";
import { EmployeesGuard } from "src/common/guards/employees.guard";
import { IAnswer } from 'src/model/dto/answer.interface';
import { IGetList } from "src/model/dto/getlist.interface";
import { ChatMessage } from "src/model/orm/chat.message.entity";
import { ChatMessagesService } from "./chat-messages.service";
import { IChatMessageCreate } from "./dto/chat.message.create.interface";
import { IInMessage } from "./dto/in.message.interface";

@Controller('api/company/chat-messages')
export class ChatMessagesController {
    constructor (private chatMessagesService: ChatMessagesService) {}       
    
    // webhook for messages from N8N
    @Post("on-message")
    public onMessage(@Body() dto: IInMessage): Promise<IAnswer<void>> {
        return this.chatMessagesService.onMessage(dto);
    }  
    
    // webhook for other events from N8N
    @Post("on-event")
    public onEvent(@Body() dto: any): Promise<IAnswer<void>> {
        return this.chatMessagesService.onEvent(dto);
    }  

    // messages list
    @Post("chunk")
    @UseGuards(EmployeesGuard)
    public messagesChunk(@Body() dto: IGetList): Promise<IAnswer<ChatMessage[]>> {
        return this.chatMessagesService.chunk(dto);
    }    
}
