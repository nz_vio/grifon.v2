import { Controller, Post, Body, UseGuards } from "@nestjs/common";
import { EmployeesGuard } from "src/common/guards/employees.guard";
import { IAnswer } from 'src/model/dto/answer.interface';
import { IGetList } from "src/model/dto/getlist.interface";
import { ActionTag } from "src/model/orm/action.tag.entity";
import { ActionTagsService } from "./action-tags.service";
import { IActionTagCreate } from "./dto/action.tag.create.interface";

@Controller('api/company/action-tags')
export class ActionTagsController {
    constructor (private actionTagsService: ActionTagsService) {}              

    // get all
    @Post("all")
    @UseGuards(EmployeesGuard)
    public all(@Body() dto: IGetList): Promise<IAnswer<ActionTag[]>> {
        return this.actionTagsService.all(dto);
    }    

    // create
    @Post("create")
    @UseGuards(EmployeesGuard)    
    public create(@Body() dto: IActionTagCreate): Promise<IAnswer<ActionTag>> {
        return this.actionTagsService.create(dto);
    }
}
