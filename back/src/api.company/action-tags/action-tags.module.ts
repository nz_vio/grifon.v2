import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { TypeOrmModule } from "@nestjs/typeorm";
import { jwtEmployeeConstants } from "src/common/auth.constants";
import { ActionTag } from "src/model/orm/action.tag.entity";
import { Employee } from "src/model/orm/employee.entity";
import { ActionTagsController } from "./action-tags.controller";
import { ActionTagsService } from "./action-tags.service";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            ActionTag,
            Employee,
        ]),
        JwtModule.register(jwtEmployeeConstants),                  
    ],    
    providers: [ActionTagsService],
    controllers: [ActionTagsController],
})
export class ActionTagsModule {}
