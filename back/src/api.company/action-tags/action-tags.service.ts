import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { IAnswer } from "src/model/dto/answer.interface";
import { IGetList } from "src/model/dto/getlist.interface";
import { ActionTag } from "src/model/orm/action.tag.entity";
import { Repository } from "typeorm";
import { IActionTagCreate } from "./dto/action.tag.create.interface";

@Injectable()
export class ActionTagsService {
    constructor(@InjectRepository(ActionTag) private actionTagRepository: Repository<ActionTag>) {}
    
    public async all(dto: IGetList): Promise<IAnswer<ActionTag[]>> {
        try {            
            const data = await this.actionTagRepository.find({where: dto.filter, order: {[dto.sortBy]: dto.sortDir}});            
            return {statusCode: 200, data};
        } catch (err) {
            const errTxt: string = `Error in ActionTagsService.all: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    } 

    public async create(dto: IActionTagCreate): Promise<IAnswer<ActionTag>> {        
        try {            
            const x = this.actionTagRepository.create(dto);
            await this.actionTagRepository.save(x);            
            return {statusCode: 200, data: x};
        } catch (err) {
            const errTxt: string = `Error in ActionTagsService.create: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }        
    }
}
    