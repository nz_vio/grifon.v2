import { Controller, Post, Body, UseGuards, Param } from "@nestjs/common";
import { EmployeesGuard } from "src/common/guards/employees.guard";
import { IAnswer } from 'src/model/dto/answer.interface';
import { IGetList } from "src/model/dto/getlist.interface";
import { Deal } from "src/model/orm/deal.entity";
import { DealsService } from "./deals.service";
import { IDealCreate } from "./dto/deal.create.interface";
import { IDealUpdate } from "./dto/deal.update.interface";

@Controller('api/company/deals')
export class DealsController {
    constructor (private dealsService: DealsService) {}    
    
    // get fragment
    @Post("chunk")
    @UseGuards(EmployeesGuard)
    public chunk(@Body() dto: IGetList): Promise<IAnswer<Deal[]>> {
        return this.dealsService.chunk(dto);
    }

    // get one    
    @Post("one/:id")
    @UseGuards(EmployeesGuard)
    public one(@Param("id") id: string): Promise<IAnswer<Deal>> {
        return this.dealsService.one(parseInt(id));
    }

    // update
    @Post("update")
    @UseGuards(EmployeesGuard)    
    public update(@Body() dto: IDealUpdate): Promise<IAnswer<void>> {
        return this.dealsService.update(dto);
    }

    // create
    @Post("create")
    @UseGuards(EmployeesGuard)    
    public create(@Body() dto: IDealCreate): Promise<IAnswer<void>> {
        return this.dealsService.create(dto);
    }

    // delete one
    @Post("delete/:id")
    @UseGuards(EmployeesGuard)    
    public delete(@Param("id") id: string): Promise<IAnswer<void>> {
        return this.dealsService.delete(parseInt(id));
    }
}
