import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { TypeOrmModule } from "@nestjs/typeorm";
import { jwtEmployeeConstants } from "src/common/auth.constants";
import { CommonModule } from "src/common/common.module";
import { Contractor } from "src/model/orm/contractor.entity";
import { Deal } from "src/model/orm/deal.entity";
import { Employee } from "src/model/orm/employee.entity";
import { DealsController } from "./deals.controller";
import { DealsService } from "./deals.service";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Deal,        
            Contractor,    
            Employee,
        ]),
        JwtModule.register(jwtEmployeeConstants),          
        CommonModule,
    ],    
    providers: [DealsService],    
    controllers: [DealsController],
    exports: [DealsService]
})
export class DealsModule {}
