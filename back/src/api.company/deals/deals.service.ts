import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { AppService } from "src/common/services/app.service";
import { IAnswer } from "src/model/dto/answer.interface";
import { IGetList } from "src/model/dto/getlist.interface";
import { Deal } from "src/model/orm/deal.entity";
import { Repository } from "typeorm";
import { IDealCreate } from "./dto/deal.create.interface";
import { IDealUpdate } from "./dto/deal.update.interface";

@Injectable()
export class DealsService {
    constructor (
        @InjectRepository(Deal) private dealRepository: Repository<Deal>,                
        private appService: AppService,
    ) {}  

    public async chunk(dto: IGetList): Promise<IAnswer<Deal[]>> {
        try {
            const dealsSortBy = `deals.${dto.sortBy}`;
            const dealsSortDir = dto.sortDir === 1 ? "ASC" : "DESC";
            const filter = this.buildListFilter(dto.filter);
            const deals = await this.dealRepository
                .createQueryBuilder("deals")
                .leftJoinAndSelect("deals.contact", "contact")
                .leftJoinAndSelect("contact.groups", "contact_groups")
                .leftJoinAndSelect("deals.client", "client")
                .leftJoinAndSelect("client.groups", "client_groups")
                .leftJoinAndSelect("deals.partner", "partner")
                .leftJoinAndSelect("partner.groups", "partner_groups")
                .leftJoinAndSelect("deals.dates", "dates")
                .leftJoinAndSelect("deals.actions", "actions")
                .leftJoinAndSelect("actions.files", "files")
                .leftJoinAndSelect("actions.employeeAction", "employeeAction")
                .leftJoinAndSelect("actions.message", "message")
                .leftJoinAndSelect("actions.reminders", "reminders")
                .leftJoinAndSelect("actions.tags", "tags")
                .where(filter)
                .orderBy({
                    [dealsSortBy]: dealsSortDir,      
                    // логика подсказывает сделать так, но это не работает!               
                    //"actions.id": "DESC",     // если используется take/skip, то нельзя отсортировать присоединенные таблицы!
                    //"dates.date": "ASC",      // пропадают строки, может когда он наталкивается на пустые массивы или что-то такое
                    //"reminders.date": "ASC",  // поэтому надо сортировать "вручную"
                })                
                .take(dto.q)
                .skip(dto.from)
                .getMany();    
            
            // "ручная" сортировка
            for (let deal of deals) {
                deal.actions.sort((a, b) => b.id - a.id); // id DESC
                deal.dates.sort((a, b) => a.date.localeCompare(b.date)); // date ASC

                for (let action of deal.actions) {
                    action.reminders.sort((a, b) => a.date.localeCompare(b.date)); // date ASC
                }
            }

            const allLength = await this.dealRepository
                .createQueryBuilder("deals")
                // джойним таблицы, по которым работает фильтр
                .leftJoinAndSelect("deals.contact", "contact")                 
                .leftJoinAndSelect("deals.client", "client")                
                .leftJoinAndSelect("deals.partner", "partner")                
                .where(filter)
                .getCount();             
            return {statusCode: 200, data: deals, allLength};
        } catch (err) {
            const errTxt: string = `Error in DealsService.chunk: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }  

    public async one(id: number): Promise<IAnswer<Deal>> {
        try {            
            const data = await this.dealRepository
                .createQueryBuilder("deals")
                .leftJoinAndSelect("deals.contact", "contact")
                .leftJoinAndSelect("contact.groups", "contact_groups")
                .leftJoinAndSelect("deals.client", "client")
                .leftJoinAndSelect("client.groups", "client_groups")
                .leftJoinAndSelect("deals.partner", "partner")
                .leftJoinAndSelect("partner.groups", "partner_groups")
                .leftJoinAndSelect("deals.dates", "dates")
                .leftJoinAndSelect("deals.actions", "actions")
                .leftJoinAndSelect("actions.files", "files")
                .leftJoinAndSelect("actions.employeeAction", "employeeAction")
                .leftJoinAndSelect("actions.message", "message")
                .leftJoinAndSelect("actions.reminders", "reminders")
                .leftJoinAndSelect("actions.tags", "tags")
                .orderBy({ // ради этой сортировки используется queryBuilder
                    "actions.id": "DESC", 
                    "dates.date": "ASC", 
                    "reminders.date": "ASC",
                }) 
                .where(`deals.id='${id}'`)
                .getOne();            
            return data ? {statusCode: 200, data} : {statusCode: 404, error: "deal not found"};
        } catch (err) {
            const errTxt: string = `Error in DealsService.one: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }

    public async update(dto: IDealUpdate): Promise<IAnswer<void>> {
        try {                       
            if (!await this.dealRepository.findOne(dto.id)) {
                return {statusCode: 404, error: "deal not found"};
            }
            
            const x = this.dealRepository.create(dto);            
            await this.dealRepository.save(x);                  
            return {statusCode: 200};
        } catch (err) {
            const errTxt: string = `Error in DealsService.update: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        } 
    }

    public async create(dto: IDealCreate): Promise<IAnswer<void>> {        
        try {            
            const x = this.dealRepository.create(dto);
            await this.dealRepository.save(x);            
            return {statusCode: 200};
        } catch (err) {
            const errTxt: string = `Error in DealsService.create: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }        
    }

    public async delete(id: number): Promise<IAnswer<void>> {
        try {
            if (!await this.dealRepository.findOne(id)) {
                return {statusCode: 404, error: "deal not found"};
            }

            await this.dealRepository.delete(id);
            return {statusCode: 200};
        } catch (err) {
            const errTxt: string = `Error in DealsService.delete: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }        
    }

    /////////////// utils ////////////////// 
    
    private buildListFilter(dtoFilter: any): string {
        let filter = "TRUE";        

        if (dtoFilter.contact_contractor_id) {
            filter += ` AND deals.contact_contractor_id = '${dtoFilter.contact_contractor_id}'`;
        }

        if (dtoFilter.company_id) {
            filter += ` AND deals.company_id = '${dtoFilter.company_id}'`;
        }
        
        if (dtoFilter.search) {
            const arrDate = (dtoFilter.search as string).match(/^(\d{1,2}).(\d{1,2}).(\d{2,4})$/);               

            if (arrDate) { // is date
                if (arrDate[3].length === 2) {
                    arrDate[3] = "20" + arrDate[3];
                }
                
                const date = new Date(parseInt(arrDate[3]), parseInt(arrDate[2]) - 1, parseInt(arrDate[1]));
                const mysqlDate = this.appService.mysqlDate(date);
                filter += ` AND DATE(deals.created_at) = '${mysqlDate}'`;
            } else {
                const numSearch = parseInt(dtoFilter.search.trim());
                const idStatement = isNaN(numSearch) ? "" : ` OR deals.id='${numSearch}'`;
                filter += ` AND (deals.name ILIKE '%${dtoFilter.search}%' OR deals.description ILIKE '%${dtoFilter.search}%' OR contact.name ILIKE '%${dtoFilter.search}%' OR client.name ILIKE '%${dtoFilter.search}%' OR partner.name ILIKE '%${dtoFilter.search}%' ${idStatement})`;
            }                
        }
        return filter;
    }

    private async fake(): Promise<void> {
        try {
            const ids = (await this.dealRepository.find()).map(d => d.id);
            ids.length && await this.dealRepository.delete(ids);
            
            for (let i = 0; i < 100; i++) {
                const d = new Deal();
                d.name = `Название сделки ${i}`;
                d.description = `Краткое описание сделки ${i} Краткое описание сделки ${i} Краткое описание сделки ${i} Краткое описание сделки ${i} Краткое описание сделки ${i}`;
                d.company_id = 1;
                d.contact_contractor_id = 1;
                d.client_contractor_id = 2;
                d.partner_contractor_id = 3;
                await this.dealRepository.save(d);
            }
        } catch (err) {
            console.log(err);
        }        
    }        
}