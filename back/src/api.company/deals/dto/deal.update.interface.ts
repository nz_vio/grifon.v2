import { Action } from "src/model/orm/action.entity";
import { Contractor } from "src/model/orm/contractor.entity";
import { DealDate } from "src/model/orm/deal.date.entity";

export interface IDealUpdate {
    readonly id: number;    
    readonly company_id: number;    
    readonly contact_contractor_id: number;    
    readonly client_contractor_id?: number;    
    readonly partner_contractor_id?: number;
    readonly name: string;
    readonly description: string;   
    readonly closed: boolean; 
    readonly created_at: string;
        
    readonly contact: Contractor;
    readonly client?: Contractor;
    readonly partner?: Contractor;
    readonly actions?: Action[];   
    readonly dates: DealDate[];
}