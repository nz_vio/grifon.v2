import { Contractor } from "src/model/orm/contractor.entity";
import { DealDate } from "src/model/orm/deal.date.entity";

export interface IDealCreate {    
    readonly company_id: number;    
    readonly name: string;
    readonly description: string; 
    readonly closed: boolean;  
     
    readonly contact: Contractor;
    readonly client?: Contractor;
    readonly partner?: Contractor;    
    readonly dates: DealDate[];
}