import { Module } from "@nestjs/common";
import { ActionStatusesModule } from "./action-statuses/action-statuses.module";
import { ActionTagsModule } from "./action-tags/action-tags.module";
import { ActionsModule } from "./actions/actions.module";
import { ChatMessagesModule } from "./chat-messages/chat-messages.module";
import { ContractorGroupsModule } from "./contractor-groups/contractor-groups.module";
import { ContractorsModule } from "./contractors/contractors.module";
import { DealsModule } from "./deals/deals.module";
import { EmployeesModule } from "./employees/employees.module";
import { ObjectsModule } from "./objects/objects.module";
import { ProxyModule } from "./proxy/proxy.module";
import { SettingsModule } from "./settings/settings.module";

@Module({
    imports: [             
        ObjectsModule,   
        ProxyModule,
        SettingsModule,    
        EmployeesModule,    
        ChatMessagesModule,
        ContractorsModule,
        ContractorGroupsModule,
        DealsModule,
        ActionsModule,
        ActionStatusesModule, 
        ActionTagsModule,       
    ],    
})
export class ApiCompanyModule {}
