import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { AppService } from "src/common/services/app.service";
import { IAnswer } from "src/model/dto/answer.interface";
import { IGetList } from "src/model/dto/getlist.interface";
import { ContractorGroup } from "src/model/orm/contractor.group.entity";
import { Repository } from "typeorm";

@Injectable()
export class ContractorGroupsService {
    constructor(
        @InjectRepository(ContractorGroup) private contractorGroupRepository: Repository<ContractorGroup>,
        private appService: AppService,
    ) {}
    
    public async all(dto: IGetList): Promise<IAnswer<ContractorGroup[]>> {
        try {            
            const data = await this.contractorGroupRepository.find({where: dto.filter, order: {[dto.sortBy]: dto.sortDir}});            
            return {statusCode: 200, data};
        } catch (err) {
            const errTxt: string = `Error in ContractorGroupsService.all: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }  
    
    public async chunk(dto: IGetList): Promise<IAnswer<ContractorGroup[]>> {
        try {
            const filter = this.appService.filterToQbfilter(dto.filter, "contractorgroups");
            const sortBy = `contractorgroups.${dto.sortBy}`;
            const sortDir = dto.sortDir === 1 ? "ASC" : "DESC";
            const data = await this.contractorGroupRepository
                .createQueryBuilder("contractorgroups")                
                .where(filter, dto.filter)
                .orderBy({[sortBy]: sortDir})
                .take(dto.q)
                .skip(dto.from)
                .getMany();
            const allLength = await this.contractorGroupRepository
                .createQueryBuilder("contractorgroups")
                .where(filter, dto.filter)
                .getCount();
            return {statusCode: 200, data, allLength};
        } catch (err) {
            const errTxt: string = `Error in ContractorGroupsService.chunk: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }  
}