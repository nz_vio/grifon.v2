import { Controller, Post, Body, UseGuards } from "@nestjs/common";
import { EmployeesGuard } from "src/common/guards/employees.guard";
import { IAnswer } from 'src/model/dto/answer.interface';
import { IGetList } from "src/model/dto/getlist.interface";
import { ContractorGroup } from "src/model/orm/contractor.group.entity";
import { ContractorGroupsService } from "./contractor-groups.service";

@Controller('api/company/contractor-groups')
export class ContractorGroupsController {
    constructor (private contractorGroupsService: ContractorGroupsService) {}              

    // get all
    @Post("all")
    @UseGuards(EmployeesGuard)
    public all(@Body() dto: IGetList): Promise<IAnswer<ContractorGroup[]>> {
        return this.contractorGroupsService.all(dto);
    }   
    
    // get fragment
    @Post("chunk")
    @UseGuards(EmployeesGuard)
    public chunk(@Body() dto: IGetList): Promise<IAnswer<ContractorGroup[]>> {
        return this.contractorGroupsService.chunk(dto);
    }
}
