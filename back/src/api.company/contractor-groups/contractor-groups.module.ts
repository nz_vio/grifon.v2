import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { TypeOrmModule } from "@nestjs/typeorm";
import { jwtEmployeeConstants } from "src/common/auth.constants";
import { CommonModule } from "src/common/common.module";
import { ContractorGroup } from "src/model/orm/contractor.group.entity";
import { Employee } from "src/model/orm/employee.entity";
import { ContractorGroupsController } from "./contractor-groups.controller";
import { ContractorGroupsService } from "./contractor-groups.service";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            ContractorGroup,            
            Employee,
        ]),
        JwtModule.register(jwtEmployeeConstants),   
        CommonModule,         
    ],    
    providers: [ContractorGroupsService],
    controllers: [ContractorGroupsController],
})
export class ContractorGroupsModule {}
