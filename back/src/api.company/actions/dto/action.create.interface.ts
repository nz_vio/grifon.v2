import { IFile } from "src/model/dto/file.interface";
import { ActionFile } from "src/model/orm/action.file.entity";
import { ActionReminder } from "src/model/orm/action.reminder.entity";
import { ActionTag } from "src/model/orm/action.tag.entity";
import { ChatMessage } from "src/model/orm/chat.message.entity";
import { Deal } from "src/model/orm/deal.entity";
import { EmployeeAction } from "src/model/orm/employee.action.entity";
import { Employee } from "src/model/orm/employee.entity";

export interface IActionCreate {    
    readonly actionstatus_id: number;  
    readonly employee: Employee;
    readonly employeeAction: EmployeeAction;
    readonly message: ChatMessage;
    readonly deal: Deal;    
    readonly comment: string;      
    readonly file?: ActionFile;
    readonly uploads: IFile[];
    readonly reminders: ActionReminder[];
    readonly tags: ActionTag[];
}