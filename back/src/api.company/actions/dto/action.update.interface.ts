import { ActionFile } from "src/model/orm/action.file.entity";
import { ActionReminder } from "src/model/orm/action.reminder.entity";
import { ActionTag } from "src/model/orm/action.tag.entity";
import { ChatMessage } from "src/model/orm/chat.message.entity";
import { EmployeeAction } from "src/model/orm/employee.action.entity";

export class IActionUpdate {
    readonly id: number;    
    readonly company_id: number;    
    readonly employee_id: number;    
    readonly employeeaction_id: number;    
    readonly chatmessage_id: number;    
    readonly deal_id: number;  
    readonly actionstatus_id: number;        
    readonly comment: string;    
    readonly created_at: string;

    readonly files: ActionFile[];    
    readonly reminders: ActionReminder[];   
    readonly employeeAction: EmployeeAction;
    readonly message: ChatMessage;  
    readonly tags: ActionTag[];  
}