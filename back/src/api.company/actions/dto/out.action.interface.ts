import { Action } from "src/model/orm/action.entity";

export interface IOutAction {
    action: Action;        
    files_root: string;
}