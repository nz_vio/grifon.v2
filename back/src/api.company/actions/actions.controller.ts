import { Controller, Post, Body, UseGuards } from "@nestjs/common";
import { EmployeesGuard } from "src/common/guards/employees.guard";
import { IAnswer } from 'src/model/dto/answer.interface';
import { ActionsService } from "./actions.service";
import { IActionCreate } from "./dto/action.create.interface";
import { IActionUpdate } from "./dto/action.update.interface";

@Controller('api/company/actions')
export class ActionsController {
    constructor (private actionsService: ActionsService) {}                  

    // create
    @Post("create")
    @UseGuards(EmployeesGuard)    
    public create(@Body() dto: IActionCreate): Promise<IAnswer<void>> {
        return this.actionsService.create(dto);
    }    

    // update
    @Post("update")
    @UseGuards(EmployeesGuard)    
    public update(@Body() dto: IActionUpdate): Promise<IAnswer<void>> {
        return this.actionsService.update(dto);
    }
}
