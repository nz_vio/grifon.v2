import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { N8NService } from "src/common/services/n8n.service";
import { SupabaseService } from "src/common/services/supabase.service";
import { IAnswer } from "src/model/dto/answer.interface";
import { GupshupResponseStatus, IGupshupResponse } from "src/model/dto/gupshup.response.interface";
import { Action } from "src/model/orm/action.entity";
import { ActionFile } from "src/model/orm/action.file.entity";
import { ChatMessage, ChatMessageType } from "src/model/orm/chat.message.entity";
import { Setting } from "src/model/orm/setting.entity";
import { Repository } from "typeorm";
import { ChatMessagesGateway } from "../chat-messages/chat-messages.gateway";
import { IActionCreate } from "./dto/action.create.interface";
import { IActionUpdate } from "./dto/action.update.interface";
import { IOutAction } from "./dto/out.action.interface";

@Injectable()
export class ActionsService {
    constructor (
        @InjectRepository(Action) private actionRepository: Repository<Action>,
        @InjectRepository(ChatMessage) private chatMessageRepository: Repository<ChatMessage>,
        @InjectRepository(Setting) private settingRepository: Repository<Setting>,
        private supabaseService: SupabaseService,                
        private chatMessagesGateway: ChatMessagesGateway,                
        private n8nService: N8NService,
    ) {}

    public async create(dto: IActionCreate): Promise<IAnswer<void>> {
        try {
            // build action
            let x = await this.buildAction(dto);                      
            await this.actionRepository.save(x);            
            x = await this.actionRepository.findOne(x.id, {relations: ["company", "message", "message.contractor", "employeeAction", "files"]});
            // translate action to chat            
            this.chatMessagesGateway.server.emit(`action-${dto.employee.company_id}`, x);
            // translate message to chat            
            const message = await this.buildActionMessage(dto);
            this.chatMessagesGateway.server.emit(`message-${dto.employee.company_id}`, message);
            // send to n8n                       
            const response: IGupshupResponse = await this.sendActionToGupshup(x);
            
            // save outer_id (will use for status updating by socket)
            if (response.status === GupshupResponseStatus.Submitted) {                                
                message.outer_id = response.messageId;
                this.chatMessageRepository.save(message);
            }

            return {statusCode: 200};
        } catch (err) {
            const errTxt: string = `Error in ActionsService.create: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }

    public async update(dto: IActionUpdate): Promise<IAnswer<void>> {
        try {                       
            if (!await this.actionRepository.findOne(dto.id)) {
                return {statusCode: 404, error: "action not found"};
            }
            
            const x = this.actionRepository.create(dto);            
            await this.actionRepository.save(x);                  
            return {statusCode: 200};
        } catch (err) {
            const errTxt: string = `Error in ActionsService.update: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        } 
    }

    ///////////////// utils ///////////////////

    private async buildAction(dto: IActionCreate): Promise<Action> {
        let action = new Action();
        action.company_id = dto.employee.company_id;
        action.employee_id = dto.employee.id;
        action.employeeaction_id = dto.employeeAction.id;
        action.chatmessage_id = dto.message.id;
        action.actionstatus_id = dto.actionstatus_id;
        action.deal = dto.deal;        
        action.comment = dto.comment;                                    
        action.files = await this.buildActionFiles(dto);  
        action.reminders = dto.reminders;
        action.tags = dto.tags;
        return action;
    }

    private async buildActionFiles(dto: IActionCreate): Promise<ActionFile[]> {
        const files: ActionFile[] = [];
        dto.file && files.push(dto.file);
        const bucket = "web-app";
        const folder = `${dto.employee.company.n8n_code}/${dto.employeeAction.direction}/docs`;
        
        for (let u of dto.uploads) {
            const buffer = Buffer.from(u.data.split(",")[1], 'base64');                 
            const extension = u.name.split(".").pop() || "dat";                
            const fileName = `${Math.round(new Date().getTime()).toString()}.${extension}`;
            const path = `${folder}/${fileName}`;                               
            await this.supabaseService.uploadFile(bucket, path, buffer, u.contentType); // upload to supabase
            const actionFile = new ActionFile();
            actionFile.url = `${bucket}/${folder}/${fileName}`;
            actionFile.name = u.name;
            actionFile.description = u.description;
            files.push(actionFile);
        }  

        return files;
    }

    private async buildActionMessage(dto: IActionCreate): Promise<ChatMessage> {
        let message = new ChatMessage();
        message.contractor_id = dto.message.contractor_id;
        message.channel = dto.message.channel;
        message.type = ChatMessageType.Text;
        message.text = `<em>[${dto.employeeAction.name}]</em>`;
        dto.comment && (message.text += `<br>${dto.comment}`);
        message.is_answer = true;
        await this.chatMessageRepository.save(message);
        message = await this.chatMessageRepository.findOne(message.id, {relations: ["contractor", "contractor.groups", "actions"]}); // здесь actions всегда пуст, но на фронте удобнее всегда иметь в этом поле массив
        return message;
    }

    private async sendActionToGupshup(action: Action): Promise<IGupshupResponse> {
        const storageUrl: string = (await this.settingRepository.findOne({where: {p: "supabase-storage-url"}}))?.v;   

        if (!storageUrl) {
            throw new Error("setting not found"); 
        }

        const outAction: IOutAction = {
            action,            
            files_root: storageUrl,
        };            
        
        return this.n8nService.actionToGupshup(outAction);
    }
}