import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { TypeOrmModule } from "@nestjs/typeorm";
import { jwtEmployeeConstants } from "src/common/auth.constants";
import { CommonModule } from "src/common/common.module";
import { Action } from "src/model/orm/action.entity";
import { ChatMessage } from "src/model/orm/chat.message.entity";
import { Employee } from "src/model/orm/employee.entity";
import { Setting } from "src/model/orm/setting.entity";
import { ChatMessagesModule } from "../chat-messages/chat-messages.module";
import { ActionsController } from "./actions.controller";
import { ActionsService } from "./actions.service";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Action,            
            Employee,
            ChatMessage,
            Setting,
        ]),
        JwtModule.register(jwtEmployeeConstants),          
        CommonModule,       
        ChatMessagesModule,    
    ],    
    providers: [ActionsService],
    controllers: [ActionsController],
})
export class ActionsModule {}
