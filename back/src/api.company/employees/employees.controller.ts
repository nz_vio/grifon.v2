import { Controller, Post, Body } from "@nestjs/common";
import { IAnswer } from 'src/model/dto/answer.interface';
import { EmployeesService } from "./employees.service";
import { IEmployeeAuthData } from "./dto/employee.authdata.interface";
import { IEmployeeLogin } from "./dto/employee.login.interface";
import { IVerify } from "./dto/verify.interface";
import { IEmployeeRecovery } from "./dto/employee.recovery.interface";

@Controller('api/company/employees')
export class EmployeesController {
    constructor (private employeesService: EmployeesService) {}       

    // authentication by email and password
    @Post("login")
    public login(@Body() dto: IEmployeeLogin): Promise<IAnswer<IEmployeeAuthData>> {                        
        return this.employeesService.login(dto);
    } 

    // e-mail verification
    @Post("verify-email")
    public verifyEmail(@Body() dto: IVerify): Promise<IAnswer<void>> {
        return this.employeesService.verifyEmail(dto);
    }

    // password recovery
    @Post("recover")
    public recover(@Body() dto: IEmployeeRecovery): Promise<IAnswer<void>> {
        return this.employeesService.recover(dto);
    }
}
