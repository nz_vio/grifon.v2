import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectRepository } from "@nestjs/typeorm";
import { AppService } from "src/common/services/app.service";
import { MailService } from "src/common/services/mail.service";
import { IAnswer } from "src/model/dto/answer.interface";
import { Employee } from "src/model/orm/employee.entity";
import { Verification } from "src/model/orm/verification.entity";
import { MoreThanOrEqual, Repository } from "typeorm";
import { IEmployeeAuthData } from "./dto/employee.authdata.interface";
import { IEmployeeLogin } from "./dto/employee.login.interface";
import { IEmployeeRecovery } from "./dto/employee.recovery.interface";
import { IVerify } from "./dto/verify.interface";

@Injectable()
export class EmployeesService {
    constructor (
        @InjectRepository(Employee) private employeeRepository: Repository<Employee>,
        @InjectRepository(Verification) private verificationRepository: Repository<Verification>,
        private jwtService: JwtService, 
        private appService: AppService,           
        private mailService: MailService,
    ) {}       
    
    // authentication by email and password
    public async login(dto: IEmployeeLogin): Promise<IAnswer<IEmployeeAuthData>> {
        try {            
            const employee = await this.validateEmployee(dto.email, dto.password);

            if (employee) {
                const payload: Object = {email: employee.email, id: employee.id};
                const data: IEmployeeAuthData = {token: this.jwtService.sign(payload), employee};
                return {statusCode: 200, data};
            } else {
                return {statusCode: 401, error: "Unauthorized"};
            }
        } catch (err) {
            const errTxt: string = `Error in EmployeesService.login: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }

    // send code to email
    public async verifyEmail(dto: IVerify): Promise<IAnswer<void>> {
        try {
            const email: string = dto.email;
            const code: string = this.appService.randomString(6, "digits");
            await this.verificationRepository.delete({email});            
            const verification: Verification = this.verificationRepository.create({email, code});
            await this.verificationRepository.save(verification);
            this.mailService.mailEmployeeEmailVerification(email, code);
            return {statusCode: 200};
        } catch (err) {
            const errTxt: string = `Error in EmployeesService.verifyEmail: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }

    // password recovery    
    public async recover(dto: IEmployeeRecovery): Promise<IAnswer<void>> {
        try {
            const employee = await this.employeeRepository.findOne({where: {email: dto.email}});

            if (!employee) {
                return {statusCode: 404, error: "e-mail not found"};
            }

            const now = new Date();
            const expiration = new Date(now.getTime() - 5 * 60 * 1000);            
            const verification = await this.verificationRepository.findOne({where: {email: dto.email, code: dto.code, created_at: MoreThanOrEqual(expiration)}});

            if (!verification) {
                return {statusCode: 401, error: "code is incorrect"};
            }  
            
            employee.password = this.appService.passwordBuildHash(dto.password);
            await this.employeeRepository.save(employee);            
            return {statusCode: 200};
        } catch (err) {
            let errTxt: string = `Error in EmployeesService.recover: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }   
    
    //////////////// utils ////////////////////
        
    private getEmployeeById(id: number): Promise<Employee> {
        return this.employeeRepository
            .createQueryBuilder("employee")            
            .leftJoinAndSelect("employee.company", "company")
            .leftJoinAndSelect("employee.group", "group")
            .leftJoinAndSelect("group.actions", "actions")        
            .where({id})
            .getOne(); 
    } 

    private getEmployeeByEmail(email: string): Promise<Employee> {
        return this.employeeRepository
            .createQueryBuilder("employee")            
            .leftJoinAndSelect("employee.company", "company")
            .leftJoinAndSelect("employee.group", "group")
            .leftJoinAndSelect("group.actions", "actions")        
            .where({email})
            .getOne(); 
    } 

    private async validateEmployee(email: string, password: string): Promise<Employee> {
        const employee = await this.employeeRepository
            .createQueryBuilder("employee")
            .addSelect("employee.password")
            .leftJoinAndSelect("employee.company", "company")
            .leftJoinAndSelect("employee.group", "group")
            .leftJoinAndSelect("group.actions", "actions")        
            .where({email})
            .getOne();      

        if (employee && employee.active && await this.appService.passwordCompareHash(password, employee.password)) {
            delete employee.password;
            return employee;
        } else {
            return null;
        }
    } 
}
