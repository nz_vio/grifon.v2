import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { TypeOrmModule } from "@nestjs/typeorm";
import { jwtEmployeeConstants } from "src/common/auth.constants";
import { CommonModule } from "src/common/common.module";
import { Employee } from "src/model/orm/employee.entity";
import { Verification } from "src/model/orm/verification.entity";
import { EmployeesController } from "./employees.controller";
import { EmployeesService } from "./employees.service";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Employee,     
            Verification,        
        ]),
        JwtModule.register(jwtEmployeeConstants),
        CommonModule,
    ],    
    providers: [EmployeesService],
    controllers: [EmployeesController],
})
export class EmployeesModule {}
