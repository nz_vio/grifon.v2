import { Employee } from "src/model/orm/employee.entity";

export interface IEmployeeAuthData {    
    token: string;    
    employee: Employee;
}