export interface IEmployeeRecovery {
    readonly email: string;
    readonly code: string;
    readonly password: string;
}