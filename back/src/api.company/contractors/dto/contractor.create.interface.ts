import { ContractorGroup } from "src/model/orm/contractor.group.entity";

export interface IContractorCreate {    
    readonly company_id: number;        
    readonly name: string;    
    readonly phone: string;    
    readonly img: string;        
    readonly banned: boolean;    
    readonly groups: ContractorGroup[];
}