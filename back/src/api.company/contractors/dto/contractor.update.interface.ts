import { ContractorGroup } from "src/model/orm/contractor.group.entity";

export interface IContractorUpdate {
    readonly id: number;    
    readonly company_id: number;        
    readonly name: string;    
    readonly phone: string;    
    readonly img: string;    
    readonly chat_active: boolean;
    readonly chat_at: string;       
    readonly banned: boolean;    
    readonly created_at: string;
    readonly groups: ContractorGroup[];
}