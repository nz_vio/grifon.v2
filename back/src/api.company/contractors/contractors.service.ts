import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { AppService } from "src/common/services/app.service";
import { UploadService2 } from "src/common/services/upload.service2";
import { IAnswer } from "src/model/dto/answer.interface";
import { IGetList } from "src/model/dto/getlist.interface";
import { ChatMessage } from "src/model/orm/chat.message.entity";
import { Contractor } from "src/model/orm/contractor.entity";
import { Like, Repository } from "typeorm";
import { IContractorCreate } from "./dto/contractor.create.interface";
import { IContractorUpdate } from "./dto/contractor.update.interface";

@Injectable()
export class ContractorsService {
    constructor(
        @InjectRepository(Contractor) private contractorRepository: Repository<Contractor>,
        @InjectRepository(ChatMessage) private chatMessageRepository: Repository<ChatMessage>,
        private uploadService2: UploadService2,
        private appService: AppService,
    ) {}
    
    public async allForChat(dto: IGetList): Promise<IAnswer<Contractor[]>> {
        try {            
            const contractors = await this.contractorRepository.find({where: dto.filter, order: {[dto.sortBy]: dto.sortDir}, relations: ["groups"]});             
            
            for (let contractor of contractors) {                
                const message = await this.chatMessageRepository.findOne({where: {contractor_id: contractor.id}, order: {created_at: "DESC"}});
                contractor.chat_messages = message ? [message] : [];                
            }

            return {statusCode: 200, data: contractors};
        } catch (err) {
            const errTxt: string = `Error in ContractorsService.allForChat: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    } 
    
    public async chunkForChat(dto: IGetList): Promise<IAnswer<Contractor[]>> {
        try {            
            const contractors = await this.contractorRepository.find({where: dto.filter, order: {[dto.sortBy]: dto.sortDir}, take: dto.q, skip: dto.from, relations: ["groups"]});  

            for (let contractor of contractors) {                
                const message = await this.chatMessageRepository.findOne({where: {contractor_id: contractor.id}, order: {created_at: "DESC"}});
                contractor.chat_messages = message ? [message] : [];                
            }

            const allLength = await this.contractorRepository.count(dto.filter);
            return {statusCode: 200, data: contractors, allLength};
        } catch (err) {
            const errTxt: string = `Error in ContractorsService.chunkForChat: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }      
    
    public async chunk(dto: IGetList): Promise<IAnswer<Contractor[]>> {
        try {
            const filter = this.appService.filterToQbfilter(dto.filter, "contractors");            
            const sortBy = `contractors.${dto.sortBy}`;
            const sortDir = dto.sortDir === 1 ? "ASC" : "DESC";
            const contractors = await this.contractorRepository
                .createQueryBuilder("contractors")
                .leftJoinAndSelect("contractors.groups", "groups")
                .where(filter, dto.filter)
                .orderBy({[sortBy]: sortDir})
                .take(dto.q)
                .skip(dto.from)
                .getMany();
            const allLength = await this.contractorRepository
                .createQueryBuilder("contractors")
                .where(filter, dto.filter)
                .getCount();
            return {statusCode: 200, data: contractors, allLength};
        } catch (err) {
            const errTxt: string = `Error in ContractorsService.chunk: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }  
    
    public async one(id: number): Promise<IAnswer<Contractor>> {
        try {
            const data = await this.contractorRepository.findOne(id, {relations: ["groups"]});                    
            return data ? {statusCode: 200, data} : {statusCode: 404, error: "contractor not found"};
        } catch (err) {
            const errTxt: string = `Error in ContractorsService.one: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }

    public async update(dto: IContractorUpdate): Promise<IAnswer<Contractor>> {
        try {                       
            if (!await this.contractorRepository.findOne(dto.id)) {
                return {statusCode: 404, error: "contractor not found"};
            }
            
            const x = this.contractorRepository.create(dto);
            await this.buildImg(x);
            await this.contractorRepository.save(x);      
            const data = await this.contractorRepository.findOne(x.id, {relations: ["groups"]});         
            return {statusCode: 200, data};
        } catch (err) {
            const errTxt: string = `Error in ContractorsService.update: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        } 
    }

    public async create(dto: IContractorCreate): Promise<IAnswer<Contractor>> {        
        try {            
            const x = this.contractorRepository.create(dto);
            await this.buildImg(x);
            await this.contractorRepository.save(x);
            const data = await this.contractorRepository.findOne(x.id, {relations: ["groups"]});        
            return {statusCode: 200, data};
        } catch (err) {
            const errTxt: string = `Error in ContractorsService.create: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }        
    }

    public async delete(id: number): Promise<IAnswer<void>> {
        try {
            if (!await this.contractorRepository.findOne(id)) {
                return {statusCode: 404, error: "contractor not found"};
            }

            await this.contractorRepository.delete(id);
            return {statusCode: 200};
        } catch (err) {
            const errTxt: string = `Error in ContractorsService.delete: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }        
    }

    private async buildImg(x: Contractor): Promise<void> {
        if (x.img && x.img.includes("base64")) { // attached new file
            const paths = await this.uploadService2.imgUploadResize(x.img, "contractors", [200]);
            x.img = paths[0];
        }
    }

    /*
    private async fake(): Promise<void> {
        try {            
            for (let i = 0; i < 100; i++) {
                const c = new Contractor();
                const firstnames = ["Андрей", "Иван", "Алексей", "Федор", "Дмитрий", "Виктор", "Константин", "Николай", "Сергей", "Петр", "Денис", "Евгений", "Виталий"]; // 13
                const lastnames = ["Иванов", "Петров", "Сидоров", "Алексеев", "Федоров", "Николаев", "Кошкин", "Мышкин", "Хрюшкин", "Коровкин", "Зайцев", "Медведев", "Волков"];
                c.company_id = 1;
                c.name = `${firstnames[this.appService.random(0, 12)]} ${lastnames[this.appService.random(0, 12)]}`;
                c.phone = `38057000000${i}`;
                await this.contractorRepository.save(c);
            }
        } catch (err) {
            console.log(err);
        }     
    }
    */
}