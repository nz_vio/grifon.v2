import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { TypeOrmModule } from "@nestjs/typeorm";
import { jwtEmployeeConstants } from "src/common/auth.constants";
import { CommonModule } from "src/common/common.module";
import { ChatMessage } from "src/model/orm/chat.message.entity";
import { Contractor } from "src/model/orm/contractor.entity";
import { Employee } from "src/model/orm/employee.entity";
import { ContractorsController } from "./contractors.controller";
import { ContractorsService } from "./contractors.service";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Contractor,
            ChatMessage,            
            Employee,
        ]),
        JwtModule.register(jwtEmployeeConstants),          
        CommonModule,   
    ],    
    providers: [ContractorsService],
    controllers: [ContractorsController],
})
export class ContractorsModule {}
