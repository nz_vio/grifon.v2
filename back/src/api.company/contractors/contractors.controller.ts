import { Controller, Post, Body, UseGuards, Param } from "@nestjs/common";
import { EmployeesGuard } from "src/common/guards/employees.guard";
import { IAnswer } from 'src/model/dto/answer.interface';
import { IGetList } from "src/model/dto/getlist.interface";
import { Contractor } from "src/model/orm/contractor.entity";
import { ContractorsService } from "./contractors.service";
import { IContractorCreate } from "./dto/contractor.create.interface";
import { IContractorUpdate } from "./dto/contractor.update.interface";

@Controller('api/company/contractors')
export class ContractorsController {
    constructor (private contractorsService: ContractorsService) {}              

    // get all with last message
    @Post("all/for-chat")
    @UseGuards(EmployeesGuard)
    public allForChat(@Body() dto: IGetList): Promise<IAnswer<Contractor[]>> {
        return this.contractorsService.allForChat(dto);
    }

    // get fragment with last message
    @Post("chunk/for-chat")
    @UseGuards(EmployeesGuard)
    public chunkForChat(@Body() dto: IGetList): Promise<IAnswer<Contractor[]>> {
        return this.contractorsService.chunkForChat(dto);
    }
    
    // get fragment
    @Post("chunk")
    @UseGuards(EmployeesGuard)
    public chunk(@Body() dto: IGetList): Promise<IAnswer<Contractor[]>> {
        return this.contractorsService.chunk(dto);
    }

    // get one    
    @Post("one/:id")
    @UseGuards(EmployeesGuard)
    public one(@Param("id") id: string): Promise<IAnswer<Contractor>> {
        return this.contractorsService.one(parseInt(id));
    }

    // update
    @Post("update")
    @UseGuards(EmployeesGuard)    
    public update(@Body() dto: IContractorUpdate): Promise<IAnswer<Contractor>> {
        return this.contractorsService.update(dto);
    }

    // create
    @Post("create")
    @UseGuards(EmployeesGuard)    
    public create(@Body() dto: IContractorCreate): Promise<IAnswer<Contractor>> {
        return this.contractorsService.create(dto);
    }

    // delete one
    @Post("delete/:id")
    @UseGuards(EmployeesGuard)    
    public delete(@Param("id") id: string): Promise<IAnswer<void>> {
        return this.contractorsService.delete(parseInt(id));
    }
}
