import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ObjectsController } from "./objects.controller";
import { ObjectsService } from "./objects.service";
import { jwtEmployeeConstants } from "../../common/auth.constants";
import { Employee } from "src/model/orm/employee.entity";
import { Contractor } from "src/model/orm/contractor.entity";
import { ChatMessage } from "src/model/orm/chat.message.entity";

@Module({
	controllers: [ObjectsController],
	providers: [ObjectsService],
	imports: [		
		JwtModule.register(jwtEmployeeConstants),			
		TypeOrmModule.forFeature([
			Employee,
			Contractor,
			ChatMessage,
		]),
	],
})
export class ObjectsModule {}
