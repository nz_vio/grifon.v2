import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { IAnswer } from 'src/model/dto/answer.interface';
import { IUpdateParam } from "src/model/dto/updateparam.interface";
import { Contractor } from "src/model/orm/contractor.entity";
import { ChatMessage } from "src/model/orm/chat.message.entity";

@Injectable()
export class ObjectsService {
    constructor (
        @InjectRepository(Contractor) private contractorRepository: Repository<Contractor>,        
        @InjectRepository(ChatMessage) private chatMessageRepository: Repository<ChatMessage>,        
    ) {}

    public async updateParam (dto: IUpdateParam): Promise<IAnswer<void>> {        
        try {                                    
            await this[`${dto.obj}Repository`].update(dto.id, {[dto.p]: dto.v});
            return {statusCode: 200};
        } catch (err) {
            const errTxt: string = `Error in ObjectsService.updateparam: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }

    public async updateEgoisticParam (dto: IUpdateParam): Promise<IAnswer<void>> {        
        try {
            if (dto.v) { // if param is true, first set all to false, because it is egoistic! :-)                                
                this[`${dto.obj}Repository`].update({}, {[dto.p]: false});
            }
            
            await this[`${dto.obj}Repository`].update(dto.id, {[dto.p]: dto.v});
            return {statusCode: 200};
        } catch (err) {
            const errTxt: string = `Error in ObjectsService.updateEgoisticParam: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }    
}
