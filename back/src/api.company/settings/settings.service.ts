import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Setting } from "src/model/orm/setting.entity";
import { IAnswer } from 'src/model/dto/answer.interface';
import { Settings } from "./dto/settings.type";

@Injectable()
export class SettingsService {
    constructor (@InjectRepository(Setting) private settingRepository: Repository<Setting>) {}    

    public async all(): Promise<IAnswer<Settings>> {        
        try {
            const sl = await this.settingRepository.find({where: {in_app: true}});            
            const data = {};            

            for (let s of sl) {
                data[s.p] = s.v;
            }

            return {statusCode: 200, data};
        } catch (err) {
            const errTxt: string = `Error in SettingsService.all: ${String(err)}`;
            console.log(errTxt);
            return {statusCode: 500, error: errTxt};
        }
    }    
}
