import { Controller, Post } from "@nestjs/common";
import { IAnswer } from 'src/model/dto/answer.interface';
import { Settings } from "./dto/settings.type";
import { SettingsService } from "./settings.service";

@Controller('api/company/settings')
export class SettingsController {
    constructor (private settingsService: SettingsService) {}    
    
    @Post("all")
    public all(): Promise<IAnswer<Settings>> {
        return this.settingsService.all();
    }    
}
