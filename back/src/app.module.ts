import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Setting } from './model/orm/setting.entity';
import { cfg } from './config';
import { ApiCompanyModule } from './api.company/api.company.module';
import { Company } from './model/orm/company.entity';
import { Mailtemplate } from './model/orm/mailtemplate.entity';
import { Employee } from './model/orm/employee.entity';
import { EmployeeGroup } from './model/orm/employee.group.entity';
import { EmployeeAction } from './model/orm/employee.action.entity';
import { Admin } from './model/orm/admin.entity';
import { AdminGroup } from './model/orm/admin.group.entity';
import { Verification } from './model/orm/verification.entity';
import { Contractor } from './model/orm/contractor.entity';
import { ChatMessage } from './model/orm/chat.message.entity';
import { ContractorGroup } from './model/orm/contractor.group.entity';
import { Deal } from './model/orm/deal.entity';
import { Action } from './model/orm/action.entity';
import { ActionFile } from './model/orm/action.file.entity';
import { DealDate } from './model/orm/deal.date.entity';
import { ActionReminder } from './model/orm/action.reminder.entity';
import { ActionStatus } from './model/orm/action.status.entity';
import { ActionTag } from './model/orm/action.tag.entity';

@Module({
	imports: [		
		TypeOrmModule.forRoot({
			type: "postgres",
			host: cfg.dbHost,
			port: cfg.dbPort,
			username: cfg.dbLogin,
			password: cfg.dbPassword,
			database: cfg.dbName,
			entities: [
				Admin,
				AdminGroup,
				Setting,			
				Mailtemplate,				
				Company,				
				Employee,
				EmployeeGroup,
				EmployeeAction,
				Verification,	
				Contractor,
				ContractorGroup,
				ChatMessage,			
				Deal,
				DealDate,
				Action,
				ActionFile,
				ActionReminder,
				ActionStatus,
				ActionTag,
			],
			synchronize: true,
		}),			
		ApiCompanyModule,
	],	
})
export class AppModule {}
