import { NestFactory } from '@nestjs/core';
import { json } from 'express';
import { AppModule } from './app.module';
import { cfg } from './config';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.enableCors({origin: cfg.corsedUrls});	
	app.use(json({limit: '50mb'}));
	await app.listen(cfg.appPort);
}

bootstrap();
