import { Injectable, CanActivate, ExecutionContext, HttpException, ForbiddenException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectRepository } from "@nestjs/typeorm";
import { Employee } from "src/model/orm/employee.entity";
import { Repository } from "typeorm";

@Injectable()
export class EmployeesGuard implements CanActivate {
    constructor(
        private jwtService: JwtService,
        @InjectRepository(Employee) private employeesRepository: Repository<Employee>,
    ) {}

    public async canActivate(context: ExecutionContext): Promise<boolean> {        
        try {
            const token: string = context.switchToHttp().getRequest().headers["token"];        
            const data = this.jwtService.verify(token);      
            const id: number = data.id;
            const employee = await this.employeesRepository.findOne(id);

            if (!employee || !employee.active) {
                throw new ForbiddenException();
            }
            
            return true;
        } catch (err) {
            throw new HttpException({statusCode: 403, error: "unauthorized"}, 200);
        }        
    }
}