// expiresIn - time in seconds    

export const jwtAdminConstants = {
    secret: "koshechki",
    signOptions: {expiresIn: 60*60*24*365}, 
};

export const jwtEmployeeConstants = {
    secret: "sobachki",
    signOptions: {expiresIn: 60*60*24*1}, 
};
