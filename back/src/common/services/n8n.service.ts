import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import axios from "axios";
import { IOutAction } from "src/api.company/actions/dto/out.action.interface";
import { IOutMessage } from "src/api.company/chat-messages/dto/out.message.interface";
import { IGupshupResponse } from "src/model/dto/gupshup.response.interface";
import { Setting } from "src/model/orm/setting.entity";
import { Repository } from "typeorm";

@Injectable()
export class N8NService {
    constructor(@InjectRepository(Setting) private settingRepository: Repository<Setting>) {}
    
    public async msgToGupshup(msg: IOutMessage): Promise<IGupshupResponse> {
        const url: string = (await this.settingRepository.findOne({where: {p: "n8n-msgtogupshup-url"}}))?.v;   
        
        if (!url) {
            throw new Error("setting not found");                    
        }

        const response = await axios.post(url, msg, {});
        return response.data;
    }    

    public async actionToGupshup(action: IOutAction): Promise<IGupshupResponse> {
        const url: string = (await this.settingRepository.findOne({where: {p: "n8n-actiontogupshup-url"}}))?.v;   
        
        if (!url) {
            throw new Error("setting not found");                    
        }

        const response = await axios.post(url, action, {});
        return response.data;
    }
}