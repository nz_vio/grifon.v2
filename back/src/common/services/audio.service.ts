import { Injectable } from "@nestjs/common";
import { createReadStream, createWriteStream } from "fs";
import { Lame } from "node-lame";
import * as prism from "prism-media";
import * as wavConverter from "wav-converter";
import { readFile, unlink, writeFile } from "fs/promises";


// WARNING: Lame encoder installed in OS required!
@Injectable()
export class AudioService {
    public async ogg2mp3(inPath: string, outPath: string, deleteInPath: boolean = true): Promise<void> {
        await this.ogg2pcm(inPath, inPath+"_pcm", deleteInPath);
        await this.pcm2wav(inPath+"_pcm", inPath+"_wav", true);
        await this.wav2mp3(inPath+"_wav", outPath, true);        
    }    
    
    public ogg2pcm(inPath: string, outPath: string, deleteInPath: boolean = true): Promise<void> {
        return new Promise((resolve, reject) => {
            const writeStream = createWriteStream(outPath);
            let error = null;
            writeStream.on('error', err => {
                error = err;
                writeStream.close();
                reject(err);
            });
            writeStream.on('close', () => {
                if (!error) {
                    deleteInPath ? unlink(inPath) : null;
                    resolve();
                }              
            });
            createReadStream(inPath)
                .pipe(new prism.opus.OggDemuxer())
                .pipe(new prism.opus.Decoder({ rate: 48000, channels: 2, frameSize: 960 }))
                .pipe(writeStream);
        });
    } 
    
    public async pcm2wav(inPath: string, outPath: string, deleteInPath: boolean = true): Promise<void> {        
        const pcmData = await readFile(inPath);
        const wavData = wavConverter.encodeWav(pcmData, {numChannels: 2, sampleRate: 48000});
        await writeFile(outPath, wavData);   
        deleteInPath ? unlink(inPath) : null;       
    }    
    
    public async wav2mp3(inPath: string, outPath: string, deleteInPath: boolean = true): Promise<void> {
        const encoder = new Lame({output: outPath, bitrate: 128}).setFile(inPath);    
        await encoder.encode();   
        deleteInPath ? unlink(inPath) : null;      
    }    
}