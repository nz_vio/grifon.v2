import { Injectable } from "@nestjs/common";
import * as bcrypt from "bcrypt";

@Injectable()
export class AppService {
    public passwordCompareHash(password, hash): Promise<boolean> {
        return new Promise((resolve, reject) => bcrypt.compare(password, hash, (err, result) => err ? reject(err) : resolve(result)));
    }

    public passwordBuildHash(password: string): string {
        return bcrypt.hashSync(password, 10);
    }

    public random(min: number, max: number): number {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public randomString(length: number, mode: string = "full"): string {
        let result: string = '';
        let characters: string = "";
        
        if (mode === "full") characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        if (mode === "lowercase") characters = "abcdefghijklmnopqrstuvwxyz0123456789";
        if (mode === "digits") characters = "0123456789";        
        
        var charactersLength = characters.length;
        
        for (let i: number = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        
        return result;
    }

    public arrayUnique(array: any[]): any[] {
        return [...new Set(array)];
    }

    public twoDigits(n: number): string {
        return (n < 10) ? `0${n}` : `${n}`;
    }    

    public percentageDiff(a: number, b: number): number {        
        return 100 * (a - b) / b;        
    }    

    public filterToQbfilter(filter: any, entity: string): string {        
        let filterParts: string[] = [];        
        
        for (let field in filter) {                            
            if (filter[field] !== "" && filter[field] !== null) {
                let operator: string = typeof(filter[field]) === "string" && filter[field].includes("%") ? "ILIKE" : "=";            
                filterParts.push(`${entity}.${field} ${operator} :${field}`);
            }            
        }        
        
        return filterParts.join(" AND ");
    }

    public getFileExtension(filename: string): string {
        return /(?:\.([^.]+))?$/.exec(filename)[1] || "";  
    }

    ////////////// dates //////////////

    public humanDate(date: Date): string {
        return date ? `${this.twoDigits(date.getDate())}.${this.twoDigits(date.getMonth()+1)}.${date.getFullYear()} ${this.twoDigits(date.getHours())}:${this.twoDigits(date.getMinutes())}` : "";
    }

    public mysqlDate(date: Date): string {
        return `${date.getFullYear()}-${this.twoDigits(date.getMonth()+1)}-${this.twoDigits(date.getDate())}`;
    }

    // date of previous monday/tuesday/...
    public prevDayOfWeek(d: Date, dayOfWeek: number): Date {
        const t = new Date(d);
        t.setDate(t.getDate() - (t.getDay() + (7 - dayOfWeek)) % 7);
        return t;    
    }

    // date of next monday/tuesday/...
    public nextDayOfWeek(d: Date, dayOfWeek: number): Date {    
        const t = new Date(d);
        t.setDate(t.getDate() + (((dayOfWeek + 7 - t.getDay()) % 7) || 7));
        return t;
    }   

    // first day of month for current date
    public firstDayOfMonth(d: Date): Date {
        return new Date(d.getFullYear(), d.getMonth(), 1);
    }
    
    // first day of quarter for current date
    public firstDayOfQuarter(d: Date): Date {
        const quarter = Math.floor((d.getMonth() / 3));
        return new Date(d.getFullYear(), quarter * 3, 1);        
    }

    // first day of year for current date
    public firstDayOfYear(d: Date): Date {
        return new Date(d.getFullYear(), 0, 1);       
    }        
}
