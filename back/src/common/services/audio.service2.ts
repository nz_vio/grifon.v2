import { Injectable } from "@nestjs/common";
import { createReadStream, createWriteStream } from "fs";
import * as prism from "prism-media";
import { unlink } from "fs/promises";

// @suldashi/lame - подправленная версия пакета lame (перед использованием добавить в package.json и установить)
// Оригинальный пакет lame на мак на ставится, с чем-то конфликтует.
// Еще надо тестировать, node.js иногда падает после конвертации.
// Можно также использовать костыльный вариант с тремя шагами конвертации (ogg->pcm->wav->mp3), там другой пакет для кодирования mp3 - см. audio.service.ts 
@Injectable()
export class AudioService2 {    
    public ogg2mp3(inPath: string, outPath: string, deleteInPath: boolean = true): Promise<void> {
        return new Promise((resolve, reject) => {
            const lame = require("@suldashi/lame");
            const mp3encoder = new lame.Encoder({
                // input
                channels: 2,        
                bitDepth: 16,       
                sampleRate: 48000,                
                // output
                bitRate: 128,
                outSampleRate: 22050,
                mode: lame.STEREO, 
            });
            const writeStream = createWriteStream(outPath);
            let error = null;
            writeStream.on('error', err => {
                error = err;
                writeStream.close();
                reject(err);
            });
            writeStream.on('close', () => {
                if (!error) {
                    deleteInPath ? unlink(inPath) : null;
                    resolve();
                }              
            });
            createReadStream(inPath)
                .pipe(new prism.opus.OggDemuxer())
                .pipe(new prism.opus.Decoder({ rate: 48000, channels: 2, frameSize: 960 }))
                .pipe(mp3encoder)
                .pipe(writeStream);
        });
    }       
}