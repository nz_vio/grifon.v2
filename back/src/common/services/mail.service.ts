import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import * as Nodemailer from "nodemailer";
import Mail from "nodemailer/lib/mailer";
import { MailOptions } from "nodemailer/lib/sendmail-transport";
import { Setting } from "src/model/orm/setting.entity";
import { Mailtemplate } from "src/model/orm/mailtemplate.entity";

@Injectable()
export class MailService {
    constructor(
        @InjectRepository(Setting) private settingRepository: Repository<Setting>,
        @InjectRepository(Mailtemplate) private mailtemplateRepository: Repository<Mailtemplate>,        
    ) {}
    
    public async send(to: string, subject: string, html: string): Promise<void> {        
        const host: string = (await this.settingRepository.findOne({where: {p: "smtp-host"}}))?.v;                
        const port: string = (await this.settingRepository.findOne({where: {p: "smtp-port"}}))?.v;                                
        const login: string = (await this.settingRepository.findOne({where: {p: "smtp-login"}}))?.v;
        const from: string = (await this.settingRepository.findOne({where: {p: "smtp-from"}}))?.v;           
        const pw: string = (await this.settingRepository.findOne({where: {p: "smtp-pw"}}))?.v;
    
        if (!host || !port || !login || !from || !pw) {  
            throw new Error("some setting not found");
        }                     
                
        const transporter: Mail = Nodemailer.createTransport({host, port: parseInt(port), secure: false, auth: {user: login, pass: pw}});
        const mailOptions: MailOptions = {from, to, subject, html};
        const data = await transporter.sendMail(mailOptions);            
        console.log(`Email sent: ${new Date()}, ${data.response}`);                    
    }       

    private async getMailtemplate(name: string): Promise<Mailtemplate> {
        let mt: Mailtemplate = await this.mailtemplateRepository.findOne({where: {name}});            
        
        if (!mt) {
            throw new Error("mailtemplate not found");
        }
            
        return mt;
    }

    private buildCycledFragment(content: string, array: any[], arrayName: string, arrayElementName: string, arrayElementFields: string[]): string {
        const cycledRawStart = `{{foreach ${arrayName} ${arrayElementName}}}`;
        const cycledRawEnd = "{{endforeach}}";
        const cycledRawStartPos = content.indexOf(cycledRawStart) + cycledRawStart.length;            
        const cycledRawEndPos = content.indexOf(cycledRawEnd, cycledRawStartPos);
        const cycledRawInner = content.substr(cycledRawStartPos, cycledRawEndPos - cycledRawStartPos);
        const cycledRaw = cycledRawStart + cycledRawInner + cycledRawEnd;
        let cycledCompiled = "";

        for (let x of array) {
            let item = cycledRawInner;

            for (let field of arrayElementFields) {
                let expressionToReplace = new RegExp(`{{${arrayElementName}.${field}}}`, "g");
                item = item.replace(expressionToReplace, x[field] || "");
            }            
            
            cycledCompiled += item;
        }

        return content.replace(cycledRaw, cycledCompiled);
    }

    public async mailTest(): Promise<void> {
        try {  
            const mt = await this.getMailtemplate("test");   
            await this.send("7573497@gmail.com", mt.subject, mt.content);   
        } catch (err) {
            console.log(`Error in MailService.mailTest: ${String(err)}`); 
        }
    }  
    
    public async mailEmployeeEmailVerification(email: string, code: string): Promise<void> {
        try {
            const mt = await this.getMailtemplate("employee-email-verification");
            const subject: string = mt.subject;
            const content: string = mt.content                
                .replace(/{{code}}/g, code);
            await this.send(email, subject, content);                 
        } catch (err) {
            console.log(`Error in MailService.mailEmployeeEmailVerification: ${String(err)}`);      
        }
    }
}