import { Injectable } from "@nestjs/common";
import axios from "axios";
import { createWriteStream, existsSync, mkdirSync } from "fs";

@Injectable()
export class DownloadService {
    public download(url: string, folder: string, filename: string): Promise<void> {
        return new Promise((resolve, reject) => {
            !existsSync(folder) ? mkdirSync(folder, {recursive: true}) : null;            
            const writeStream = createWriteStream(`${folder}/${filename}`);
            let error = null;
            writeStream.on('error', err => {
                error = err;
                writeStream.close();
                reject(err);
            });
            writeStream.on('close', () => {
                if (!error) {
                    resolve();
                }              
            });
            axios
                .get(url, {responseType: "stream"})
                .then(res => res.data.pipe(writeStream));            
        });      
    }    
}