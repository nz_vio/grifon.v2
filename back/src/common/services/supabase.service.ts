import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { createClient, SupabaseClient } from '@supabase/supabase-js'
import { Setting } from "src/model/orm/setting.entity";
import { Repository } from "typeorm";

type SupabaseCompatibleFile = ArrayBuffer | ArrayBufferView | Blob | Buffer | File | FormData | ReadableStream | ReadableStream | URLSearchParams | string;

@Injectable()
export class SupabaseService {
    constructor(@InjectRepository(Setting) private settingRepository: Repository<Setting>) {}

    private async initClient(): Promise<SupabaseClient> {
        const apiUrl: string = (await this.settingRepository.findOne({where: {p: "supabase-api-url"}}))?.v;   
        const apiKey: string = (await this.settingRepository.findOne({where: {p: "supabase-api-key"}}))?.v;   
        return createClient(apiUrl, apiKey);
    }

    public async uploadFile(bucket: string, path: string, file: SupabaseCompatibleFile, contentType?: string): Promise<any> {
        const options = contentType ? {contentType} : null;
        const client = await this.initClient();
        const {data, error} = await client.storage.from(bucket).upload(path, file, options);                
        
        if (error) {
            throw new Error(error.message);
        }        
    }
}