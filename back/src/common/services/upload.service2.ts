import { Injectable } from '@nestjs/common';
import * as sharp from 'sharp';
import * as fs from "fs";

// этот сервис обрабатывает данные не из файлов, а из строки
@Injectable()
export class UploadService2 {    
    public async imgUploadResize(data: string, folder: string, widths: number[]): Promise<string[]> {                
        const diskFolder = `../static/img/${folder}`;
        const diskSubfolder = `${new Date().getFullYear ()}-${new Date().getMonth() + 1}`;
        const diskFullFolder = `${diskFolder}/${diskSubfolder}`;            
        !fs.existsSync(diskFullFolder) ? fs.mkdirSync(diskFullFolder, {recursive: true}) : null;            
        const paths: string[] = [];                        
                
        for (let w of widths) {
            let filename = await this.saveResizedImg(diskFullFolder, data, w);
            paths.push(`${diskSubfolder}/${filename}`);
        }                                     

        return paths;         
    }
    
    public async imgUpload(data: string, folder: string): Promise<string> {                
        const diskFolder: string = `../static/img/${folder}`;
        const diskSubfolder: string = `${new Date ().getFullYear ()}-${new Date().getMonth() + 1}`;
        const diskFullFolder: string = `${diskFolder}/${diskSubfolder}`;            
        !fs.existsSync(diskFullFolder) ? fs.mkdirSync(diskFullFolder, {recursive: true}) : null;                    
        const filename = await this.saveFile(diskFullFolder, data);
        const path = `${diskSubfolder}/${filename}`;
        return path;         
    }
    
    private saveFile(folder: string, data: string): Promise<string> {
        return new Promise((resolve, reject) => {
            const fileName = Math.round(new Date().getTime()).toString() + this.getFileExtension(data);            
            const buffer = Buffer.from(data.split(",")[1], 'base64');
            fs.writeFile(`${folder}/${fileName}`, buffer, (err) => err ? reject(err) : resolve(fileName));
        });
    }

    private async saveResizedImg(folder: string, data: string, width: number): Promise<string> {        
        const fileName: string = Math.round(new Date().getTime()).toString()+`_${width}.jpg`;        
        const buffer = Buffer.from(data.split(",")[1], 'base64');        
        await sharp(buffer)
            .rotate() // поворачиваем в соответствии с метаданными, которые по умолчанию не используются (и не надо, т.к. не все устройства их понимают!)    
            .resize({width, withoutEnlargement: true})
            .jpeg({quality: 80})
            .toFile(`${folder}/${fileName}`);
        return fileName;        
    }

    private getFileExtension(data: string): string {
        const mime = this.getMime(data);

        if (mime === "image/jpeg") return ".jpg";
        if (mime === "image/png") return ".png";
        
        return "";
    }

    private getMime(data: string): string {
        return data.substring(data.indexOf(":")+1, data.indexOf(";"));
    }
}
