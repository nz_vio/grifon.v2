import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Mailtemplate } from "src/model/orm/mailtemplate.entity";
import { Setting } from "src/model/orm/setting.entity";
import { AppService } from "./services/app.service";
import { AudioService } from "./services/audio.service";
import { DownloadService } from "./services/download.service";
import { MailService } from "./services/mail.service";
import { N8NService } from "./services/n8n.service";
import { SupabaseService } from "./services/supabase.service";
import { UploadService } from "./services/upload.service";
import { UploadService2 } from "./services/upload.service2";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Setting,
            Mailtemplate,            
        ]),            
    ],
    providers: [
        AppService,               
        UploadService,
        UploadService2,
        DownloadService,
        AudioService,        
        MailService,       
        SupabaseService,   
        N8NService,
    ],
    exports: [
        AppService,           
        UploadService,
        UploadService2,
        DownloadService,
        MailService, 
        AudioService,              
        SupabaseService,
        N8NService,
    ],
})
export class CommonModule {}
