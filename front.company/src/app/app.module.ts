import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppRouteReuseStrategy } from './app.routereusestrategy';
import { CCModule } from './common.components/cc.module';
import { ServicesModule } from './services/services.module';
import { HomeModule } from './views/home/home.module';

@NgModule({
	bootstrap: [AppComponent],
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		HttpClientModule,
		AppRoutingModule,
		ServicesModule,				
		HomeModule,
		CCModule,
	],
	providers: [
		{provide: RouteReuseStrategy, useClass: AppRouteReuseStrategy},
	],		
})
export class AppModule { }
