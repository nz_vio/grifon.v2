import { Injectable } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { Lang } from "../model/orm/lang.model";
import { Settings } from "../model/orm/settings.type";

@Injectable()
export class AppService {
    // data    
    public lang: Lang = null;
    public settings: Settings = null;    
    // iface    
    public win: HTMLElement = null; 
    public sidebarActive: boolean = true;
    // error notifications
    public errorActive: boolean = false;
    public errorMessage: string = "";
    public errorTimer: number = null;  
    
    constructor(
        private titleService: Title,        
        private router: Router,
    ) {}        

    get url(): string[] {return this.router.url.split("/");}
    get storageUrl(): string {return this.settings["supabase-storage-url"];}
    
    public showError(error: any): void {
        this.errorTimer ? clearTimeout(this.errorTimer) : null;
        this.errorMessage = typeof(error) !== "string" ? JSON.stringify(error) : error;        
        this.errorActive = true;
        this.errorTimer = window.setTimeout (() => {
            this.errorActive = false;
            this.errorTimer = null;
        }, 3000);        
        console.log(error);
    }

    public setTitle (title: string) {        
        this.titleService.setTitle(`${this.lang.phrases["common-name"]} - ${title}`);
    }    

    public validateEmail (email: string): boolean {
        const re: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test (email.toLowerCase());
    }    

    public pause(ms: number): Promise<void> {
        return new Promise((resolve, reject) => {
            setTimeout(() => resolve(), ms);
        });
    }     

    // когда нажали на элемент, проверяем всю иерархию DOM на наличие хотя бы одного из указанных class
    public pathHasClasses(elements: HTMLElement[], classNames: string[]): boolean {
        for (let element of elements) {
            for (let className of classNames) {
                if (element.classList?.contains(className)) {
                    return true;
                }
            }            
        }

        return false;
    }    

    // когда нажали на элемент, проверяем всю иерархию DOM на наличие хотя бы одного из указанных id
    public pathHasIds(elements: HTMLElement[], ids: string[]): boolean {
        for (let element of elements) {
            for (let id of ids) {
                if (element.id === id) {
                    return true;
                }
            }            
        }

        return false;
    }

    public twoDigits(n: number): string {
        return (n < 10) ? `0${n}` : `${n}`;
    }

    public formattedDateTime(date: Date): string {
        return date ? `${this.twoDigits(date.getDate())}.${this.twoDigits(date.getMonth()+1)}.${date.getFullYear()} ${this.twoDigits(date.getHours())}:${this.twoDigits(date.getMinutes())}` : "";
    }

    public formattedDate(date: Date): string {
        return date ? `${this.twoDigits(date.getDate())}.${this.twoDigits(date.getMonth()+1)}.${date.getFullYear()}` : "";
    }

    public randomString(length: number, mode: string = "full"): string {
        let result: string = '';
        let characters: string = "";
        
        if (mode === "full") characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        if (mode === "lowercase") characters = "abcdefghijklmnopqrstuvwxyz0123456789";
        if (mode === "digits") characters = "0123456789";        
        
        var charactersLength = characters.length;
        
        for (let i: number = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        
        return result;
    }

    public daysInMonth (month: number, year: number): number {
        return new Date(year, month, 0).getDate();
    }

    public range(a: number, b: number): number[] {
        const arr: number[] = [];

        for (let i = a; i <= b; i++) {
            arr.push(i);
        }

        return arr;
    }    

    public mysqlDate(date: Date): string {
        return `${date.getFullYear()}-${this.twoDigits(date.getMonth()+1)}-${this.twoDigits(date.getDate())}`;
    }

    public capitalize(s: string): string {
        return s.charAt(0).toUpperCase() + s.slice(1);
    } 
    
    public contrastColor(index: number): string { // every color will contrast with neighbors
        const hue = index * 137.508; // use golden angle approximation
        return `hsl(${hue},50%,60%)`;
    }  

    public sort(arr: any[], by: string = "pos", dir: number = 1): void {
        arr.sort((a: any, b: any) => {
            let x: any = (typeof(a[by]) === "string") ? (a[by] as string).toLowerCase() : a[by];
            let y: any = (typeof(b[by]) === "string") ? (b[by] as string).toLowerCase() : b[by];
                            
            if (dir === 1) {
                if (x > y) return 1;
                if (x < y) return -1;                    
                return 0;
            } else if (dir === -1) {
                if (x < y) return 1;
                if (x > y) return -1;
                return 0;
            }

            return 0;
        });
    } 

    public rnd(min: number, max: number): number {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public clone(src: any): any {
        return JSON.parse(JSON.stringify(src));
    }

    public openWindow(url: string): void {
        const link = document.createElement('a');
        link.href = url;            
        link.setAttribute("target", "_blank");
        document.body.appendChild(link);            
        link.click();   
    }    

    public seconds2time (s: number): string {
        const minutes = Math.floor(s / 60);
        const seconds = Math.round(s - minutes * 60);
        return `${minutes}:${this.twoDigits(seconds)}`;
    }  
}