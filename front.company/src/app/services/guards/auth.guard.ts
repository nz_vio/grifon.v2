import { Injectable } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard {    
    constructor (
        private EmployeeService: EmployeeService,
        private router: Router,
    ) { }
    
    public canActivate (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {        
        if (this.EmployeeService.authData.value !== null) {
            return true;
        } else {
            this.router.navigateByUrl ("/auth/login");
            return false;
        }        
    }    
}
