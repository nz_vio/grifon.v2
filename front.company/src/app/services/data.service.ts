import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { filter } from "rxjs/operators";
import { Router } from "@angular/router";
import { IAnswer } from "../model/dto/answer.interface";
import { Settings } from "../model/orm/settings.type";
import { IEmployeeAuthData } from "../model/dto/employee.authdata.interface";
import { IEmployeeLogin } from "../model/dto/employee.login.interface";
import { IVerify } from "../model/dto/verify.interface";
import { IEmployeeRecovery } from "../model/dto/employee.recovery.interface";
import { Contractor } from "../model/orm/contractor.model";
import { IGetList } from "../model/dto/getlist.interface";
import { ChatMessage } from "../model/orm/chat.message.model";
import { ContractorGroup } from "../model/orm/contractor.group.model";
import { CfgRepository } from "./repositories/cfg.repository";
import { Deal } from "../model/orm/deal.model";
import { IActionCreate } from "../model/dto/action.create.interface";
import { Action } from "../model/orm/action.model";
import { ActionStatus } from "../model/orm/action.status.model";
import { ActionTag } from "../model/orm/action.tag.model";

@Injectable()
export class DataService {
    public authData: BehaviorSubject<IEmployeeAuthData> = new BehaviorSubject(null);    
    
    constructor (
        private cfgRepository: CfgRepository,
        private http: HttpClient,        
        private router: Router,
    ) {} 

    get cfg(): any {return this.cfgRepository.data;}

    public updateParam (obj: string, id: number, p: string, v:any): Observable<IAnswer<void>> {return this.sendRequest("objects/update-param", {obj, id, p, v}, true);}    
    public updateEgoisticParam (obj: string, id: number, p: string, v:boolean): Observable<IAnswer<void>> {return this.sendRequest("objects/update-egoistic-param", {obj, id, p, v}, true);}                    
    
    public settingsAll(): Observable<IAnswer<Settings>> {return this.sendRequest("settings/all");}     
    
    public employeesLogin(dto: IEmployeeLogin): Observable<IAnswer<IEmployeeAuthData>> {return this.sendRequest("employees/login", dto);}     
    public employeesVerifyEmail(dto: IVerify): Observable<IAnswer<void>> {return this.sendRequest("employees/verify-email", dto);}
    public employeesRecover(dto: IEmployeeRecovery): Observable<IAnswer<void>> {return this.sendRequest("employees/recover", dto);}

    public contractorsAllForChat(dto: IGetList): Observable<IAnswer<Contractor[]>> {return this.sendRequest(`contractors/all/for-chat`, dto, true);}
    public contractorsChunkForChat(dto: IGetList): Observable<IAnswer<Contractor[]>> {return this.sendRequest(`contractors/chunk/for-chat`, dto, true);}
    public contractorsChunk(dto: IGetList): Observable<IAnswer<Contractor[]>> {return this.sendRequest(`contractors/chunk`, dto, true);}
    public contractorsOne(id: number): Observable<IAnswer<Contractor>> {return this.sendRequest(`contractors/one/${id}`, null, true);}
    public contractorsUpdate(x: Contractor): Observable<IAnswer<Contractor>> {return this.sendRequest("contractors/update", x, true);}
    public contractorsCreate(x: Contractor): Observable<IAnswer<Contractor>> {return this.sendRequest("contractors/create", x, true);}
    public contractorsDelete(id: number): Observable<IAnswer<void>> {return this.sendRequest(`contractors/delete/${id}`, null, true);}

    public contractorGroupsAll(dto: IGetList): Observable<IAnswer<ContractorGroup[]>> {return this.sendRequest("contractor-groups/all", dto, true);}
    public contractorsGroupsChunk(dto: IGetList): Observable<IAnswer<ContractorGroup[]>> {return this.sendRequest(`contractor-groups/chunk`, dto, true);}
    
    public chatMessagesChunk(dto: IGetList): Observable<IAnswer<ChatMessage[]>> {return this.sendRequest(`chat-messages/chunk`, dto, true);}    

    public dealsChunk(dto: IGetList): Observable<IAnswer<Deal[]>> {return this.sendRequest(`deals/chunk`, dto, true);}
    public dealsOne(id: number): Observable<IAnswer<Deal>> {return this.sendRequest(`deals/one/${id}`, null, true);}
    public dealsUpdate(x: Deal): Observable<IAnswer<void>> {return this.sendRequest("deals/update", x, true);}
    public dealsCreate(x: Deal): Observable<IAnswer<void>> {return this.sendRequest("deals/create", x, true);}
    public dealsDelete(id: number): Observable<IAnswer<void>> {return this.sendRequest(`deals/delete/${id}`, null, true);}

    public actionsCreate(dto: IActionCreate): Observable<IAnswer<void>> {return this.sendRequest("actions/create", dto, true);}
    public actionsUpdate(x: Action): Observable<IAnswer<void>> {return this.sendRequest("actions/update", x, true);}

    public actionStatusesAll(dto: IGetList): Observable<IAnswer<ActionStatus[]>> {return this.sendRequest("action-statuses/all", dto, true);}

    public actionTagsAll(dto: IGetList): Observable<IAnswer<ActionTag[]>> {return this.sendRequest("action-tags/all", dto, true);}
    public actionTagsCreate(x: ActionTag): Observable<IAnswer<ActionTag>> {return this.sendRequest("action-tags/create", x, true);}
        
    private sendRequest (url: string, body: Object = null, authNeeded: boolean = false, withProgress: boolean = false): Observable<any> {        
        const headers: HttpHeaders = authNeeded ? new HttpHeaders({token: this.authData.value.token}) : null;
        
        if (withProgress) {
            return this.http
                .post(`${this.cfg.serverUrl}/${this.cfg.apiPath}/${url}`, body, {headers, observe: "events", reportProgress: true})
                .pipe(filter(res => this.processResponse(res)));
        } else {
            return this.http
                .post(`${this.cfg.serverUrl}/${this.cfg.apiPath}/${url}`, body, {headers})
                .pipe(filter(res => this.processResponse(res)));                    
        }                  
    }
    
    private sendDownloadRequest(url: string, body: Object = {}, authNeeded: boolean = false, filename: string): void {
        const headers: HttpHeaders = authNeeded ? new HttpHeaders({token: this.authData.value.token}) : null;
        this.http.post(`${this.cfg.serverUrl}/${this.cfg.apiPath}/${url}`, body, {headers, responseType: 'blob'}).subscribe(response => {
            let dataType = response.type;
            let binaryData = [];
            binaryData.push(response);
            let downloadLink = document.createElement('a');
            downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
            downloadLink.setAttribute('download', filename);
            document.body.appendChild(downloadLink);
            downloadLink.click();
        });
    }

    private processResponse(res: any): boolean {        
        if (res.statusCode === 403) {
            console.log(res.error);
            this.router.navigateByUrl("/auth/logout");
            return false;
        } 
            
        return true;        
    }   
}