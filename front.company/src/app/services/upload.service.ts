import { Injectable } from "@angular/core";
import { IFile } from "../model/file.interface";
import { IHTMLInputEvent } from "../model/htmlinputevent.interface";
import { CfgRepository } from "./repositories/cfg.repository";

@Injectable()
export class UploadService {
    constructor(private cfgRepository: CfgRepository) {}  

    get cfg(): any {return this.cfgRepository.data;}

    public selectImg(): Promise<string> {
        return new Promise((resolve, reject) => {            
            const element = document.createElement("input");
            element.style.display = "none";
            element.type = "file";
            element.onchange = async (event: IHTMLInputEvent) => {                
                if (event.target.files[0].size > this.cfg.maxImgSize) {
                    reject("too big file");                    
                } else {
                    const data = await this.readFile(event.target.files[0]);                    
                    this.isImg(data) ? resolve(data) : reject("use only JPG or PNG");
                }                
            }
            document.body.appendChild(element);
            element.click();
        });
    }

    public selectFile(): Promise<IFile> {
        return new Promise((resolve, reject) => {                        
            const element = document.createElement("input");
            element.style.display = "none";
            element.type = "file";
            element.onchange = async (event: IHTMLInputEvent) => {                
                if (event.target.files[0].size > this.cfg.maxImgSize) {
                    reject("too big file");
                } else {
                    const name = event.target.files[0].name;
                    const data = await this.readFile(event.target.files[0]);
                    const contentType = event.target.files[0].type;
                    resolve({name, data, contentType});
                }
            }
            document.body.appendChild(element);
            element.click();
        });
    }

    public selectFiles(): Promise<IFile[]> {
        return new Promise((resolve, reject) => {            
            const reader = new FileReader();
            const element = document.createElement("input");
            element.setAttribute("multiple", "multiple");
            element.style.display = "none";
            element.type = "file";
            element.onchange = async (event: IHTMLInputEvent) => {                
                const selectedFiles = Array.from(event.target.files).filter(f => f.size <= this.cfg.maxFileSize);

                if (selectedFiles.length !== event.target.files.length) {
                    reject("one of files is too big");
                } else {
                    const files: IFile[] = [];

                    for (let selectedFile of selectedFiles) {                        
                        const name = selectedFile.name;
                        const data = await this.readFile(selectedFile);
                        const contentType = selectedFile.type || "text/plain";                        
                        files.push({name, data, contentType});
                    }

                    resolve(files);
                }                
            }
            document.body.appendChild(element);
            element.click();
        });
    }

    public readFile(file: File): Promise<string> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = () => resolve(reader.result as string);
            reader.readAsDataURL(file);                        
        });        
    }

    public getMime(data: string): string {
        return data.substring(data.indexOf(":")+1, data.indexOf(";"));
    }

    public isImg(data: string): boolean {
        const mime = this.getMime(data);
        return mime === "image/jpeg" || mime === "image/png";
    }
}