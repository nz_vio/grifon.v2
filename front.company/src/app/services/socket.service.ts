import { Injectable } from "@angular/core";
import { io, Socket } from "socket.io-client";
import { CfgRepository } from "./repositories/cfg.repository";

@Injectable()
export class SocketService {    
    public socket: Socket = null;     
    
    constructor(private cfgRepository: CfgRepository) {}

    get cfg(): any {return this.cfgRepository.data;}

    public connect(): void {
        this.socket = io(this.cfg.serverUrl, {path: "/" + this.cfg.socketPath});            
        this.initEvents();
    }

    public disconnect(): void {
        // "?" is important because sometimes pages try to disconnect before init
        this.socket?.disconnect();        
        this.socket?.off(); // unsubscribe from all events        
    }

    private initEvents(): void {        
        this.socket.on("connect", () => console.log("socket connected"));
        this.socket.on("disconnect", () => console.log("socket disconnected"));   
        this.socket.on("connect_error", () => console.log("connection error"));
        this.socket.on("connect_timeout", () => console.log("connection error"));           
    }    
}
