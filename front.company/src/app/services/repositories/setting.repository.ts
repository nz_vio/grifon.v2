import { Injectable } from '@angular/core';

import { Settings } from 'src/app/model/orm/settings.type';
import { DataService } from '../data.service';

@Injectable()
export class SettingRepository {
    constructor(protected dataService: DataService) {}
    
    public loadAll(): Promise<Settings> {
        return new Promise((resolve, reject) => 
            this.dataService
                .settingsAll()
                .subscribe(
                    res => resolve(res.data), 
                    err => reject(err.message)));
    }
}
