import { Injectable } from "@angular/core";
import { Chunk } from "src/app/model/chunk";
import { IGetList } from "src/app/model/dto/getlist.interface";
import { Deal } from "src/app/model/orm/deal.model";
import { DataService } from "../data.service";
import { Repository } from "./_repository";

@Injectable()
export class DealRepository extends Repository<Deal> {
    protected schema: string = "deal";

    constructor(protected dataService: DataService) {
        super(dataService);
    }

    public loadChunk(part: number = 0, chunkLength: number = 10, sortBy: string = "id", sortDir: number = 1, filter: any = {}): Promise<Chunk<Deal>> {        
        const dto: IGetList = {from: part * chunkLength, q: chunkLength, sortBy, sortDir, filter};        
        return new Promise((resolve, reject) =>             
            this.dataService
                .dealsChunk(dto)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(new Chunk(res.data.map(d => new Deal().build(d)), res.allLength)) : reject(res.error), 
                    err => reject(err.message)));
    }   
    
    public loadOne(id: number): Promise<Deal> {
        return new Promise((resolve, reject) => 
            this.dataService
                .dealsOne(id)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(new Deal().build(res.data)) : reject(res.statusCode), 
                    err => reject(err.message)));
    }

    public create(x: Deal): Promise<void> {
        return new Promise((resolve, reject) => 
            this.dataService
                .dealsCreate(x)
                .subscribe(
                    res => res.statusCode === 200 ? resolve() : reject(res.error), 
                    err => reject(err.message)));
    }

    public update(x: Deal): Promise<void> {
        return new Promise((resolve, reject) => 
            this.dataService
                .dealsUpdate(x)
                .subscribe(
                    res => res.statusCode === 200 ? resolve() : reject(res.error), 
                    err => reject(err.message)));
    }

    public delete(id: number): Promise<void> {
        return new Promise((resolve, reject) => 
            this.dataService
                .dealsDelete(id)
                .subscribe(
                    res => res.statusCode === 200 ? resolve() : reject(res.error), 
                    err => reject(err.message)));
    }
}