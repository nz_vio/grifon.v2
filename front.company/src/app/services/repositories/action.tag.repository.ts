import { Injectable } from "@angular/core";
import { IGetList } from "src/app/model/dto/getlist.interface";
import { ActionTag } from "src/app/model/orm/action.tag.model";
import { DataService } from "../data.service";
import { Repository } from "./_repository";

@Injectable()
export class ActionTagRepository extends Repository<ActionTag> {
    constructor(protected dataService: DataService) {
        super(dataService);
    }

    public loadAll(sortBy: string, sortDir: number, filter: any = {}): Promise<ActionTag[]> {
        const dto: IGetList = {sortBy, sortDir, filter};       
        return new Promise((resolve, reject) =>             
            this.dataService
                .actionTagsAll(dto)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(res.data.map(d => new ActionTag().build(d))) : reject(res.error), 
                    err => reject(err.message)));
    }

    public create(x: ActionTag): Promise<ActionTag> {
        return new Promise((resolve, reject) => 
            this.dataService
                .actionTagsCreate(x)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(new ActionTag().build(res.data)) : reject(res.error), 
                    err => reject(err.message)));
    }
}