import { Injectable } from "@angular/core";
import { Chunk } from "src/app/model/chunk";
import { IGetList } from "src/app/model/dto/getlist.interface";
import { Contractor } from "src/app/model/orm/contractor.model";
import { DataService } from "../data.service";
import { Repository } from "./_repository";

@Injectable()
export class ContractorRepository extends Repository<Contractor> {
    protected schema: string = "contractor";

    constructor(protected dataService: DataService) {
        super(dataService);
    }

    public loadAllForChat(sortBy: string = "id", sortDir: number = 1, filter: any = {}): Promise<Contractor[]> {        
        const dto: IGetList = {sortBy, sortDir, filter};   
        return new Promise((resolve, reject) =>            
            this.dataService
                .contractorsAllForChat(dto)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(res.data.map(d => new Contractor().build(d))) : reject(res.error), 
                    err => reject(err.message)));
    }

    public loadChunkForChat(part: number = 0, chunkLength: number = 10, sortBy: string = "id", sortDir: number = 1, filter: any = {}): Promise<Chunk<Contractor>> {        
        const dto: IGetList = {from: part * chunkLength, q: chunkLength, sortBy, sortDir, filter};
        return new Promise((resolve, reject) =>             
            this.dataService
                .contractorsChunkForChat(dto)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(new Chunk(res.data.map(d => new Contractor().build(d)), res.allLength)) : reject(res.error), 
                    err => reject(err.message)));
    }

    public loadChunk(part: number = 0, chunkLength: number = 10, sortBy: string = "id", sortDir: number = 1, filter: any = {}): Promise<Chunk<Contractor>> {        
        const dto: IGetList = {from: part * chunkLength, q: chunkLength, sortBy, sortDir, filter};        
        return new Promise((resolve, reject) =>             
            this.dataService
                .contractorsChunk(dto)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(new Chunk(res.data.map(d => new Contractor().build(d)), res.allLength)) : reject(res.error), 
                    err => reject(err.message)));
    }

    public loadOne(id: number): Promise<Contractor> {
        return new Promise((resolve, reject) => 
            this.dataService
                .contractorsOne(id)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(new Contractor().build(res.data)) : reject(res.statusCode), 
                    err => reject(err.message)));
    }

    public create(x: Contractor): Promise<Contractor> {
        return new Promise((resolve, reject) => 
            this.dataService
                .contractorsCreate(x)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(new Contractor().build(res.data)) : reject(res.error), 
                    err => reject(err.message)));
    }

    public update(x: Contractor): Promise<Contractor> {
        return new Promise((resolve, reject) => 
            this.dataService
                .contractorsUpdate(x)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(new Contractor().build(res.data)) : reject(res.error), 
                    err => reject(err.message)));
    }

    public delete(id: number): Promise<void> {
        return new Promise((resolve, reject) => 
            this.dataService
                .contractorsDelete(id)
                .subscribe(
                    res => res.statusCode === 200 ? resolve() : reject(res.error), 
                    err => reject(err.message)));
    }
}