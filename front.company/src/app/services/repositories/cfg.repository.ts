import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CfgRepository {
    public data: any = null;

    constructor(private http: HttpClient) {}

    public loadAll(): Promise<void> {
        return new Promise((resolve, reject) => 
            this.http
                .get("/assets/json/cfg.json")
                .subscribe(
                    res => {
                        this.data = res;
                        resolve();
                    }, 
                    err => reject(err.message)));
    }    
}
