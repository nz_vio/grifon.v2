import { Injectable } from "@angular/core";
import { Chunk } from "src/app/model/chunk";
import { IGetList } from "src/app/model/dto/getlist.interface";
import { ContractorGroup } from "src/app/model/orm/contractor.group.model";
import { DataService } from "../data.service";
import { Repository } from "./_repository";

@Injectable()
export class ContractorGroupRepository extends Repository<ContractorGroup> {
    constructor(protected dataService: DataService) {
        super(dataService);
    }

    public loadAll(sortBy: string, sortDir: number, filter: any = {}): Promise<ContractorGroup[]> {
        const dto: IGetList = {sortBy, sortDir, filter};       
        return new Promise((resolve, reject) =>             
            this.dataService
                .contractorGroupsAll(dto)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(res.data.map(d => new ContractorGroup().build(d))) : reject(res.error), 
                    err => reject(err.message)));
    } 
    
    public loadChunk(part: number = 0, chunkLength: number = 10, sortBy: string = "id", sortDir: number = 1, filter: any = {}): Promise<Chunk<ContractorGroup>> {                
        const dto: IGetList = {from: part * chunkLength, q: chunkLength, sortBy, sortDir, filter};    
        return new Promise((resolve, reject) =>             
            this.dataService
                .contractorsGroupsChunk(dto)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(new Chunk(res.data.map(d => new ContractorGroup().build(d)), res.allLength)) : reject(res.error), 
                    err => reject(err.message)));
    }
}