import { Injectable } from "@angular/core";
import { IActionCreate } from "src/app/model/dto/action.create.interface";
import { Action } from "src/app/model/orm/action.model";
import { DataService } from "../data.service";
import { Repository } from "./_repository";

@Injectable()
export class ActionRepository extends Repository<Action> {
    constructor(protected dataService: DataService) {
        super(dataService);
    }    

    public create(dto: IActionCreate): Promise<void> {
        return new Promise((resolve, reject) => 
            this.dataService
                .actionsCreate(dto)
                .subscribe(
                    res => res.statusCode === 200 ? resolve() : reject(res.error), 
                    err => reject(err.message)));
    }  
    
    public update(x: Action): Promise<void> {
        return new Promise((resolve, reject) => 
            this.dataService
                .actionsUpdate(x)
                .subscribe(
                    res => res.statusCode === 200 ? resolve() : reject(res.error), 
                    err => reject(err.message)));
    }
}