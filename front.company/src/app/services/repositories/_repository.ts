import { Chunk } from 'src/app/model/chunk';
import { Model } from 'src/app/model/orm/model';
import { DataService } from '../data.service';

export abstract class Repository<T> {
    protected schema: string; // used as prefix of repository object name in ObjectsService at the back-end

    constructor(protected dataService: DataService) {}    
    
    public updateParam (id: number, p: string, v: any): Promise<void> {
        return new Promise((resolve, reject) => 
            this.dataService
                .updateParam(this.schema, id, p, v)
                .subscribe(
                    res => res.statusCode === 200 ? resolve() : reject(res.error), 
                    err => reject(err.message)));        
    }   
    
    // этих функций может не быть, но если будут - то форма их вот такая
    public loadAll?(sortBy: string, sortDir: number, filter?: any): Promise<T[]>;
    public loadChunk?(part: number, chunkLength: number, sortBy: string, sortDir: number, filter?: any, created_at?: Date): Promise<Chunk<T>>;
    public create?(x: any): Promise<any>;
}
