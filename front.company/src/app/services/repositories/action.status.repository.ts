import { Injectable } from "@angular/core";
import { IGetList } from "src/app/model/dto/getlist.interface";
import { ActionStatus } from "src/app/model/orm/action.status.model";
import { DataService } from "../data.service";
import { Repository } from "./_repository";

@Injectable()
export class ActionStatusRepository extends Repository<ActionStatus> {
    constructor(protected dataService: DataService) {
        super(dataService);
    }

    public loadAll(sortBy: string, sortDir: number, filter: any = {}): Promise<ActionStatus[]> {
        const dto: IGetList = {sortBy, sortDir, filter};       
        return new Promise((resolve, reject) =>             
            this.dataService
                .actionStatusesAll(dto)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(res.data.map(d => new ActionStatus().build(d))) : reject(res.error), 
                    err => reject(err.message)));
    }
}