import { Injectable } from "@angular/core";
import { Chunk } from "src/app/model/chunk";
import { IGetList } from "src/app/model/dto/getlist.interface";
import { ChatMessage } from "src/app/model/orm/chat.message.model";
import { DataService } from "../data.service";
import { Repository } from "./_repository";

@Injectable()
export class ChatMessageRepository extends Repository<ChatMessage> {
    protected schema: string = "chatMessage";

    constructor(protected dataService: DataService) {
        super(dataService);
    }    

    public loadChunk(part: number = 0, chunkLength: number = 10, sortBy: string = "id", sortDir: number = 1, filter: any = {}, created_at: Date = null): Promise<Chunk<ChatMessage>> {        
        const dto: IGetList = {from: part * chunkLength, q: chunkLength, sortBy, sortDir, filter, created_at};   
        return new Promise((resolve, reject) =>             
            this.dataService
                .chatMessagesChunk(dto)
                .subscribe(
                    res => res.statusCode === 200 ? resolve(new Chunk(res.data.map(d => new ChatMessage().build(d)), res.allLength)) : reject(res.error),
                    err => reject(err.message)));
    }    
}