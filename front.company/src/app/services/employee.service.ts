import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IEmployeeAuthData } from "../model/dto/employee.authdata.interface";
import { IEmployeeLogin } from "../model/dto/employee.login.interface";
import { IEmployeeRecovery } from "../model/dto/employee.recovery.interface";
import { IVerify } from "../model/dto/verify.interface";
import { Employee } from "../model/orm/employee.model";
import { DataService } from './data.service';

@Injectable()
export class EmployeeService {    
    constructor(private dataService: DataService) {
        const stored = localStorage.getItem("authdata");
        stored ? this.init(JSON.parse(stored)) : null;        
    }

    get authData(): BehaviorSubject<IEmployeeAuthData> {return this.dataService.authData;}  
    get storedData(): string {return localStorage.getItem("authdata");}  
    set storedData(v: string) {localStorage.setItem("authdata", v);}
    
    private init(data: IEmployeeAuthData): void {        
        this.authData.next({token: data.token, employee: new Employee().build(data.employee)});                
    }    
        
    public logout(): void {        
        this.authData.next(null);        
        localStorage.removeItem("authdata");                 
    }

    private save(): void {        
        this.authData.value ? this.storedData = JSON.stringify(this.authData.value) : null;                     
    }

    public login(dto: IEmployeeLogin): Promise<number> {
        return new Promise((resolve, reject) => 
            this.dataService
                .employeesLogin(dto)
                .subscribe(res => {                
                    if (res.statusCode === 200) {    
                        this.init(res.data);                                
                        this.save();                                        
                    }

                    resolve(res.statusCode);
                }, err => {
                    reject(err.message);                
                }
            ));        
    }
    
    public verifyEmail(dto: IVerify): Promise<void> {
        return new Promise((resolve, reject) => 
            this.dataService
                .employeesVerifyEmail(dto)
                .subscribe(
                    res => resolve(), 
                    err => reject(err.message)));
    }    
    
    public recover(dto: IEmployeeRecovery): Promise<number> {
        return new Promise((resolve, reject) => 
            this.dataService
                .employeesRecover(dto)
                .subscribe(
                    res => resolve(res.statusCode),
                    err => reject(err.message)));
    }    
}
