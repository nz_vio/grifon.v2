import { NgModule } from '@angular/core';
import { AppService } from './app.service';
import { AuthGuard } from './guards/auth.guard';
import { DataService } from './data.service';
import { EmployeeService } from './employee.service';
import { LangRepository } from './repositories/lang.repository';
import { SettingRepository } from './repositories/setting.repository';
import { SocketService } from './socket.service';
import { SoundService } from './sound.service';
import { ChatMessageRepository } from './repositories/chat.message.repository';
import { ContractorRepository } from './repositories/contractor.repository';
import { ContractorGroupRepository } from './repositories/contractor.group.repository';
import { CfgRepository } from './repositories/cfg.repository';
import { UploadService } from './upload.service';
import { DealRepository } from './repositories/deal.repository';
import { ActionRepository } from './repositories/action.repository';
import { ActionStatusRepository } from './repositories/action.status.repository';
import { ActionTagRepository } from './repositories/action.tag.repository';

@NgModule({
    imports: [],
    declarations: [],
    exports: [],
    providers: [
        AppService,
        DataService,      
        EmployeeService,
        AuthGuard,        
        SocketService,
        SoundService,
        UploadService,
        CfgRepository,
        LangRepository,        
        SettingRepository,    
        ChatMessageRepository,
        ContractorRepository,
        ContractorGroupRepository,
        DealRepository,
        ActionRepository,
        ActionStatusRepository,
        ActionTagRepository,
    ],
})
export class ServicesModule {}
