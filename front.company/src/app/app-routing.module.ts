import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './services/guards/auth.guard';
import { AccountModule } from './views/account/account.module';
import { AuthModule } from './views/auth/auth.module';
import { ChatModule } from './views/chat/chat.module';
import { ContractorsModule } from './views/contractors/contractors.module';
import { DealsModule } from './views/deals/deals.module';
import { HomePage } from './views/home/home.page';
import { SettingsModule } from './views/settings/settings.module';

const routes: Routes = [
	{path: "", component: HomePage, pathMatch: "full", canActivate: [AuthGuard]},
	{path: "auth", loadChildren: () => AuthModule}, 		
	{path: "chat", loadChildren: () => ChatModule, canActivate: [AuthGuard]}, 		
	{path: "contractors", loadChildren: () => ContractorsModule, canActivate: [AuthGuard]}, 		
	{path: "deals", loadChildren: () => DealsModule, canActivate: [AuthGuard]}, 		
	{path: "account", loadChildren: () => AccountModule, canActivate: [AuthGuard]}, 		
	{path: "settings", loadChildren: () => SettingsModule, canActivate: [AuthGuard]}, 		
	{path: "**", redirectTo: "/"},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
