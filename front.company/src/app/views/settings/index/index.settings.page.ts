import { Component, OnInit } from "@angular/core";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";

@Component({
    selector: "index-settings-page",
    templateUrl: "index.settings.page.html",
    styleUrls: [],
})
export class IndexSettingsPage implements OnInit {
    constructor(
        private appService: AppService,        
    ) {}
    
    get lang(): Lang {return this.appService.lang;}        

    public ngOnInit(): void {        
        this.initTitle();           
    }
    
    private initTitle(): void {
        this.appService.setTitle(this.lang.phrases["settings-title"]);        
    }    
}