import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { CCModule } from "src/app/common.components/cc.module";
import { IndexSettingsPage } from "./index/index.settings.page";

let routes = RouterModule.forChild ([            
	{path: "", component: IndexSettingsPage, pathMatch: "full"},		
]);

@NgModule({	
    imports: [	
		CommonModule,
		RouterModule,
        FormsModule,        
        routes,
		CCModule,
	],
	declarations: [
		IndexSettingsPage,
	],    		    
})
export class SettingsModule {}