import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { CCModule } from "src/app/common.components/cc.module";
import { IndexAccountPage } from "./index/index.account.page";

let routes = RouterModule.forChild ([            
	{path: "", component: IndexAccountPage, pathMatch: "full"},		
]);

@NgModule({	
    imports: [	
		CommonModule,
		RouterModule,
        FormsModule,        
        routes,
		CCModule,
	],
	declarations: [
		IndexAccountPage,
	],    		    
})
export class AccountModule {}