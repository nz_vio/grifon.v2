import { Component, OnInit } from "@angular/core";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";

@Component({
    selector: "index-account-page",
    templateUrl: "index.account.page.html",
    styleUrls: [],
})
export class IndexAccountPage implements OnInit {
    constructor(
        private appService: AppService,        
    ) {}
    
    get lang(): Lang {return this.appService.lang;}    
        
    public ngOnInit(): void {        
        this.initTitle();                  
    }
    
    private initTitle(): void {
        this.appService.setTitle(this.lang.phrases["account-title"]);        
    }    
}