import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { CCModule } from "src/app/common.components/cc.module";
import { ReplyToComponent } from "./components/reply-to/reply-to.component";
import { IndexChatPage } from "./pages/index/index.chat.page";
import { ContactsNotChattingComponent } from "./components/contacts-notchatting/contacts-notchatting.component";
import { ContactsChattingComponent } from "./components/contacts-chatting/contacts-chatting.component";
import { WinDealsComponent } from "./components/win-deals/win-deals.component";
import { PanelActionsComponent } from "./components/panel-actions/panel-actions.component";
import { PanelContactComponent } from "./components/panel-contact/panel-contact.component";
import { WinActionComponent } from "./components/win-action/win-action.component";

let routes = RouterModule.forChild ([            
	{path: "", component: IndexChatPage, pathMatch: "full", data: {mark: "chat"}}, // marked for reuse				
	{path: ":contractor_id", component: IndexChatPage, data: {mark: "chat"}},	// marked for reuse			
]);

@NgModule({	
    imports: [	
		CommonModule,
		RouterModule,
        FormsModule,        
        routes,
		CCModule,		
	],
	declarations: [
		IndexChatPage,
		ContactsChattingComponent,
		ContactsNotChattingComponent,
		PanelContactComponent,
		PanelActionsComponent,
		WinActionComponent,				
		ReplyToComponent,		
		WinDealsComponent,				
	],    		    
})
export class ChatModule {}