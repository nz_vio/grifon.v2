import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { BehaviorSubject } from "rxjs";
import { IChatMessageCreate } from "src/app/model/dto/chat.message.create.interface";
import { Action } from "src/app/model/orm/action.model";
import { ChatMessage, ChatMessageChannel, ChatMessageType } from "src/app/model/orm/chat.message.model";
import { Contractor } from "src/app/model/orm/contractor.model";
import { Employee } from "src/app/model/orm/employee.model";
import { Lang } from "src/app/model/orm/lang.model";
import { Quote } from "src/app/model/quote";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";
import { ChatMessageRepository } from "src/app/services/repositories/chat.message.repository";
import { ContractorRepository } from "src/app/services/repositories/contractor.repository";
import { SocketService } from "src/app/services/socket.service";
import { SoundService } from "src/app/services/sound.service";

@Component({
    selector: "index-chat-page",
    templateUrl: "index.chat.page.html",
    styleUrls: ["index.chat.page.scss"],    
})
export class IndexChatPage implements OnInit, OnDestroy {
    @ViewChild("messagesblock", {static: false}) messagesRef: ElementRef; 
    // active contacts
    public contractors: Contractor[] = [];
    public contractorsReady: boolean = false;    
    public contractor: Contractor = null;       
    // messages
    public messages: ChatMessage[] = [];
    public messagesReady: boolean = false;
    private messagesPart: number = 0;
    private loadChunkLength: number = 100;
    private messagesExhausted: boolean = false;
    private messagesCreatedAt: Date = null; 
    public messagesLoadingMore: boolean = false; 
    // new message
    public myMessageReplyTo: Quote = null; // на какое сообщение отвечаем
    public myMessageText: string = ""; // мое сообщение     
    // inactive contacts
    public notChattingContactsActive: boolean = false; 
    public notChattingContactsReload: BehaviorSubject<void> = new BehaviorSubject(null);  
    // contact panel
    public contactPanelActive: boolean = false;
    // actions panel
    public actionMessage: ChatMessage = null;    
    // socket
    public socketActive: boolean = false;

    constructor(
        private appService: AppService,     
        private employeeService: EmployeeService,   
        private chatMessageRepository: ChatMessageRepository,
        private contractorRepository: ContractorRepository,
        private socketService: SocketService,
        private soundService: SoundService,
        private route: ActivatedRoute,
        private router: Router,
    ) {}
    
    get lang(): Lang {return this.appService.lang;}   
    get employee(): Employee {return this.employeeService.authData.value.employee;}     
    get contractorSelected(): boolean {return !!this.route.snapshot.params["contractor_id"];} // indicates that contact selected before all contacts loading - required for layout
    get contractorsFilter(): any {return {company_id: this.employee.company_id, chat_active: true};}
    get messagesElement(): HTMLElement {return this.messagesRef?.nativeElement;}
    get messagesScrolledToTop(): boolean {return this.messagesElement.scrollHeight - this.messagesElement.offsetHeight + this.messagesElement.scrollTop < 300;}
    get messagesCanLoadMore(): boolean {return !this.messagesLoadingMore && !this.messagesExhausted && this.messagesScrolledToTop;}
    get messagesFilter(): any {return {contractor_id: this.contractor.id};}

    public async ngOnInit(): Promise<void> {        
        this.initTitle();         
        await this.initContractors();        
        this.route.params.subscribe(p => {              
            this.initMessages(p["contractor_id"]);                                  
        });
        this.initSocket(); // after contractors init!
    }

    public ngOnDestroy(): void {
        this.socketService.disconnect(); // disconnect and unsubscribe from all
    }
    
    private initTitle(): void {
        this.appService.setTitle(this.lang.phrases["chat-title"]);        
    }    
    
    private async initContractors(): Promise<void> {
        try {               
            this.contractors = await this.contractorRepository.loadAllForChat("chat_at", -1, this.contractorsFilter);            
            this.contractorsReady = true;
        } catch (err) {
            this.appService.showError(err);
        }
    }

    private async initMessages(contractor_id: string): Promise<void> {
        try {
            if (contractor_id) {                
                this.actionMessage = null;
                this.contractor = this.contractors.find(c => c.id === parseInt(contractor_id));
                
                if (!this.contractor) {
                    this.router.navigateByUrl("/chat");
                    return;
                }                
                
                this.contractor._hasNewMessage = false;
                this.messagesReady = false;
                this.messages = [];
                this.messagesPart = 0;
                this.messagesCreatedAt = new Date();                
                const chunk = await this.chatMessageRepository.loadChunk(this.messagesPart, this.loadChunkLength, "created_at", -1, this.messagesFilter, this.messagesCreatedAt);                
                this.messages = chunk.data;
                this.messagesExhausted = !chunk.allLength || this.messagesPart + 1 === Math.ceil(chunk.allLength / this.loadChunkLength);                 
                this.messagesReady = true;
            } else {
                this.contractor = null;
            }
        } catch (err) {
            console.log(err);
        }
    }

    private initSocket(): void {        
        this.socketService.connect();                
        this.socketService.socket.on(`message-${this.employee.company_id}`, (data: ChatMessage) => this.onMessage(data));  
        this.socketService.socket.on(`message-read-${this.employee.company_id}`, (data: number) => this.onMessageRead(data));  
        this.socketService.socket.on(`action-${this.employee.company_id}`, (data: Action) => this.onAction(data));  
        this.socketService.socket.on("connect", () => this.socketActive = true);
        this.socketService.socket.on("disconnect", () => this.socketActive = false);   
    }

    public async onScroll(): Promise<void> {
        try {			
            if (this.messagesCanLoadMore) {
				this.messagesLoadingMore = true;
				this.messagesPart++;                
                const chunk = await this.chatMessageRepository.loadChunk(this.messagesPart, this.loadChunkLength, "created_at", -1, this.messagesFilter, this.messagesCreatedAt);                
                this.messages = [...this.messages, ...chunk.data];
                this.messagesExhausted = !chunk.allLength || this.messagesPart + 1 === Math.ceil(chunk.allLength / this.loadChunkLength); 
				this.messagesLoadingMore = false;		                
			}			
		} catch (err) {
			this.messagesLoadingMore = false;
			this.appService.showError(err);
		}
    } 
    
    public onAddContractor(contractor: Contractor): void {
        this.contractors.push(contractor);
        this.router.navigateByUrl(`/chat/${contractor.id}`);
    }

    public async onDeleteChat(): Promise<void> {
        try {
            this.router.navigateByUrl("/chat");            
            const index = this.contractors.findIndex(c => c.id === this.contractor.id);
            this.contractors.splice(index, 1);            
            await this.contractorRepository.updateParam(this.contractor.id, "chat_active", false);
            this.notChattingContactsReload.next();
        } catch (err) {
            this.appService.showError(err);
        }        
    }
    
    private onMessage(data: ChatMessage): void {        
        const newMessage = new ChatMessage().build(data);            
        const tempContractor = this.contractors.find(c => c.id === newMessage.contractor_id); // find author of message in contacts list                
        
        // update messages list
        if (this.contractor && tempContractor && this.contractor.id === tempContractor.id) { // we chatting with this author now
            this.messages.unshift(newMessage); 
            this.messagesElement.scrollTo(0, 0);
        }

        // update contacts list
        if (tempContractor) { // author exists in list           
            tempContractor.chat_messages = [newMessage]; // set last message            
            tempContractor.chat_at = new Date();
            this.contractors.sort((a, b) => (b.chat_at as any) - (a.chat_at as any)); // move to top

            if (tempContractor.id !== this.contractor?.id) { // author is not current contractor - mark it
                tempContractor._hasNewMessage = true;
            }            
        } else { // author not in list
            newMessage.contractor._hasNewMessage = true;            
            newMessage.contractor.chat_messages = [newMessage]; // last message
            this.contractors.unshift(newMessage.contractor);
        }  
        
        // sound
        !newMessage.is_answer ? this.soundService.alertOnMessage() : null;
    }

    private onMessageRead(id: number): void {
        const message = this.messages.find(m => m.id === id);
        message ? message.is_read = true : null;
    }

    private onAction(data: Action): void {
        const message = this.messages.find(m => m.id === data.chatmessage_id);
        message && message.actions.push(new Action().build(data));        
    }

    public messageCreate(): void {
        try {
            this.myMessageText = this.myMessageText.trim();

            if (!this.myMessageText.length) {
                return;
            }

            const dto: IChatMessageCreate = {
                contractor_id: this.contractor.id,
                channel: this.myMessageReplyTo ? 
                    this.myMessageReplyTo.message.channel : 
                    (this.messages.length ? this.messages[0].channel : ChatMessageChannel.Whatsapp),
                type: ChatMessageType.Text, // TODO
                text: this.myMessageReplyTo ? 
                    `<blockquote>${this.myMessageReplyTo.name}, ${this.myMessageReplyTo.message.datetimeCreatedAt}: ${this.getMyMessageReplyToText()}</blockquote>${this.myMessageText}` :
                    this.myMessageText,
            }            
            this.socketService.socket.emit("create", dto); // it can be done by http and DataService, but socket is faster than http
            // reset
            this.myMessageText = "";
            this.myMessageReplyTo = null;
        } catch (err) {
            console.log(err);
        }
    }

    public onReply(message: ChatMessage): void {
        const name = message.is_answer ? this.employee.name : this.contractor.name;
        this.myMessageReplyTo = new Quote(name, message);        
    }

    private getMyMessageReplyToText(): string {
        if (this.myMessageReplyTo.message.type === ChatMessageType.Text) return this.myMessageReplyTo.message.textStripped;
        if (this.myMessageReplyTo.message.type === ChatMessageType.Audio) return this.lang.phrases["chat-audio-msg"];
        if (this.myMessageReplyTo.message.type === ChatMessageType.File) return this.lang.phrases["chat-file-msg"];
        if (this.myMessageReplyTo.message.type === ChatMessageType.Image) return this.myMessageReplyTo.message.text || this.lang.phrases["chat-image-msg"];
        return "";
    }
}