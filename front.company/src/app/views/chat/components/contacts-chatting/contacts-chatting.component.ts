import { Component, DoCheck, EventEmitter, Input, IterableDiffer, IterableDiffers, OnChanges, OnInit, Output, SimpleChanges } from "@angular/core";
import { ChatMessage, ChatMessageType } from "src/app/model/orm/chat.message.model";
import { Contractor } from "src/app/model/orm/contractor.model";
import { Employee } from "src/app/model/orm/employee.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";

// contractors with chat_active=true
@Component({
    selector: "contacts-chatting",
    templateUrl: "contacts-chatting.component.html",
    styleUrls: ["contacts-chatting.component.scss"],
})
export class ContactsChattingComponent implements DoCheck {    
    @Input() contractors: Contractor[];
    @Input() contractorsReady: boolean;
    @Input() contractor: Contractor;    
    @Input() contractorSelected: boolean; // indicates that contact selected before all contacts loading - required for layout behavior    
    @Output() activateNotChattingContacts: EventEmitter<void> = new EventEmitter();

    public contractorsSearch: string = "";
    public contractorsFiltered: Contractor[] = [];
    public iterableDiffer: IterableDiffer<Contractor>; // array changes detector

    constructor(
        private appService: AppService,
        private employeeService: EmployeeService,
        private iterableDiffers: IterableDiffers, // built-in Angular detectors repository         
    ) {
        this.iterableDiffer = iterableDiffers.find([]).create(null); //init detector
    }

    get lang(): Lang {return this.appService.lang;}
    get employee(): Employee {return this.employeeService.authData.value.employee;}   

    public ngDoCheck(): void {        
        if (this.iterableDiffer.diff(this.contractors)) { // detect change in contractors list, including first load too
            this.contractorsSearch = "";
            this.applyFilter();
        }   
    }

    public applyFilter(): void {        
        // cloning array, not linking!
        this.contractorsFiltered = this.contractorsSearch.length ? 
            [...this.contractors.filter(c => c.name.toLowerCase().includes(this.contractorsSearch.toLowerCase()))] : 
            [...this.contractors];        
    }

    public onAdd(): void {
        this.activateNotChattingContacts.emit();
    }

    public getMessageText(message: ChatMessage): string {
        if (message.type === ChatMessageType.Text) return message.textStripped;
        if (message.type === ChatMessageType.Audio) return this.lang.phrases["chat-audio-msg"];
        if (message.type === ChatMessageType.File) return this.lang.phrases["chat-file-msg"];
        if (message.type === ChatMessageType.Image) return message.text ? message.textStripped : this.lang.phrases["chat-image-msg"];
        return "";
    }
}