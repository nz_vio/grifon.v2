import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from "@angular/core";
import { BehaviorSubject, Subscription } from "rxjs";
import { ContractorAlphaGroup } from "src/app/model/contractor.alphagroup.interface";
import { Contractor } from "src/app/model/orm/contractor.model";
import { Employee } from "src/app/model/orm/employee.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";
import { ContractorRepository } from "src/app/services/repositories/contractor.repository";

// contractors with chat_active=false
@Component({
    selector: "contacts-notchatting",
    templateUrl: "contacts-notchatting.component.html",
    styleUrls: ["contacts-notchatting.component.scss"],
})
export class ContactsNotChattingComponent implements OnInit, OnDestroy {
    @Input() active: boolean;
    @Input() reload: BehaviorSubject<void>;
    @Output() activeChange: EventEmitter<boolean> = new EventEmitter();
    @Output() addContractor: EventEmitter<Contractor> = new EventEmitter();
    @ViewChild("contractorsblock", {static: false}) elementRef: ElementRef; 
    
    public alphaGroups: ContractorAlphaGroup[] = [];
    public loadingMore: boolean = false;     
    private part: number = 0;
    private chunkLength: number = 20;
    private exhausted: boolean = false;        
    private reloadSubscription: Subscription = null;

    constructor (
        private appService: AppService,
        private contractorRepository: ContractorRepository,
        private employeeService: EmployeeService,
    ) {}

    get lang(): Lang {return this.appService.lang;}   
    get employee(): Employee {return this.employeeService.authData.value.employee;}    
    get element(): HTMLElement {return this.elementRef.nativeElement;}
    get canLoadMore(): boolean {return !this.loadingMore && !this.exhausted && this.scrolledToBottom;}
    get scrolledToBottom(): boolean {return this.element.scrollHeight - this.element.scrollTop < this.element.clientHeight + 100;}
    get filter(): any {return {company_id: this.employee.company_id, chat_active: false};}

    public ngOnInit(): void {                
        this.initSubscription(); // этот компонент будет загружать список по команде извне, при создании в BehaviorSubject существует состояние, поэтому первая загрузка произойдет автоматически
    }

    public ngOnDestroy(): void {
        this.reloadSubscription.unsubscribe();
    }    

    private async initContractors(): Promise<void> {
        try {
            this.initAlphaGroups();
            this.part = 0;                        
            const chunk = await this.contractorRepository.loadChunkForChat(this.part, this.chunkLength, "name", 1, this.filter);
            this.buildAlphaGroups(chunk.data);
            this.exhausted = !chunk.allLength || this.part + 1 === Math.ceil(chunk.allLength / this.chunkLength);   
        } catch (err) {
            this.appService.showError(err);
        }
    }

    private initAlphaGroups(): void {
        const alphabet = "0123456789abcdefghijklmnopqrstuvwxyz_абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        this.alphaGroups = [];

        for (let i = 0; i < alphabet.length; i++) {
            this.alphaGroups.push({letter: alphabet.charAt(i), contractors: []});
        }
    }

    private initSubscription(): void {
        this.reloadSubscription = this.reload.subscribe(() => this.initContractors());
    }

    private buildAlphaGroups(contractors: Contractor[]): void {
        for (let c of contractors) {
            const group = this.alphaGroups.find(g => g.letter === c.firstLetter.toLowerCase());
            group ? group.contractors.push(c) : this.alphaGroups[36].contractors.push(c);
        }        
    }

    public async onScroll(): Promise<void> {
        try {			
            if (this.canLoadMore) {
				this.loadingMore = true;
				this.part++;                
                const chunk = await this.contractorRepository.loadChunkForChat(this.part, this.chunkLength, "name", 1, this.filter);
                this.buildAlphaGroups(chunk.data);
                this.exhausted = !chunk.allLength || this.part + 1 === Math.ceil(chunk.allLength / this.chunkLength); 
				this.loadingMore = false;		                
			}			
		} catch (err) {
			this.loadingMore = false;
			this.appService.showError(err);
		}
    } 

    public onClose(): void {
        this.activeChange.emit(false);
    }

    public async onSelect(contractor: Contractor): Promise<void> {
        try {
            this.addContractor.emit(contractor);
            this.activeChange.emit(false);
            await this.contractorRepository.updateParam(contractor.id, "chat_active", true);
            this.initContractors();
        } catch (err) {
            this.appService.showError(err);
        }        
    }
}