import { Component, EventEmitter, HostListener, Input, Output } from "@angular/core";
import { Contractor } from "src/app/model/orm/contractor.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";

@Component({
    selector: "panel-contact",
    templateUrl: "panel-contact.component.html",
    styleUrls: ["panel-contact.component.scss"],
})
export class PanelContactComponent {
    @Input() active: boolean;
    @Input() contractor: Contractor;
    @Output() activeChange: EventEmitter<boolean> = new EventEmitter();

    constructor(private appService: AppService) {}
    
    get lang(): Lang {return this.appService.lang;}       

    public close(): void {
        this.activeChange.emit(false);
    }    
}