import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ChatMessage, ChatMessageType } from "src/app/model/orm/chat.message.model";
import { Lang } from "src/app/model/orm/lang.model";
import { Quote } from "src/app/model/quote";
import { AppService } from "src/app/services/app.service";

@Component({
    selector: "reply-to",
    templateUrl: "reply-to.component.html",
    styleUrls: ["reply-to.component.scss"],
})
export class ReplyToComponent {
    @Input() quote: Quote;
    @Output() quoteChange: EventEmitter<ChatMessage> = new EventEmitter();   

    constructor(private appService: AppService) {}

    get lang(): Lang {return this.appService.lang;}
    get text(): string {
        if (this.quote.message.type === ChatMessageType.Text) return this.quote.message.textStripped;
        if (this.quote.message.type === ChatMessageType.Audio) return this.lang.phrases["chat-audio-msg"];
        if (this.quote.message.type === ChatMessageType.File) return this.lang.phrases["chat-file-msg"];
        if (this.quote.message.type === ChatMessageType.Image) return this.quote.message.text || this.lang.phrases["chat-image-msg"];
        return "";
    }
    
    public onDelete(): void {
        this.quoteChange.emit(null);
    }
}