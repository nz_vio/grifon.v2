import { Component, Directive, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from "@angular/core";
import { IActionCreate } from "src/app/model/dto/action.create.interface";
import { IFile } from "src/app/model/file.interface";
import { ActionFile } from "src/app/model/orm/action.file.model";
import { ActionReminder } from "src/app/model/orm/action.reminder.model";
import { ActionStatus } from "src/app/model/orm/action.status.model";
import { ActionTag } from "src/app/model/orm/action.tag.model";
import { ChatMessage } from "src/app/model/orm/chat.message.model";
import { Deal } from "src/app/model/orm/deal.model";
import { EmployeeAction, EmployeeActionDirection } from "src/app/model/orm/employee.action.model";
import { Employee } from "src/app/model/orm/employee.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";
import { ActionRepository } from "src/app/services/repositories/action.repository";
import { ActionStatusRepository } from "src/app/services/repositories/action.status.repository";

@Component({
    selector: "win-action",
    templateUrl: "win-action.component.html",
    styleUrls: [
        "win-action.scss",        
        "../../../../common.styles/popups.scss",
    ],
})
export class WinActionComponent implements OnChanges, OnInit {
    @Input() message: ChatMessage;
    @Input() employeeAction: EmployeeAction;
    @Input() active: boolean;    
    @Output() activeChange: EventEmitter<boolean> = new EventEmitter();    

    // action data
    public messageFileDescription: string = "";
    public uploads: IFile[] = [];
    public comment: string = "";
    public deal: Deal = null;
    public reminders: ActionReminder[] = [];
    public actionstatus_id: number = null;
    public tags: ActionTag[] = [];
    // related data
    public actionStatuses: ActionStatus[] = [];
    // iface
    public dealsListActive: boolean = false;
    public loading: boolean = false;    
    // validation
    public errorDeal: boolean = false;

    constructor(
        private appService: AppService,    
        private employeeService: EmployeeService,      
        private actionRepository: ActionRepository, 
        private actionStatusRepository: ActionStatusRepository, 
    ) {}
    
    get lang(): Lang {return this.appService.lang;}        
    get employee(): Employee {return this.employeeService.authData.value.employee;}    
    
    public ngOnChanges(changes: SimpleChanges): void {        
        if (changes.active && changes.active.currentValue) {
            this.initAction();             
        }
    }

    public ngOnInit(): void {
        this.initActionStatuses();   
    }

    private initAction(): void {
        this.messageFileDescription = "";
        this.uploads = [];
        this.comment = "";
        this.deal = null;
        this.reminders = [];
        this.actionstatus_id = null;
        this.tags = [];
    }

    private async initActionStatuses(): Promise<void> {
        try {
            this.actionStatuses = await this.actionStatusRepository.loadAll("name", 1);
        } catch (err) {
            this.appService.showError(err);
        }
    }

    public onClose(): void {
        this.activeChange.emit(false);
    }

    public onUploaded(uploads: IFile[]): void {
        this.uploads = [...this.uploads, ...uploads];
    }

    public onDeleteUpload(i: number): void {
        this.uploads.splice(i, 1);
    }

    public onNewDeal(): void {
        this.deal = new Deal().init(this.employee.company_id, this.message.contractor);
    }   
    
    public async onCreate(): Promise<void> {
        try {
            if (this.validate()) {
                this.loading = true;            
                const dto = this.buildDTO();
                await this.actionRepository.create(dto);            
                this.loading = false;
                this.onClose();            
            }            
        } catch (err) {
            this.appService.showError(err);
            this.loading = false;
        }
    }

    private validate(): boolean {
        let error = false;

        if (!this.deal) {
            this.errorDeal = true;
            error = true;
        } else {
            this.errorDeal = false;
        }

        return !error;
    }

    protected buildDTO(): IActionCreate {
        const dto: IActionCreate = {
            actionstatus_id: this.actionstatus_id,
            employee: this.employee,
            employeeAction: this.employeeAction,
            message: this.message,
            deal: this.deal,
            comment: this.comment,
            uploads: this.uploads,
            reminders: this.reminders,
            tags: this.tags,
        };

        if (this.employeeAction.direction === EmployeeActionDirection.In && this.message.fileurl) {
            dto.file = new ActionFile();
            dto.file.url = this.message.fileurl;
            dto.file.name = this.message.filename;
            dto.file.description = this.messageFileDescription;            
        }        
        
        return dto;
    }
}