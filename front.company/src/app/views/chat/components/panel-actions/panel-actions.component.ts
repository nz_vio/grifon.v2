import { Component, EventEmitter, HostListener, Input, Output } from "@angular/core";
import { ChatMessage } from "src/app/model/orm/chat.message.model";
import { EmployeeAction, EmployeeActionDirection } from "src/app/model/orm/employee.action.model";
import { Employee } from "src/app/model/orm/employee.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";

@Component({
    selector: "panel-actions",
    templateUrl: "panel-actions.component.html",
    styleUrls: ["panel-actions.component.scss"],
})
export class PanelActionsComponent {        
    @Input() message: ChatMessage;
    @Output() messageChange: EventEmitter<boolean> = new EventEmitter();

    public employeeAction: EmployeeAction = null;
    public actionActive: boolean = false;    

    constructor(
        private appService: AppService,
        private employeeService: EmployeeService,
    ) {}
    
    get lang(): Lang {return this.appService.lang;}       
    get employee(): Employee {return this.employeeService.authData.value.employee;}

    public onClose(): void {
        this.messageChange.emit(null);
    }    
    
    public onActionClick(employeeAction: EmployeeAction): void {        
        this.employeeAction = employeeAction;
        this.actionActive = true;        
    }
}