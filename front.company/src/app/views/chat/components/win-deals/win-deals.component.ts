import { Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from "@angular/core";
import { Deal } from "src/app/model/orm/deal.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { DealRepository } from "src/app/services/repositories/deal.repository";

@Component({
    selector: "win-deals",
    templateUrl: "win-deals.component.html",
    styleUrls: [
        "win-deals.component.scss",
        "../../../../common.styles/popups.scss",
        "../../../../common.styles/deals.scss",
    ],
})
export class WinDealsComponent implements OnChanges {
    @Input() contact_contractor_id: number;
    @Input() active: boolean;
    @Output() activeChange: EventEmitter<boolean> = new EventEmitter();
    @Output() selected: EventEmitter<Deal> = new EventEmitter();

    @ViewChild("dealselement", {static: false}) dealsElementRef: ElementRef;    
    
    // deals
    public deal: Deal = null;
    public deals: Deal[] = [];    
    public dealsSearch: string = "";    
    public dealsLoadingMore: boolean = false;     
    private dealsPart: number = 0;
    private dealsChunkLength: number = 20;
    private dealsExhausted: boolean = false;   
    public dealsReady: boolean = false;
    
    constructor(
        private appService: AppService,        
        private dealRepository: DealRepository,
    ) {}
    
    get lang(): Lang {return this.appService.lang;}           
    get dealsElement(): HTMLElement {return this.dealsElementRef.nativeElement;}
    get dealsCanLoadMore(): boolean {return !this.dealsLoadingMore && !this.dealsExhausted && this.dealsScrolledToBottom;}
    get dealsScrolledToBottom(): boolean {return this.dealsElement.scrollHeight - this.dealsElement.scrollTop < this.dealsElement.clientHeight + 100;}
    get dealsFilter(): any {return {contact_contractor_id: this.contact_contractor_id, search: this.dealsSearch ? this.dealsSearch : null};}

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.active && changes.active.currentValue) {
            this.deal = null;
            this.dealsSearch = "";
            this.initDeals();
        }
    }    
    
    public async initDeals(): Promise<void> {
        try {
            this.deals = [];
            this.dealsReady = false;
            this.dealsPart = 0;            
            const chunk = await this.dealRepository.loadChunk(this.dealsPart, this.dealsChunkLength, "id", -1, this.dealsFilter);                                        
            this.deals = chunk.data;                        
            this.dealsExhausted = !chunk.allLength || this.dealsPart + 1 === Math.ceil(chunk.allLength / this.dealsChunkLength);   
            this.dealsReady = true;
        } catch (err) {
            this.appService.showError(err);
        }
    }

    public async onScroll(): Promise<void> {
        try {			            
            if (this.dealsCanLoadMore) {
				this.dealsLoadingMore = true;
				this.dealsPart++;                
                const chunk = await this.dealRepository.loadChunk(this.dealsPart, this.dealsChunkLength, "id", -1, this.dealsFilter);                                        
                this.deals = [...this.deals, ...chunk.data];
                this.dealsExhausted = !chunk.allLength || this.dealsPart + 1 === Math.ceil(chunk.allLength / this.dealsChunkLength); 
				this.dealsLoadingMore = false;		                
			}			
		} catch (err) {
			this.dealsLoadingMore = false;
			this.appService.showError(err);
		}
    }
    
    public onClose(): void {
        this.activeChange.emit(false);
    }

    public onSelect(): void {
        this.selected.emit(this.deal);
        this.onClose();
    }
}