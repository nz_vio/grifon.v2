import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { IEmployeeLogin } from "src/app/model/dto/employee.login.interface";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";

@Component({
    selector: "login-auth-page",
    templateUrl: "login.auth.page.html",
    styleUrls: [
        "../auth.scss", 
        "../../../common.styles/forms.scss",
    ],
})
export class LoginAuthPage implements OnInit {
    public email: string = "";
    public password: string = "";    
    public formLoading: boolean = false;    
    // errors before send
    public formErrorEmail: boolean = false;
    public formErrorPassword: boolean = false;
    // error after send
    public formErrorDenied: boolean = false;
    
    constructor(
        private appService: AppService,
        private EmployeeService: EmployeeService,
        private router: Router,
    ) {}
    
    get lang(): Lang {return this.appService.lang;}    

    public ngOnInit(): void {
        this.checkAuth();
        this.initTitle();
    } 
    
    private checkAuth(): void {
        this.EmployeeService.authData.value !== null ? this.router.navigateByUrl("/") : null;
    }

    private initTitle(): void {
        this.appService.setTitle(this.lang.phrases["login-title"]);        
    }

    public async login(): Promise<void> {
        try {            
            if (this.formValidate()) {
                this.formLoading = true;
                this.formErrorDenied = false;
                const dto: IEmployeeLogin = {email: this.email, password: this.password};
                const statusCode: number = await this.EmployeeService.login(dto);
                this.formLoading = false;
    
                if (statusCode === 200) {
                    this.router.navigateByUrl("/");                      
                } else if (statusCode === 401) {
                    this.formErrorDenied = true;                    
                } else {
                    this.appService.showError(this.lang.phrases["common-error"]);
                } 
            }            
        } catch (err) {
            this.appService.showError(err);
            this.formLoading = false;
        }
    }    

    public formValidate(): boolean {
        let error = false;
        this.email = this.email.trim();
        this.password = this.password.trim();        

        if (!this.email.length) {
            error = true;
            this.formErrorEmail = true;
        } else {
            this.formErrorEmail = false;
        }

        if (!this.password.length) {
            error = true;
            this.formErrorPassword = true;
        } else {
            this.formErrorPassword = false;
        }
        
        return !error;
    }
}