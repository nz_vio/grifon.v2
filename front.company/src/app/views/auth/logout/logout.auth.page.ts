import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Timeout } from "src/app/decorators/timeout";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";

@Component({
    selector: "logout-auth-page",
    template: "",
})
export class LogoutAuthPage implements OnInit {
    constructor(
        private EmployeeService: EmployeeService,
        private router: Router,
        private appService: AppService,
    ) {}

    @Timeout(1)
    public async ngOnInit(): Promise<void> {                
        this.EmployeeService.logout();            
        this.router.navigateByUrl("/auth/login");
    }
}