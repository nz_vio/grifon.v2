import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { CCModule } from "src/app/common.components/cc.module";
import { LoginAuthPage } from "./login/login.auth.page";
import { LogoutAuthPage } from "./logout/logout.auth.page";
import { RecoverAuthPage } from "./recover/recover.auth.page";

let routes = RouterModule.forChild ([            
	{path: "login", component: LoginAuthPage, pathMatch: "full"},
	{path: "logout", component: LogoutAuthPage, pathMatch: "full"},	
	{path: "recover", component: RecoverAuthPage, pathMatch: "full"},
]);

@NgModule({	
    imports: [	
		CommonModule,
		RouterModule,
        FormsModule,        
        routes,
		CCModule,
	],
	declarations: [
		LoginAuthPage,	
		LogoutAuthPage,		
		RecoverAuthPage,
	],    		    
})
export class AuthModule {}