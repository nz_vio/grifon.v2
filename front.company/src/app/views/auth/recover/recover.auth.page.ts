import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { IEmployeeRecovery } from "src/app/model/dto/employee.recovery.interface";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";

@Component({
    selector: "recover-auth-page",
    templateUrl: "recover.auth.page.html",
    styleUrls: [
        "../auth.scss", 
        "../../../common.styles/forms.scss",
    ],
})
export class RecoverAuthPage implements OnInit {
    public email: string = "";    
    public code: string = "";
    public password: string = "";
    public password2: string = "";
    public formLoading: boolean = false;    
    public formCodeSending: boolean = false;  
    public formCodeSent: boolean = false; 
    public formSent: boolean = false;
    // errors before send
    public formErrorEmail: boolean = false;
    public formErrorCode: boolean = false;
    public formErrorPassword: boolean = false;
    public formErrorPassword2: boolean = false;
    // errors after send
    public formErrorEmailNotExists: boolean = false;
    public formErrorCodeInvalid: boolean = false;
    
    constructor(
        private appService: AppService,
        private EmployeeService: EmployeeService,    
        private router: Router,
    ) {}
    
    get lang(): Lang {return this.appService.lang;}    

    public ngOnInit(): void {
        this.checkAuth();
        this.initTitle();
    }  
    
    private checkAuth(): void {
        this.EmployeeService.authData.value !== null ? this.router.navigateByUrl("/") : null;
    }

    private initTitle(): void {
        this.appService.setTitle(this.lang.phrases["recover-title"]);        
    }
    
    public async recover(): Promise<void> {
        try {                        
            if (this.formValidate()) {
                this.formLoading = true;
                this.formErrorCodeInvalid = false;
                this.formErrorEmailNotExists = false;
                const dto: IEmployeeRecovery = {email: this.email, password: this.password, code: this.code};
                const statusCode = await this.EmployeeService.recover(dto);
                this.formLoading = false;

                if (statusCode === 200) {                    
                    this.formSent = true;
                } else if (statusCode === 404) {
                    this.formErrorEmailNotExists = true;
                } else if (statusCode === 401) {
                    this.formErrorCodeInvalid = true;
                    this.formCodeSent = false;
                } else {
                    this.appService.showError(this.lang.phrases["common-error"]);
                }                
            }            
        } catch (err) {
            this.appService.showError(err);
            this.formLoading = false;
        }
    }

    public async verifyEmail(): Promise<void> {
        try {
            if (this.emailValidate()) {
                this.formCodeSending = true;
                await this.EmployeeService.verifyEmail({email: this.email});
                this.formCodeSending = false;
                this.formCodeSent = true;                
            }
        } catch (err) {
            this.appService.showError(err);
            this.formCodeSending = false;
        }
    }    

    public formValidate(): boolean {
        let error = false;
        this.email = this.email.trim();
        this.password = this.password.trim();   
        this.password2 = this.password2.trim();
        this.code = this.code.trim();             

        if (!this.email.length || !this.appService.validateEmail(this.email)) {
            error = true;
            this.formErrorEmail = true;
        } else {
            this.formErrorEmail = false;
        }

        if (this.password.length < 6) {
            error = true;
            this.formErrorPassword = true;
        } else {
            this.formErrorPassword = false;
        }

        if (this.password !== this.password2) {
            error = true;
            this.formErrorPassword2 = true;
        } else {
            this.formErrorPassword2 = false;
        }

        if (this.code.length < 6) {
            error = true;
            this.formErrorCode = true;
        } else {
            this.formErrorCode = false;
        }        
        
        return !error;
    }

    private emailValidate(): boolean {
        let error = false;

        if (!this.email.length || !this.appService.validateEmail(this.email)) {
            error = true;
            this.formErrorEmail = true;
        } else {
            this.formErrorEmail = false;
        }

        return !error;
    }
}