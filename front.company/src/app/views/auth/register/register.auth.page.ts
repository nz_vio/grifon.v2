import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { IUserRegister } from "src/app/model/dto/user.register.interface";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { UserService } from "src/app/services/user.service";
import { GithubService } from "src/app/services/github.service";
import { GoogleService } from "src/app/services/google.service";

@Component({
    selector: "register-auth-page",
    templateUrl: "register.auth.page.html",
    styleUrls: ["../auth.scss"],
})
export class RegisterAuthPage implements OnInit {
    public name: string = "";
    public company: string = "";
    public email: string = "";
    public password: string = "";        
    public agree: boolean = false;
    public code: string = "";
    public formLoading: boolean = false; 
    public formCodeSending: boolean = false;  
    public formCodeSent: boolean = false; 
    // errors before send
    public formErrorName: boolean = false;
    public formErrorEmail: boolean = false;
    public formErrorPassword: boolean = false;
    public formErrorAgree: boolean = false;
    public formErrorCode: boolean = false;
    // errors after send
    public formErrorEmailInUse: boolean = false;
    public formErrorCodeInvalid: boolean = false;
        
    constructor(
        private appService: AppService,
        private userService: UserService,    
        private googleService: GoogleService,
        private githubService: GithubService,
        private router: Router,
    ) {}
    
    get lang(): Lang {return this.appService.lang;}    

    public ngOnInit(): void {
        this.checkAuth();
        this.initTitle();
    }  
    
    private checkAuth(): void {
        this.userService.authData.value !== null ? this.router.navigateByUrl("/") : null;
    }

    private initTitle(): void {
        this.appService.setTitle(this.lang.phrases["register-title"]);        
    }

    public async register(): Promise<void> {
        try {            
            if (this.formValidate()) {
                this.formLoading = true;                
                this.formErrorEmailInUse = false;
                this.formErrorCodeInvalid = false;
                const dto: IUserRegister = {name: this.name, company: this.company, email: this.email, password: this.password, code: this.code};
                const statusCode = await this.userService.register(dto);
                this.formLoading = false;

                if (statusCode === 201) {                    
                    this.router.navigateByUrl("/auth/register/connect");                      
                } else if (statusCode === 409) {
                    this.formErrorEmailInUse = true;
                } else if (statusCode === 401) {
                    this.formErrorCodeInvalid = true;
                    this.formCodeSent = false;
                } else {
                    this.appService.showError(this.lang.phrases["common-error"]);
                }                
            }            
        } catch (err) {
            this.appService.showError(err);
            this.formLoading = false;
        }
    }

    public async verifyEmail(): Promise<void> {
        try {
            if (this.emailValidate()) {
                this.formCodeSending = true;
                await this.userService.verifyEmail({email: this.email});
                this.formCodeSending = false;
                this.formCodeSent = true;                
            }
        } catch (err) {
            this.appService.showError(err);
            this.formCodeSending = false;
        }
    }

    public enterWithGoogle(): void {
        this.googleService.login();
    }

    public enterWithGithub(): void {
        this.githubService.login();
    }

    public formValidate(): boolean {
        let error = false;
        this.email = this.email.trim();
        this.password = this.password.trim();   
        this.name = this.name.trim();
        this.company = this.company.trim();
        this.code = this.code.trim();     

        if (!this.name.length) {
            error = true;
            this.formErrorName = true;
        } else {
            this.formErrorName = false;
        }

        if (!this.email.length || !this.appService.validateEmail(this.email)) {
            error = true;
            this.formErrorEmail = true;
        } else {
            this.formErrorEmail = false;
        }

        if (this.password.length < 6) {
            error = true;
            this.formErrorPassword = true;
        } else {
            this.formErrorPassword = false;
        }

        if (this.code.length < 6) {
            error = true;
            this.formErrorCode = true;
        } else {
            this.formErrorCode = false;
        }

        if (!this.agree) {
            error = true;
            this.formErrorAgree = true;
        } else {
            this.formErrorAgree = false;
        }
        
        return !error;
    }

    private emailValidate(): boolean {
        let error = false;

        if (!this.email.length || !this.appService.validateEmail(this.email)) {
            error = true;
            this.formErrorEmail = true;
        } else {
            this.formErrorEmail = false;
        }

        return !error;
    }
}