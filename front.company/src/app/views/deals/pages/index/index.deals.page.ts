import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Action } from "src/app/model/orm/action.model";
import { ActionStatus } from "src/app/model/orm/action.status.model";
import { Deal } from "src/app/model/orm/deal.model";
import { Employee } from "src/app/model/orm/employee.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";
import { ActionRepository } from "src/app/services/repositories/action.repository";
import { ActionStatusRepository } from "src/app/services/repositories/action.status.repository";
import { DealRepository } from "src/app/services/repositories/deal.repository";

@Component({
    selector: "index-deals-page",
    templateUrl: "index.deals.page.html",    
    styleUrls: ["../../../../common.styles/deals.scss"],
})
export class IndexDealsPage implements OnInit {
    @ViewChild("dealsblock", {static: false}) dealsElementRef: ElementRef; 

    // deals list    
    public deals: Deal[] = [];
    public dealsSearch: string = "";
    public dealsLength: number = 0;
    public dealsReady: boolean = false;
    public dealsLoadingMore: boolean = false;     
    private dealsPart: number = 0;
    private dealsChunkLength: number = 20;
    private dealsExhausted: boolean = false; 
    // edit deal and action
    public mode: string = null;
    public deal: Deal = null;
    public dealLoading: boolean = false;
    public action: Action = null;
    public actionLoading: boolean = false;   
    // related data
    public actionStatuses: ActionStatus[] = []; 
    // iface
    public splitted: boolean = false;     
    public sidebarActive: boolean = false;

    constructor(
        private appService: AppService,  
        private employeeService: EmployeeService, 
        private dealRepository: DealRepository,               
        private actionRepository: ActionRepository,
        private actionStatusRepository: ActionStatusRepository,
        private route: ActivatedRoute,
        private router: Router,
    ) {}
    
    get lang(): Lang {return this.appService.lang;}   
    get employee(): Employee {return this.employeeService.authData.value.employee;}   
    get dealsElement(): HTMLElement {return this.dealsElementRef.nativeElement;}
    get dealsCanLoadMore(): boolean {return !this.dealsLoadingMore && !this.dealsExhausted && this.dealsScrolledToBottom;}
    get dealsScrolledToBottom(): boolean {return this.dealsElement.scrollHeight - this.dealsElement.scrollTop < this.dealsElement.clientHeight + 100;}
    get dealsFilter(): any {return {company_id: this.employee.company_id, search: this.dealsSearch ? this.dealsSearch : null};}

    public ngOnInit(): void {        
        this.initTitle();  
        this.initDeals();     
        this.initActionStatuses();           
        this.route.params.subscribe(p => {              
            this.initDealAndAction(p["mode"], p["deal_id"], p["action_id"]);                                  
        });         
    }

    private initTitle(): void {
        this.appService.setTitle(this.lang.phrases["deals-title"]);        
    }  

    public async initDeals(): Promise<void> {
        try {            
            this.dealsReady = false;            
            this.dealsPart = 0;                  
            const chunk = await this.dealRepository.loadChunk(this.dealsPart, this.dealsChunkLength, "id", -1, this.dealsFilter);                
            this.deals = chunk.data;
            this.dealsLength = chunk.allLength;
            this.dealsExhausted = !chunk.allLength || this.dealsPart + 1 === Math.ceil(chunk.allLength / this.dealsChunkLength);   
            this.dealsReady = true;
        } catch (err) {
            this.appService.showError(err);
        }
    }

    private async initDealAndAction(mode: string, deal_id: string, action_id: string): Promise<void> {
        try {
            if (mode === "edit") {                
                // if not in list - try to load (maybe not scrolled to this item or even not loaded yet!)              
                this.deal = this.deals.find(d => d.id === parseInt(deal_id)) || await this.dealRepository.loadOne(parseInt(deal_id)); 
                this.splitted = true;

                if (action_id) {
                    this.action = this.deal.actions.find(a => a.id === parseInt(action_id));
                    !this.action && this.router.navigateByUrl("/deals");
                } else {
                    this.action = null;
                }
            } else if (mode === "create") {
                this.deal = new Deal().init(this.employee.company_id);
                this.action = null;
                this.splitted = true;
            } else {
                // move out and then reset
                this.splitted = false;
                await this.appService.pause(300);
                this.deal = null;
                this.action = null;                
            }

            this.mode = mode;
        } catch (err) {
            err === 404 ? this.router.navigateByUrl("/deals") : this.appService.showError(err);            
        }
    }

    private async initActionStatuses(): Promise<void> {
        try {
            this.actionStatuses = await this.actionStatusRepository.loadAll("name", 1);
        } catch (err) {
            this.appService.showError(err);
        }
    }
    
    public async dealsOnScroll(): Promise<void> {
        try {			            
            if (this.dealsCanLoadMore) {
				this.dealsLoadingMore = true;
				this.dealsPart++;                
                const chunk = await this.dealRepository.loadChunk(this.dealsPart, this.dealsChunkLength, "id", -1, this.dealsFilter);                    
                this.deals = [...this.deals, ...chunk.data];
                this.dealsExhausted = !chunk.allLength || this.dealsPart + 1 === Math.ceil(chunk.allLength / this.dealsChunkLength); 
				this.dealsLoadingMore = false;		                
			}			
		} catch (err) {
			this.dealsLoadingMore = false;
			this.appService.showError(err);
		}
    }

    public async dealOnSave(deal: Deal): Promise<void> {
        try {
            this.dealLoading = true;               
            this.mode === "edit" ? await this.dealRepository.update(deal) : await this.dealRepository.create(deal);            
            this.dealLoading = false;
            this.router.navigateByUrl("/deals");            
            this.initDeals();
        } catch (err) {
            this.appService.showError(err);
            this.dealLoading = false;
        }
    }

    public async dealOnDelete(): Promise<void> {
        try {
            this.dealLoading = true;
            await this.dealRepository.delete(this.deal.id);
            this.dealLoading = false;
            this.router.navigateByUrl("/deals");            
            this.initDeals();
        } catch (err) {
            this.appService.showError(err);
            this.dealLoading = false;
        }
    }

    public async actionOnSave(action: Action): Promise<void> {
        try {
            this.actionLoading = true;               
            await this.actionRepository.update(action);
            this.actionLoading = false;
            this.router.navigateByUrl("/deals");            
            this.initDeals();
        } catch (err) {
            this.appService.showError(err);
            this.actionLoading = false;
        }
    }
}