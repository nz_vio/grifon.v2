import { Component, EventEmitter, Input, Output, SimpleChanges } from "@angular/core";
import { Deal } from "src/app/model/orm/deal.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";

@Component({
    selector: "deal-edit",
    templateUrl: "deal-edit.component.html",    
})
export class DealEditComponent {
    @Input() deal: Deal;
    @Input() loading: boolean;    
    @Output() save: EventEmitter<Deal> = new EventEmitter();
    @Output() delete: EventEmitter<void> = new EventEmitter();

    public temp: Deal = null;
    public errorContact: boolean = false;

    constructor(private appService: AppService) {}

    get lang(): Lang {return this.appService.lang;}

    public ngOnChanges(changes: SimpleChanges): void {        
        if (changes.deal && changes.deal.currentValue) {
            this.temp = new Deal().build(this.deal);            
        }
    }

    public onSave(): void {    
        if (this.validate()) {
            this.save.emit(this.temp);        
        }        
    }

    public onDelete(): void {
        if (window.confirm(this.lang.phrases["common-sure"])) {
            this.delete.emit();
        }        
    }  
    
    private validate(): boolean {
        let error = false;

        if (!this.temp.contact) {
            this.errorContact = true;
            error = true;
        } else {
            this.errorContact = false;
        }

        return !error;
    }
}