import { Component, EventEmitter, Input, Output, SimpleChanges } from "@angular/core";
import { Action } from "src/app/model/orm/action.model";
import { ActionStatus } from "src/app/model/orm/action.status.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";

@Component({
    selector: "action-edit",
    templateUrl: "action-edit.component.html",  
    styleUrls: ["../../../../common.styles/forms.scss"],
})
export class ActionEditComponent {
    @Input() action: Action;
    @Input() actionStatuses: ActionStatus[];
    @Input() loading: boolean;    
    @Output() save: EventEmitter<Action> = new EventEmitter();
    @Output() delete: EventEmitter<void> = new EventEmitter();

    public temp: Action = null;
    public errorContact: boolean = false;

    constructor(private appService: AppService) {}

    get lang(): Lang {return this.appService.lang;}

    public ngOnChanges(changes: SimpleChanges): void {        
        if (changes.action && changes.action.currentValue) {
            this.temp = new Action().build(this.action);            
        }
    }

    public onSave(): void {    
        if (this.validate()) {
            this.save.emit(this.temp);        
        }        
    }

    public onDelete(): void {
        if (window.confirm(this.lang.phrases["common-sure"])) {
            this.delete.emit();
        }        
    }  
    
    private validate(): boolean {
        let error = false;
        // TODO
        return !error;
    }
}