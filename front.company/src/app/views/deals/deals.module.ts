import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { CCModule } from "src/app/common.components/cc.module";
import { ActionEditComponent } from "./components/action-edit/action-edit.component";
import { DealEditComponent } from "./components/deal-edit/deal-edit.component";
import { IndexDealsPage } from "./pages/index/index.deals.page";

let routes = RouterModule.forChild ([            
	{path: "", component: IndexDealsPage, pathMatch: "full", data: {mark: "deals"}}, // marked for reuse					
	{path: ":mode", component: IndexDealsPage, data: {mark: "deals"}},	
	{path: ":mode/:deal_id", component: IndexDealsPage, data: {mark: "deals"}},		
	{path: ":mode/:deal_id/:action_id", component: IndexDealsPage, data: {mark: "deals"}},		
]);

@NgModule({	
    imports: [	
		CommonModule,
		RouterModule,
        FormsModule,        
        routes,
		CCModule,
	],
	declarations: [
		IndexDealsPage,		
		DealEditComponent,
		ActionEditComponent,
	],    		    
})
export class DealsModule {}