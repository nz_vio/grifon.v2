import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ContractorAlphaGroup } from "src/app/model/contractor.alphagroup.interface";
import { Contractor } from "src/app/model/orm/contractor.model";
import { Employee } from "src/app/model/orm/employee.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";
import { ContractorRepository } from "src/app/services/repositories/contractor.repository";

@Component({
    selector: "index-contractors-page",
    templateUrl: "index.contractors.page.html",
    styleUrls: ["../../../../common.styles/contractors.scss"],
})
export class IndexContractorsPage implements OnInit {
    @ViewChild("contractorsblock", {static: false}) contractorsElementRef: ElementRef; 

    // contractors list
    public contractorsAlphaGroups: ContractorAlphaGroup[] = [];
    public contractors: Contractor[] = [];
    public contractorsSearch: string = "";
    public contractorsLength: number = 0;
    public contractorsReady: boolean = false;
    public contractorsLoadingMore: boolean = false;     
    private contractorsPart: number = 0;
    private contractorsChunkLength: number = 20;
    private contractorsExhausted: boolean = false;      
    // edit contractor
    public contractor: Contractor = null;
    public contractorMode: string = null;
    public contractorLoading: boolean = false;
    // iface
    public splitted: boolean = false;       

    constructor(
        private appService: AppService,  
        private employeeService: EmployeeService, 
        private contractorRepository: ContractorRepository,               
        private route: ActivatedRoute,
        private router: Router,
    ) {}
    
    get lang(): Lang {return this.appService.lang;}   
    get employee(): Employee {return this.employeeService.authData.value.employee;}   
    get contractorsElement(): HTMLElement {return this.contractorsElementRef.nativeElement;}
    get contractorsCanLoadMore(): boolean {return !this.contractorsLoadingMore && !this.contractorsExhausted && this.contractorsScrolledToBottom;}
    get contractorsScrolledToBottom(): boolean {return this.contractorsElement.scrollHeight - this.contractorsElement.scrollTop < this.contractorsElement.clientHeight + 100;}
    get contractorsFilter(): any {return {company_id: this.employee.company_id, name: this.contractorsSearch ? `%${this.contractorsSearch}%` : null};}

    public ngOnInit(): void {        
        this.initTitle();  
        this.initContractors();                
        this.route.params.subscribe(p => {              
            this.initContractor(p["mode"], p["contractor_id"]);                                  
        });         
    }

    private initTitle(): void {
        this.appService.setTitle(this.lang.phrases["contractors-title"]);        
    }  

    public async initContractors(): Promise<void> {
        try {            
            this.contractorsReady = false;
            this.initContractorsAlphaGroups();
            this.contractorsPart = 0;                  
            const chunk = await this.contractorRepository.loadChunk(this.contractorsPart, this.contractorsChunkLength, "name", 1, this.contractorsFilter);    
            this.contractorsBuildAlphaGroups(chunk.data);            
            this.contractors = chunk.data;
            this.contractorsLength = chunk.allLength;
            this.contractorsExhausted = !chunk.allLength || this.contractorsPart + 1 === Math.ceil(chunk.allLength / this.contractorsChunkLength);   
            this.contractorsReady = true;
        } catch (err) {
            this.appService.showError(err);
        }
    }

    private initContractorsAlphaGroups(): void {
        const alphabet = "0123456789abcdefghijklmnopqrstuvwxyz_абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        this.contractorsAlphaGroups = [];

        for (let i = 0; i < alphabet.length; i++) {
            this.contractorsAlphaGroups.push({letter: alphabet.charAt(i), contractors: []});
        }
    }

    private async initContractor(mode: string, contractor_id: string): Promise<void> {
        try {
            if (mode === "edit" || mode === "view") {                                
                // if not in list - try to load (maybe not scrolled to this item or even not loaded yet!)              
                this.contractor = this.contractors.find(c => c.id === parseInt(contractor_id)) || await this.contractorRepository.loadOne(parseInt(contractor_id));                 
                this.splitted = true;
            } else if (mode === "create") {                
                this.contractor = new Contractor().init(this.employee.company_id);                
                this.splitted = true;
            } else {
                // move out and then reset
                this.splitted = false;             
                await this.appService.pause(300);
                this.contractor = null;                
            }

            this.contractorMode = mode; // switch mode after data init!
        } catch (err) {
            err === 404 ? this.router.navigateByUrl("/contractors") : this.appService.showError(err);            
        }
    }    

    private contractorsBuildAlphaGroups(contractors: Contractor[]): void {
        for (let c of contractors) {
            const group = this.contractorsAlphaGroups.find(g => g.letter === c.firstLetter.toLowerCase());
            group ? group.contractors.push(c) : this.contractorsAlphaGroups[36].contractors.push(c);
        }        
    }

    public async contractorsOnScroll(): Promise<void> {
        try {			            
            if (this.contractorsCanLoadMore) {
				this.contractorsLoadingMore = true;
				this.contractorsPart++;                
                const chunk = await this.contractorRepository.loadChunk(this.contractorsPart, this.contractorsChunkLength, "name", 1, this.contractorsFilter);    
                this.contractorsBuildAlphaGroups(chunk.data);
                this.contractors = [...this.contractors, ...chunk.data];
                this.contractorsExhausted = !chunk.allLength || this.contractorsPart + 1 === Math.ceil(chunk.allLength / this.contractorsChunkLength); 
				this.contractorsLoadingMore = false;		                
			}			
		} catch (err) {
			this.contractorsLoadingMore = false;
			this.appService.showError(err);
		}
    }    

    public async contractorOnSave(contractor: Contractor): Promise<void> {
        try {
            this.contractorLoading = true;               
            this.contractorMode === "edit" ? await this.contractorRepository.update(contractor) : await this.contractorRepository.create(contractor);            
            this.contractorLoading = false;
            this.router.navigateByUrl("/contractors");            
            this.initContractors();
        } catch (err) {
            this.appService.showError(err);
            this.contractorLoading = false;
        }
    }

    public async contractorOnDelete(): Promise<void> {
        try {
            this.contractorLoading = true;
            await this.contractorRepository.delete(this.contractor.id);
            this.contractorLoading = false;
            this.router.navigateByUrl("/contractors");            
            this.initContractors();
        } catch (err) {
            this.appService.showError(err);
            this.contractorLoading = false;
        }
    }
}