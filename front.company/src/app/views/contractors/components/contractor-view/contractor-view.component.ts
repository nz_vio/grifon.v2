import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Contractor } from "src/app/model/orm/contractor.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";

@Component({
    selector: "contractor-view",
    templateUrl: "contractor-view.component.html",
    styleUrls: ["contractor-view.component.scss"],
})
export class ContractorViewComponent {
    @Input() contractor: Contractor;    

    constructor(private appService: AppService) {}

    get lang(): Lang {return this.appService.lang;}    
}