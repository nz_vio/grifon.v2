import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from "@angular/core";
import { Contractor } from "src/app/model/orm/contractor.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";

@Component({
    selector: "contractor-edit",
    templateUrl: "contractor-edit.component.html",    
})
export class ContractorEditComponent implements OnChanges {
    @Input() contractor: Contractor;        
    @Input() loading: boolean;    
    @Output() save: EventEmitter<Contractor> = new EventEmitter();
    @Output() delete: EventEmitter<void> = new EventEmitter();

    public temp: Contractor = null;
    public errorName: boolean = false;
    public errorPhone: boolean = false;
    
    constructor(private appService: AppService) {}

    get lang(): Lang {return this.appService.lang;}

    public ngOnChanges(changes: SimpleChanges): void {        
        if (changes.contractor && changes.contractor.currentValue) {
            this.temp = new Contractor().build(this.contractor);            
        }
    }

    public onSave(): void {    
        if (this.validate()) {
            this.save.emit(this.temp);        
        }        
    }

    public onDelete(): void {
        if (window.confirm(this.lang.phrases["common-sure"])) {
            this.delete.emit();
        }        
    }  
    
    private validate(): boolean {
        let error = false;

        if (!this.temp.name.length) {
            this.errorName = true;
            error = true;
        } else {
            this.errorName = false;
        }

        if (!this.temp.phone.length) {
            this.errorPhone = true;
            error = true;
        } else {
            this.errorPhone = false;
        }

        return !error;
    }
}