import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { CCModule } from "src/app/common.components/cc.module";
import { ContractorEditComponent } from "./components/contractor-edit/contractor-edit.component";
import { ContractorViewComponent } from "./components/contractor-view/contractor-view.component";
import { IndexContractorsPage } from "./pages/index/index.contractors.page";

let routes = RouterModule.forChild ([            
	{path: "", component: IndexContractorsPage, pathMatch: "full", data: {mark: "contractors"}}, // marked for reuse					
	{path: ":mode", component: IndexContractorsPage, data: {mark: "contractors"}},	
	{path: ":mode/:contractor_id", component: IndexContractorsPage, data: {mark: "contractors"}},
]);

@NgModule({	
    imports: [	
		CommonModule,
		RouterModule,
        FormsModule,        
        routes,
		CCModule,
	],
	declarations: [
		IndexContractorsPage,		
		ContractorViewComponent,
		ContractorEditComponent,
	],    		    
})
export class ContractorsModule {}