import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Model } from "src/app/model/orm/model";

@Component({
    selector: "flex-radio",
    templateUrl: "flex-radio.component.html",
    styleUrls: ["flex-radio.component.scss"],
})
export class FlexRadioComponent {
    @Input() items: Model;
    @Input() visibleField: string;
    @Input() value: number;
    @Output() valueChange: EventEmitter<number> = new EventEmitter();

    public onSelect(item: Model): void {
        this.valueChange.emit(item.id);
    }
}