import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from "@angular/core";
import { Contractor } from "src/app/model/orm/contractor.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { ContractorRepository } from "src/app/services/repositories/contractor.repository";

// TODO: должно ли это окно сохранять изменения? 
@Component({
    selector: "win-contractor-edit",
    templateUrl: "win-contractor-edit.component.html",
    styleUrls: [
        "win-contractor-edit.component.scss",
        "../../common.styles/popups.scss",
    ],
})
export class WinContractorEditComponent implements OnChanges {    
    @Input() contractor: Contractor;
    @Input() active: boolean;
    @Output() activeChange: EventEmitter<boolean> = new EventEmitter();  
    @Output() saved: EventEmitter<Contractor> = new EventEmitter();  
    
    public temp: Contractor = null;
    public errorName: boolean = false;
    public errorPhone: boolean = false;
    public loading: boolean = false;

    constructor(
        private appService: AppService,            
        private contractorRepository: ContractorRepository,
    ) {}
    
    get lang(): Lang {return this.appService.lang;}      
    
    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.active && changes.active.currentValue) {
            this.temp = new Contractor().build(this.contractor);            
        }
    }    
    
    public onClose(): void {
        this.activeChange.emit(false);
    }    

    public async onSave(): Promise<void> {
        try {
            if (this.validate()) {
                this.loading = true;
                const res = this.temp.id ? await this.contractorRepository.update(this.temp) : await this.contractorRepository.create(this.temp);
                this.loading = false;
                this.saved.emit(res);
                this.onClose();
            }
        } catch (err) {
            this.loading = false;
            this.appService.showError(err);
        }        
    }

    private validate(): boolean {
        let error = false;

        if (!this.temp.name.length) {
            this.errorName = true;
            error = true;
        } else {
            this.errorName = false;
        }

        if (!this.temp.phone.length) {
            this.errorPhone = true;
            error = true;
        } else {
            this.errorPhone = false;
        }

        return !error;
    }
}