import { Component, Input } from "@angular/core";
import { Contractor } from "src/app/model/orm/contractor.model";
import { Deal } from "src/app/model/orm/deal.model";
import { Employee } from "src/app/model/orm/employee.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";

@Component({
    selector: "the-deal",
    templateUrl: "deal.component.html",
    styleUrls: ["../../common.styles/forms.scss",]
})
export class DealComponent {
    @Input() deal: Deal;
    @Input() canChangeContact: boolean;
    @Input() errorContact: boolean;

    // contractor selection
    public contractorSelectFor: string = ""; // какому полю будет присвоен контрагент после выбора
    public contractorsListActive: boolean = false; // окно вкл/выкл
    // contractor edition/creation
    public contractorEditing: Contractor = null; // копия контрагента для правки
    public contractorEditFor: string = ""; // какому полю будет присвоен контрагент после правки
    public contractorEditActive: boolean = false; // окно вкл/выкл

    constructor(
        private appService: AppService,
        private employeeService: EmployeeService,
    ) {}
    
    get lang(): Lang {return this.appService.lang;}           
    get employee(): Employee {return this.employeeService.authData.value.employee;}

    public onSelectContractor(field: string): void {
        this.contractorSelectFor = field; 
        this.contractorsListActive = true;
    }

    public onEditContractor(field: string): void {
        this.contractorEditFor = field;
        this.contractorEditing = this.deal[field];
        this.contractorEditActive = true;
    }

    public onCreateContractor(field: string): void {
        this.contractorEditFor = field;
        this.contractorEditing = new Contractor().init(this.employee.company_id);
        this.contractorEditActive = true;
    }
}