import { Component, ElementRef, HostListener, Input, OnInit, ViewChild } from "@angular/core";
import { Lang } from "src/app/model/orm/lang.model";
import { Model } from "src/app/model/orm/model";
import { AppService } from "src/app/services/app.service";
import { ContractorGroupRepository } from "src/app/services/repositories/contractor.group.repository";
import { Repository } from "src/app/services/repositories/_repository";

@Component({
    selector: "flex-select-infinite",
    templateUrl: "flex-select-infinite.component.html",
    styleUrls: ["../flex-select.scss"],
})
export class FlexSelectInfiniteComponent implements OnInit {
    @Input() items: Model[];
    @Input() visibleField: string;
    @Input() object: string;
    @Input() withSearch: boolean = true;

    @ViewChild("btnelement", {static: false}) btnElementRef: ElementRef;     
    @ViewChild("sitemselement", {static: false}) sitemsElementRef: ElementRef;    

    public unique: number = null;
    // selectable items
    private sitemsRepository: Repository<Model>;
    public sitems: Model[] = [];
    public sitemsSearch: string = "";    
    public sitemsLoadingMore: boolean = false;     
    private sitemsPart: number = 0;
    private sitemsChunkLength: number = 20;
    private sitemsExhausted: boolean = false;   
    public sitemsReady: boolean = false;
    // menu
    public menuTop: string = "";    
    public menuLeft: string = "";  
    private menuListHeight: number = 210;
    private menuSearchHeight: number = 60;
    private menuWidth: number = 230;
    public listItemHeight: number = 40;
    public menuActive: boolean = false;    

    constructor(
        private appService: AppService,        
        private contractorGroupRepository: ContractorGroupRepository,
        // private someOtherRepository: SomeOtherRepository, - any compatible repo
    ) {}

    get lang(): Lang {return this.appService.lang;}    
    get btnElement(): HTMLElement {return this.btnElementRef.nativeElement;}    
    get menuHeight(): number {return this.menuListHeight + (this.withSearch ? this.menuSearchHeight : 0);}
    get sitemsElement(): HTMLElement {return this.sitemsElementRef.nativeElement;}
    get sitemsCanLoadMore(): boolean {return !this.sitemsLoadingMore && !this.sitemsExhausted && this.sitemsScrolledToBottom;}
    get sitemsScrolledToBottom(): boolean {return this.sitemsElement.scrollHeight - this.sitemsElement.scrollTop < this.sitemsElement.clientHeight + 50;}
    get sitemsFilter(): any {return {name: this.sitemsSearch ? `%${this.sitemsSearch}%` : null};} // обратить внимание, что если планируется использовать сущность, привязанную к компании, фильтр надо доработать

    public ngOnInit(): void {
        this.unique = this.appService.rnd(111111111, 999999999);    
        this.sitemsRepository = this[`${this.object}Repository`];
        this.initSelectableItems();
    }

    public async initSelectableItems(): Promise<void> {
        try {
            this.sitemsReady = false;
            this.sitems = [];            
            this.sitemsPart = 0;                                    
            const chunk = await this.sitemsRepository.loadChunk(this.sitemsPart, this.sitemsChunkLength, this.visibleField, 1, this.sitemsFilter);                                        
            this.sitems = chunk.data;            
            this.sitemsExhausted = !chunk.allLength || this.sitemsPart + 1 === Math.ceil(chunk.allLength / this.sitemsChunkLength);   
            this.sitemsReady = true;
        } catch (err) {
            this.appService.showError(err);
        }
    }

    public async menuOnActivate(): Promise<void> {        
        const btnLeft = this.btnElement.getBoundingClientRect().left; 
        const btnRight = this.btnElement.getBoundingClientRect().right; 
        const btnTop = this.btnElement.getBoundingClientRect().top;   
        this.menuLeft = (btnLeft + this.menuWidth > window.innerWidth - 30) ? `${btnRight - this.menuWidth}px` : `${btnLeft}px`;
        this.menuTop = (btnTop + this.menuHeight > window.innerHeight - 30) ? `${btnTop - this.menuHeight - 5}px` : `${btnTop + this.listItemHeight + 5}px`;        
        this.menuActive = true;
    }

    public sitemsIsActive(id: number): boolean {
        return this.items.map(item => item.id).includes(id);
    }

    public sitemsOnClick(sitem: Model): void {
        if (this.items.map(item => item.id).includes(sitem.id)) {
            const index = this.items.findIndex(item => item.id === sitem.id);
            this.items.splice(index, 1);
        } else {
            this.items.push(sitem);
        }
    }

    public async sitemsOnScroll(): Promise<void> {
        try {			            
            if (this.sitemsCanLoadMore) {
				this.sitemsLoadingMore = true;
				this.sitemsPart++;                
                const chunk = await this.sitemsRepository.loadChunk(this.sitemsPart, this.sitemsChunkLength, this.visibleField, 1, this.sitemsFilter);                                        
                this.sitems = [...this.sitems, ...chunk.data];
                this.sitemsExhausted = !chunk.allLength || this.sitemsPart + 1 === Math.ceil(chunk.allLength / this.sitemsChunkLength); 
				this.sitemsLoadingMore = false;		                
			}			
		} catch (err) {
			this.sitemsLoadingMore = false;
			this.appService.showError(err);
		}
    }

    @HostListener('window:resize')
    public onResize(): void {        
        this.menuActive ? this.menuActive = false : null;            
    }

    @HostListener('document:click', ['$event'])
    public onDocumentClick(event: PointerEvent): void {        
        if(!this.appService.pathHasIds(event.composedPath() as HTMLElement[], [`fs-item-btn-${this.unique}`, `fs-menu-${this.unique}`])) {            
            this.menuActive = false;            
        }
    }    
}