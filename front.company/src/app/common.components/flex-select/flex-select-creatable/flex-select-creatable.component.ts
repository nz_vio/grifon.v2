import { Component, ElementRef, HostListener, Input, OnInit, ViewChild } from "@angular/core";
import { Lang } from "src/app/model/orm/lang.model";
import { Model } from "src/app/model/orm/model";
import { AppService } from "src/app/services/app.service";
import { ActionTagRepository } from "src/app/services/repositories/action.tag.repository";
import { Repository } from "src/app/services/repositories/_repository";

@Component({
    selector: "flex-select-creatable",
    templateUrl: "flex-select-creatable.component.html",
    styleUrls: ["../flex-select.scss"],
})
export class FlexSelectCreatableComponent implements OnInit {
    @Input() items: Model[];
    @Input() visibleField: string;
    @Input() object: string;    

    @ViewChild("btnelement", {static: false}) btnElementRef: ElementRef;         

    public unique: number = null;
    // selectable items
    private sitemsRepository: Repository<Model>;
    public sitems: Model[] = [];
    public sitemsFiltered: Model[] = [];
    public sitemsSearch: string = "";    
    public sitemsReady: boolean = false;
    // menu
    public menuTop: string = "";    
    public menuLeft: string = "";  
    private menuListHeight: number = 210;
    private menuSearchHeight: number = 60;
    private menuWidth: number = 230;
    public listItemHeight: number = 40;
    public menuActive: boolean = false;    

    constructor(
        private appService: AppService,        
        private actionTagRepository: ActionTagRepository,        
        // private someOtherRepository: SomeOtherRepository, - any compatible repo
    ) {}

    get lang(): Lang {return this.appService.lang;}    
    get btnElement(): HTMLElement {return this.btnElementRef.nativeElement;}    
    get menuHeight(): number {return this.menuListHeight + this.menuSearchHeight;}   
    get sitemsCanCreate(): boolean {return this.sitemsSearch.length && !this.sitems.find(si => si[this.visibleField] === this.sitemsSearch);}

    public ngOnInit(): void {
        this.unique = this.appService.rnd(111111111, 999999999);    
        this.sitemsRepository = this[`${this.object}Repository`];
        this.initSelectableItems();
    }

    public async initSelectableItems(): Promise<void> {
        try {
            this.sitemsReady = false;
            this.sitems = [];                        
            this.sitems = await this.sitemsRepository.loadAll(this.visibleField, 1); 
            this.sitemsFiltered = this.sitems;                                       
            this.sitemsReady = true;
        } catch (err) {
            this.appService.showError(err);
        }
    }

    public async menuOnActivate(): Promise<void> {        
        const btnLeft = this.btnElement.getBoundingClientRect().left; 
        const btnRight = this.btnElement.getBoundingClientRect().right; 
        const btnTop = this.btnElement.getBoundingClientRect().top;   
        this.menuLeft = (btnLeft + this.menuWidth > window.innerWidth - 30) ? `${btnRight - this.menuWidth}px` : `${btnLeft}px`;
        this.menuTop = (btnTop + this.menuHeight > window.innerHeight - 30) ? `${btnTop - this.menuHeight - 5}px` : `${btnTop + this.listItemHeight + 5}px`;        
        this.menuActive = true;
    }

    public sitemsIsActive(id: number): boolean {
        return this.items.map(item => item.id).includes(id);
    }

    public sitemsOnClick(sitem: Model): void {
        if (this.items.map(item => item.id).includes(sitem.id)) {
            const index = this.items.findIndex(item => item.id === sitem.id);
            this.items.splice(index, 1);
        } else {
            this.items.push(sitem);
        }
    }    

    public async sitemsOnCreate(): Promise<void> {
        try {            
            this.sitemsReady = false;
            const fieldValue = this.sitemsSearch;
            this.sitemsSearch = "";
            const created = await this.sitemsRepository.create({[this.visibleField]: fieldValue});
            this.items.push(created);            
            this.sitems.push(created);
            this.sitems.sort((a, b) => (a[this.visibleField] as string).localeCompare(b[this.visibleField]));
            this.sitemsFiltered = this.sitems;
            this.sitemsReady = true;
        } catch (err) {
            console.log(err);
        }
    }

    public sitemsOnSearch(): void {
        this.sitemsSearch = this.sitemsSearch.trim();
        this.sitemsFiltered = this.sitems.filter(si => (si[this.visibleField] as string).toLowerCase().includes(this.sitemsSearch.toLowerCase()));
    }

    @HostListener('window:resize')
    public onResize(): void {        
        this.menuActive ? this.menuActive = false : null;            
    }

    @HostListener('document:click', ['$event'])
    public onDocumentClick(event: PointerEvent): void {        
        if(!this.appService.pathHasIds(event.composedPath() as HTMLElement[], [`fs-item-btn-${this.unique}`, `fs-menu-${this.unique}`])) {            
            this.menuActive = false;            
        }
    }    
}