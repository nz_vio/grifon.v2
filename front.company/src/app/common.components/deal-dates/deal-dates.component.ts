import { Component, Input } from "@angular/core";
import { DealDate } from "src/app/model/orm/deal.date.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";

@Component({
    selector: "deal-dates",
    templateUrl: "deal-dates.component.html",
    styleUrls: ["deal-dates.component.scss"],
})
export class DealDatesComponent {
    @Input() dates: DealDate[];

    constructor(private appService: AppService) {}

    get lang(): Lang {return this.appService.lang;}

    public onCreate(): void {
        this.dates.push(new DealDate().init());
    }

    public onDelete(i: number): void {
        this.dates.splice(i, 1);
    }
}