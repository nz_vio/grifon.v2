import { Component, EventEmitter, Input, Output, ViewEncapsulation } from "@angular/core";
import { Lang } from "src/app/model/orm/lang.model";
import { Model } from "src/app/model/orm/model";
import { AppService } from "src/app/services/app.service";

// часть формы - присоединенный объект
@Component({
    selector: "in-form-object",
    templateUrl: "in-form-object.component.html",
    styleUrls: ["../../common.styles/forms.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class InFormObjectComponent {
    @Input() obj: Model;
    @Input() visibleField: string;
    @Input() deletable: boolean;
    @Input() selectable: boolean;
    @Input() creatable: boolean;
    @Input() editable: boolean;
    @Output() deleteObject: EventEmitter<void> = new EventEmitter();
    @Output() createObject: EventEmitter<void> = new EventEmitter();
    @Output() selectObject: EventEmitter<void> = new EventEmitter();
    @Output() editObject: EventEmitter<void> = new EventEmitter();

    constructor(private appService: AppService) {}

    get lang(): Lang {return this.appService.lang;}

    public onDelete(): void {
        this.deleteObject.emit();
    }

    public onSelect(): void {
        this.selectObject.emit();
    }

    public onCreate(): void {
        this.createObject.emit();
    }

    public onEdit(): void {
        this.editObject.emit();
    }
}