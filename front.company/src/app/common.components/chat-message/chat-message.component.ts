import { Component, EventEmitter, Input, Output, ViewEncapsulation } from "@angular/core";
import { DomSanitizer, SafeResourceUrl, SafeUrl } from "@angular/platform-browser";
import { ChatMessage, ChatMessageType } from "src/app/model/orm/chat.message.model";
import { Company } from "src/app/model/orm/company.model";
import { Employee } from "src/app/model/orm/employee.model";
import { Lang } from "src/app/model/orm/lang.model";
import { Settings } from "src/app/model/orm/settings.type";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";
import { CfgRepository } from "src/app/services/repositories/cfg.repository";
import { ChatMessageRepository } from "src/app/services/repositories/chat.message.repository";
import { SettingRepository } from "src/app/services/repositories/setting.repository";

@Component({
    selector: "chat-message",
    templateUrl: "chat-message.component.html",
    styleUrls: ["chat-message.component.scss"],
    encapsulation: ViewEncapsulation.None,
})
export class ChatMessageComponent {
    @Input() message: ChatMessage;
    @Input() inAction: boolean;
    @Input() buttonsEnabled: boolean = true;
    @Output() reply: EventEmitter<void> = new EventEmitter();
    @Output() actions: EventEmitter<void> = new EventEmitter();

    constructor(
        private appService: AppService,
        private chatMessageRepository: ChatMessageRepository,        
        private employeeService: EmployeeService,        
    ) {}

    get lang(): Lang {return this.appService.lang;}    
    get storageUrl(): string {return this.appService.storageUrl;}
    get employee(): Employee {return this.employeeService.authData.value.employee;}
    get company(): Company {return this.employee.company;}    

    public onReply(): void {
        this.reply.emit();
    }

    public onActions(): void {
        this.actions.emit();
    }

    public onTaskBtnClick(): void {
        try {
            this.message.is_task = !this.message.is_task;
            this.chatMessageRepository.updateParam(this.message.id, "is_task", this.message.is_task);
        }  catch (err) {
            this.appService.showError(err);
        }        
    }
}