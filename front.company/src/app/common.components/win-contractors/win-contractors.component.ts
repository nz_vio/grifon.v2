import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from "@angular/core";
import { Timeout } from "src/app/decorators/timeout";
import { ContractorAlphaGroup } from "src/app/model/contractor.alphagroup.interface";
import { Contractor } from "src/app/model/orm/contractor.model";
import { Employee } from "src/app/model/orm/employee.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";
import { ContractorRepository } from "src/app/services/repositories/contractor.repository";

@Component({
    selector: "win-contractors",
    templateUrl: "win-contractors.component.html",
    styleUrls: [
        "win-contractors.component.scss",
        "../../common.styles/popups.scss",
        "../../common.styles/contractors.scss",
    ],
})
export class WinContractorsComponent implements OnChanges {    
    @Input() active: boolean;
    @Output() activeChange: EventEmitter<boolean> = new EventEmitter();
    @Output() selected: EventEmitter<Contractor> = new EventEmitter();

    @ViewChild("contractorselement", {static: false}) contractorsElementRef: ElementRef;        
    
    public contractor: Contractor = null;
    public contractorsAlphaGroups: ContractorAlphaGroup[] = [];
    public contractorsSearch: string = "";    
    public contractorsLoadingMore: boolean = false;     
    private contractorsPart: number = 0;
    private contractorsChunkLength: number = 20;
    private contractorsExhausted: boolean = false;   
    public contractorsReady: boolean = false;
    
    constructor(
        private appService: AppService,        
        private employeeService: EmployeeService,
        private contractorRepository: ContractorRepository,
    ) {}
    
    get lang(): Lang {return this.appService.lang;}      
    get employee(): Employee {return this.employeeService.authData.value.employee;}        
    get contractorsFilter(): any {return {company_id: this.employee.company_id, name: this.contractorsSearch ? `%${this.contractorsSearch}%` : null};}
    get contractorsElement(): HTMLElement {return this.contractorsElementRef.nativeElement;}
    get contractorsCanLoadMore(): boolean {return !this.contractorsLoadingMore && !this.contractorsExhausted && this.contractorsScrolledToBottom;}
    get contractorsScrolledToBottom(): boolean {return this.contractorsElement.scrollHeight - this.contractorsElement.scrollTop < this.contractorsElement.clientHeight + 100;}

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.active && changes.active.currentValue) {
            this.contractor = null;
            this.initContractors();
        }
    }    
    
    public async initContractors(): Promise<void> {
        try {            
            this.contractorsReady = false;
            this.initContractorsAlphaGroups();
            this.contractorsPart = 0;                  
            const chunk = await this.contractorRepository.loadChunk(this.contractorsPart, this.contractorsChunkLength, "name", 1, this.contractorsFilter);                            
            this.buildAlphaGroups(chunk.data);                                    
            this.contractorsExhausted = !chunk.allLength || this.contractorsPart + 1 === Math.ceil(chunk.allLength / this.contractorsChunkLength);   
            this.contractorsReady = true;            
        } catch (err) {
            this.appService.showError(err);
        }
    }

    private initContractorsAlphaGroups(): void {
        const alphabet = "0123456789abcdefghijklmnopqrstuvwxyz_абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        this.contractorsAlphaGroups = [];

        for (let i = 0; i < alphabet.length; i++) {
            this.contractorsAlphaGroups.push({letter: alphabet.charAt(i), contractors: []});
        }
    }

    public async onScroll(): Promise<void> {
        try {			            
            if (this.contractorsCanLoadMore) {
				this.contractorsLoadingMore = true;
				this.contractorsPart++;                
                const chunk = await this.contractorRepository.loadChunk(this.contractorsPart, this.contractorsChunkLength, "name", 1, this.contractorsFilter);    
                this.buildAlphaGroups(chunk.data);                
                this.contractorsExhausted = !chunk.allLength || this.contractorsPart + 1 === Math.ceil(chunk.allLength / this.contractorsChunkLength); 
				this.contractorsLoadingMore = false;	
			}			
		} catch (err) {
			this.contractorsLoadingMore = false;
			this.appService.showError(err);
		}
    }

    private buildAlphaGroups(contractors: Contractor[]): void {
        for (let c of contractors) {
            const group = this.contractorsAlphaGroups.find(g => g.letter === c.firstLetter.toLowerCase());
            group ? group.contractors.push(c) : this.contractorsAlphaGroups[36].contractors.push(c);
        }        
    }
    
    public onClose(): void {
        this.activeChange.emit(false);
    }

    public onSelect(): void {
        this.selected.emit(this.contractor);
        this.onClose();
    }
}