import { Component, Input, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { AppService } from "src/app/services/app.service";

@Component({
    selector: "audio-player",
    templateUrl: "audio-player.component.html",
    styleUrls: ["audio-player.component.scss"],
})
export class AudioPlayerComponent implements OnChanges {
    @Input() src: string;

    public audioElement: HTMLAudioElement;
    public canPlay: boolean = false;
    public playing: boolean = false;    
    public currentTime: number = 0;

    constructor(private appService: AppService) {}

    get strCurrentTime(): string {return this.appService.seconds2time(this.currentTime);}
    get strDuration(): string {return this.appService.seconds2time(this.audioElement?.duration || 0);}            

    public ngOnChanges(changes: SimpleChanges): void {
        this.initAudio();
    }

    private initAudio(): void {
        if (this.src) {            
            this.canPlay = false;
            this.playing = false;
            this.audioElement = new Audio(this.src);
            this.currentTime = 0;            
            this.audioElement.addEventListener("canplaythrough", () => this.canPlay = true);
            this.audioElement.addEventListener("timeupdate", () => this.currentTime = this.audioElement.currentTime);
            this.audioElement.addEventListener("ended", () => this.playing = false);
        }        
    }

    public playOrPause(): void {
        if (this.playing) {
            this.audioElement.pause();
            this.playing = false;
        } else {
            this.audioElement.play();
            this.playing = true;
        }
    }
}