import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AudioPlayerComponent } from './audio-player/audio-player.component';
import { CheckBoxComponent } from './check-box/check-box.component';
import { ErrorNotificationComponent } from './error-notification/errornotification.component';
import { FilePickerComponent } from './file-picker/file-picker.component';
import { HeaderComponent } from './header/header.component';
import { MessageMenubtnComponent } from './menubtn/message-menubtn/message-menubtn.component';
import { MessagesMenubtnComponent } from './menubtn/messages-menubtn/messages-menubtn.component';
import { FlexRadioComponent } from './flex-radio/flex-radio.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { InFormObjectComponent } from './in-form-object/in-form-object.component';
import { ContractorComponent } from './contractor/contractor.component';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { DealDatesComponent } from './deal-dates/deal-dates.component';
import { WinContractorsComponent } from './win-contractors/win-contractors.component';
import { WinContractorEditComponent } from './win-contractor-edit/win-contractor-edit.component';
import { DealComponent } from './deal/deal.component';
import { ActionFileComponent } from './action-file/action-file.component';
import { ChatMessageComponent } from './chat-message/chat-message.component';
import { ActionRemindersComponent } from './action-reminders/action-reminders.component';
import { FlexSelectInfiniteComponent } from './flex-select/flex-select-infinite/flex-select-infinite.component';
import { FlexSelectCreatableComponent } from './flex-select/flex-select-creatable/flex-select-creatable.component';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,        
    ],
    declarations: [
        ErrorNotificationComponent,      
        SidebarComponent,  
        HeaderComponent,
        MessagesMenubtnComponent,
        MessageMenubtnComponent,
        FlexRadioComponent,
        FlexSelectInfiniteComponent,
        FlexSelectCreatableComponent,
        CheckBoxComponent,
        AudioPlayerComponent,
        FilePickerComponent,
        InFormObjectComponent,     
        ContractorComponent,   
        DatePickerComponent,
        DealDatesComponent,
        WinContractorsComponent,
		WinContractorEditComponent,		
        DealComponent,
        ActionFileComponent,
        ActionRemindersComponent,
        ChatMessageComponent,	
    ],
    exports: [
        ErrorNotificationComponent,        
        SidebarComponent,
        HeaderComponent,
        MessagesMenubtnComponent,
        MessageMenubtnComponent,
        FlexRadioComponent,
        FlexSelectInfiniteComponent,
        FlexSelectCreatableComponent,
        CheckBoxComponent,
        AudioPlayerComponent,
        FilePickerComponent,
        InFormObjectComponent,        
        ContractorComponent,
        DatePickerComponent,
        DealDatesComponent,
        WinContractorsComponent,
		WinContractorEditComponent,	
        DealComponent,	
        ActionFileComponent,
        ActionRemindersComponent,
        ChatMessageComponent,	
    ],    
})
export class CCModule {}
