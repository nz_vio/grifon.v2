import { Component, Input, Output, EventEmitter, OnInit, HostListener, ViewChild, ElementRef } from "@angular/core";
import { IDay } from './day.interface';
import { AppService } from "src/app/services/app.service";
import { Lang } from "src/app/model/orm/lang.model";

@Component({
    selector: "date-picker",
    templateUrl: "./date-picker.component.html",
    styleUrls: ["./date-picker.component.scss"],
})
export class DatePickerComponent implements OnInit {
    @Input() date: string;
    @Output() dateChange: EventEmitter<string> = new EventEmitter();   

    @ViewChild("btnelement", {static: false}) btnElementRef: ElementRef;     
    
    // dates
    public days: IDay[] = []; // will select
    public year: number = 0; // will select
    public month: number = 0; // will select
    private currentYear: number = 0; // now selected
    private currentMonth: number = 0; // now selected
    private currentDay: number = 0; // now selected    
    // iface
    public unique: number = null;
    public btnHeight: number = 40;
    public calendarActive: boolean = false;
    public calendarHeight: number = 342;
    public calendarWidth: number = 252;
    public calendarTop: string = "";    
    public calendarLeft: string = "";  

    constructor(private appService: AppService) {}
    
    get lang(): Lang {return this.appService.lang;}
    get btnElement(): HTMLElement {return this.btnElementRef.nativeElement;}    
    get formattedDate(): string {
        if (this.date) {
            const dateParts = this.date.split("-");
            const day = parseInt(dateParts[2]);
            const month = parseInt(dateParts[1]);
            const year = parseInt(dateParts[0]);
            return `${day} ${this.lang.phrases["date-monthr-"+month]} ${year}`;
        }
        
        return this.lang.phrases["common-empty3"];
    }
    
    public ngOnInit(): void {         
        this.unique = this.appService.rnd(111111111, 999999999);   
        this.initDates();        
    }    

    private initDates(): void {
        let iniDate = this.date ? new Date(this.date) : new Date();
        this.year = this.currentYear = iniDate.getFullYear();
        this.month = this.currentMonth = iniDate.getMonth();
        this.currentDay = iniDate.getDate();
        this.buildDays();        
    }

    private buildDays(): void {
        let firstDayOfMonth: number = new Date(this.year, this.month).getDay() - 1;
        (firstDayOfMonth === -1) ? firstDayOfMonth = 6 : null;
        let daysInMonth: number = 32 - new Date(this.year, this.month, 32).getDate();        
        this.days = [];

        for (let i: number = 0; i < firstDayOfMonth; i++) {
            let day: IDay = {hidden: true};
            this.days.push(day);
        }

        for (let j: number = 0; j < daysInMonth; j++) {
            let day: IDay = {n: j+1};
            day.current = (j+1 === this.currentDay && this.month === this.currentMonth && this.year === this.currentYear);            
            let index: number = firstDayOfMonth + j + 1;
            day.holiday = (!(index % 7) || !((index+1) % 7));            
            this.days.push(day);                            
        }
    }    

    public onMonthBack(): void {
        if (this.month === 0) {
            this.month = 11;
            this.year--;
        } else {
            this.month--;
        }

        this.buildDays();
    }

    public onMonthForward(): void {
        if (this.month === 11) {
            this.month = 0;
            this.year++;
        } else {
            this.month++;
        }

        this.buildDays();
    }       

    public onSetDate(day: IDay): void {
        this.currentDay = day.n;
        this.currentMonth = this.month;
        this.currentYear = this.year;        
        this.buildDays();
    }    

    public onApply(): void {        
        this.dateChange.emit(`${this.currentYear}-${this.appService.twoDigits(this.currentMonth+1)}-${this.appService.twoDigits(this.currentDay)}`);
        this.calendarActive = false;
    } 
    
    public onActivate(): void {
        const btnLeft = this.btnElement.getBoundingClientRect().left; 
        const btnRight = this.btnElement.getBoundingClientRect().right; 
        const btnTop = this.btnElement.getBoundingClientRect().top;   
        this.calendarLeft = (btnLeft + this.calendarWidth > window.innerWidth - 30) ? `${btnRight - this.calendarWidth}px` : `${btnLeft}px`;
        this.calendarTop = (btnTop + this.calendarHeight > window.innerHeight - 30) ? `${btnTop - this.calendarHeight - 5}px` : `${btnTop + this.btnHeight + 5}px`;        
        this.calendarActive = true;
    }

    @HostListener('document:click', ['$event'])
    public onDocumentClick(event: PointerEvent): void {        
        if(!this.appService.pathHasIds(event.composedPath() as HTMLElement[], [`dp-date-${this.unique}`, `dp-calendar-${this.unique}`])) {            
            this.calendarActive = false;            
        }
    } 

    @HostListener('window:resize')
    public onResize(): void {        
        this.calendarActive && (this.calendarActive = false);
    }
}
