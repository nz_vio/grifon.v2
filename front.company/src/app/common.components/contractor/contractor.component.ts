import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Contractor } from "src/app/model/orm/contractor.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { UploadService } from "src/app/services/upload.service";

// форма редактирования контрагента вынесена в отдельный компонент, потому что используется в других модулях
@Component({
    selector: "the-contractor",
    templateUrl: "contractor.component.html",
    styleUrls: [
        "contractor.component.scss",
        "../../common.styles/forms.scss",
    ],
})
export class ContractorComponent {
    @Input() x: Contractor;    
    @Input() closable: boolean;
    @Input() errorName: boolean;
    @Input() errorPhone: boolean;
    @Output() save: EventEmitter<void> = new EventEmitter();
    
    public errorImg: boolean = false;
    
    constructor(
        private appService: AppService,
        private uploadService: UploadService,
    ) {}

    get lang(): Lang {return this.appService.lang;}    

    public async onSelectImg(): Promise<void> {
        try {
            this.errorImg = false;
            this.x.img = await this.uploadService.selectImg();            
        } catch (err) {
            this.errorImg = true;
        }        
    }    

    public onDeleteImg(): void {
        this.x.img = null;
    }

    public onSave(): void {        
        this.save.emit();        
    }     
}