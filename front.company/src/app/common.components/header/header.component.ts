import { Component } from "@angular/core";
import { AppService } from "src/app/services/app.service";

@Component({
    selector: "the-header",
    templateUrl: "header.component.html",
    styleUrls: ["header.component.scss"],
})
export class HeaderComponent {
    constructor(private appService: AppService) {}

    get sidebarActive(): boolean {return this.appService.sidebarActive;}
    set sidebarActive(v: boolean) {this.appService.sidebarActive = v;}
}