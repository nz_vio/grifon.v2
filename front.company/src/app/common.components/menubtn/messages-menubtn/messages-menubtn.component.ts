import { Component, EventEmitter, Output } from "@angular/core";
import { MenubtnComponent } from "../menubtn.component";

@Component({
    selector: "messages-menubtn",
    templateUrl: "messages-menubtn.component.html",
    styleUrls: [        
        "../menubtn.component.scss",
        "messages-menubtn.component.scss",
    ],
})
export class MessagesMenubtnComponent extends MenubtnComponent {    
    @Output() contact: EventEmitter<void> = new EventEmitter();
    @Output() delete: EventEmitter<void> = new EventEmitter();        
    
    public items: string[] = ["contact", "delete"];
}