import { Directive, HostListener } from "@angular/core";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";

@Directive()
export abstract class MenubtnComponent {
    public unique: number = null;
    public active: boolean = false;    

    constructor(private appService: AppService) {}
    
    get lang(): Lang {return this.appService.lang;}   

    public ngOnInit(): void {
        this.unique = this.appService.rnd(111111111, 999999999);        
    }

    @HostListener('document:click', ['$event'])
    public onDocumentClick(event: PointerEvent): void {        
        if(!this.appService.pathHasIds(event.composedPath() as HTMLElement[], [`menubtn-${this.unique}`])) {            
            this.active = false;            
        }
    }

    public onSelect(item: string): void {
        this[item].emit();
        this.active = false;
    }
}