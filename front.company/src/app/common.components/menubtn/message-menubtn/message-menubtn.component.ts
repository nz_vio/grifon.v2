import { Component, ElementRef, EventEmitter, HostListener, Input, Output, ViewChild } from "@angular/core";
import { MenubtnComponent } from "../menubtn.component";

@Component({
    selector: "message-menubtn",
    templateUrl: "message-menubtn.component.html",
    styleUrls: [        
        "../menubtn.component.scss",
        "message-menubtn.component.scss",
    ],
})
export class MessageMenubtnComponent extends MenubtnComponent {        
    @Input() is_task: boolean;
    @Output() reply: EventEmitter<void> = new EventEmitter();    
    @Output() actions: EventEmitter<void> = new EventEmitter();    
    
    @ViewChild("btnelement", {static: false}) btnElementRef: ElementRef; 
    
    public menuTop: string = "";    
    public menuLeft: string = "";    

    get items(): string[] {return this.is_task ? ["reply", "actions"] : ["reply"];}
    get btnElement(): HTMLElement {return this.btnElementRef.nativeElement;}    
    get menuHeight(): number {return this.items.length * 40 + 10;}

    public activate(): void {        
        this.menuLeft = `${this.btnElement.getBoundingClientRect().left - 120}px`;   
        const btnTop = this.btnElement.getBoundingClientRect().top;     
        
        if (btnTop + this.menuHeight > window.innerHeight - 30) {
            this.menuTop = `${btnTop - this.menuHeight - 5}px`;            
        } else {
            this.menuTop = `${btnTop + 30}px`;            
        }

        this.active = true;
    }

    @HostListener('window:resize')
    public onResize(): void {        
        this.active ? this.active = false : null;            
    }
}