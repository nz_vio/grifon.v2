import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";

@Component({
    selector: "action-file",
    templateUrl: "action-file.component.html",
    styleUrls: ["action-file.component.scss"],
})
export class ActionFileComponent {
    @Input() name: string;
    @Input() url: string;
    @Input() description: string; // TODO: 2 ways binding!
    @Output() delete: EventEmitter<void> = new EventEmitter();
    @Output() descriptionChange: EventEmitter<string> = new EventEmitter();

    constructor(private appService: AppService) {}

    get lang(): Lang {return this.appService.lang;}  
    get storageUrl(): string {return this.appService.storageUrl;}

    public onDelete(): void {
        this.delete.emit();
    }

    public onDescriptionChange(): void {
        this.descriptionChange.emit(this.description);
    }
}