import { AfterViewInit, Component } from "@angular/core";
import { Employee } from "src/app/model/orm/employee.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { EmployeeService } from "src/app/services/employee.service";

@Component({
    selector: "the-sidebar",
    templateUrl: "sidebar.component.html",
    styleUrls: ["sidebar.component.scss"],
})
export class SidebarComponent implements AfterViewInit {
    constructor(
        private appService: AppService,
        private employeeService: EmployeeService,
    ) {}

    get lang(): Lang {return this.appService.lang;} 
    get url(): string[] {return this.appService.url;}    
    get active(): boolean {return this.appService.sidebarActive;}
    set active(v: boolean) {this.appService.sidebarActive = v;}
    get employee(): Employee {return this.employeeService.authData.value.employee;}

    public ngAfterViewInit(): void {
        document
            .querySelectorAll(".sbn-items a")
            .forEach(element => element.addEventListener("click", () => window.innerWidth < 1000 && (this.appService.sidebarActive = false)));
    }    
}