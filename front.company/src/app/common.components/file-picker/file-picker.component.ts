import { Component, EventEmitter, Output } from "@angular/core";
import { IFile } from "src/app/model/file.interface";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";
import { CfgRepository } from "src/app/services/repositories/cfg.repository";
import { UploadService } from "src/app/services/upload.service";

@Component({
    selector: "file-picker",
    templateUrl: "file-picker.component.html",
    styleUrls: ["file-picker.component.scss"],
})
export class FilePickerComponent {
    @Output() picked: EventEmitter<IFile[]> = new EventEmitter();    
    
    public error: boolean = false;
    public active: boolean = false;
    
    constructor(
        private appService: AppService,
        private uploadService: UploadService,
        private cfgRepository: CfgRepository,
    ) {}       
    
    get lang(): Lang {return this.appService.lang;} 
    get cfg(): any {return this.cfgRepository.data;}

    public async onClick(): Promise<void> {
        try {
            this.error = false;
            const files = await this.uploadService.selectFiles();
            this.picked.emit(files);
        } catch (err) {
            this.error = true;
        }
    }

    public async onDrop(event: DragEvent): Promise<void> {
        event.preventDefault();
        event.stopPropagation();
        this.active = false;        
        const droppedFiles = event.dataTransfer.items ? 
            Array.from(event.dataTransfer.items).filter(item => item.kind === "file").map(item => item.getAsFile()) : 
            Array.from(event.dataTransfer.files);        
        const acceptedFiles = droppedFiles.filter(f => f.size <= this.cfg.maxFileSize);        
        const files: IFile[] = [];

        for (let acceptedFile of acceptedFiles) {
            const name = acceptedFile.name;
            const data = await this.uploadService.readFile(acceptedFile);  
            const contentType = acceptedFile.type  || "text/plain";          
            files.push({name, data, contentType});
        }

        this.picked.emit(files);        
    }

    public onDragOver(event: DragEvent): void {        
        event.preventDefault();
        event.stopPropagation();
        this.active = true;
    }

    public onDragEnter(event: DragEvent): void {
        event.preventDefault();
        event.stopPropagation();
        this.active = true;
    }

    public onDragLeave(event: DragEvent): void {
        event.preventDefault();
        event.stopPropagation();
        this.active = false;
    }    
}