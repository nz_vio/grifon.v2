import { Component, Input } from "@angular/core";
import { ActionReminder } from "src/app/model/orm/action.reminder.model";
import { Lang } from "src/app/model/orm/lang.model";
import { AppService } from "src/app/services/app.service";

@Component({
    selector: "action-reminders",
    templateUrl: "action-reminders.component.html",
    styleUrls: ["action-reminders.component.scss"],
})
export class ActionRemindersComponent {
    @Input() reminders: ActionReminder[];

    constructor(private appService: AppService) {}

    get lang(): Lang {return this.appService.lang;}

    public onCreate(): void {
        this.reminders.push(new ActionReminder().init());
    }

    public onDelete(i: number): void {
        this.reminders.splice(i, 1);
    }
}