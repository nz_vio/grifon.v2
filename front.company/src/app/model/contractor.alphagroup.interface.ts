import { Contractor } from "./orm/contractor.model";

export interface ContractorAlphaGroup {
    letter: string;
    contractors: Contractor[];
}