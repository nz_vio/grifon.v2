import { ChatMessage } from "./orm/chat.message.model";

export class Quote {
    constructor(
        public name: string = "", 
        public message: ChatMessage = null,
    ) {}
}