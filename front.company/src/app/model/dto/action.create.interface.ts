import { IFile } from "../file.interface";
import { ActionFile } from "../orm/action.file.model";
import { ActionReminder } from "../orm/action.reminder.model";
import { ActionTag } from "../orm/action.tag.model";
import { ChatMessage } from "../orm/chat.message.model";
import { Deal } from "../orm/deal.model";
import { EmployeeAction } from "../orm/employee.action.model";
import { Employee } from "../orm/employee.model";

export interface IActionCreate {    
    actionstatus_id: number;
    employee: Employee;
    employeeAction: EmployeeAction;
    message: ChatMessage;
    deal: Deal;    
    comment: string;    
    file?: ActionFile;
    uploads: IFile[];
    reminders: ActionReminder[];
    tags: ActionTag[];
}