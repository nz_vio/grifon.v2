import { ChatMessageChannel, ChatMessageType } from "../orm/chat.message.model";

export interface IChatMessageCreate {
    readonly contractor_id: number;
    readonly channel: ChatMessageChannel;
    readonly type: ChatMessageType;
    readonly text: string;    
}