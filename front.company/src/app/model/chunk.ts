export class Chunk<T> {
    constructor(
        public data: T[] = [],
        public allLength: number = 0,
    ) {}    
}