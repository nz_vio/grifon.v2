export interface IFile {
    name: string;
    description?: string;
    data: string;
    contentType: string;
}