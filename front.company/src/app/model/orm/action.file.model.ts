import { Model } from "./model";

export class ActionFile extends Model {
    public id: number;
    public action_id: number;
    public url: string;
    public name: string;
    public description: string;        
}