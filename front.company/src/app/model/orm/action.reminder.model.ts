import { Model } from "./model";

export class ActionReminder extends Model {    
    id: number;    
    action_id: number;    
    date: string;    
    description: string;
    
    public init(): ActionReminder {
        this.date = this.mysqlDate(new Date());
        this.description = "";
        return this;
    }
}