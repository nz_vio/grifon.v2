import { Model } from "./model";

export class Company extends Model {
    public id: number;
    public name: string;    
    public email: string;
    public n8n_code: string;    
}
