import { Model } from "./model";

export enum EmployeeActionDirection {
    In = "in",
    Out = "out",
}

export class EmployeeAction extends Model {
    public id: number;
    public group_id: number;
    public code: string;
    public direction: EmployeeActionDirection;
    public name: string;
    public response: string;
}