import { Model } from "./model";

export class ContractorGroup extends Model {
    public id: number;    
    public name: string;
}