import { Model } from "./model";

export class ActionStatus extends Model {    
    public id: number;    
    public name: string;
}