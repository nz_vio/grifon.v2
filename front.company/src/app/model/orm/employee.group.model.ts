import { EmployeeAction } from "./employee.action.model";
import { Model } from "./model";

export class EmployeeGroup extends Model {
    public id: number;
    public code: string;
    public name: string;
    // relations
    public actions?: EmployeeAction[];
}