import { Action } from "./action.model";
import { Contractor } from "./contractor.model";
import { Model } from "./model";

export enum ChatMessageChannel {
    Whatsapp = "whatsapp",      
}

export enum ChatMessageType {
    Text = "text",  
    Audio= "audio",
    File = "file",
    Image = "image",
}

export class ChatMessage extends Model {
    public id: number;
    public outer_id: string;
    public contractor_id: number;
    public channel: ChatMessageChannel;
    public type: ChatMessageType;    
    public text?: string;
    public fileurl?: string;    
    public filename?: string;    
    public is_task: boolean;
    public is_answer: boolean;
    public is_read: boolean;
    public created_at: Date;
    // relations
    public contractor?: Contractor;
    public actions?: Action[];
    
    get dateCreatedAt(): string {
        return this.created_at ? `${this.twoDigits(this.created_at.getDate())}.${this.twoDigits(this.created_at.getMonth()+1)}.${this.created_at.getFullYear()}` : "";
    }

    get datetimeCreatedAt(): string {
        if (this.created_at) {
            const date = new Date();
            return date.getDate() === this.created_at.getDate() && date.getMonth() === this.created_at.getMonth() && date.getFullYear() === this.created_at.getFullYear() ?
                `${this.twoDigits(this.created_at.getHours())}:${this.twoDigits(this.created_at.getMinutes())}` :
                `${this.twoDigits(this.created_at.getDate())}.${this.twoDigits(this.created_at.getMonth()+1)}.${this.created_at.getFullYear().toString().substring(2)} ${this.twoDigits(this.created_at.getHours())}:${this.twoDigits(this.created_at.getMinutes())}`;
        }
        
        return "";
    }

    get textStripped(): string {
        return String(this.text).replace(/(<([^>]+)>)/gi, "");
    }    
    
    public build (o: Object): ChatMessage {
        for (let field in o) {
            if (field === "created_at" || field === "audio_expired_at") {
                this[field] = o[field] ? new Date (o[field]) : null;
            } else if (field === "contractor") {
                this[field] = o[field] ? new Contractor().build(o[field]) : null;
            } else if (field === "actions") {
                this[field] = o[field].map(a => new Action().build(a));
            } else {
                this[field] = o[field];
            }            
        }
        
        return this;
    }
}