import { Action } from "./action.model";
import { Contractor } from "./contractor.model";
import { DealDate } from "./deal.date.model";
import { Model } from "./model";

export class Deal extends Model {
    public id: number;    
    public company_id: number;    
    public contact_contractor_id: number;   
    public client_contractor_id: number;    
    public partner_contractor_id: number;
    public name: string;    
    public description: string;   
    public closed: boolean;   
    public created_at: Date;
    // relations
    public contact?: Contractor;
    public client?: Contractor;
    public partner?: Contractor;
    public dates?: DealDate[];
    public actions?: Action[];

    get formattedCreatedAt(): string {return this.created_at ? `${this.twoDigits(this.created_at.getDate())}.${this.twoDigits(this.created_at.getMonth()+1)}.${this.created_at.getFullYear()} ${this.twoDigits(this.created_at.getHours())}:${this.twoDigits(this.created_at.getMinutes())}` : "";}
    get formattedCreatedAtShort(): string {return this.created_at ? `${this.twoDigits(this.created_at.getDate())}.${this.twoDigits(this.created_at.getMonth()+1)}` : "";}
    get descriptionShort(): string {return this.description ? String(this.description).substring(0, 128) + (this.description?.length > 128 ? "..." : "") : "";}

    public build (o: Object): Deal {
        for (let field in o) {
            if (field === "created_at") {
                this[field] = o[field] ? new Date (o[field]) : null;
            } else if (field === "contact" || field === "client" || field === "partner") {
                this[field] = o[field] ? new Contractor().build(o[field]) : null;
            } else if (field === "dates") {
                this[field] = o[field].map(d => new DealDate().build(d));
            } else if (field === "actions") {
                this[field] = o[field].map(a => new Action().build(a));
            } else {
                this[field] = o[field];
            }            
        }
        
        return this;
    }
    
    public init(company_id: number, contact?: Contractor): Deal {
        this.company_id = company_id;        
        this.name = "";
        this.description = "";        
        this.closed = false;
        this.dates = [];        
        contact && (this.contact = contact);        
        return this;
    }
}