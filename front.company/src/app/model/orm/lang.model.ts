import { Model } from "./model";

export class Lang extends Model {
    public name: string;
    public title: string;
    public dir: string;
    public phrases: Object;
}