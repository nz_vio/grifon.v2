import { Company } from "./company.model";
import { EmployeeGroup } from "./employee.group.model";
import { Model } from "./model";

export class Employee extends Model {
    public id: number;    
    public company_id: number;
    public group_id: number;    
    public email: string;
    public password: string;
    public name: string;    
    public img: string;
    public active: boolean;    
    // relations
    public company?: Company;
    public group?: EmployeeGroup;    

    get firstLetter(): string {return this.name?.length ? this.name.charAt(0) : "";}
}