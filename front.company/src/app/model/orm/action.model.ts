import { ActionFile } from "./action.file.model";
import { ActionReminder } from "./action.reminder.model";
import { ActionStatus } from "./action.status.model";
import { ActionTag } from "./action.tag.model";
import { ChatMessage } from "./chat.message.model";
import { Company } from "./company.model";
import { Deal } from "./deal.model";
import { EmployeeAction } from "./employee.action.model";
import { Employee } from "./employee.model";
import { Model } from "./model";

export class Action extends Model {
    public id: number;    
    public company_id: number;    
    public employee_id: number;    
    public employeeaction_id: number;   
    public chatmessage_id: number;    
    public deal_id: number;      
    public actionstatus_id: number; 
    public comment: string;    
    public created_at: Date;
    // relations    
    public files?: ActionFile[];
    public reminders?: ActionReminder[];
    public company?: Company;
    public employee?: Employee;
    public employeeAction?: EmployeeAction;
    public message?: ChatMessage;
    public deal?: Deal;
    public status?: ActionStatus;
    public tags?: ActionTag[];

    get formattedCreatedAt(): string {return this.created_at ? `${this.twoDigits(this.created_at.getDate())}.${this.twoDigits(this.created_at.getMonth()+1)}.${this.created_at.getFullYear()} ${this.twoDigits(this.created_at.getHours())}:${this.twoDigits(this.created_at.getMinutes())}` : "";}

    public build (o: Object): Action {
        for (let field in o) {
            if (field === "created_at") {
                this[field] = o[field] ? new Date(o[field]) : null;
            } else if (field === "message") {
                this[field] = o[field] ? new ChatMessage().build(o[field]) : null;
            } else {
                this[field] = o[field];
            }            
        }
        
        return this;
    }
}