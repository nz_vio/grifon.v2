import { Model } from "./model";

export class ActionTag extends Model {
    public id: number;    
    public name: string;    
}