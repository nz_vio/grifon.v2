import { ChatMessage } from "./chat.message.model";
import { Company } from "./company.model";
import { ContractorGroup } from "./contractor.group.model";
import { Model } from "./model";

export class Contractor extends Model {
    public id: number;
    public company_id: number;    
    public name: string;
    public phone: string;
    public img: string;
    public chat_active: boolean;
    public chat_at: Date;    
    public banned: boolean;    
    public created_at: Date;
    // relations
    public company?: Company;
    public groups?: ContractorGroup[];
    public chat_messages?: ChatMessage[];
    // helpers
    public _hasNewMessage?: boolean;    

    get firstLetter(): string {return this.name?.length ? this.name.charAt(0) : "";}    
    get groupNames(): string {return this.groups.map(g => g.name).join(", ");}
    get formattedCreatedAt(): string {return this.created_at ? `${this.twoDigits(this.created_at.getDate())}.${this.twoDigits(this.created_at.getMonth()+1)}.${this.created_at.getFullYear()} ${this.twoDigits(this.created_at.getHours())}:${this.twoDigits(this.created_at.getMinutes())}` : "";}
    get imgOut(): string {
        return this.img ? 
            (this.img.includes("base64") ? `url(${this.img})` : `url(/img/contractors/${this.img})`) :
            "none";
    }    

    public build (o: Object): Contractor {
        for (let field in o) {
            if (field === "chat_at" || field === "created_at") {
                this[field] = new Date (o[field]);
            } else if (field === "chat_messages") {
                this[field] = o[field].map(m => new ChatMessage().build(m));
            } else {
                this[field] = o[field];
            }            
        }
        
        return this;
    }

    public init(company_id: number): Contractor {
        this.company_id = company_id;        
        this.name = "";
        this.phone = "";        
        this.groups = [];
        return this;
    }
}