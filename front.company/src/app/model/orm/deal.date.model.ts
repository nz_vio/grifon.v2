import { Model } from "./model";

export class DealDate extends Model {    
    public id: number;    
    public deal_id: number;    
    public date: string;    
    public description: string;  
    
    public init(): DealDate {
        this.date = this.mysqlDate(new Date());
        this.description = "";
        return this;
    }
}