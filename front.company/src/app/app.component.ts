import { AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Settings } from './model/orm/settings.type';
import { AppService } from './services/app.service';
import { CfgRepository } from './services/repositories/cfg.repository';
import { LangRepository } from './services/repositories/lang.repository';
import { SettingRepository } from './services/repositories/setting.repository';
import { SoundService } from './services/sound.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit, AfterViewInit {	
	@ViewChild("win", {static: false}) winRef: ElementRef; 

	private dataReady: boolean = false;
	
	constructor(
		private appService: AppService,
		private cfgRepository: CfgRepository,
		private langRepository: LangRepository,
		private settingRepository: SettingRepository,
		private router: Router,		
		private soundService: SoundService, // to unlock sounds
	) {}

	get settings(): Settings {return this.appService.settings;}
	get ready(): boolean {return this.dataReady && this.settings["active"] === "1";}	
	get authenticated(): boolean {return this.appService.url[1] !== "auth";}

	public async ngOnInit(): Promise<void> {
		await this.initLangs();
		await this.initCfg(); // cfg before data from back-end
		await this.initSettings();		
		this.dataReady = true;
		this.removePreloader();
	}

	public async ngAfterViewInit(): Promise<void> {	
		this.appService.sidebarActive = window.innerWidth > 1000;
		this.appService.win = await this.initWin();
		this.router.events
			.pipe(filter(event => event instanceof NavigationEnd))
			.subscribe(event => this.appService.win.scrollTop ? setTimeout(() => {this.appService.win.scrollTo(0, 0);}, 1) : null);
	}

	private initWin(): Promise<HTMLElement> {
		return new Promise((resolve, reject) => {
			const getWin = () => this.winRef?.nativeElement ? resolve(this.winRef.nativeElement) : setTimeout(() => getWin(), 50);
			getWin();
		});
	}

	private async initCfg(): Promise<void> {
		try {
			await this.cfgRepository.loadAll();			
		} catch (err) {
			this.appService.showError(err);
		}
	}

	private async initLangs(): Promise<void> {
		try {
			const ll = await this.langRepository.loadAll();			
			this.appService.lang = ll[0]; // now using single lang			
		} catch (err) {
			this.appService.showError(err);			
		}		
	}

	private async initSettings(): Promise<void> {
		try {
			this.appService.settings = await this.settingRepository.loadAll();			
		} catch (err) {
			this.appService.showError(err);	
		}
	}	

	private removePreloader(): void {
		document.getElementById("preloader").remove();
	}

	@HostListener("window:resize")
	private onResize(): void {
		this.appService.sidebarActive = window.innerWidth >= 1000;
	}
}
